@echo on
@title "CMMI BUILD"
e:
cd "D:\personal project\GIT_Repo\maprojava\login"
call mvn install -Dmaven.test.skip=true -fn -o -am -offline -Dmaven.clean.failOnError=false
pause
cd "D:\personal project\TomcatCMMI\webapps\"
del GAPANALYSIS-2.0.war
rmdir /S/Q GAPANALYSIS-2.0
copy "D:\personal project\GIT_Repo\maprojava\login\target\GAPANALYSIS-2.0.war" "D:\personal project\TomcatCMMI\webapps\"

cd D:\personal project\TomcatCMMI\bin\
catalina jpda run
pause
exit
