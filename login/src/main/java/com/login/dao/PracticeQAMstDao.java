package com.login.dao;
import com.login.dao.impl.IPracticeQAMstDao;
import com.login.dao.impl.IQuestionMstDao;
import com.login.dao.impl.IWorkProductMstDao;
import com.login.domain.PracticeQAMst;
import com.login.domain.QuestionMst;
import com.login.domain.WorkProductMst;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.login.dao.basics.CustomDaoImpl;

@Repository("practiceQAMstDao")
public class PracticeQAMstDao extends CustomDaoImpl<Long,PracticeQAMst> implements IPracticeQAMstDao{
	
	@Autowired
	EntityManagerFactory entityManagerFactory;

	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager em) {
		this.entityManager = em;
		super.setEntityManager(entityManager);
	}

	@PostConstruct
	public void init() {
		super.setEntityManagerFactory(entityManagerFactory);
		super.setEntityManager(entityManager);
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(
			EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	private static final Logger logger = Logger.getLogger(PracticeQAMstDao.class);


	
	}
