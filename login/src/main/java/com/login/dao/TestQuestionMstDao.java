package com.login.dao;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.login.dao.basics.CustomDaoImpl;
import com.login.dao.impl.IPracticeMstDao;
import com.login.dao.impl.ITestMstDao;
import com.login.dao.impl.ITestQuestionMstDao;
import com.login.domain.PracticeMst;
import com.login.domain.TestMst;
import com.login.domain.TestQuestionMst;


@Repository("testQuestionMstDao")
public class TestQuestionMstDao extends CustomDaoImpl<Long,TestQuestionMst> implements ITestQuestionMstDao{
	
	@Autowired
	EntityManagerFactory entityManagerFactory;

	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager em) {
		this.entityManager = em;
		super.setEntityManager(entityManager);
	}

	@PostConstruct
	public void init() {
		super.setEntityManagerFactory(entityManagerFactory);
		super.setEntityManager(entityManager);
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(
			EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	private static final Logger logger = Logger.getLogger(TestQuestionMstDao.class);


	
	}
