package com.login.dao.impl;


import com.login.domain.RelationMappingCapabilityPracticeArea;

import com.login.dao.basics.CustomDao;




public interface IRelationMappingCapabilityPracticeAreaDao extends CustomDao<RelationMappingCapabilityPracticeArea, Long>{


}
