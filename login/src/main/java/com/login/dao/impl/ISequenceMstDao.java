package com.login.dao.impl;

import org.springframework.data.jpa.repository.JpaRepository;

import com.login.dao.basics.CustomDao;
import com.login.domain.SequenceMst;




public interface ISequenceMstDao extends CustomDao<SequenceMst, Long>{


}
