package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "WorkProductMst.getAll", query = "select h FROM WorkProductMst h WHERE h.tenantId=:tenantId and h.cmmiVersion=:cmmiVersion and h.isActive=1 order by h.id"),
	@NamedQuery(name = "WorkProductMst.getByTenantAndProcess", query = "select h FROM WorkProductMst h where h.isActive=1  and h.cmmiVersion=:cmmiVersion and h.tenantId=:tenantId and h.process=:process order by h.id"),
	@NamedQuery(name = "WorkProductMst.getByTenantAndProject", query = "select h FROM WorkProductMst h where h.isActive=1 and h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.project=:project order by h.id"),
	@NamedQuery(name = "WorkProductMst.getUnique", query = "select h FROM WorkProductMst h WHERE h.organizationUnit=:organizationUnit and h.businessUnit=:businessUnit  and h.cmmiVersion=:cmmiVersion and h.project=:project and  h.tenantId=:tenantId and h.workProductId=:workProductId and h.isActive=1 order by h.id"),	
})

@Table(indexes = { @Index(name = "IDX_UserMst", columnList = "tenantId,workProductId,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","workProductId"}))
public class WorkProductMst extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String workProductId="";
	
@Column(columnDefinition = "nvarchar(2000)", nullable = false)
private String workProductName="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String questionId="";

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String cmmiVersion="";

public String getWorkProductName() {
	if (null != workProductName) {
	} else {
		workProductName = initvar(workProductName);
	}
	return workProductName;
}

public void setWorkProductName(String workProductName) {
	this.workProductName = workProductName;
}

public String getQuestionId() {
	if (null != questionId) {
	} else {
		questionId = initvar(questionId);
	}
	return questionId;
}

public void setQuestionId(String questionId) {
	this.questionId = questionId;
}

public String getWorkProductId() {
	if (null != workProductId) {
	} else {
		workProductId = initvar(workProductId);
	}
	return workProductId;
}

public void setWorkProductId(String workProductId) {
	this.workProductId = workProductId;
}

public String getCmmiVersion() {
	if (null == cmmiVersion) {
		return initvar(cmmiVersion);
	} else {
		return cmmiVersion;
	}
}

public void setCmmiVersion(String cmmiVersion) {
	this.cmmiVersion = cmmiVersion;
}





}
