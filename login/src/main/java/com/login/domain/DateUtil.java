/**
 * 
 */
package com.login.domain;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;



public class DateUtil {

	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String DATE_FORMAT_YYYYMMDD = "yyyy-MM-dd";
	public static final String DATE_FORMAT_YYYYMMDD_HHMMSS = "yyyy-MM-dd HH:mm:ss";

	public static Date addMonth(Date currDate, Integer noOfMonths) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(currDate.getTime());
		cal.add(Calendar.MONTH, noOfMonths);
		return cal.getTime();
	}

	/**
	 * Returns the day of the specified date.
	 * 
	 * @param date
	 *            Input date object
	 * @return Day of the month. Values range from 1 - 31.
	 */
	public static Integer getDayOfDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(date.getTime());
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	public static Date getDateFromStringAndFormat(String strDate, String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		try {
			if ((null != strDate) && !strDate.isEmpty()) {
				Date formattedDate = dateFormat.parse(strDate);
				return formattedDate;
			}
		} catch (Exception e) {
		}
		return null;
	}

	public static Date addDaysToDate(Date date, int daysToAdd) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(date.getTime());
		cal.add(Calendar.DAY_OF_MONTH, daysToAdd);
		return cal.getTime();
	}
	
	
	/**
	 * Returns month end date for specified date given
	 * @param date
	 * @return monthenddate
	 * @throws ParseException 
	 */
	public static Date GetLastDayOfMonth(Date date){
		Calendar calendar = Calendar.getInstance();  
        calendar.setTime(date);  
        calendar.add(Calendar.MONTH, 1);  
        calendar.set(Calendar.DAY_OF_MONTH, 1);  
        calendar.add(Calendar.DATE, -1);  
        Date lastDayOfMonth = calendar.getTime();  
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
			lastDayOfMonth= sdf.parse(sdf.format(lastDayOfMonth));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lastDayOfMonth;
	}
	
	public static Date getFirstDateOfMonth(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

	public static Date addHoursToDate(Date date, int hours) {
		Calendar timeCal = Calendar.getInstance();
		timeCal.setTime(date);
		timeCal.add(Calendar.HOUR, hours);
		return timeCal.getTime();
	}

	public static int getDiffDays(Date startDate, Date endDate) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(startDate);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(endDate);
		long daysBetween = 0;
		while (cal1.before(cal2)) {
			cal1.add(Calendar.DAY_OF_MONTH, 1);
			daysBetween++;
		}
		return (int) daysBetween;
	}


	public static int diffInMonths(final Date fromDate, final Date toDate) {
		//
		Calendar startCalendar = new GregorianCalendar();
		startCalendar.setTime(fromDate);
		Calendar endCalendar = new GregorianCalendar();
		endCalendar.setTime(toDate);

		int diffYear = endCalendar.get(Calendar.YEAR)
				- startCalendar.get(Calendar.YEAR);
		int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH)
				- startCalendar.get(Calendar.MONTH);
		return diffMonth;
	}


	public static boolean isBetweenTwoDates(Date dateToBeCompared,
			Date firstDate, Date secondDate) {
		if (((DateUtil.compareDates(firstDate, dateToBeCompared) < 0) && (DateUtil
				.compareDates(secondDate, dateToBeCompared) < 0))
				|| ((DateUtil.compareDates(firstDate, dateToBeCompared) > 0) && (DateUtil
						.compareDates(secondDate, dateToBeCompared) > 0))) {
			return false;
		} else {
			return true;
		}
	}

	public static int compareDates(Date firstDate, Date secondDate) {
		Calendar firstCal = Calendar.getInstance();
		firstCal.setTime(firstDate);
		firstCal.set(Calendar.HOUR, 0);
		firstCal.set(Calendar.HOUR_OF_DAY, 0);
		firstCal.set(Calendar.MINUTE, 0);
		firstCal.set(Calendar.SECOND, 0);
		firstCal.set(Calendar.MILLISECOND, 0);

		Calendar secondCal = Calendar.getInstance();
		secondCal.setTime(secondDate);
		secondCal.set(Calendar.HOUR, 0);
		secondCal.set(Calendar.HOUR_OF_DAY, 0);
		secondCal.set(Calendar.MINUTE, 0);
		secondCal.set(Calendar.SECOND, 0);
		secondCal.set(Calendar.MILLISECOND, 0);

		if (firstCal.getTime().compareTo(secondCal.getTime()) < 0) {
			return -1;
		} else if (firstCal.getTime().compareTo(secondCal.getTime()) > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	public static Date getDateFromString(String strDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		try {
			if ((null != strDate) && !strDate.isEmpty()) {
				Date formattedDate = dateFormat.parse(strDate);
				return formattedDate;
			}
		} catch (Exception e) {
		}
		return null;
	}

	public static String getStringFromDate(String dateFormatStr, Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatStr);
		try {
			if (null != date) {
				String strDate = dateFormat.format(date);
				return strDate;
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}

	public static String getStringFromDate(Date dtDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		try {
			if (null != dtDate) {
				String strDate = dateFormat.format(dtDate);
				return strDate;
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}

	public static long getCurrentTime() {
		java.util.Date date = new java.util.Date();
		return date.getTime();
	}

	public static Timestamp getCurrentTimestamp() {
		java.util.Date date = new java.util.Date();
		return new Timestamp(date.getTime());
	}

	/**
	 * Move the input date forward and returns the next occurrence of the day.
	 * Returns the same date if the day of the specified date and the input day
	 * are same. e.g.
	 * 
	 * <p>
	 * Input Date = 05-June-2015, Day = 10, Output = 10-June-2015
	 * </p>
	 * <p>
	 * Input Date = 05-June-2015, Day = 02, Output = 02-July-2015
	 * </p>
	 * <p>
	 * Input Date = 05-June-2015, Day = 05, Output = 05-June-2015
	 * </p>
	 * 
	 * @param date
	 *            Date
	 * @param day
	 *            Day of the month
	 * @return Date
	 */
	public static Date moveToDay(Date date, int day) {

		Calendar calender = Calendar.getInstance();
		calender.setTimeInMillis(date.getTime());

		while (calender.get(Calendar.DAY_OF_MONTH) != day) {
			calender.add(Calendar.DAY_OF_MONTH, 1);
		}

		return calender.getTime();
	}

	public static String convertDateFormatToCustomDDMMYYYY(String dateFormat,
			String str) {

		String toDate = null;
		// DateFormat srcDf = new SimpleDateFormat("MM/dd/yyyy");

		DateFormat destDf = new SimpleDateFormat(dateFormat);
		DateFormat srcDf = new SimpleDateFormat(DATE_FORMAT_YYYYMMDD);
		try {
			/*
			 * toDate = (String) request .getParameter("effectiveToDate");
			 */

			// parse the date string into Date object
			if (str != null && !str.isEmpty()) {
				Date date = srcDf.parse(str);

				// format the date into another format
				toDate = destDf.format(date);
			}

			// System.out.println("Converted date is : " + fromDate);
			/*
			 * Date dateEffToDate = destDf.parse(toDate);
			 * bDPItemRateMst.setEffectiveFromDate(dateEffToDate);
			 */

		} catch (Exception e) {
			return null;

		}
		return toDate;
	}

	public static String convertDateFormatToCustomDDMMYYYYHHMMSS(
			String dateFormat, String str) {

		String toDate = null;
		// DateFormat srcDf = new SimpleDateFormat("MM/dd/yyyy");

		DateFormat destDf = new SimpleDateFormat(dateFormat);
		DateFormat srcDf = new SimpleDateFormat(DATE_FORMAT_YYYYMMDD_HHMMSS);
		try {

			// parse the date string into Date object
			if (str != null && !str.isEmpty()) {
				Date date = srcDf.parse(str);

				// format the date into another format
				toDate = destDf.format(date);
			}

		} catch (Exception e) {
			return null;

		}
		return toDate;
	}

	public static String getStringDate(Date date) {
		SimpleDateFormat sdfr = new SimpleDateFormat(DATE_FORMAT_YYYYMMDD);

		String dateStr = null;

		try {
			if (date != null)
				dateStr = sdfr.format(date);

		} catch (Exception ex) {
			return null;
		}
		return dateStr;
	}

	public static String getStringDateYYYYMMDDHHMMSS(Date date) {
		SimpleDateFormat sdfr = new SimpleDateFormat(
				DATE_FORMAT_YYYYMMDD_HHMMSS);

		String dateStr = null;

		try {
			if (date != null)
				dateStr = sdfr.format(date);

		} catch (Exception ex) {
			return null;
		}
		return dateStr;
	}

	/**
	 * Returns maximum date i.e. 31.12.2099.
	 * 
	 * @param setDateTime
	 * @return
	 */
	public static Date getDefaultMaxDate(boolean setDateTime) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2099);
		calendar.set(Calendar.MONTH, 12);
		calendar.set(Calendar.DAY_OF_MONTH, 31);

		if (setDateTime) {
			calendar.set(Calendar.HOUR, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.AM_PM, Calendar.PM);
		}

		return calendar.getTime();
	}

}
