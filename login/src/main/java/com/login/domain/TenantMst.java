package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "tenantMst.getAll", query = "select h FROM TenantMst h where h.isActive=1 order by h.id"),
	@NamedQuery(name = "tenantMst.getUnique", query = "select h FROM TenantMst h where h.isActive=1 and h.tenantId=:tenantId and h.organizationCode=:organizationCode order by h.id"),
	@NamedQuery(name = "tenantMst.getTenant", query = "select h FROM TenantMst h where h.isActive=1 and h.tenantId=:tenantId order by h.id"),
		 	
})

@Table(indexes = { @Index(name = "IDX_TenantMst", columnList = "tenantId,organizationCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "tenantId","organizationCode"}))
public class TenantMst  extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
public String organizationCode="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
public String logoPath="";


@Column(nullable = true)
public Integer practiceLevel;

@Column(columnDefinition = "nvarchar(100)", nullable = true)
public String organizationName="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
public String businessModel="";




@Column(columnDefinition = "nvarchar(10)" , nullable = true)
public String licenseExpiryDate="01-01-1900";




public String action="";


@Column(columnDefinition = "nvarchar(200)", nullable = true)
public String address="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
public String telephone1="";



public String getOrganizationCode() {
	if (null != organizationCode) {
	} else {
		organizationCode = initvar(organizationCode);
	}
	return organizationCode;
}

public void setOrganizationCode(String organizationCode) {
	this.organizationCode = organizationCode;
}

public String getOrganizationName() {
	if (null != organizationName) {
	} else {
		organizationName = " ";
	}
	return organizationName;
}

public void setOrganizationName(String organizationName) {
	this.organizationName = organizationName;
}

public String getAddress() {
	if (null != address) {
	} else {
		address = " ";
	}
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getTelephone1() {
	if (null != telephone1) {
	} else {
		telephone1 = "0";
	}
	return telephone1;
}

public void setTelephone1(String telephone1) {
	this.telephone1 = telephone1;
}

public String getAction() {
	if (null != action) {
	} else {
		action = initvar(action);
	}
	return action;
}

public void setAction(String action) {
	this.action = action;
}

public String getBusinessModel() {
	if (null != businessModel) {
	} else {
		businessModel = initvar(businessModel);
	}
	return businessModel;
}

public void setBusinessModel(String businessModel) {
	this.businessModel = businessModel;
}



public Integer getPracticeLevel() {
	if (null != practiceLevel) {
	} else {
		practiceLevel = 1;
	}
	return practiceLevel;
}

public void setPracticeLevel(Integer practiceLevel) {
	this.practiceLevel = practiceLevel;
}

public String getLicenseExpiryDate() {
		return licenseExpiryDate;
}

public void setLicenseExpiryDate(String licenseExpiryDate) {
	this.licenseExpiryDate = licenseExpiryDate;
}

public String getLogoPath() {
	return logoPath;
}

public void setLogoPath(String logoPath) {
	this.logoPath = logoPath;
}




}
