package com.login.domain;

import org.apache.struts2.ServletActionContext;

public class ApplicationConstants {
public static final String TEMPLATE_PATH = ServletActionContext.getServletContext().getRealPath("//templates");
public static final String TEMPLATE_GapSummaryReport = TEMPLATE_PATH + "//GapSummaryReport.xlsx";
public static final String Application_Path = "http://localhost:8588";
public static final String AUTH_PENDING = "Pending";
public static final String RESPONSE_SUCCESS = "SUCCESS~";
public static final String AUTH_REJECTED = "Rejected";
public static final String AUTH_AUTHORIZED = "Authorized";
public static final String AUTH_CLOSED = "Closed";
public static final String AUTH_DRAFT = "Draft";
public static final String AUTH_PREV_PENDING = "Previous Pending";
public static final String AUTH_NA = "N.A.";
public static final String AUTH_IN_PROGRESS = "Authorization In Progress";
public static final String DEFAULT_TENANT = "0";
public static final String DEFAULT_process = "null";
public static final String SEQ_TENANTIDSEQ="TENANTIDSEQ";
public static final String SEQ_CATEGORYSEQ="CATEGORYSEQ";
public static final String SEQ_CAPABILITYSEQ="CAPABILITYSEQ";
public static final String SEQ_QUESTIONMST="QUESTIONMSTSEQ";
public static final String SEQ_PRACTICEAREASEQ="PRACTICEAREASEQ";
public static final String SEQ_WORKPRODUCTSEQ="WORKPRODUCTSEQ";
public static final String SEQ_ORGCODESEQ="ORGCODESEQ";
public static final String LOOKUP_PRACTICEAREALOOKUP="PRACTICEAREALOOKUP";
public static final String LOOKUP_COMPLIANCELOOKUP="COMPLIANCE";
public static final String LOOKUP_COMPLIANCEREPORTLOOKUP="COMPLIANCEREPORT";
public static final String LOOKUP_COVERAGE="COVERAGE";
public static final String LOOKUP_PRACTICELOOKUP="PRACTICELOOKUP";
public static final String LOOKUP_PRACTICELEVEL="PRACTICELEVEL";
public static final String LOOKUP_USERTYPELOOKUP="USERTYPELOOKUP";
public static final String LOOKUP_PROCESSAREALOOKUP="PROCESSAREALOOKUP";
public static final String LOOKUP_BUSINESSMODEL="BUSINESSMODEL";
public static final String LOOKUP_CMMIVERSION="CMMIVERSION";
public static final String FAILED="FAILED";
public static final String SUCCESS="SUCCESS";
public static final String UPLOAD_PATH="c:\\archive\\";

public static final String SESSION_PARAM_ACCESSOBJECT = "ACCESS_OBJECT";
public static final String DEFAULT_PASSWORD="abcd1234";
public static final String REPORT_SHEETGASUMMARY="GA Summary DEV & SVC";
public static final String REPORT_SHEETGASHEET="GA Sheet";
public static final String REPORT_PIIDSHEET="OT";
public static final String REPORT_SHEETCOMPLIANCESHEET="Compliance %";
}
