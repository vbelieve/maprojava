package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "capabilityMst.getAll", query = "select h FROM CapabilityMst h where h.isActive=1 and h.tenantId=:tenantId and h.cmmiVersion=:cmmiVersion order by h.id"),
	@NamedQuery(name = "capabilityMst.getBase", query = "select h FROM CapabilityMst h where h.isActive=1 and h.tenantId=:tenantId and h.cmmiVersion=:cmmiVersion order by h.id"),
	@NamedQuery(name = "capabilityMst.getByTenantAndProject", query = "select h FROM CapabilityMst h where h.isActive=1 and h.cmmiVersion=:cmmiVersion and h.project=:project order by h.id"),
	@NamedQuery(name = "capabilityMst.getByName", query = "select h FROM CapabilityMst h WHERE h.tenantId=:tenantId and h.cmmiVersion=:cmmiVersion and h.capabilityName=:capabilityName and h.isActive=1 order by h.id"),
	//@NamedQuery(name = "capabilityMst.getUnique", query = "select h FROM CapabilityMst h WHERE h.organizationUnit=:organizationUnit and h.businessUnit=:businessUnit and h.project=:project and h.tenantId=:tenantId and h.capabilityCode=:capabilityCode and h.isActive=1 order by h.id"),
	@NamedQuery(name = "capabilityMst.getUnique", query = "select h FROM CapabilityMst h WHERE h.tenantId=:tenantId"
			+ " and h.capabilityCode=:capabilityCode and h.cmmiVersion=:cmmiVersion and h.isActive=1 order by h.id"),
	 	})

@Table(indexes = { @Index(name = "IDX_CapabilityMst", columnList = "tenantId,capabilityCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","capabilityCode"}))
public class CapabilityMst extends Base{

@GeneratedValue(strategy = GenerationType.AUTO)
private String capabilityCode;
private String capabilityName;

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String cmmiVersion="";


public String getCapabilityName() {
	return capabilityName;
}
public void setCapabilityName(String capabilityName) {
	this.capabilityName = capabilityName;
}

public String getCapabilityCode() {
	if (null != capabilityCode) {
	} else {
		capabilityCode = initvar(capabilityCode);
	}
	return capabilityCode;
}
public void setCapabilityCode(String capabilityCode) {
	this.capabilityCode = capabilityCode;
}
public String getCmmiVersion() {
	if (null == cmmiVersion) {
		return initvar(cmmiVersion);
	} else {
		return cmmiVersion;
	}
}
public void setCmmiVersion(String cmmiVersion) {
	this.cmmiVersion = cmmiVersion;
}



}
