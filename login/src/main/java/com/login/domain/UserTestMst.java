package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "userTestMst.getAll", query = "select h FROM UserTestMst h where h.tenantId=:tenantId and h.authStatus=:authStatus order by h.id"),
	@NamedQuery(name = "userTestMst.getUnique", query = "select h FROM UserTestMst h WHERE h.tenantId=:tenantId and h.testName=:testName and h.questionId=:questionId  order by h.id"),

//	@NamedQuery(name = "answerMst.getAllActiveProjects", query = "select h FROM ProjectMst h where h.isActive=1 order by h.id"),	
//	@NamedQuery(name = "projectMst.getByTenantAndProcess", query = "select h FROM ProjectMst h where h.isActive=1 and h.tenantId=:tenantId and h.process=:process order by h.id"),
//	@NamedQuery(name = "projectMst.getByName", query = "select h FROM ProjectMst h WHERE h.tenantId=:tenantId and h.projectName=:projectName and h.isActive=1 order by h.id"),
//	@NamedQuery(name = "projectMst.getProjectByProjectCode", query = "select h FROM ProjectMst h WHERE h.projectCode=:projectCode and h.isActive=1 order by h.id"),
//	@NamedQuery(name = "projectMst.getUnique", query = "select h FROM ProjectMst h WHERE h.tenantId=:tenantId and h.businessUnitCode=:businessUnitCode and h.projectCode=:projectCode and h.isActive=1 order by h.id"),
	
	 	})
//@Table(indexes = { @Index(name = "IDX_ProjectMst", columnList = "tenantId,projectCode,isActive") },
//		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","projectCode","businessUnitCode"}))
public class UserTestMst extends Base{
	
@Column(nullable = false)
private Long questionId=0L;

@Column(nullable = false)
private Long answereid=0L;

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String testName;

@Column(nullable = false)
private Integer score=0;

public Long getQuestionId() {
	if (null == questionId) {
		return initvar(questionId);
	} else {
		return questionId;
	}
}

public void setQuestionId(Long questionId) {
	this.questionId = questionId;
}

public Long getAnswereid() {
	if (null == answereid) {
		return initvar(answereid);
	} else {
		return answereid;
	}
}

public void setAnswereid(Long answereid) {
	this.answereid = answereid;
}

public String getTestName() {
	if (null == testName) {
		return initvar(testName);
	} else {
		return testName;
	}
}

public void setTestName(String testName) {
	this.testName = testName;
}

public Integer getScore() {
	if (null == score) {
		return initvar(score);
	} else {
		return score;
	}
}

public void setScore(Integer score) {
	this.score = score;
}






}
