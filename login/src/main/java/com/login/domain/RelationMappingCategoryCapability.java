package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "relationMappingCategoryCapability.getAll", query = "select h FROM RelationMappingCategoryCapability h WHERE h.tenantId=:tenantId and h.cmmiVersion=:cmmiVersion and h.isActive=1 order by h.id"),
	@NamedQuery(name = "relationMappingCategoryCapability.getByTenantAndProcess", query = "select h FROM RelationMappingCategoryCapability h where h.isActive=1  and h.cmmiVersion=:cmmiVersion and h.tenantId=:tenantId and h.process=:process order by h.id"),
	@NamedQuery(name = "relationMappingCategoryCapability.getByTenantAndProject", query = "select h FROM RelationMappingCategoryCapability h where h.isActive=1  and h.cmmiVersion=:cmmiVersion and h.project=:project order by h.id"),
	@NamedQuery(name = "relationMappingCategoryCapability.getCategory", query = "select h FROM RelationMappingCategoryCapability h WHERE  h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.categoryCode=:categoryCode and h.isActive=1 order by h.id"),
	@NamedQuery(name = "relationMappingCategoryCapability.getCapability", query = "select h FROM RelationMappingCategoryCapability h WHERE h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.capabilityCode=:capabilityCode and h.isActive=1 order by h.id"),
	@NamedQuery(name = "relationMappingCategoryCapability.getUnique", query = "select h FROM RelationMappingCategoryCapability h WHERE  h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.categoryCode=:categoryCode and h.capabilityCode=:capabilityCode and h.isActive=1 order by h.id"),	
})

@Table(indexes = { @Index(name = "IDX_RelationMappingCategoryCapability", columnList = "tenantId,categoryCode,capabilityCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","categoryCode","capabilityCode"}))
public class RelationMappingCategoryCapability extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String categoryCode="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String capabilityCode="";

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String cmmiVersion="";


public String getCategoryCode() {
	if (null != categoryCode) {
	} else {
		categoryCode = initvar(categoryCode);
	}
	return categoryCode;
}

public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
}

public String getCapabilityCode() {
	if (null != capabilityCode) {
	} else {
		capabilityCode = initvar(capabilityCode);
	}
	return capabilityCode;
}

public void setCapabilityCode(String capabilityCode) {
	this.capabilityCode = capabilityCode;
}

public String getCmmiVersion() {
	if (null == cmmiVersion) {
		return initvar(cmmiVersion);
	} else {
		return cmmiVersion;
	}
}

public void setCmmiVersion(String cmmiVersion) {
	this.cmmiVersion = cmmiVersion;
}



}
