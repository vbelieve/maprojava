package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "practiceQAMst.getAll", query = "select h FROM PracticeQAMst h WHERE h.tenantId=:tenantId and h.isActive=1 order by h.id"),
	@NamedQuery(name = "practiceQAMst.getUnique", query = "select h FROM PracticeQAMst h WHERE h.tenantId=:tenantId and h.process=:process and  h.practiceQAId=:practiceQAId and h.isActive=1 order by h.id"),	
})

@Table(indexes = { @Index(name = "IDX_PracticeQAMst", columnList = "tenantId,practiceCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","workProductId"}))
public class PracticeQAMst extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String practiceCode="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String practiceAreaCode="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String workProductId="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String workProductName="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String questionId="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String questionDescription="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String answereType="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String answereValue="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String practiceQAId="";

public String getPracticeCode() {
	if (null != practiceCode) {
	} else {
		practiceCode = initvar(practiceCode);
	}
	return practiceCode;
}

public void setPracticeCode(String practiceCode) {
	this.practiceCode = practiceCode;
}

public String getPracticeAreaCode() {
	if (null != practiceAreaCode) {
	} else {
		practiceAreaCode = initvar(practiceAreaCode);
	}
	return practiceAreaCode;
}

public void setPracticeAreaCode(String practiceAreaCode) {
	this.practiceAreaCode = practiceAreaCode;
}

public String getWorkProductId() {
	if (null != workProductId) {
	} else {
		workProductId = initvar(workProductId);
	}
	return workProductId;
}

public void setWorkProductId(String workProductId) {
	this.workProductId = workProductId;
}

public String getWorkProductName() {
	if (null != workProductName) {
	} else {
		workProductName = initvar(workProductName);
	}
	return workProductName;
}

public void setWorkProductName(String workProductName) {
	this.workProductName = workProductName;
}

public String getQuestionId() {
	if (null != questionId) {
	} else {
		questionId = initvar(questionId);
	}
	return questionId;
}

public void setQuestionId(String questionId) {
	this.questionId = questionId;
}

public String getQuestionDescription() {
	if (null != questionDescription) {
	} else {
		questionDescription = initvar(questionDescription);
	}
	return questionDescription;
}

public void setQuestionDescription(String questionDescription) {
	this.questionDescription = questionDescription;
}

public String getAnswereType() {
	if (null != answereType) {
	} else {
		answereType = initvar(answereType);
	}
	return answereType;
}

public void setAnswereType(String answereType) {
	this.answereType = answereType;
}

public String getAnswereValue() {
	if (null != answereValue) {
	} else {
		answereValue = initvar(answereValue);
	}
	return answereValue;
}

public void setAnswereValue(String answereValue) {
	this.answereValue = answereValue;
}

public String getPracticeQAId() {
	if (null != practiceQAId) {
	} else {
		practiceQAId = initvar(practiceQAId);
	}
	return practiceQAId;
}

public void setPracticeQAId(String practiceQAId) {
	this.practiceQAId = practiceQAId;
}



}
