package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "testMst.getAll", query = "select h FROM TestMst h where h.tenantId=:tenantId and h.authStatus=:authStatus order by h.id"),
	@NamedQuery(name = "testMst.getUnique", query = "select h FROM TestMst h where h.tenantId=:tenantId and h.testName=:testName order by h.id"),
//	@NamedQuery(name = "projectMst.getAllActiveProjects", query = "select h FROM ProjectMst h where h.isActive=1 order by h.id"),	
//	@NamedQuery(name = "projectMst.getByTenantAndProcess", query = "select h FROM ProjectMst h where h.isActive=1 and h.tenantId=:tenantId and h.process=:process order by h.id"),
//	@NamedQuery(name = "projectMst.getByName", query = "select h FROM ProjectMst h WHERE h.tenantId=:tenantId and h.projectName=:projectName and h.isActive=1 order by h.id"),
//	@NamedQuery(name = "projectMst.getProjectByProjectCode", query = "select h FROM ProjectMst h WHERE h.projectCode=:projectCode and h.isActive=1 order by h.id"),
//	@NamedQuery(name = "projectMst.getUnique", query = "select h FROM ProjectMst h WHERE h.tenantId=:tenantId and h.businessUnitCode=:businessUnitCode and h.projectCode=:projectCode and h.isActive=1 order by h.id"),
//	
	 	})
////@Table(indexes = { @Index(name = "IDX_ProjectMst", columnList = "tenantId,projectCode,isActive") },
////		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","projectCode","businessUnitCode"}))
public class TestMst extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String testName;

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String questionGroup;


@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String testGroup;


public String getTestName() {
	if (null == testName) {
		return initvar(testName);
	} else {
		return testName;
	}
}


public void setTestName(String testName) {
	this.testName = testName;
}


public String getQuestionGroup() {
	if (null == questionGroup) {
		return initvar(questionGroup);
	} else {
		return questionGroup;
	}
}


public void setQuestionGroup(String questionGroup) {
	this.questionGroup = questionGroup;
}


public String getTestGroup() {
	if (null == testGroup) {
		return initvar(testGroup);
	} else {
		return testGroup;
	}
}


public void setTestGroup(String testGroup) {
	this.testGroup = testGroup;
}



}
