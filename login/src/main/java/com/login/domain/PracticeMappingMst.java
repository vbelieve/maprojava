package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "PracticeMappingMst.getAll", query = "select h FROM PracticeMst h where h.isActive=1 and h.tenantId=:tenantId order by h.id"),
	//@NamedQuery(name = "PracticeMappingMst.getUnique", query = "select h FROM PracticeMst h WHERE h.tenantId=:tenantId and h.categoryCode=:categoryCode and h.capabilityCode=:capabilityCode and h.practiceAreaCode:practiceAreaCode and h.practiceCode=:practiceCode and h.levelCode=:levelCode and h.isActive=1 order by h.id"),	
})

@Table(indexes = { @Index(name = "IDX_PracticeMappingMst", columnList = "tenantId,categoryCode,capabilityCode,practiceAreaCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","categoryCode","capabilityCode","practiceAreaCode","practiceCode","levelCode"}))
public class PracticeMappingMst extends Base{

@Column(columnDefinition = "nvarchar(20)", nullable = false)
private String categoryCode="";

@Column(columnDefinition = "nvarchar(20)", nullable = false)
private String capabilityCode="";

@Column(columnDefinition = "nvarchar(20)", nullable = false)
private String practiceAreaCode="";

@Column(columnDefinition = "nvarchar(20)", nullable = false)
private String practiceCode="";

@Column(columnDefinition = "nvarchar(20)", nullable = false)
private String levelCode="";

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String cmmiVersion="";

public String getCategoryCode() {
	if (null != categoryCode) {
	} else {
		categoryCode = initvar(categoryCode);
	}
	return categoryCode;
}

public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
}

public String getCapabilityCode() {
	if (null != capabilityCode) {
	} else {
		capabilityCode = initvar(capabilityCode);
	}
	return capabilityCode;
}

public void setCapabilityCode(String capabilityCode) {
	this.capabilityCode = capabilityCode;
}

public String getPracticeAreaCode() {
	if (null != practiceAreaCode) {
	} else {
		practiceAreaCode = initvar(practiceAreaCode);
	}
	return practiceAreaCode;
}

public void setPracticeAreaCode(String practiceAreaCode) {
	this.practiceAreaCode = practiceAreaCode;
}

public String getPracticeCode() {
	if (null != practiceCode) {
	} else {
		practiceCode = initvar(practiceCode);
	}
	return practiceCode;
}

public void setPracticeCode(String practiceCode) {
	this.practiceCode = practiceCode;
}

public String getLevelCode() {
	if (null != levelCode) {
	} else {
		levelCode = initvar(levelCode);
	}
	return levelCode;
}

public void setLevelCode(String levelCode) {
	this.levelCode = levelCode;
}

public String getCmmiVersion() {
	if (null == cmmiVersion) {
		return initvar(cmmiVersion);
	} else {
		return cmmiVersion;
	}
}

public void setCmmiVersion(String cmmiVersion) {
	this.cmmiVersion = cmmiVersion;
}



}
