package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "PracticeWorkProductMst.getAll", query = "select h FROM PracticeWorkProductMst h WHERE h.tenantId=:tenantId and h.cmmiVersion=:cmmiVersion and h.isActive=1 order by h.id"),
	@NamedQuery(name = "PracticeWorkProductMst.getByTenantAndProcess", query = "select h FROM PracticeWorkProductMst h where h.isActive=1  and h.cmmiVersion=:cmmiVersion and h.tenantId=:tenantId and h.process=:process order by h.id"),
	@NamedQuery(name = "PracticeWorkProductMst.getByTenantAndProject", query = "select h FROM PracticeWorkProductMst h where h.isActive=1  and h.cmmiVersion=:cmmiVersion and h.tenantId=:tenantId and h.project=:project order by h.id"),
	@NamedQuery(name = "PracticeWorkProductMst.getUnique", query = "select h FROM PracticeWorkProductMst h WHERE tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.practiceCode=:practiceCode and h.workProductName=:workProductName and h.project=:project and h.isActive=1 order by h.id"),
	@NamedQuery(name = "PracticeWorkProductMst.getByPracticeCode", query = "select h FROM PracticeWorkProductMst h WHERE h.organizationUnit=:organizationUnit  and h.cmmiVersion=:cmmiVersion and h.businessUnit=:businessUnit and h.project=:project and  h.tenantId=:tenantId and h.practiceCode=:practiceCode  and h.isActive=1 order by h.id"),
	
})

@Table(indexes = { @Index(name = "IDX_PracticeWorkProductMst", columnList = "tenantId,practiceCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","practiceCode","workProductName","project"}))
public class PracticeWorkProductMst extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String entryDate="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String fieldName="";


@Column(columnDefinition = "nvarchar(1000)", nullable = true)
private String practiceAreaCode="";

@Column(columnDefinition = "nvarchar(1000)", nullable = true)
private String practiceCode="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String workProductId="";
	
@Column(columnDefinition = "nvarchar(2000)", nullable = true)
private String workProductName="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String questionId="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String questionDescription="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String q1="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String q2="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
private String q3="";


@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String answereType="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String answereValue="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String entryStatus="";

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String cmmiVersion="";

public String getPracticeAreaCode() {
	if (null != practiceAreaCode) {
	} else {
		practiceAreaCode = initvar(practiceAreaCode);
	}
	return practiceAreaCode;
}

public void setPracticeAreaCode(String practiceAreaCode) {
	this.practiceAreaCode = practiceAreaCode;
}

public String getPracticeCode() {
	if (null != practiceCode) {
	} else {
		practiceCode = initvar(practiceCode);
	}
	return practiceCode;
}

public void setPracticeCode(String practiceCode) {
	this.practiceCode = practiceCode;
}

public String getWorkProductId() {
	if (null != workProductId) {
	} else {
		workProductId = initvar(workProductId);
	}
	return workProductId;
}

public void setWorkProductId(String workProductId) {
	this.workProductId = workProductId;
}

public String getWorkProductName() {
	if (null != workProductName) {
	} else {
		workProductName = initvar(workProductName);
	}
	return workProductName;
}

public void setWorkProductName(String workProductName) {
	this.workProductName = workProductName;
}

public String getQuestionId() {
	if (null != questionId) {
	} else {
		questionId = initvar(questionId);
	}
	return questionId;
}

public void setQuestionId(String questionId) {
	this.questionId = questionId;
}

public String getQuestionDescription() {
	if (null != questionDescription) {
	} else {
		questionDescription = initvar(questionDescription);
	}
	return questionDescription;
}

public void setQuestionDescription(String questionDescription) {
	this.questionDescription = questionDescription;
}

public String getAnswereType() {
	if (null != answereType) {
	} else {
		answereType = initvar(answereType);
	}
	return answereType;
}

public void setAnswereType(String answereType) {
	this.answereType = answereType;
}

public String getAnswereValue() {
	if (null != answereValue) {
	} else {
		answereValue = initvar(answereValue);
	}
	return answereValue;
}

public void setAnswereValue(String answereValue) {
	this.answereValue = answereValue;
}

public String getEntryDate() {
	if (null != entryDate) {
	} else {
		entryDate = initvar(entryDate);
	}
	return entryDate;
}

public void setEntryDate(String entryDate) {
	this.entryDate = entryDate;
}

public String getEntryStatus() {
	if (null != entryStatus) {
	} else {
		entryStatus = initvar(entryStatus);
	}
	return entryStatus;
}

public void setEntryStatus(String entryStatus) {
	this.entryStatus = entryStatus;
}

public String getFieldName() {
	if (null != fieldName) {
	} else {
		fieldName = initvar(fieldName);
	}
	return fieldName;
}

public void setFieldName(String fieldName) {
	this.fieldName = fieldName;
}

public String getQ1() {
	if (null != q1) {
	} else {
		q1 = initvar(q1);
	}
	return q1;
}

public void setQ1(String q1) {
	this.q1 = q1;
}

public String getQ2() {
	if (null != q2) {
	} else {
		q2 = initvar(q2);
	}
	return q2;
}

public void setQ2(String q2) {
	this.q2 = q2;
}

public String getQ3() {
	if (null != q3) {
	} else {
		q3 = initvar(q3);
	}
	return q3;
}

public void setQ3(String q3) {
	this.q3 = q3;
}

public String getCmmiVersion() {
	if (null == cmmiVersion) {
		return initvar(cmmiVersion);
	} else {
		return cmmiVersion;
	}
}

public void setCmmiVersion(String cmmiVersion) {
	this.cmmiVersion = cmmiVersion;
}



}
