package com.login.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "userMst.getAllUser", query = "select h FROM UserMst h where h.isActive=1 and h.tenantId=:tenantId order by h.id"),
	@NamedQuery(name = "userMst.getAllUserForAdmin", query = "select h FROM UserMst h where h.tenantId=:tenantId order by h.id"),
	@NamedQuery(name = "userMst.getUserByUserName", query = "select h FROM UserMst h WHERE h.tenantId=:tenantId and h.UserName=:userName order by h.id"),
	@NamedQuery(name = "userMst.getUser", query = "select h FROM UserMst h WHERE h.tenantId=:tenantId and h.UserName=:userName and h.password=:password and h.isActive=1 order by h.id")
	
	 	})

@Table(indexes = { @Index(name = "IDX_UserMst", columnList = "tenantId,UserName,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","UserName"}))

public class UserMst extends Base{
	
	@Column(columnDefinition = "nvarchar(100)", nullable = false)
	private String userType;
	
	private String oldPassword="";
	
	@Column(columnDefinition = "nvarchar(100)", nullable = false)
	private String UserName;

	@Column(columnDefinition = "nvarchar(100)", nullable = true)
	private String firstName;
	
	private String action;
	
	@Column(columnDefinition = "nvarchar(100)", nullable = true)
	private String lastName;
	
	@Column(columnDefinition = "nvarchar(100)", nullable = true)
	private String email;
	
	@Column(columnDefinition = "nvarchar(100)", nullable = true)
	private String designation;
	
	@Column(columnDefinition = "nvarchar(100)", nullable = true)
	private String landline;
	
	@Column(columnDefinition = "nvarchar(100)", nullable = true)
	private String mobile;
	
	@Column(columnDefinition = "nvarchar(1000)", nullable = true)
	private String address;
	
	
	@Column(columnDefinition = "nvarchar(100)", nullable = false)
	private String password;

	@Column(columnDefinition = "nvarchar(100)", nullable = true)
	private String project;
	
	@Column(nullable = true)
	private Date lastLoginDate=null;
	
	@Column(nullable = false)
	private Boolean isFirstTimeLogin=Boolean.TRUE;
	
	@Column(nullable = false)
	private Boolean isUserLocked=Boolean.FALSE;

	@Column(nullable = false)
	private Boolean isUserSuspended=Boolean.FALSE;
	
	@Column(nullable = false)
	private Integer noOfFailedAttempts=0;
	
	@Column(nullable = false)
	private Integer allowedDailyFailedAttempts=0;
	
	@Column(columnDefinition = "nvarchar(10)", nullable = true)
	private String cmmiVersion;
	
	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserType() {
		if (null != userType) {
		} else {
			userType = initvar(userType);
		}
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Boolean getIsFirstTimeLogin() {
		if (null != isFirstTimeLogin) {
		} else {
			isFirstTimeLogin = initvar(isFirstTimeLogin);
		}
		return isFirstTimeLogin;
	}

	public void setIsFirstTimeLogin(Boolean isFirstTimeLogin) {
		this.isFirstTimeLogin = isFirstTimeLogin;
	}

	public Boolean getIsUserLocked() {
		if (null != isUserLocked) {
		} else {
			isUserLocked = initvar(isUserLocked);
		}
		return isUserLocked;
	}

	public void setIsUserLocked(Boolean isUserLocked) {
		this.isUserLocked = isUserLocked;
	}

	public Boolean getIsUserSuspended() {
		if (null != isUserSuspended) {
		} else {
			isUserSuspended = initvar(isUserSuspended);
		}
		return isUserSuspended;
	}

	public void setIsUserSuspended(Boolean isUserSuspended) {
		this.isUserSuspended = isUserSuspended;
	}

	public Integer getNoOfFailedAttempts() {
		if (null != noOfFailedAttempts) {
		} else {
			noOfFailedAttempts = initvar(noOfFailedAttempts);
		}
		return noOfFailedAttempts;
	}

	public void setNoOfFailedAttempts(Integer noOfFailedAttempts) {
		this.noOfFailedAttempts = noOfFailedAttempts;
	}

	public Integer getAllowedDailyFailedAttempts() {
		if (null != allowedDailyFailedAttempts) {
		} else {
			allowedDailyFailedAttempts = initvar(allowedDailyFailedAttempts);
		}
		return allowedDailyFailedAttempts;
	}

	public void setAllowedDailyFailedAttempts(Integer allowedDailyFailedAttempts) {
		this.allowedDailyFailedAttempts = allowedDailyFailedAttempts;
	}

	public Date getLastLoginDate() {
		if (null != lastLoginDate) {
		} else {
			lastLoginDate = initvar(lastLoginDate);
		}
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getFirstName() {
		if (null != firstName) {
		} else {
			firstName = initvar(firstName);
		}
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		if (null != lastName) {
		} else {
			lastName = initvar(lastName);
		}
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		if (null != email) {
		} else {
			email = initvar(email);
		}
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDesignation() {
		if (null != designation) {
		} else {
			designation = initvar(designation);
		}
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getLandline() {
		if (null != landline) {
		} else {
			landline = initvar(landline);
		}
		return landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public String getMobile() {
		if (null != mobile) {
		} else {
			mobile = initvar(mobile);
		}
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		if (null != address) {
		} else {
			address = initvar(address);
		}
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAction() {
		if (null != action) {
		} else {
			action = initvar(action);
		}
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getOldPassword() {
		if (null != oldPassword) {
		} else {
			oldPassword = initvar(oldPassword);
		}
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getProject() {
		if (null != project) {
		} else {
			project = initvar(project);
		}
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getCmmiVersion() {
		if (null == cmmiVersion) {
			return initvar(cmmiVersion);
		} else {
			return cmmiVersion;
		}
	}

	public void setCmmiVersion(String cmmiVersion) {
		this.cmmiVersion = cmmiVersion;
	}
	
	

}
