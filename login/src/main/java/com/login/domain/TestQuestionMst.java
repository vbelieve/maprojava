package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "testQuestionMst.getAll", query = "select h FROM TestQuestionMst h where h.tenantId=:tenantId and h.authStatus=:authStatus order by h.id"),
	@NamedQuery(name = "testQuestionMst.getUnique", query = "select h FROM TestQuestionMst h WHERE h.tenantId=:tenantId and h.questionId=:questionId order by h.id"),

	
	//	@NamedQuery(name = "practiceMst.getAllActive", query = "select h FROM PracticeMst h where h.isActive=1  and h.cmmiVersion=:cmmiVersion order by h.tenantId"),
//	@NamedQuery(name = "practiceMst.getPracticeByTenantOrganization", query = "select h FROM PracticeMst h where h.isActive=1 and h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.organizationUnit=:organizationUnit order by h.tenantId"),
//	@NamedQuery(name = "practiceMst.getPracticeByTenantOrganizationBusinesss", query = "select h FROM PracticeMst h where h.isActive=1 and h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.organizationUnit=:organizationUnit and h.businessUnit=:businessUnit order by h.tenantId"),
//	@NamedQuery(name = "practiceMst.getPracticeByTenantOrganizationBusinesssProject", query = "select h FROM PracticeMst h where h.isActive=1  and h.cmmiVersion=:cmmiVersion and h.organizationUnit=:organizationUnit and h.businessUnit=:businessUnit and h.project=:project order by h.tenantId"),
//	@NamedQuery(name = "practiceMst.getAllSum", query = "select h FROM PracticeMst h where h.isActive=1 and h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion order by h.id"),
//	@NamedQuery(name = "practiceMst.getByTenantAndProcess", query = "select h FROM PracticeMst h where h.isActive=1 and h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.process=:process order by h.id"),
//	@NamedQuery(name = "practiceMst.getUnique", query = "select h FROM PracticeMst h WHERE h.organizationUnit=:organizationUnit and h.businessUnit=:businessUnit  and h.cmmiVersion=:cmmiVersion and h.project=:project and  h.tenantId=:tenantId and h.practiceCode=:practiceCode and h.isActive=1 order by h.id"),
//	@NamedQuery(name = "practiceMst.getByTenantAndProject", query = "select h FROM PracticeMst h WHERE h.tenantId=:tenantId "
//			+ "and h.project=:project and h.isActive=1  and h.cmmiVersion=:cmmiVersion order by h.id"),
//	@NamedQuery(name = "practiceMst.getPracticeByTenantProject", query = "select h FROM PracticeMst h WHERE h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.project=:project and  h.isActive=1 order by h.id"),
//	@NamedQuery(name = "practiceMst.getByTenantProjectAndPractice", query = "select h FROM PracticeMst h WHERE h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.project=:project and h.practiceCode=:practiceCode and h.isActive=1"),
//	
})
//
//@Table(indexes = { @Index(name = "IDX_PracticeMst", columnList = "tenantId,project,practiceCode,isActive") },
//		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","practiceCode"}))
public class TestQuestionMst extends Base{
	
	
@Column(nullable = false)
private Long questionId=0L;

@Column(columnDefinition = "nvarchar(2000)", nullable = false)
private String description="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String answerGroup="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String questionGroup="";

@Column(nullable=false)
private Integer priority=0;



public Long getQuestionId() {
	if (null == questionId) {
		return initvar(questionId);
	} else {
		return questionId;
	}
}

public void setQuestionId(Long questionId) {
	this.questionId = questionId;
}

public String getDescription() {
	if (null == description) {
		return initvar(description);
	} else {
		return description;
	}
}

public void setDescription(String description) {
	this.description = description;
}

public String getAnswerGroup() {
	if (null == answerGroup) {
		return initvar(answerGroup);
	} else {
		return answerGroup;
	}
}

public void setAnswerGroup(String answerGroup) {
	this.answerGroup = answerGroup;
}

public String getQuestionGroup() {
	if (null == questionGroup) {
		return initvar(questionGroup);
	} else {
		return questionGroup;
	}
}

public void setQuestionGroup(String questionGroup) {
	this.questionGroup = questionGroup;
}

public Integer getPriority() {
	if (null == priority) {
		return initvar(priority);
	} else {
		return priority;
	}
}

public void setPriority(Integer priority) {
	this.priority = priority;
}






}
