package com.login.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;

public class ApplicationUtility {
	static SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		
	
	
	public static Object jsonStringToObject(Class<?> classname, String jsonStr) {
		Object object = null;
		try {
			Gson gson = new Gson();
			object = (Object) gson.fromJson(jsonStr, classname);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}

	public static String objectToJSONString(Object txnObj) {
		String josnString = "";
		try {
			Gson gson = new Gson();
			josnString = gson.toJson(txnObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return josnString;
	}
	
	

	public static Date getDate(String dateStr) throws ParseException
	{
		
		if(isValidData(dateStr))
		{
			return sdf.parse(dateStr);
		}
		else
		{
			return null;	
		}
		
	}
	
	
	public static Boolean isValidData(String param) {
		if (null != param && !param.isEmpty() && !param.equalsIgnoreCase("") && !param.equalsIgnoreCase(" ")) {
			return true;
		}
		return false;
	}
	
	public static String checkDefaultAmount(String param) {
		if (null != param && !param.isEmpty() && !param.equalsIgnoreCase("") && !param.equalsIgnoreCase(" ")) {
			return param;
		}
		return "0.00";
	}
	
	public static String checkDefaultNumber(String param) {
		if (null != param && !param.isEmpty() && !param.equalsIgnoreCase("") && !param.equalsIgnoreCase(" ")) {
			return param;
		}
		return "0";
	}
	
	public static String checkDefaultBoolean(String param) {
		if (null != param && !param.isEmpty() && !param.equalsIgnoreCase("") && !param.equalsIgnoreCase(" ")) {
			if(param.equalsIgnoreCase("1") || param.equalsIgnoreCase("0"))
			return param;
		}
		
		return "0";
	}
	
	
	public static String removeComma(String val) {
		String[] numbers = val.split(",");
		val = "";
		String vAmt = "";
		for (String number : numbers) {
			String str1 = number;
			vAmt = val.concat(str1);
			val = vAmt;
		}
		return vAmt;
	}
	
}
