package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "organizationUnitMst.getAll", query = "select h FROM OrganizationUnitMst h where h.isActive=1 and h.tenantId=:tenantId order by h.id"),
	@NamedQuery(name = "organizationUnitMst.getAllActiveOrganizationUnit", query = "select h FROM OrganizationUnitMst h where h.isActive=1 order by h.tenantId"),
	@NamedQuery(name = "organizationUnitMst.getByTenantAndProcess", query = "select h FROM OrganizationUnitMst h where h.isActive=1 and h.tenantId=:tenantId and h.process=:process order by h.id"),
	@NamedQuery(name = "organizationUnitMst.getByName", query = "select h FROM OrganizationUnitMst h WHERE h.tenantId=:tenantId and h.organizationUnitName=:organizationUnitName and h.isActive=1 order by h.id"),
	@NamedQuery(name = "organizationUnitMst.getUnique", query = "select h FROM OrganizationUnitMst h WHERE h.tenantId=:tenantId and h.organizationUnitCode=:organizationUnitCode and h.isActive=1 order by h.id"),
	
	 	})
@Table(indexes = { @Index(name = "IDX_OrganizationUnitMst", columnList = "tenantId,organizationUnitCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","organizationUnitCode"}))
public class OrganizationUnitMst extends Base{

@GeneratedValue(strategy = GenerationType.AUTO)
@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String organizationUnitCode;

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String organizationUnitName;

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String organizationModel;

public String getOrganizationUnitCode() {
	if (null != organizationUnitCode) {
	} else {
		organizationUnitCode = initvar(organizationUnitCode);
	}
	return organizationUnitCode;
}
public void setOrganizationUnitCode(String organizationUnitCode) {
	this.organizationUnitCode = organizationUnitCode;
}
public String getOrganizationUnitName() {
	if (null != organizationUnitName) {
	} else {
		organizationUnitName = initvar(organizationUnitName);
	}
	return organizationUnitName;
}
public void setOrganizationUnitName(String organizationUnitName) {
	this.organizationUnitName = organizationUnitName;
}
public String getOrganizationModel() {
	if (null != organizationModel) {
	} else {
		organizationModel = initvar(organizationModel);
	}
	return organizationModel;
}
public void setOrganizationModel(String organizationModel) {
	this.organizationModel = organizationModel;
}


}
