package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "practiceMst.getAll", query = "select h FROM PracticeMst h where h.isActive=1 and h.tenantId=:tenantId and h.cmmiVersion=:cmmiVersion order by h.id"),
	@NamedQuery(name = "practiceMst.getAllActive", query = "select h FROM PracticeMst h where h.isActive=1  and h.cmmiVersion=:cmmiVersion order by h.tenantId"),
	@NamedQuery(name = "practiceMst.getPracticeByTenantOrganization", query = "select h FROM PracticeMst h where h.isActive=1 and h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.organizationUnit=:organizationUnit order by h.tenantId"),
	@NamedQuery(name = "practiceMst.getPracticeByTenantOrganizationBusinesss", query = "select h FROM PracticeMst h where h.isActive=1 and h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.organizationUnit=:organizationUnit and h.businessUnit=:businessUnit order by h.tenantId"),
	@NamedQuery(name = "practiceMst.getPracticeByTenantOrganizationBusinesssProject", query = "select h FROM PracticeMst h where h.isActive=1  and h.cmmiVersion=:cmmiVersion and h.organizationUnit=:organizationUnit and h.businessUnit=:businessUnit and h.project=:project order by h.tenantId"),
	@NamedQuery(name = "practiceMst.getAllSum", query = "select h FROM PracticeMst h where h.isActive=1 and h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion order by h.id"),
	@NamedQuery(name = "practiceMst.getByTenantAndProcess", query = "select h FROM PracticeMst h where h.isActive=1 and h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.process=:process order by h.id"),
	@NamedQuery(name = "practiceMst.getUnique", query = "select h FROM PracticeMst h WHERE h.organizationUnit=:organizationUnit and h.businessUnit=:businessUnit  and h.cmmiVersion=:cmmiVersion and h.project=:project and  h.tenantId=:tenantId and h.practiceCode=:practiceCode and h.isActive=1 order by h.id"),
	@NamedQuery(name = "practiceMst.getByTenantAndProject", query = "select h FROM PracticeMst h WHERE h.tenantId=:tenantId "
			+ "and h.project=:project and h.isActive=1  and h.cmmiVersion=:cmmiVersion order by h.id"),
	@NamedQuery(name = "practiceMst.getPracticeByTenantProject", query = "select h FROM PracticeMst h WHERE h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.project=:project and  h.isActive=1 order by h.id"),
	@NamedQuery(name = "practiceMst.getByTenantProjectAndPractice", query = "select h FROM PracticeMst h WHERE h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.project=:project and h.practiceCode=:practiceCode and h.isActive=1"),
	
})

@Table(indexes = { @Index(name = "IDX_PracticeMst", columnList = "tenantId,project,practiceCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","practiceCode"}))
public class PracticeMst extends Base{

@Column(columnDefinition = "nvarchar(2000)", nullable = false)
private String practiceName="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
private String currentGapNptes="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
private String currentPractice="";


@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String practiceCode="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String practiceAreaCode="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String categoryCode="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String capabilityCode="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
private String capabilityName="";

@Column(nullable=true)
private Integer practiceLevelCode=0;

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
private String categoryName="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
private String practiceAreaName="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
private String practiceDescription="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
private String practiceValue="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
private String practiceExamples="";

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String practiceComplianceLevel="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String practiceBusinessModel="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String practiceBusinessUnit="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String practiceProject="";

@Column(nullable = true)
private Boolean isSaved=Boolean.FALSE;


@Column(nullable = true)
private Double practiceCompliance=0d;

@Column(nullable = true)
private Integer priority=0;

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String cmmiVersion="";


public String getPracticeName() {
	if (null != practiceName) {
	} else {
		practiceName = initvar(practiceName);
	}
	return practiceName;
}

public void setPracticeName(String practiceName) {
	this.practiceName = practiceName;
}

public String getPracticeCode() {
	if (null != practiceCode) {
	} else {
		practiceCode = initvar(practiceCode);
	}
	return practiceCode;
}

public void setPracticeCode(String practiceCode) {
	this.practiceCode = practiceCode;
}

public String getPracticeAreaCode() {
	if (null != practiceAreaCode) {
	} else {
		practiceAreaCode = initvar(practiceAreaCode);
	}
	return practiceAreaCode;
}

public void setPracticeAreaCode(String practiceAreaCode) {
	this.practiceAreaCode = practiceAreaCode;
}

public String getPracticeDescription() {
	if (null != practiceDescription) {
	} else {
		practiceDescription = initvar(practiceDescription);
	}
	return practiceDescription;
}

public void setPracticeDescription(String practiceDescription) {
	this.practiceDescription = practiceDescription;
}

public String getPracticeValue() {
	if (null != practiceValue) {
	} else {
		practiceValue = initvar(practiceValue);
	}
	return practiceValue;
}

public void setPracticeValue(String practiceValue) {
	this.practiceValue = practiceValue;
}

public String getPracticeExamples() {
	if (null != practiceExamples) {
	} else {
		practiceExamples = initvar(practiceExamples);
	}
	return practiceExamples;
}

public void setPracticeExamples(String practiceExamples) {
	this.practiceExamples = practiceExamples;
}

public String getPracticeComplianceLevel() {
	if (null != practiceComplianceLevel) {
	} else {
		practiceComplianceLevel = initvar(practiceComplianceLevel);
	}
	return practiceComplianceLevel;
}

public void setPracticeComplianceLevel(String practiceComplianceLevel) {
	this.practiceComplianceLevel = practiceComplianceLevel;
}

public String getCurrentGapNptes() {
	if (null != currentGapNptes) {
	} else {
		currentGapNptes = initvar(currentGapNptes);
	}
	return currentGapNptes;
}

public void setCurrentGapNptes(String currentGapNptes) {
	this.currentGapNptes = currentGapNptes;
}

public Double getPracticeCompliance() {
	if (null != practiceCompliance) {
	} else {
		practiceCompliance = initvar(practiceCompliance);
	}
	return practiceCompliance;
}

public void setPracticeCompliance(Double practiceCompliance) {
	this.practiceCompliance = practiceCompliance;
}

public String getPracticeBusinessModel() {
	if (null != practiceBusinessModel) {
	} else {
		practiceBusinessModel = initvar(practiceBusinessModel);
	}
	return practiceBusinessModel;
}

public void setPracticeBusinessModel(String practiceBusinessModel) {
	this.practiceBusinessModel = practiceBusinessModel;
}

public String getPracticeBusinessUnit() {
	if (null != practiceBusinessUnit) {
	} else {
		practiceBusinessUnit = initvar(practiceBusinessUnit);
	}
	return practiceBusinessUnit;
}

public void setPracticeBusinessUnit(String practiceBusinessUnit) {
	this.practiceBusinessUnit = practiceBusinessUnit;
}

public String getPracticeProject() {
	if (null != practiceProject) {
	} else {
		practiceProject = initvar(practiceProject);
	}
	return practiceProject;
}

public void setPracticeProject(String practiceProject) {
	this.practiceProject = practiceProject;
}

public String getCategoryCode() {
	if (null != categoryCode) {
	} else {
		categoryCode = initvar(categoryCode);
	}
	return categoryCode;
}

public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
}

public String getCapabilityCode() {
	if (null != capabilityCode) {
	} else {
		capabilityCode = initvar(capabilityCode);
	}
	return capabilityCode;
}

public void setCapabilityCode(String capabilityCode) {
	this.capabilityCode = capabilityCode;
}

public String getCapabilityName() {
	if (null != capabilityName) {
	} else {
		capabilityName = initvar(capabilityName);
	}
	return capabilityName;
}

public void setCapabilityName(String capabilityName) {
	this.capabilityName = capabilityName;
}

public String getCategoryName() {
	if (null != categoryName) {
	} else {
		categoryName = initvar(categoryName);
	}
	return categoryName;
}

public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
}

public String getPracticeAreaName() {
	if (null != practiceAreaName) {
	} else {
		practiceAreaName = initvar(practiceAreaName);
	}
	return practiceAreaName;
}

public void setPracticeAreaName(String practiceAreaName) {
	this.practiceAreaName = practiceAreaName;
}



public String getCurrentPractice() {
	if (null != currentPractice) {
	} else {
		currentPractice = initvar(currentPractice);
	}
	return currentPractice;
}

public void setCurrentPractice(String currentPractice) {
	this.currentPractice = currentPractice;
}

public Boolean getIsSaved() {
	if (null != isSaved) {
	} else {
		isSaved = initvar(isSaved);
	}
	return isSaved;
}

public void setIsSaved(Boolean isSaved) {
	this.isSaved = isSaved;
}

public Integer getPracticeLevelCode() {
	if (null == practiceLevelCode) {
		return initvar(practiceLevelCode);
	} else {
		return practiceLevelCode;
	}
}

public void setPracticeLevelCode(Integer practiceLevelCode) {
	this.practiceLevelCode = practiceLevelCode;
}

public Integer getPriority() {
	return priority;
}

public void setPriority(Integer priority) {
	this.priority = priority;
}

public String getCmmiVersion() {
	if (null == cmmiVersion) {
		return initvar(cmmiVersion);
	} else {
		return cmmiVersion;
	}
}

public void setCmmiVersion(String cmmiVersion) {
	this.cmmiVersion = cmmiVersion;
}




}
