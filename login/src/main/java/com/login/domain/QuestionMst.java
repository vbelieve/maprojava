package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "QuestionMst.getAll", query = "select h FROM QuestionMst h WHERE h.tenantId=:tenantId and h.isActive=1 order by h.id"),
	//@NamedQuery(name = "QuestionMst.getByTenantAndProcess", query = "select h FROM QuestionMst h where h.isActive=1 and h.tenantId=:tenantId and h.process=:process order by h.id"),
	@NamedQuery(name = "QuestionMst.getUnique", query = "select h FROM QuestionMst h WHERE h.tenantId=:tenantId and h.process=:process and h.questionId=:questionId and h.isActive=1 order by h.id"),
	@NamedQuery(name = "QuestionMst.getByQuestionId", query = "select h FROM QuestionMst h WHERE h.tenantId=:tenantId and h.questionId=:questionId and h.isActive=1 order by h.id"),
	
})

@Table(indexes = { @Index(name = "IDX_QuestionMst", columnList = "tenantId,questionId,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","questionId"}))
public class QuestionMst extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String questionId="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String questionDescription="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String answereType="";

public String getQuestionId() {
	if (null != questionId) {
	} else {
		questionId = initvar(questionId);
	}
	return questionId;
}

public void setQuestionId(String questionId) {
	this.questionId = questionId;
}

public String getQuestionDescription() {
	if (null != questionDescription) {
	} else {
		questionDescription = initvar(questionDescription);
	}
	return questionDescription;
}

public void setQuestionDescription(String questionDescription) {
	this.questionDescription = questionDescription;
}

public String getAnswereType() {
	if (null != answereType) {
	} else {
		answereType = initvar(answereType);
	}
	return answereType;
}

public void setAnswereType(String answereType) {
	this.answereType = answereType;
}



}
