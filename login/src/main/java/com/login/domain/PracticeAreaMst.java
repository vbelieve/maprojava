package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "practiceAreaMst.getAll", query = "select h FROM PracticeAreaMst h where h.isActive=1 and h.tenantId=:tenantId and h.cmmiVersion=:cmmiVersion order by h.id"),
	@NamedQuery(name = "practiceAreaMst.getByTenantAndProcess", query = "select h FROM PracticeAreaMst h where h.isActive=1 and h.tenantId=:tenantId and h.process=:process  and h.cmmiVersion=:cmmiVersion order by h.practiceAreaName"),
	@NamedQuery(name = "practiceAreaMst.getByTenantAndProject", query = "select h FROM PracticeAreaMst h where h.isActive=1 and  h.project=:project  and h.cmmiVersion=:cmmiVersion order by h.id"),
	@NamedQuery(name = "practiceAreaMst.getBase", query = "select h FROM PracticeAreaMst h where h.isActive=1 and  h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion order by h.id"),
	
	@NamedQuery(name = "practiceAreaMst.getByProjectPracticeArea", query = "select h FROM PracticeAreaMst h where h.tenantId=:tenantId and  h.project=:project and h.practiceAreaCode=:practiceAreaCode  and h.cmmiVersion=:cmmiVersion  and h.isActive=1"),
	
	//@NamedQuery(name = "practiceAreaMst.getUnique", query = "select h FROM PracticeAreaMst h where h.organizationUnit=:organizationUnit and h.businessUnit=:businessUnit and h.project=:project and  h.isActive=1 and  h.tenantId=:tenantId and h.practiceAreaCode=:practiceAreaCode order by h.id"),
	@NamedQuery(name = "practiceAreaMst.getUnique", query = "select h FROM PracticeAreaMst h where h.isActive=1 and  h.tenantId=:tenantId and h.practiceAreaCode=:practiceAreaCode  and h.cmmiVersion=:cmmiVersion order by h.id"),
	 	})

@Table(indexes = { @Index(name = "IDX_PracticeAreaMst", columnList = "tenantId,practiceAreaCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","practiceAreaCode"}))
public class PracticeAreaMst extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
public String practiceAreaCode="";

@Column(columnDefinition = "nvarchar(2000)", nullable = false)
public String practiceAreaName="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String businessModel="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String practiceAreaIntent="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String categoryCode="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String capabilityCode="";


@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String practiceAreaValue="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
public String workProductList="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String practiceAreaAdditionalInfo="";

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String cmmiVersion="";

public String getPracticeAreaCode() {
	if (null != practiceAreaCode) {
	} else {
		practiceAreaCode = initvar(practiceAreaCode);
	}
	return practiceAreaCode;
}

public void setPracticeAreaCode(String practiceAreaCode) {
	this.practiceAreaCode = practiceAreaCode;
}

public String getPracticeAreaName() {
	if (null != practiceAreaName) {
	} else {
		practiceAreaName = initvar(practiceAreaName);
	}
	return practiceAreaName;
}

public void setPracticeAreaName(String practiceAreaName) {
	this.practiceAreaName = practiceAreaName;
}

public String getPracticeAreaIntent() {
	if (null != practiceAreaIntent) {
	} else {
		practiceAreaIntent = initvar(practiceAreaIntent);
	}
	return practiceAreaIntent;
}

public void setPracticeAreaIntent(String practiceAreaIntent) {
	this.practiceAreaIntent = practiceAreaIntent;
}

public String getPracticeAreaValue() {
	if (null != practiceAreaValue) {
	} else {
		practiceAreaValue = initvar(practiceAreaValue);
	}
	return practiceAreaValue;
}

public void setPracticeAreaValue(String practiceAreaValue) {
	this.practiceAreaValue = practiceAreaValue;
}

public String getPracticeAreaAdditionalInfo() {
	if (null != practiceAreaAdditionalInfo) {
	} else {
		practiceAreaAdditionalInfo = initvar(practiceAreaAdditionalInfo);
	}
	return practiceAreaAdditionalInfo;
}

public void setPracticeAreaAdditionalInfo(String practiceAreaAdditionalInfo) {
	this.practiceAreaAdditionalInfo = practiceAreaAdditionalInfo;
}

public String getWorkProductList() {
	if (null != workProductList) {
	} else {
		workProductList = initvar(workProductList);
	}
	return workProductList;
}

public void setWorkProductList(String workProductList) {
	this.workProductList = workProductList;
}

public String getBusinessModel() {
	if (null != businessModel) {
	} else {
		businessModel = initvar(businessModel);
	}
	return businessModel;
}

public void setBusinessModel(String businessModel) {
	this.businessModel = businessModel;
}

public String getCategoryCode() {
	if (null != categoryCode) {
	} else {
		categoryCode = initvar(categoryCode);
	}
	return categoryCode;
}

public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
}

public String getCapabilityCode() {
	if (null != capabilityCode) {
	} else {
		capabilityCode = initvar(capabilityCode);
	}
	return capabilityCode;
}

public void setCapabilityCode(String capabilityCode) {
	this.capabilityCode = capabilityCode;
}

public String getCmmiVersion() {
	if (null == cmmiVersion) {
		return initvar(cmmiVersion);
	} else {
		return cmmiVersion;
	}
}

public void setCmmiVersion(String cmmiVersion) {
	this.cmmiVersion = cmmiVersion;
}



}
