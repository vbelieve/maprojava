package com.login.domain.basics;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.login.domain.ApplicationConstants;

@MappedSuperclass
public class Base implements Serializable {
	
	private static final long serialVersionUID = 1701926931204630606L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;

	@Version
	protected Integer version = 0;

	public Date initvar(Date val){ 
		String intiaiDate ="01-JAN-1900";
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		try {
			return sdf.parse(intiaiDate);
		} catch (ParseException e) {
			return null;
		}
	}
	
	public Map<String,String> initvar(Map<String,String> val)
	{
		val = new HashMap<String,String>();
		return val;
		
	}
	
	public Boolean initvar(Boolean val)
	{
		return Boolean.FALSE;
		
	}
	
	public String initvar(String val)
	{
		return " ";
		
	}
	public Double initvar(Double val)
	{
		return 0.00;
		
	}
	public Integer initvar(Integer val)
	{
		return 0;
		
	}
	public Long initvar(Long val)
	{
		return 0l;
		
	}


	
	@Column(columnDefinition="nvarchar(60)", nullable = false)
	protected String tenantId="";
	@Transient
	protected Integer complianceScore=0;
	@Transient
	protected Integer totalComplianceScore=0;
	

	@Column(columnDefinition="nvarchar(60)", nullable = true)
	protected String process="";
	
	@Column(columnDefinition="nvarchar(60)", nullable = true)
	protected String organizationUnit="";
	
	@Column(columnDefinition="nvarchar(60)", nullable = true)
	protected String businessUnit="";
	
	@Column(columnDefinition="nvarchar(60)", nullable = true)
	protected String project="";
	
	
	@Column(nullable = true)
	protected Long activityId=0L;
	
	@Column(columnDefinition="nvarchar(300)", nullable = true)
	private String description = "";

	@Column(columnDefinition="nvarchar(40)", nullable = true)
	protected String createdBy="";

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true)
	protected Date createdDate;

	@Column(columnDefinition="nvarchar(40)", nullable = true)
	protected String lastModifiedBy="";

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true)
	protected Date lastModifiedDate;
	
	@Column(nullable=true)
	protected Integer isActive;
	
	@Column(columnDefinition="nvarchar(40)", nullable = true)
	protected String deprecatedBy="";

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true)
	protected Date deprecatedDate;
	
	@Basic
	Boolean deprecated=false;


	
	@Column(columnDefinition="nvarchar(40)", nullable = true)
	private String authStatus;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}


	public Long getActivityId() {
		return activityId;
	}

	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getDeprecatedBy() {
		return deprecatedBy;
	}

	public void setDeprecatedBy(String deprecatedBy) {
		this.deprecatedBy = deprecatedBy;
	}

	public Date getDeprecatedDate() {
		return deprecatedDate;
	}

	public void setDeprecatedDate(Date deprecatedDate) {
		this.deprecatedDate = deprecatedDate;
	}

	public Boolean getDeprecated() {
		return deprecated;
	}

	public void setDeprecated(Boolean deprecated) {
		this.deprecated = deprecated;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	public String getAuthStatus() {
		return authStatus;
	}

	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}

	public String getTenantId() {
		if (null != tenantId) {
		} else {
			tenantId = ApplicationConstants.DEFAULT_TENANT;
		}
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getProcess() {
		if (null != process) {
		} else {
			process = initvar(process);
		}
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public String getOrganizationUnit() {
		if (null != organizationUnit) {
		} else {
			organizationUnit = initvar(organizationUnit);
		}
		return organizationUnit;
	}

	public void setOrganizationUnit(String organizationUnit) {
		this.organizationUnit = organizationUnit;
	}

	public String getBusinessUnit() {
		if (null != businessUnit) {
		} else {
			businessUnit = initvar(businessUnit);
		}
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getProject() {
		if (null != project) {
		} else {
			project = initvar(project);
		}
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public Integer getComplianceScore() {
		if (null != complianceScore) {
		} else {
			complianceScore = initvar(complianceScore);
		}
		return complianceScore;
	}

	public void setComplianceScore(Integer complianceScore) {
		this.complianceScore = complianceScore;
	}

	public Integer getTotalComplianceScore() {
		if (null != totalComplianceScore) {
		} else {
			totalComplianceScore = initvar(totalComplianceScore);
		}
		return totalComplianceScore;
	}

	public void setTotalComplianceScore(Integer totalComplianceScore) {
		this.totalComplianceScore = totalComplianceScore;
	}
	
	
}
