package com.login.domain.basics;

import java.util.Date;

import org.springframework.context.annotation.Bean;

import com.login.domain.ApplicationConstants;


public class DashBoardVo {

	private String tenantId = "";
	private String process = "";
	private String tenantProcessPracticeAreaPercent="";
	private String tenantProcessPracticeAreaSum="";
	private String tenantProcessPracticeAreaPracticeCount="";
	private String tenantProcessPracticeAreaCode="";
	private String tenantName="";
	private String projectName="";
	private String projectCode="";
	private String categoryCode="";
	private String practiceCode="";
	private String capabilityCode="";
	private Double complianceScore=0D;
	private Double totalComplianceScore=0D;
	
	private String practiceAreaCode="";
	
	
	
	public String getTenantId() {
		if (null != tenantId) {
		} else {
			tenantId = "";
		}
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getProcess() {
		if (null != process) {
		} else {
			process = "";
		}
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public String getTenantProcessPracticeAreaPercent() {
		if (null != tenantProcessPracticeAreaPercent) {
		} else {
			tenantProcessPracticeAreaPercent = "0";
		}
		return tenantProcessPracticeAreaPercent;
	}
	public void setTenantProcessPracticeAreaPercent(String tenantProcessPracticeAreaPercent) {
		this.tenantProcessPracticeAreaPercent = tenantProcessPracticeAreaPercent;
	}
	public String getTenantProcessPracticeAreaSum() {
		if (null != tenantProcessPracticeAreaSum) {
		} else {
			tenantProcessPracticeAreaSum = "0";
		}
		return tenantProcessPracticeAreaSum;
	}
	public void setTenantProcessPracticeAreaSum(String tenantProcessPracticeAreaSum) {
		this.tenantProcessPracticeAreaSum = tenantProcessPracticeAreaSum;
	}
	public String getTenantProcessPracticeAreaPracticeCount() {
		if (null != tenantProcessPracticeAreaPracticeCount) {
		} else {
			tenantProcessPracticeAreaPracticeCount = "0";
		}
		return tenantProcessPracticeAreaPracticeCount;
	}
	public void setTenantProcessPracticeAreaPracticeCount(String tenantProcessPracticeAreaPracticeCount) {
		this.tenantProcessPracticeAreaPracticeCount = tenantProcessPracticeAreaPracticeCount;
	}
	public String getTenantProcessPracticeAreaCode() {
		if (null != tenantProcessPracticeAreaCode) {
		} else {
			tenantProcessPracticeAreaCode = "";
		}
		return tenantProcessPracticeAreaCode;
	}
	public void setTenantProcessPracticeAreaCode(String tenantProcessPracticeAreaCode) {
		this.tenantProcessPracticeAreaCode = tenantProcessPracticeAreaCode;
	}
	public String getTenantName() {
		if (null != tenantName) {
		} else {
			tenantName = "";
		}
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCapabilityCode() {
		return capabilityCode;
	}
	public void setCapabilityCode(String capabilityCode) {
		this.capabilityCode = capabilityCode;
	}
	public Double getComplianceScore() {
		
		return complianceScore;
	}
	public void setComplianceScore(Double complianceScore) {
		this.complianceScore = complianceScore;
	}
	public String getPracticeAreaCode() {
		return practiceAreaCode;
	}
	public void setPracticeAreaCode(String practiceAreaCode) {
		this.practiceAreaCode = practiceAreaCode;
	}
	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	public Double getTotalComplianceScore() {
		
		return totalComplianceScore;
	}
	public void setTotalComplianceScore(Double totalComplianceScore) {
		this.totalComplianceScore = totalComplianceScore;
	}
	public String getPracticeCode() {
		
		return practiceCode;
	}
	public void setPracticeCode(String practiceCode) {
		this.practiceCode = practiceCode;
	}

	
		
	}
