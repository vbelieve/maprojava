package com.login.domain.basics;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Bean;

import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.RelationMappingPracticeAreaPractice;



public class AccessObject {

	private String tenantId = "";
	private String loginId = "";
	private String userType = "";
	private String userFirstName = "";
	private String ipAddr = "";
	private String responseMsg = "FAILED";
	private String orgName="";
	private String process="";
	private String allProcess="";
	private String category="";
	private String capability="";
	private String practiceArea="";
	private String practice="";
	private String msg="";
	private String organizationUnit="";
	private String businessUnit="";
	private String project="";
	private String selectedProject="";
	private String selectedProjectName="";
	private String businessModel="";
	private Integer practiceLevel=0;
	private String cmmiVersion="";
	
	List<CategoryMst> categoryMst;
	List<CapabilityMst> capabilityMst;
	List<PracticeAreaMst> practiceAreaMst;
	List<RelationMappingCategoryCapability> relationMappingCategoryCapability;
	List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea;
	List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice;
	private String menu="";
	
	public String getTenantId() {
		if (null != tenantId) {
		} else {
			tenantId = "";
		}
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getLoginId() {
		if (null != loginId) {
		} else {
			loginId = "";
		}
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserType() {
		if (null != userType) {
		} else {
			userType = "";
		}
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getIpAddr() {
		if (null != ipAddr) {
		} else {
			ipAddr = "";
		}
		return ipAddr;
	}
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}
	public String getResponseMsg() {
		if (null != responseMsg) {
		} else {
			responseMsg = "FAILED";
		}
		return responseMsg;
	}
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	public String getOrgName() {
		if (null != orgName) {
		} else {
			orgName = "";
		}
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getProcess() {
		if (null != process) {
		} else {
			process = "";
		}
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public String getAllProcess() {
		if (null != allProcess) {
		} else {
			allProcess = "";
		}
		return allProcess;
	}
	public void setAllProcess(String allProcess) {
		this.allProcess = allProcess;
	}
	public String getCategory() {
		if (null != category) {
		} else {
			category = "";
		}
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCapability() {
		if (null != capability) {
		} else {
			capability = "";
		}
		return capability;
	}
	public void setCapability(String capability) {
		this.capability = capability;
	}
	public String getPracticeArea() {
		if (null != practiceArea) {
		} else {
			practiceArea = "";
		}
		return practiceArea;
	}
	public void setPracticeArea(String practiceArea) {
		this.practiceArea = practiceArea;
	}
	public String getPractice() {
		if (null != practice) {
		} else {
			practice = "";
		}
		return practice;
	}
	public void setPractice(String practice) {
		this.practice = practice;
	}
	public String getMsg() {
		if (null != msg) {
		} else {
			msg = "";
		}
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getUserFirstName() {
		if (null != userFirstName) {
		} else {
			userFirstName = "";
		}
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getOrganizationUnit() {
		if (null != organizationUnit) {
		} else {
			organizationUnit = "";
		}
		return organizationUnit;
	}
	public void setOrganizationUnit(String organizationUnit) {
		this.organizationUnit = organizationUnit;
	}
	public String getBusinessUnit() {
		if (null != businessUnit) {
		} else {
			businessUnit = "";
		}
		return businessUnit;
	}
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getProject() {
		if (null != project) {
		} else {
			project = "";
		}
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getBusinessModel() {
		if (null != businessModel) {
		} else {
			businessModel = "";
		}
		return businessModel;
	}
	public void setBusinessModel(String businessModel) {
		this.businessModel = businessModel;
	}
	public Integer getPracticeLevel() {
		if (null != practiceLevel) {
		} else {
			practiceLevel = 0;
		}
		return practiceLevel;
	}
	public void setPracticeLevel(Integer practiceLevel) {
		this.practiceLevel = practiceLevel;
	}
	public String getSelectedProject() {
		if (null != selectedProject) {
		} else {
			selectedProject = "";
		}
		return selectedProject;
	}
	public void setSelectedProject(String selectedProject) {
		this.selectedProject = selectedProject;
	}
	public String getSelectedProjectName() {
		if (null != selectedProjectName) {
		} else {
			selectedProjectName = "";
		}
		return selectedProjectName;
	}
	public void setSelectedProjectName(String selectedProjectName) {
		this.selectedProjectName = selectedProjectName;
	}
	public String getMenu() {
		return menu;
	}
	public void setMenu(String menu) {
		this.menu = menu;
	}
	public List<CategoryMst> getCategoryMst() {
				return categoryMst;
	}
	public void setCategoryMst(List<CategoryMst> categoryMst) {
		this.categoryMst = categoryMst;
	}
	public List<CapabilityMst> getCapabilityMst() {
		
		return capabilityMst;
	}
	public void setCapabilityMst(List<CapabilityMst> capabilityMst) {
		this.capabilityMst = capabilityMst;
	}
	public List<PracticeAreaMst> getPracticeAreaMst() {
		return practiceAreaMst;
	}
	public void setPracticeAreaMst(List<PracticeAreaMst> practiceAreaMst) {
		this.practiceAreaMst = practiceAreaMst;
	}
	public List<RelationMappingCategoryCapability> getRelationMappingCategoryCapability() {
	
		return relationMappingCategoryCapability;
	}
	public void setRelationMappingCategoryCapability(
			List<RelationMappingCategoryCapability> relationMappingCategoryCapability) {
		this.relationMappingCategoryCapability = relationMappingCategoryCapability;
	}
	public List<RelationMappingCapabilityPracticeArea> getRelationMappingCapabilityPracticeArea() {
	
		return relationMappingCapabilityPracticeArea;
	}
	public void setRelationMappingCapabilityPracticeArea(
			List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea) {
		this.relationMappingCapabilityPracticeArea = relationMappingCapabilityPracticeArea;
	}
	public List<RelationMappingPracticeAreaPractice> getRelationMappingPracticeAreaPractice() {
	
		return relationMappingPracticeAreaPractice;
	}
	public void setRelationMappingPracticeAreaPractice(
			List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice) {
		this.relationMappingPracticeAreaPractice = relationMappingPracticeAreaPractice;
	}
	public String getCmmiVersion() {
		if (null == cmmiVersion) {
			return "";
		} else {
			return cmmiVersion;
		}
	}
	public void setCmmiVersion(String cmmiVersion) {
		this.cmmiVersion = cmmiVersion;
	}
	
	
	

	}
