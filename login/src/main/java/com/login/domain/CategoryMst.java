package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "categoryMst.getBaseCategory", query = "select h FROM CategoryMst h where h.isActive=1 and h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion order by h.id"),
	@NamedQuery(name = "categoryMst.getAll", query = "select h FROM CategoryMst h where h.isActive=1 and h.tenantId=:tenantId and h.cmmiVersion=:cmmiVersion order by h.id"),
	@NamedQuery(name = "categoryMst.getByTenantAndProcess", query = "select h FROM CategoryMst h where h.isActive=1 and h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.process=:process order by h.id"),
	@NamedQuery(name = "categoryMst.getByTenantAndProject", query = "select h FROM CategoryMst h where h.isActive=1 and h.project=:project  and h.cmmiVersion=:cmmiVersion order by h.id"),
	@NamedQuery(name = "categoryMst.getByName", query = "select h FROM CategoryMst h WHERE h.tenantId=:tenantId and h.categoryName=:categoryName  and h.cmmiVersion=:cmmiVersion and h.isActive=1 order by h.id"),
	@NamedQuery(name = "categoryMst.getUnique", query = "select h FROM CategoryMst h WHERE h.tenantId=:tenantId and h.categoryCode=:categoryCode  and h.cmmiVersion=:cmmiVersion and h.isActive=1 and h.organizationUnit=:organizationUnit and h.businessUnit=:businessUnit and h.project=:project order by h.id"),
	
	@NamedQuery(name = "categoryMst.getUniqueCmmiVersion", query = "select h FROM CategoryMst h WHERE h.tenantId=:tenantId and h.categoryCode=:categoryCode and h.isActive=1 and h.organizationUnit=:organizationUnit and h.businessUnit=:businessUnit and h.project=:project and h.cmmiVersion=:cmmiVersion order by h.id"),
	 	})
@Table(indexes = { @Index(name = "IDX_CategoryMst", columnList = "tenantId,categoryCode,isActive") },
uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","categoryCode"}))

public class CategoryMst extends Base{

@GeneratedValue(strategy = GenerationType.AUTO)
private String categoryCode;
private String categoryName;

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String cmmiVersion="";

public String getCategoryName() {
	return categoryName;
}

public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
}

public String getCategoryCode() {
	if (null != categoryCode) {
	} else {
		categoryCode = initvar(categoryCode);
	}
	return categoryCode;
}

public void setCategoryCode(String categoryCode) {
	this.categoryCode = categoryCode;
}

public String getCmmiVersion() {
	if (null == cmmiVersion) {
		return initvar(cmmiVersion);
	} else {
		return cmmiVersion;
	}
}

public void setCmmiVersion(String cmmiVersion) {
	this.cmmiVersion = cmmiVersion;
}


}
