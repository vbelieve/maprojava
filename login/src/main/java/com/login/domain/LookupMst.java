package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "lookupMst.getAll", query = "select h FROM LookupMst h  WHERE h.tenantId=:tenantId order by h.id"),
	@NamedQuery(name = "lookupMst.getAllByTenant", query = "select h FROM LookupMst h  WHERE h.tenantId=:tenantId and h.isActive=1 order by h.id"),
	@NamedQuery(name = "lookupMst.getLookupByLookupType", query = "select h FROM LookupMst h  WHERE h.tenantId=:tenantId and h.lookupType=:lookupType order by h.id"),
	@NamedQuery(name = "lookupMst.getLookupByLookupTypeAndValue", query = "select h FROM LookupMst h  WHERE h.tenantId=:tenantId and h.lookupType=:lookupType and h.lookupValue=:lookupValue order by h.id"),
	
	 	})
@Table(indexes = { @Index(name = "IDX_LookupMst", columnList = "tenantId,lookupType,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","lookupType"}))

public class LookupMst extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String lookupType;

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String displayValue;

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String lookupValue;

public String getLookupType() {
	if (null != lookupType) {
	} else {
		lookupType = initvar(lookupType);
	}
	return lookupType;
}

public void setLookupType(String lookupType) {
	this.lookupType = lookupType;
}

public String getDisplayValue() {
	if (null != displayValue) {
	} else {
		displayValue = initvar(displayValue);
	}
	return displayValue;
}

public void setDisplayValue(String displayValue) {
	this.displayValue = displayValue;
}

public String getLookupValue() {
	if (null != lookupValue) {
	} else {
		lookupValue = initvar(lookupValue);
	}
	return lookupValue;
}

public void setLookupValue(String lookupValue) {
	this.lookupValue = lookupValue;
}





}
