package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	
	@NamedQuery(name = "piidMst.getByProjectPracticeArea", query = "select h FROM PiidMst h where h.tenantId=:tenantId and  h.project=:project and h.practiceAreaCode=:practiceAreaCode and h.isActive=1"),
	@NamedQuery(name = "piidMst.getByTenantAndProject", query = "select h FROM PiidMst h where h.tenantId=:tenantId and  h.project=:project and h.isActive=1"),
	
	
	@NamedQuery(name = "piidMst.getUnique", query = "select h FROM PiidMst h where h.isActive=1 and  h.tenantId=:tenantId "
			+ "and h.practiceAreaCode=:practiceAreaCode and h.practiceCode=:practiceCode and h.artifect=:artifect and h.project=:project order by h.id"),
	 	})

@Table(indexes = { @Index(name = "IDX_PiidMst", columnList = "tenantId,piidCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","piidCode"}))
public class PiidMst extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
public String piidCode="";

@Column(columnDefinition = "nvarchar(2000)", nullable = false)
public String level="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String practiceAreaCode="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String practiceCode="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String practiceDescription="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String artifect="";


@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String reference="";

@Column(columnDefinition = "nvarchar(100)", nullable = true)
public String type="";

@Column(columnDefinition = "nvarchar(2000)", nullable = true)
public String atmAffirmation="";

public String getPiidCode() {
	return piidCode;
}

public void setPiidCode(String piidCode) {
	this.piidCode = piidCode;
}

public String getLevel() {
	return level;
}

public void setLevel(String level) {
	this.level = level;
}

public String getPracticeAreaCode() {
	return practiceAreaCode;
}

public void setPracticeAreaCode(String practiceAreaCode) {
	this.practiceAreaCode = practiceAreaCode;
}

public String getPracticeCode() {
	return practiceCode;
}

public void setPracticeCode(String practiceCode) {
	this.practiceCode = practiceCode;
}

public String getPracticeDescription() {
	return practiceDescription;
}

public void setPracticeDescription(String practiceDescription) {
	this.practiceDescription = practiceDescription;
}

public String getArtifect() {
	return artifect;
}

public void setArtifect(String artifect) {
	this.artifect = artifect;
}

public String getReference() {
	return reference;
}

public void setReference(String reference) {
	this.reference = reference;
}

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

public String getAtmAffirmation() {
	return atmAffirmation;
}

public void setAtmAffirmation(String atmAffirmation) {
	this.atmAffirmation = atmAffirmation;
}


}
