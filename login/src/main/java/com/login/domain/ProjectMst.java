package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "projectMst.getAll", query = "select h FROM ProjectMst h where h.isActive=1 and h.tenantId=:tenantId order by h.id"),
	@NamedQuery(name = "projectMst.getAllActiveProjects", query = "select h FROM ProjectMst h where h.isActive=1 order by h.id"),	
	@NamedQuery(name = "projectMst.getByTenantAndProcess", query = "select h FROM ProjectMst h where h.isActive=1 and h.tenantId=:tenantId and h.process=:process order by h.id"),
	@NamedQuery(name = "projectMst.getByName", query = "select h FROM ProjectMst h WHERE h.tenantId=:tenantId and h.projectName=:projectName and h.isActive=1 order by h.id"),
	@NamedQuery(name = "projectMst.getProjectByProjectCode", query = "select h FROM ProjectMst h WHERE h.projectCode=:projectCode and h.isActive=1 order by h.id"),
	@NamedQuery(name = "projectMst.getUnique", query = "select h FROM ProjectMst h WHERE h.tenantId=:tenantId and h.businessUnitCode=:businessUnitCode and h.projectCode=:projectCode and h.isActive=1 order by h.id"),
	
	 	})
@Table(indexes = { @Index(name = "IDX_ProjectMst", columnList = "tenantId,projectCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","projectCode","businessUnitCode"}))
public class ProjectMst extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String projectCode;

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String organizationModel;


@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String projectName;

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String businessUnitCode;

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String cmmiVersion="";


public String getProjectCode() {
	if (null != projectCode) {
	} else {
		projectCode = initvar(projectCode);
	}
	return projectCode;
}
public void setProjectCode(String projectCode) {
	this.projectCode = projectCode;
}
public String getProjectName() {
	if (null != projectName) {
	} else {
		projectName = initvar(projectName);
	}
	return projectName;
}
public void setProjectName(String projectName) {
	this.projectName = projectName;
}
public String getBusinessUnitCode() {
	if (null != businessUnitCode) {
	} else {
		businessUnitCode = initvar(businessUnitCode);
	}
	return businessUnitCode;
}
public void setBusinessUnitCode(String businessUnitCode) {
	this.businessUnitCode = businessUnitCode;
}
public String getOrganizationModel() {
	if (null != organizationModel) {
	} else {
		organizationModel = initvar(organizationModel);
	}
	return organizationModel;
}
public void setOrganizationModel(String organizationModel) {
	this.organizationModel = organizationModel;
}
public String getCmmiVersion() {
	if (null == cmmiVersion) {
		return initvar(cmmiVersion);
	} else {
		return cmmiVersion;
	}
}
public void setCmmiVersion(String cmmiVersion) {
	this.cmmiVersion = cmmiVersion;
}



}
