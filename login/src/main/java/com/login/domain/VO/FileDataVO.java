package com.login.domain.VO;

import java.io.Serializable;

public class FileDataVO implements Serializable{

	private static final long serialVersionUID = 1L;

	private String projectCode="";
	private String category="";
	private String capabilityArea="";
	private String practiceArea="";
	private String practiceGroup="";
	private String practice="";
	private String practiceDetails="";
	private String value="";
	private String exampleActivities="";
	private String exampleWorkProduct="";
	private String currentPractice="";
	private String compliance="";
	private String gaps="";
	private String artifect="";
	
	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCapabilityArea() {
		return capabilityArea;
	}
	public void setCapabilityArea(String capabilityArea) {
		this.capabilityArea = capabilityArea;
	}
	public String getPracticeArea() {
		return practiceArea;
	}
	public void setPracticeArea(String practiceArea) {
		this.practiceArea = practiceArea;
	}
	public String getPracticeGroup() {
		return practiceGroup;
	}
	public void setPracticeGroup(String practiceGroup) {
		this.practiceGroup = practiceGroup;
	}
	public String getPractice() {
		return practice;
	}
	public void setPractice(String practice) {
		this.practice = practice;
	}
	public String getPracticeDetails() {
		return practiceDetails;
	}
	public void setPracticeDetails(String practiceDetails) {
		this.practiceDetails = practiceDetails;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getExampleActivities() {
		return exampleActivities;
	}
	public void setExampleActivities(String exampleActivities) {
		this.exampleActivities = exampleActivities;
	}
	public String getExampleWorkProduct() {
		return exampleWorkProduct;
	}
	public void setExampleWorkProduct(String exampleWorkProduct) {
		this.exampleWorkProduct = exampleWorkProduct;
	}
	public String getCurrentPractice() {
		return currentPractice;
	}
	public void setCurrentPractice(String currentPractice) {
		this.currentPractice = currentPractice;
	}
	public String getCompliance() {
		return compliance;
	}
	public void setCompliance(String compliance) {
		this.compliance = compliance;
	}
	public String getGaps() {
		return gaps;
	}
	public void setGaps(String gaps) {
		this.gaps = gaps;
	}
	public String getArtifect() {
		return artifect;
	}
	public void setArtifect(String artifect) {
		this.artifect = artifect;
	}

	
}
