package com.login.domain.VO;

import java.io.Serializable;

public class FileUploadRequestVO  implements Serializable{

	private static final long serialVersionUID = 1L;

	private String requestId = "";
	private String fileLocation = "";
	private String tenantId = "";
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getFileLocation() {
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	

	
}
