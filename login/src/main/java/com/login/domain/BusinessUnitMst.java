package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "businessUnitMst.getAll", query = "select h FROM BusinessUnitMst h where h.isActive=1 and h.tenantId=:tenantId order by h.id"),
	@NamedQuery(name = "businessUnitMst.getAllActiveBusinessUnit", query = "select h FROM BusinessUnitMst h where h.isActive=1 order by h.tenantId"),
	@NamedQuery(name = "businessUnitMst.getByTenantAndProcess", query = "select h FROM BusinessUnitMst h where h.isActive=1 and h.tenantId=:tenantId and h.process=:process order by h.id"),
	@NamedQuery(name = "businessUnitMst.getByName", query = "select h FROM BusinessUnitMst h WHERE h.tenantId=:tenantId and h.businessUnitName=:businessUnitName and h.isActive=1 order by h.id"),
	@NamedQuery(name = "businessUnitMst.getUnique", query = "select h FROM BusinessUnitMst h WHERE h.tenantId=:tenantId and h.businessUnitCode=:businessUnitCode and h.isActive=1 order by h.id"),
	
	 	})
@Table(indexes = { @Index(name = "IDX_BusinessUnitMst", columnList = "tenantId,businessUnitCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","businessUnitCode"}))
public class BusinessUnitMst extends Base{

@GeneratedValue(strategy = GenerationType.AUTO)
@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String businessUnitCode;

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String businessUnitName;



public String getBusinessUnitCode() {
	if (null != businessUnitCode) {
	} else {
		businessUnitCode = initvar(businessUnitCode);
	}
	return businessUnitCode;
}
public void setBusinessUnitCode(String businessUnitCode) {
	this.businessUnitCode = businessUnitCode;
}
public String getBusinessUnitName() {
	if (null != businessUnitName) {
	} else {
		businessUnitName = initvar(businessUnitName);
	}
	return businessUnitName;
}
public void setBusinessUnitName(String businessUnitName) {
	this.businessUnitName = businessUnitName;
}



}
