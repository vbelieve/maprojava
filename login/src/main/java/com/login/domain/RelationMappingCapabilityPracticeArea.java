package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "relationMappingCapabilityPracticeArea.getAll", query = "select h FROM RelationMappingCapabilityPracticeArea h WHERE h.tenantId=:tenantId and h.cmmiVersion=:cmmiVersion and h.isActive=1 order by h.id"),
	@NamedQuery(name = "relationMappingCapabilityPracticeArea.getByTenantAndProcess", query = "select h FROM RelationMappingCapabilityPracticeArea h where h.isActive=1  and h.cmmiVersion=:cmmiVersion and h.tenantId=:tenantId and h.process=:process order by h.id"),
	@NamedQuery(name = "relationMappingCapabilityPracticeArea.getByTenantAndProject", query = "select h FROM RelationMappingCapabilityPracticeArea h where h.isActive=1  and h.cmmiVersion=:cmmiVersion and h.project=:project order by h.id"),
	@NamedQuery(name = "relationMappingCapabilityPracticeArea.getPracticeArea", query = "select h FROM RelationMappingCapabilityPracticeArea h WHERE h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.practiceAreaCode=:practiceAreaCode and h.isActive=1 order by h.id"),
	@NamedQuery(name = "relationMappingCapabilityPracticeArea.getCapability", query = "select h FROM RelationMappingCapabilityPracticeArea h WHERE h.organizationUnit=:organizationUnit  and h.cmmiVersion=:cmmiVersion and h.businessUnit=:businessUnit and h.project=:project and  h.tenantId=:tenantId and h.capabilityCode=:capabilityCode and h.isActive=1 order by h.id"),
	@NamedQuery(name = "relationMappingCapabilityPracticeArea.getUnique", query = "select h FROM RelationMappingCapabilityPracticeArea h WHERE h.organizationUnit=:organizationUnit  and h.cmmiVersion=:cmmiVersion and h.businessUnit=:businessUnit and h.project=:project and  h.tenantId=:tenantId and h.capabilityCode=:capabilityCode and h.practiceAreaCode=:practiceAreaCode and h.isActive=1 order by h.id"),	
})

@Table(indexes = { @Index(name = "IDX_RelationMappingCapabilityPracticeArea", columnList = "tenantId,capabilityCode,practiceAreaCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","capabilityCode","practiceAreaCode"}))
public class RelationMappingCapabilityPracticeArea extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String capabilityCode="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String practiceAreaCode="";

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String cmmiVersion="";

public String getCapabilityCode() {
	if (null != capabilityCode) {
	} else {
		capabilityCode = initvar(capabilityCode);
	}
	return capabilityCode;
}

public void setCapabilityCode(String capabilityCode) {
	this.capabilityCode = capabilityCode;
}

public String getPracticeAreaCode() {
	if (null != practiceAreaCode) {
	} else {
		practiceAreaCode = initvar(practiceAreaCode);
	}
	return practiceAreaCode;
}

public void setPracticeAreaCode(String practiceAreaCode) {
	this.practiceAreaCode = practiceAreaCode;
}

public String getCmmiVersion() {
	if (null == cmmiVersion) {
		return initvar(cmmiVersion);
	} else {
		return cmmiVersion;
	}
}

public void setCmmiVersion(String cmmiVersion) {
	this.cmmiVersion = cmmiVersion;
}


}
