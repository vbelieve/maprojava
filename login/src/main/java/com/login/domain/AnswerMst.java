package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "answerMst.getAll", query = "select h FROM AnswerMst h where h.tenantId=:tenantId and h.authStatus=:authStatus order by h.id"),
	@NamedQuery(name = "answerMst.getUnique", query = "select h FROM AnswerMst h WHERE h.tenantId=:tenantId and h.answerId=:answerId order by h.id"),

//	@NamedQuery(name = "answerMst.getAllActiveProjects", query = "select h FROM ProjectMst h where h.isActive=1 order by h.id"),	
//	@NamedQuery(name = "projectMst.getByTenantAndProcess", query = "select h FROM ProjectMst h where h.isActive=1 and h.tenantId=:tenantId and h.process=:process order by h.id"),
//	@NamedQuery(name = "projectMst.getByName", query = "select h FROM ProjectMst h WHERE h.tenantId=:tenantId and h.projectName=:projectName and h.isActive=1 order by h.id"),
//	@NamedQuery(name = "projectMst.getProjectByProjectCode", query = "select h FROM ProjectMst h WHERE h.projectCode=:projectCode and h.isActive=1 order by h.id"),
//	@NamedQuery(name = "projectMst.getUnique", query = "select h FROM ProjectMst h WHERE h.tenantId=:tenantId and h.businessUnitCode=:businessUnitCode and h.projectCode=:projectCode and h.isActive=1 order by h.id"),
	
	 	})
//@Table(indexes = { @Index(name = "IDX_ProjectMst", columnList = "tenantId,projectCode,isActive") },
//		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","projectCode","businessUnitCode"}))
public class AnswerMst extends Base{
	
@Column(nullable = false)
private Long answerId=0L;

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String answereDescription;

@Column(nullable = false)
private Integer score=0;


@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String answereGroup;



public Long getAnswerId() {
	if (null == answerId) {
		return initvar(answerId);
	} else {
		return answerId;
	}
}


public void setAnswerId(Long answerId) {
	this.answerId = answerId;
}


public String getAnswereDescription() {
	if (null == answereDescription) {
		return initvar(answereDescription);
	} else {
		return answereDescription;
	}
}


public void setAnswereDescription(String answereDescription) {
	this.answereDescription = answereDescription;
}



public Integer getScore() {
	if (null == score) {
		return initvar(score);
	} else {
		return score;
	}
}


public void setScore(Integer score) {
	this.score = score;
}


public String getAnswereGroup() {
	if (null == answereGroup) {
		return initvar(answereGroup);
	} else {
		return answereGroup;
	}
}


public void setAnswereGroup(String answereGroup) {
	this.answereGroup = answereGroup;
}



}
