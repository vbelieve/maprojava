package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.login.domain.basics.Base;

@Entity
@NamedQueries({
	@NamedQuery(name = "sequenceMst.getAll", query = "select h FROM SequenceMst h WHERE h.tenantId=:tenantId and h.isActive=1"
			+ " order by h.id"),
	 @NamedQuery(name = "sequenceMst.getSequenceBySequenceType", query = "select h FROM SequenceMst h "
					+ " WHERE h.tenantId=:tenantId and h.sequenceType=:sequenceType order by h.id"),
	 	})
@Table(indexes = { @Index(name = "IDX_SequenceMst", columnList = "tenantId,sequenceType,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","sequenceType"}))

public class SequenceMst extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String sequenceType;

@Column(columnDefinition = "nvarchar(100)", nullable = true)
private String description;

@Column(nullable = true)
private Integer startNo;

@Column(nullable = true)
private Integer endNo;

@Column(nullable = true)
private Integer nextSeqNo;

public String getSequenceType() {
	return sequenceType;
}

public void setSequenceType(String sequenceType) {
	this.sequenceType = sequenceType;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public Integer getStartNo() {
	return startNo;
}

public void setStartNo(Integer startNo) {
	this.startNo = startNo;
}

public Integer getEndNo() {
	return endNo;
}

public void setEndNo(Integer endNo) {
	this.endNo = endNo;
}

public Integer getNextSeqNo() {
	return nextSeqNo;
}

public void setNextSeqNo(Integer nextSeqNo) {
	this.nextSeqNo = nextSeqNo;
}







}
