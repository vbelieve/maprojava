package com.login.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;

import com.login.domain.basics.Base;

@Entity
@Audited(auditParents = { Base.class })
@NamedQueries({
	@NamedQuery(name = "relationMappingPracticeAreaPractice.getAll", query = "select h FROM RelationMappingPracticeAreaPractice h WHERE h.tenantId=:tenantId and h.cmmiVersion=:cmmiVersion and h.isActive=1 order by h.id"),
	@NamedQuery(name = "relationMappingPracticeAreaPractice.getByTenantAndProcess", query = "select h FROM RelationMappingPracticeAreaPractice h where h.isActive=1  and h.cmmiVersion=:cmmiVersion and h.tenantId=:tenantId and h.process=:process order by h.id"),
	@NamedQuery(name = "relationMappingPracticeAreaPractice.getByTenantAndProject", query = "select h FROM RelationMappingPracticeAreaPractice h where h.isActive=1  and h.cmmiVersion=:cmmiVersion and h.project=:project order by h.id"),
	@NamedQuery(name = "relationMappingPracticeAreaPractice.getPracticeArea", query = "select h FROM RelationMappingPracticeAreaPractice h WHERE h.organizationUnit=:organizationUnit  and h.cmmiVersion=:cmmiVersion and h.businessUnit=:businessUnit and h.project=:project and  h.tenantId=:tenantId and h.practiceAreaCode=:practiceAreaCode and h.isActive=1 order by h.id"),
	@NamedQuery(name = "relationMappingPracticeAreaPractice.getPractice", query = "select h FROM RelationMappingPracticeAreaPractice h WHERE h.tenantId=:tenantId and h.practiceCode=:practiceCode  and h.cmmiVersion=:cmmiVersion and h.isActive=1 order by h.id"),
	@NamedQuery(name = "relationMappingPracticeAreaPractice.getByProjectAndPractice", query = "select h FROM RelationMappingPracticeAreaPractice h WHERE h.tenantId=:tenantId and h.project=:project  and h.cmmiVersion=:cmmiVersion and h.practiceCode=:practiceCode and h.isActive=1"),
	@NamedQuery(name = "relationMappingPracticeAreaPractice.getPracticeByLevel", query = "select h FROM RelationMappingPracticeAreaPractice h WHERE h.tenantId=:tenantId  and h.cmmiVersion=:cmmiVersion and h.practiceLevelCode=:practiceLevelCode and h.isActive=1 order by h.id"),
	@NamedQuery(name = "relationMappingPracticeAreaPractice.getUnique", query = "select h FROM RelationMappingPracticeAreaPractice h WHERE h.organizationUnit=:organizationUnit  and h.cmmiVersion=:cmmiVersion and h.businessUnit=:businessUnit and h.project=:project and  h.tenantId=:tenantId and h.practiceAreaCode=:practiceAreaCode and h.practiceCode=:practiceCode  and h.isActive=1 order by h.id"),	
})

@Table(indexes = { @Index(name = "IDX_RelationMappingPracticeAreaPractice", columnList = "tenantId,practiceCode,practiceAreaCode,isActive") },
		uniqueConstraints = @UniqueConstraint(columnNames = { "id","tenantId","practiceAreaCode","practiceCode","practiceLevelCode"}))
public class RelationMappingPracticeAreaPractice extends Base{

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String practiceCode="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String practiceAreaCode="";

@Column(columnDefinition = "nvarchar(100)", nullable = false)
private String practiceLevelCode="";


@Column(nullable = true)
private Integer practiceLevel=0;

@Column(columnDefinition = "nvarchar(10)", nullable = true)
private String cmmiVersion="";


public String getPracticeAreaCode() {
	if (null != practiceAreaCode) {
	} else {
		practiceAreaCode = initvar(practiceAreaCode);
	}
	return practiceAreaCode;
}

public void setPracticeAreaCode(String practiceAreaCode) {
	this.practiceAreaCode = practiceAreaCode;
}

public String getPracticeCode() {
	if (null != practiceCode) {
	} else {
		practiceCode = initvar(practiceCode);
	}
	return practiceCode;
}

public void setPracticeCode(String practiceCode) {
	this.practiceCode = practiceCode;
}

public String getPracticeLevelCode() {
	if (null != practiceLevelCode) {
	} else {
		practiceLevelCode = initvar(practiceLevelCode);
	}
	return practiceLevelCode;
}

public void setPracticeLevelCode(String practiceLevelCode) {
	this.practiceLevelCode = practiceLevelCode;
}

public Integer getPracticeLevel() {
	if (null != practiceLevel) {
	} else {
		practiceLevel = initvar(practiceLevel);
	}
	return practiceLevel;
}

public void setPracticeLevel(Integer practiceLevel) {
	this.practiceLevel = practiceLevel;
}

public String getCmmiVersion() {
	if (null == cmmiVersion) {
		return initvar(cmmiVersion);
	} else {
		return cmmiVersion;
	}
}

public void setCmmiVersion(String cmmiVersion) {
	this.cmmiVersion = cmmiVersion;
}


}
