package com.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;







@EnableJpaAuditing
@SpringBootApplication


public class LoginApplication extends SpringBootServletInitializer{
	public static void main(String[] args) {
		SpringApplication.run(LoginApplication.class, args);
	}
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	      return application.sources(LoginApplication.class);
	   }

//	protected SpringApplicationBuilder configure(SpringApplicationBuilder applicationBuilder)
//	{
//		return applicationBuilder.sources(LoginApplication.class);
//	}
}
