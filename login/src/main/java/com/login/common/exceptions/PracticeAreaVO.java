package com.login.common.exceptions;

import com.login.domain.basics.Base;

public class PracticeAreaVO extends Base {

	private String practiceAreaCode="";
	private String practiceAreaName="";
	private String practiceAreaAdditionalInfo="";
	private String practiceAreaIntent="";
	private String practiceAreaValue="";
	private String businessModel="";
	public String getPracticeAreaCode() {
		if (null == practiceAreaCode) {
			return initvar(practiceAreaCode);
		} else {
			return practiceAreaCode;
		}
	}
	public void setPracticeAreaCode(String practiceAreaCode) {
		this.practiceAreaCode = practiceAreaCode;
	}
	public String getPracticeAreaName() {
		if (null == practiceAreaName) {
			return initvar(practiceAreaName);
		} else {
			return practiceAreaName;
		}
	}
	public void setPracticeAreaName(String practiceAreaName) {
		this.practiceAreaName = practiceAreaName;
	}
	public String getPracticeAreaAdditionalInfo() {
		if (null == practiceAreaAdditionalInfo) {
			return initvar(practiceAreaAdditionalInfo);
		} else {
			return practiceAreaAdditionalInfo;
		}
	}
	public void setPracticeAreaAdditionalInfo(String practiceAreaAdditionalInfo) {
		this.practiceAreaAdditionalInfo = practiceAreaAdditionalInfo;
	}
	public String getPracticeAreaIntent() {
		if (null == practiceAreaIntent) {
			return initvar(practiceAreaIntent);
		} else {
			return practiceAreaIntent;
		}
	}
	public void setPracticeAreaIntent(String practiceAreaIntent) {
		this.practiceAreaIntent = practiceAreaIntent;
	}
	public String getPracticeAreaValue() {
		if (null == practiceAreaValue) {
			return initvar(practiceAreaValue);
		} else {
			return practiceAreaValue;
		}
	}
	public void setPracticeAreaValue(String practiceAreaValue) {
		this.practiceAreaValue = practiceAreaValue;
	}
	public String getBusinessModel() {
		if (null == businessModel) {
			return initvar(businessModel);
		} else {
			return businessModel;
		}
	}
	public void setBusinessModel(String businessModel) {
		this.businessModel = businessModel;
	}
	
	
	
	
	
}
