package com.login.common.exceptions;

import com.login.domain.basics.Base;

public class RelationshipPracticeAreaPracticeVO extends Base {

	private String practiceAreaCode="";
	private String practiceCode="";
	private String practiceLevelCode="";
	private String practiceLevel="";
	
	public String getPracticeAreaCode() {
		if (null == practiceAreaCode) {
			return initvar(practiceAreaCode);
		} else {
			return practiceAreaCode;
		}
	}
	public void setPracticeAreaCode(String practiceAreaCode) {
		this.practiceAreaCode = practiceAreaCode;
	}
	public String getPracticeCode() {
		if (null == practiceCode) {
			return initvar(practiceCode);
		} else {
			return practiceCode;
		}
	}
	public void setPracticeCode(String practiceCode) {
		this.practiceCode = practiceCode;
	}
	public String getPracticeLevelCode() {
		if (null == practiceLevelCode) {
			return initvar(practiceLevelCode);
		} else {
			return practiceLevelCode;
		}
	}
	public void setPracticeLevelCode(String practiceLevelCode) {
		this.practiceLevelCode = practiceLevelCode;
	}
	public String getPracticeLevel() {
		if (null == practiceLevel) {
			return initvar(practiceLevel);
		} else {
			return practiceLevel;
		}
	}
	public void setPracticeLevel(String practiceLevel) {
		this.practiceLevel = practiceLevel;
	}
	
	
	
}
