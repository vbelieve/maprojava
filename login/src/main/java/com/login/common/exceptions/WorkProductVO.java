package com.login.common.exceptions;

import com.login.domain.basics.Base;

public class WorkProductVO extends Base {

	private String workProductId="";
	private String workProductName="";
	public String getWorkProductId() {
		if (null == workProductId) {
			return initvar(workProductId);
		} else {
			return workProductId;
		}
	}
	public void setWorkProductId(String workProductId) {
		this.workProductId = workProductId;
	}
	public String getWorkProductName() {
		if (null == workProductName) {
			return initvar(workProductName);
		} else {
			return workProductName;
		}
	}
	public void setWorkProductName(String workProductName) {
		this.workProductName = workProductName;
	}

	
		
	
	
	
}
