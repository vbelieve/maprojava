package com.login.common.exceptions;

import com.login.domain.basics.Base;

public class RelationshipPracticeWorkProductVO extends Base {

	private String practiceCode="";
	private String workProduct="";
	public String getPracticeCode() {
		if (null == practiceCode) {
			return initvar(practiceCode);
		} else {
			return practiceCode;
		}
	}
	public void setPracticeCode(String practiceCode) {
		this.practiceCode = practiceCode;
	}
	public String getWorkProduct() {
		if (null == workProduct) {
			return initvar(workProduct);
		} else {
			return workProduct;
		}
	}
	public void setWorkProduct(String workProduct) {
		this.workProduct = workProduct;
	}
	
	
	
}
