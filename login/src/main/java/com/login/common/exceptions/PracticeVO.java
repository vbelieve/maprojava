package com.login.common.exceptions;

import com.login.domain.basics.Base;

public class PracticeVO extends Base {

	private String practiceCode="";
	private String practiceName="";
	private String practiceDescription="";
	private String practiceValue="";
	private String practiceExamples="";
	private Integer practiceLevelCode=0;
	public String getPracticeCode() {
		if (null == practiceCode) {
			return initvar(practiceCode);
		} else {
			return practiceCode;
		}
	}
	public void setPracticeCode(String practiceCode) {
		this.practiceCode = practiceCode;
	}
	public String getPracticeName() {
		if (null == practiceName) {
			return initvar(practiceName);
		} else {
			return practiceName;
		}
	}
	public void setPracticeName(String practiceName) {
		this.practiceName = practiceName;
	}
	public String getPracticeDescription() {
		if (null == practiceDescription) {
			return initvar(practiceDescription);
		} else {
			return practiceDescription;
		}
	}
	public void setPracticeDescription(String practiceDescription) {
		this.practiceDescription = practiceDescription;
	}
	public String getPracticeValue() {
		if (null == practiceValue) {
			return initvar(practiceValue);
		} else {
			return practiceValue;
		}
	}
	public void setPracticeValue(String practiceValue) {
		this.practiceValue = practiceValue;
	}
	public String getPracticeExamples() {
		if (null == practiceExamples) {
			return initvar(practiceExamples);
		} else {
			return practiceExamples;
		}
	}
	public void setPracticeExamples(String practiceExamples) {
		this.practiceExamples = practiceExamples;
	}
	public Integer getPracticeLevelCode() {
		if (null == practiceLevelCode) {
			return initvar(practiceLevelCode);
		} else {
			return practiceLevelCode;
		}
	}
	public void setPracticeLevelCode(Integer practiceLevelCode) {
		this.practiceLevelCode = practiceLevelCode;
	}

	
}
