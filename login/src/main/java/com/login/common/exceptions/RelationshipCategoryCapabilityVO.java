package com.login.common.exceptions;

import com.login.domain.basics.Base;

public class RelationshipCategoryCapabilityVO extends Base {

	private String categoryCode="";
	private String capabilityCode="";
	public String getCategoryCode() {
		if (null == categoryCode) {
			return initvar(categoryCode);
		} else {
			return categoryCode;
		}
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCapabilityCode() {
		if (null == capabilityCode) {
			return initvar(capabilityCode);
		} else {
			return capabilityCode;
		}
	}
	public void setCapabilityCode(String capabilityCode) {
		this.capabilityCode = capabilityCode;
	}

	
	
}
