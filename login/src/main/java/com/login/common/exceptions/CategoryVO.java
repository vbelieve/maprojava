package com.login.common.exceptions;

import com.login.domain.basics.Base;

public class CategoryVO extends Base {

	private String categoryCode="";
	private String categoryName="";
	
	public String getCategoryCode() {
		if (null == categoryCode) {
			return initvar(categoryCode);
		} else {
			return categoryCode;
		}
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		if (null == categoryName) {
			return initvar(categoryName);
		} else {
			return categoryName;
		}
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
	
	
}
