package com.login.common.exceptions;

import com.login.domain.basics.Base;

public class RelationshipCapabilityPracticeAreaVO extends Base {

	private String capabilityCode="";
	private String practiceAreaCode="";
	public String getCapabilityCode() {
		if (null == capabilityCode) {
			return initvar(capabilityCode);
		} else {
			return capabilityCode;
		}
	}
	public void setCapabilityCode(String capabilityCode) {
		this.capabilityCode = capabilityCode;
	}
	public String getPracticeAreaCode() {
		if (null == practiceAreaCode) {
			return initvar(practiceAreaCode);
		} else {
			return practiceAreaCode;
		}
	}
	public void setPracticeAreaCode(String practiceAreaCode) {
		this.practiceAreaCode = practiceAreaCode;
	}
	
	
		
	
	
	
}
