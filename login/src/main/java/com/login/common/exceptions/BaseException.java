package com.login.common.exceptions;

public class BaseException extends RuntimeException  {


	private static final long serialVersionUID = 1L;

	private String error;

	private Throwable t;

	public BaseException() {
	}

	public BaseException(String err) {
		this.error = err;
	}

	public BaseException(String err, Throwable t) {
		this.error = err;
		this.t = t;
	}

	public String getError() {
		return error;
	}


}
