package com.login.common.exceptions;

import com.login.domain.basics.Base;

public class CapabilityVO extends Base {

	private String capabilityCode="";
	private String capabilityName="";
	
	public String getCapabilityCode() {
		if (null == capabilityCode) {
			return initvar(capabilityCode);
		} else {
			return capabilityCode;
		}
	}
	public void setCapabilityCode(String capabilityCode) {
		this.capabilityCode = capabilityCode;
	}
	public String getCapabilityName() {
		if (null == capabilityName) {
			return initvar(capabilityName);
		} else {
			return capabilityName;
		}
	}
	public void setCapabilityName(String capabilityName) {
		this.capabilityName = capabilityName;
	}
	
	
	
	
	
}
