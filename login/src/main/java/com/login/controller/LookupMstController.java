package com.login.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.ApplicationConstants;
import com.login.domain.LookupMst;
import com.login.domain.WorkProductMst;
import com.login.domain.basics.AccessObject;
import com.login.services.LookupMstService;

@RequestMapping("/lookup")
//@CrossOrigin(origins=GlobalConstants.angularPort)

@RestController
public class LookupMstController {
	
	@Autowired
	LookupMstService service;
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(LookupMst object,HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		service.saveOrUpDate(object,accessObject);
		accessObject.setMsg("SUCCESS~Lookup Details Created / Updated Successfully.");
		return new ModelAndView(
	    	       new RedirectView("/lookupMstCrud", true),
	    	       model);
	}
	
//	@PostMapping("/save")
//	public LookupMst save(@RequestBody LookupMst object)
//	{
//		AccessObject accessObject=new AccessObject();
//		object.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
//		object.setIsActive(1);
//		object.setTenantId(accessObject.getTenantId());
//		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase(""))
//		{
//			object.setTenantId(ApplicationConstants.DEFAULT_TENANT);
//		}
//		return service.saveOrUpDate(object);
//			
//	}
	
	@PutMapping("/update")
	public LookupMst update(@RequestBody LookupMst LookupMst)
	{
		return service.Save(LookupMst);
	}
	
	@GetMapping("/all")
	public List<LookupMst> getall(){
		return service.getAllList();
	}

	@GetMapping("/allLookupType")
	public Map<String,String> getAllLookupType(){
		return service.getAllLookupType(ApplicationConstants.DEFAULT_TENANT);
	}
	
	@GetMapping("/{tenantId}/{lookupType}")
	public List<LookupMst> getByName(@PathVariable("tenantId")String tenantId, @PathVariable("lookupType") String lookupType){
		List<LookupMst> lookupMst=new ArrayList<LookupMst>();
		lookupMst=service.getLookupMst(tenantId, lookupType);
		return lookupMst; 
	}
	
	@GetMapping("/getAll/{tenantId}")
	public List<LookupMst> getByName(@PathVariable("tenantId")String tenantId){
		List<LookupMst> lookupMst=new ArrayList<LookupMst>();
		lookupMst=service.getAllByTenant(tenantId);
		return lookupMst; 
	}
	
	public List<LookupMst> getAllActive(){
		return null;
	}
	
	
	
	
}
