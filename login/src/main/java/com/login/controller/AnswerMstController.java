package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.CategoryVO;
import com.login.common.exceptions.PracticeVO;
import com.login.domain.AnswerMst;
import com.login.domain.ApplicationConstants;
import com.login.domain.CategoryMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.TenantMst;
import com.login.domain.TestMst;
import com.login.domain.basics.AccessObject;
import com.login.services.AnswerMstService;
import com.login.services.CategoryMstService;
import com.login.services.PracticeAreaMstService;

@RequestMapping("/answerMst")
@RestController
public class AnswerMstController {
    private static final Logger logger = LoggerFactory.getLogger(AnswerMstController.class);
	
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AnswerMstService answerMstService;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

	private AccessObject getAccessObjectApi() {
		AccessObject accessObject=new AccessObject();
		accessObject.setLoginId("API");
		return accessObject;
	}
	
	@RequestMapping(value = "/getAll", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<AnswerMst> getAll() {
		String tenantId = "";
		tenantId= request.getParameter("tenantId");
		
		String authStatus = "";
		authStatus = request.getParameter("authStatus");
		List<AnswerMst> answerList=null;
		answerList=answerMstService.getAll(tenantId,authStatus);
		
		
		if(null!=answerList && answerList.size()>0)
		{
			
			return answerList;
		}
		
		return answerList;
	}
	
	@RequestMapping(value="/saveObject",method=RequestMethod.POST)
	public String saveObject(HttpServletRequest request)
	{
		
		String tenantId = "";
		String answerId = "";
		String answereDescription = "";
		String score = "";
		String answereGroup = "";
		
		tenantId= request.getParameter("tenantId");
		answerId = request.getParameter("answerId");
		Long answerIdLong = Long.parseLong(answerId);
		answereDescription= request.getParameter("answereDescription");
		score= request.getParameter("score");
		Integer scoreInt=Integer.parseInt(score);	
		answereGroup= request.getParameter("answereGroup");
		
		
		AnswerMst answerMst = new AnswerMst();
		answerMst.setTenantId(tenantId);
		answerMst.setAnswerId(answerIdLong);
		answerMst.setAnswereDescription(answereDescription);
		answerMst.setScore(scoreInt);
		answerMst.setAnswereGroup(answereGroup);
		answerMst=answerMstService.saveOrUpDate(answerMst, getAccessObjectApi());
		return "Success";
	}
	
	
}
