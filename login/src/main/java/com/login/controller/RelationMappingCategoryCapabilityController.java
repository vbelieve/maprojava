package com.login.controller;

import com.login.services.RelationMappingCategoryCapabilityService;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.RelationshipCategoryCapabilityVO;
import com.login.common.exceptions.WorkProductVO;
import com.login.domain.ApplicationConstants;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.WorkProductMst;
import com.login.domain.basics.AccessObject;

@RequestMapping("/relationMappingCategoryCapability")
//@CrossOrigin(origins="http://localhost:4200")
@RestController
public class RelationMappingCategoryCapabilityController {
	
	@Autowired
	RelationMappingCategoryCapabilityService service;
	
	@Autowired
	private HttpServletRequest request;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

	
//	@PostMapping("/save")
//	public RelationMappingCategoryCapability save(@RequestBody RelationMappingCategoryCapability object)
//	{
//		
//		return service.saveOrUpDate(object);
//			
//	}
	
	public String validateCategory(RelationMappingCategoryCapability object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getCategoryCode() || object.getCategoryCode().equalsIgnoreCase("") || object.getCategoryCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Relation. Category is Blank";
		}
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase("") || object.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Relation. TenantId  is Blank";
		}
		if(null==object.getCapabilityCode() || object.getCapabilityCode().equalsIgnoreCase("") || object.getCapabilityCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Relation. Capability  is Blank";
		}
		
		return response+"~"+message;
	}
	
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		String tenantId=request.getParameter("tenantId");
		String categoryCode=request.getParameter("categoryCode");
		String capabilityCode=request.getParameter("capabilityCode");
		String mode=request.getParameter("mode");
		String[] allCapabilities=capabilityCode.split(",");
		
		
		for(String cap:allCapabilities)
		{
			RelationMappingCategoryCapability relationMappingCategoryCapability=new RelationMappingCategoryCapability();
			relationMappingCategoryCapability.setTenantId(accessObject.getTenantId());
			relationMappingCategoryCapability.setCategoryCode(categoryCode);
			relationMappingCategoryCapability.setCapabilityCode(cap);
			relationMappingCategoryCapability.setProcess(accessObject.getProcess());
			relationMappingCategoryCapability.setOrganizationUnit(accessObject.getOrganizationUnit());
			relationMappingCategoryCapability.setBusinessUnit(accessObject.getBusinessUnit());
			relationMappingCategoryCapability.setProject(accessObject.getProject());
			relationMappingCategoryCapability.setCmmiVersion(accessObject.getCmmiVersion());
			if(null!=mode && mode.equalsIgnoreCase("DELETE"))
			{
				String resp="";
				resp=service.delete(relationMappingCategoryCapability, accessObject,accessObject.getCmmiVersion());
				if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
				{
					accessObject.setMsg("SUCCESS~Relation Deleted Successfully.");
					
					return new ModelAndView(
				    	       new RedirectView("/relationMappingCategoryCapabilityCrud", true),
				    	       model);
				}
				else
				{
					accessObject.setMsg(resp);
					return new ModelAndView(
				    	       new RedirectView("/relationMappingCategoryCapabilityCrud", true),
				    	       model);
				}
			}
			
			String validationResp="";
			validationResp=validateCategory(relationMappingCategoryCapability);
			if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
			service.saveOrUpDate(relationMappingCategoryCapability,accessObject,accessObject.getCmmiVersion());
			if(mode.equalsIgnoreCase("CREATE"))
			{
				accessObject.setMsg("SUCCESS~Relation Created Successfully.");	
				
			}
			if(mode.equalsIgnoreCase("EDIT"))
			{
				accessObject.setMsg("SUCCESS~Relation Updated Successfully.");
				
			}
			
			
			}
			else
			{
				
				accessObject.setMsg(validationResp);
				return new ModelAndView(
			    	       new RedirectView("/relationMappingCategoryCapabilityCrud", true),
			    	       model);	
			}
			
		}
		
		return new ModelAndView(
	    	       new RedirectView("/relationMappingCategoryCapabilityCrud", true),
	    	       model);
	}

	
	
	

	
	
	
	@GetMapping("/{tenantId}")
	public List<RelationMappingCategoryCapability> getall(@PathVariable("tenantId")String tenantId)
	{
		AccessObject accessObject=getAccessObject();
		List<RelationMappingCategoryCapability> relationMappingCategoryCapability=new ArrayList<RelationMappingCategoryCapability>();
		relationMappingCategoryCapability=(List<RelationMappingCategoryCapability>) service.getAll(tenantId,accessObject.getCmmiVersion());
						return relationMappingCategoryCapability;
	}

	@RequestMapping(value = "/getAjax", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RelationshipCategoryCapabilityVO getAjax() {
		RelationshipCategoryCapabilityVO vo=new RelationshipCategoryCapabilityVO();
		AccessObject accessObject=getAccessObject();
		String categoryCode = "";
		String capabilityCode = "";
		categoryCode= request.getParameter("categoryCode");
		capabilityCode= request.getParameter("capabilityCode");
		RelationMappingCategoryCapability relationMappingCategoryCapability=service.getUnique(accessObject.getTenantId(),
				categoryCode,capabilityCode, accessObject.getProcess(),
				accessObject.getOrganizationUnit(),
				accessObject.getBusinessUnit(),
				accessObject.getProject(),accessObject.getCmmiVersion());
		if(null!=relationMappingCategoryCapability)
		{
			vo.setCategoryCode(relationMappingCategoryCapability.getCategoryCode());;
			vo.setCapabilityCode(relationMappingCategoryCapability.getCapabilityCode());;
		}
		return vo;
	}
	
	
	

	
	
	
	
}
