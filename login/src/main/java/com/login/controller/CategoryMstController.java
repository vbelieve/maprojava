package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.CategoryVO;
import com.login.domain.ApplicationConstants;
import com.login.domain.CategoryMst;
import com.login.domain.TenantMst;
import com.login.domain.basics.AccessObject;
import com.login.services.CategoryMstService;

@RequestMapping("/category")
@RestController
public class CategoryMstController {
    private static final Logger logger = LoggerFactory.getLogger(CategoryMstController.class);
	
	@Autowired
	CategoryMstService service;
	
	@Autowired
	private HttpServletRequest request;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

	
//	@PostMapping("/save")
//	public CategoryMst save(@RequestBody CategoryMst object)
//	{
//		return service.saveOrUpDate(object);
//			
//	}

	public String validateCategory(CategoryMst category)
	{
		String response="SUCCESS";
		String message="";
		if(null==category.getCategoryCode() || category.getCategoryCode().equalsIgnoreCase("") || category.getCategoryCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Category. Category Code is Blank";
		}
		if(null==category.getTenantId() || category.getTenantId().equalsIgnoreCase("") || category.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Category. TenantId  is Blank";
		}
		if(null==category.getCategoryName() || category.getCategoryName().equalsIgnoreCase("") || category.getCategoryName().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Category. CategoryName  is Blank";
		}
		
		return response+"~"+message;
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String tenantId=request.getParameter("tenantId");
		String categoryCode=request.getParameter("categoryCode");
		String categoryName=request.getParameter("categoryName");
		String mode=request.getParameter("mode");
		
		ModelMap model = new ModelMap();
		
		
		
		
		
		CategoryMst categoryMst=new CategoryMst();
		categoryMst.setCmmiVersion(accessObject.getCmmiVersion());
		categoryMst.setTenantId(accessObject.getTenantId());
		categoryMst.setCategoryCode(categoryCode);
		categoryMst.setCategoryName(categoryName);
		categoryMst.setProcess(accessObject.getProcess());
		categoryMst.setOrganizationUnit(accessObject.getOrganizationUnit());
		categoryMst.setBusinessUnit(accessObject.getBusinessUnit());
		categoryMst.setProject(accessObject.getProject());
		categoryMst.setCmmiVersion(accessObject.getCmmiVersion());
		if(null!=mode && mode.equalsIgnoreCase("DELETE"))
		{
			String resp="";
			resp=service.delete(categoryMst, accessObject,accessObject.getCmmiVersion());
			if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
				accessObject.setMsg("SUCCESS~Category Deleted Successfully.");
								return new ModelAndView(
			    	       new RedirectView("/categoryMstCrud", true),
			    	       model);
			}
			else
			{
				accessObject.setMsg(resp);
				return new ModelAndView(
			    	       new RedirectView("/categoryMstCrud", true),
			    	       model);
			}
		}
		
		String validationResp="";
		validationResp=validateCategory(categoryMst);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
		service.saveOrUpDate(categoryMst,accessObject,accessObject.getCmmiVersion());
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~Category Created Successfully.");	
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~Category Updated Successfully.");
		}
		
		return new ModelAndView(
	    	       new RedirectView("/categoryMstCrud", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/categoryMstCrud", true),
		    	       model);	
		}
	}

	
	
	
	@GetMapping("/{tenantId}")
	public List<CategoryMst> getall(@PathVariable("tenantId")String tenantId)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		
		logger.info("Category");
		List<CategoryMst> categoryMst=new ArrayList<CategoryMst>();
		categoryMst=(List<CategoryMst>) service.getAllCategory(tenantId,accessObject.getCmmiVersion());
						return categoryMst;
	}


	
	
	public List<CategoryMst> getAllActive(){
		return null;
	}
	
	
	@RequestMapping(value = "/getCategoryAjax", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CategoryVO categoryAjax() {
		CategoryVO vo=new CategoryVO();
		AccessObject accessObject=getAccessObject();
		String categoryCode = "";
		categoryCode= request.getParameter("categoryCode");
		CategoryMst category=service.getUniqueCmmiVersion(accessObject.getTenantId(),
				categoryCode, accessObject.getProcess(),
				accessObject.getOrganizationUnit(),
				accessObject.getBusinessUnit(),
				accessObject.getProject(),accessObject.getCmmiVersion());
		if(null!=category)
		{
			vo.setCategoryCode(category.getCategoryCode());
			vo.setCategoryName(category.getCategoryName());
		}
		return vo;
	}
	
}
