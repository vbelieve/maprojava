package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.CategoryVO;
import com.login.common.exceptions.PracticeVO;
import com.login.domain.AnswerMst;
import com.login.domain.ApplicationConstants;
import com.login.domain.CategoryMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.TenantMst;
import com.login.domain.TestMst;
import com.login.domain.TestQuestionMst;
import com.login.domain.basics.AccessObject;
import com.login.services.CategoryMstService;
import com.login.services.PracticeAreaMstService;
import com.login.services.TestQuestionMstService;

@RequestMapping("/testQuestionMst")
@RestController
public class TestQuestionMstController {
    private static final Logger logger = LoggerFactory.getLogger(TestQuestionMstController.class);
	
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private TestQuestionMstService testQuestionMstService;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

	private AccessObject getAccessObjectApi() {
		AccessObject accessObject=new AccessObject();
		accessObject.setLoginId("API");
		return accessObject;
	}
	
	@RequestMapping(value = "/getAll", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<TestQuestionMst> getAll() {
		String tenantId = "";
		tenantId= request.getParameter("tenantId");
		
		String authStatus = "";
		authStatus = request.getParameter("authStatus");
		List<TestQuestionMst> testQuestionList=null;
		testQuestionList=testQuestionMstService.getAll(tenantId,authStatus);
		

		if(null!=testQuestionList && testQuestionList.size()>0)
		{
			
			return testQuestionList;
		}
		
		
		return testQuestionList;
	}
	
	@RequestMapping(value="/saveObject",method=RequestMethod.POST)
	public String saveObject(HttpServletRequest request)
	{
		
		String tenantId = "";
		String questionId = "";
		String description = "";
		String answerGroup = "";
		String questionGroup = "";
		String priority = "";
		
		tenantId= request.getParameter("tenantId");
		questionId = request.getParameter("questionId");
		Long questionIdLong = Long.parseLong(questionId);
		description= request.getParameter("description");
		answerGroup= request.getParameter("answerGroup");
		questionGroup= request.getParameter("questionGroup");
		priority= request.getParameter("priority");
		Integer priorityInt=Integer.parseInt(priority);
		
		TestQuestionMst testQuestionMst = new TestQuestionMst();
		testQuestionMst.setTenantId(tenantId);
		testQuestionMst.setQuestionId(questionIdLong);
		testQuestionMst.setDescription(description);
		testQuestionMst.setAnswerGroup(answerGroup);
		testQuestionMst.setQuestionGroup(questionGroup);
		testQuestionMst.setPriority(priorityInt);
		testQuestionMst=testQuestionMstService.saveOrUpDate(testQuestionMst, getAccessObjectApi());
		return "Success";
	}

	
	
}
