package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.basics.AccessObject;
import com.login.services.CapabilityMstService;

@RequestMapping("/reports")
@RestController
public class ReportsController {
	
	@Autowired
	CapabilityMstService service;
	
//	@PostMapping("/save")
//	public CapabilityMst save(@RequestBody CapabilityMst object)
//	{
//		return service.saveOrUpDate(object);
//			
//	}
//	
	@RequestMapping(value="/getProjectReport",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		
		accessObject.setMsg("SUCCESS~Capability Details Created / Updated Successfully.");
		return new ModelAndView(
	    	       new RedirectView("/capabilityMstCrud", true),
	    	       model);
	}
	
	

	
}
