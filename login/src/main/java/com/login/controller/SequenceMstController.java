package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.ApplicationConstants;
import com.login.domain.LookupMst;
import com.login.domain.SequenceMst;
import com.login.domain.basics.AccessObject;
import com.login.services.SequenceMstService;

@RequestMapping("/sequence")
//@CrossOrigin(origins=GlobalConstants.angularPort)
//@CrossOrigin(origins="http://localhost:4200")
@RestController
public class SequenceMstController {
	
	@Autowired
	SequenceMstService service;
	
	
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(SequenceMst object,HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		service.saveOrUpDate(object,accessObject);
		accessObject.setMsg("SUCCESS~Sequence Details Created / Updated Successfully.");
		return new ModelAndView(
	    	       new RedirectView("/sequenceMstCrud", true),
	    	       model);
	}
	
	
	

	@GetMapping("/{tenantId}/{sequenceType}")
	public SequenceMst getByName(@PathVariable("tenantId")String tenantId, @PathVariable("sequenceType") String sequenceType){
		SequenceMst sequenceMst=new SequenceMst();
		sequenceMst=service.getSequenceMst(tenantId, sequenceType);
		return sequenceMst; 
	}
	
	@GetMapping("/getNext/{tenantId}/{sequenceType}")
	public String getNext(@PathVariable("tenantId")String tenantId, @PathVariable("sequenceType") String sequenceType){
		String nextSeqNo="";
		try {
			nextSeqNo=service.getNextSeqNo(tenantId, sequenceType);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nextSeqNo; 
	}
	
	@GetMapping("/getAll/{tenantId}")
	public List<SequenceMst> getAll(@PathVariable("tenantId")String tenantId){
		List<SequenceMst> sequence=new ArrayList<SequenceMst>();
		try {
			sequence=service.getAll(tenantId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sequence; 
	}
	
	public List<SequenceMst> getAllActive(){
		return null;
	}
	
	
	
	
}
