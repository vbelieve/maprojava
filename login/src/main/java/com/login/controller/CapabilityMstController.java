package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.CapabilityVO;
import com.login.common.exceptions.CategoryVO;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.basics.AccessObject;
import com.login.services.CapabilityMstService;

@RequestMapping("/capability")
@RestController
public class CapabilityMstController {
	
	@Autowired
	CapabilityMstService service;
	
	@Autowired
	private HttpServletRequest request;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

//	@PostMapping("/save")
//	public CapabilityMst save(@RequestBody CapabilityMst object)
//	{
//		return service.saveOrUpDate(object);
//			
//	}
//	
	
	public String validateCategory(CapabilityMst object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getCapabilityCode() || object.getCapabilityCode().equalsIgnoreCase("") || object.getCapabilityCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Capability. Capability Code is Blank";
		}
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase("") || object.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Capability. TenantId  is Blank";
		}
		if(null==object.getCapabilityName() || object.getCapabilityName().equalsIgnoreCase("") || object.getCapabilityName().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Capability. Capability  is Blank";
		}
		
		return response+"~"+message;
	}
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String tenantId=request.getParameter("tenantId");
		String capabilityCode=request.getParameter("capabilityCode");
		String capabilityName=request.getParameter("capabilityName");
		String mode=request.getParameter("mode");
		ModelMap model = new ModelMap();
		CapabilityMst capabilityMst=new CapabilityMst();
		capabilityMst.setTenantId(accessObject.getTenantId());
		capabilityMst.setCapabilityCode(capabilityCode);
		capabilityMst.setCapabilityName(capabilityName);
		capabilityMst.setProcess(accessObject.getProcess());
		capabilityMst.setOrganizationUnit(accessObject.getOrganizationUnit());
		capabilityMst.setBusinessUnit(accessObject.getBusinessUnit());
		capabilityMst.setProject(accessObject.getProject());
		capabilityMst.setCmmiVersion(accessObject.getCmmiVersion());
		if(null!=mode && mode.equalsIgnoreCase("DELETE"))
		{
			String resp="";
			resp=service.delete(capabilityMst, accessObject,accessObject.getCmmiVersion());
			if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
				accessObject.setMsg("SUCCESS~capability Deleted Successfully.");
				
				return new ModelAndView(
			    	       new RedirectView("/capabilityMstCrud", true),
			    	       model);
			}
			else
			{
				accessObject.setMsg(resp);
				return new ModelAndView(
			    	       new RedirectView("/capabilityMstCrud", true),
			    	       model);
			}
		}
		
		String validationResp="";
		validationResp=validateCategory(capabilityMst);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
		service.saveOrUpDate(capabilityMst,accessObject,accessObject.getCmmiVersion());
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~Capability Created Successfully.");
			
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~Capability Updated Successfully.");
			
		}
		
		return new ModelAndView(
	    	       new RedirectView("/capabilityMstCrud", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/capabilityMstCrud", true),
		    	       model);	
		}
		
	}
	
	
	@GetMapping("/getBycategory")
	public List<CapabilityMst> getByCategory(@RequestParam String tenantId,@RequestParam Integer categoryId)
	{	
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		
		List<CapabilityMst> capabilityMst=new ArrayList<CapabilityMst>();
		capabilityMst=(List<CapabilityMst>) service.getByCategory(tenantId, categoryId,accessObject.getCmmiVersion());
						return capabilityMst;
	}

	
	
	
	@GetMapping("/{tenantId}")
	public List<CapabilityMst> getall(@PathVariable("tenantId")String tenantId)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		
		List<CapabilityMst> capabilityMst=new ArrayList<CapabilityMst>();
		capabilityMst=(List<CapabilityMst>) service.getAllCapability(tenantId,accessObject.getCmmiVersion());
						return capabilityMst;
	}


	
	
	public List<CapabilityMst> getAllActive(){
		return null;
	}
	
	
	@RequestMapping(value = "/getAjax", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CapabilityVO getAjax() {
		CapabilityVO vo=new CapabilityVO();
		AccessObject accessObject=getAccessObject();
		String capabilityCode = "";
		capabilityCode= request.getParameter("capabilityCode");
		CapabilityMst capability=service.getUnique(accessObject.getTenantId(),
				capabilityCode, accessObject.getProcess(),
				accessObject.getOrganizationUnit(),
				accessObject.getBusinessUnit(),
				accessObject.getProject(),accessObject.getCmmiVersion());
		if(null!=capability)
		{
			vo.setCapabilityCode(capability.getCapabilityCode());
			vo.setCapabilityName(capability.getCapabilityName());
		}
		return vo;
	}
	
}
