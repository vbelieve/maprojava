package com.login.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.ApplicationConstants;
import com.login.domain.ApplicationUtility;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.LookupMst;
import com.login.domain.PracticeMst;
import com.login.domain.PracticeWorkProductMst;
import com.login.domain.ProjectMst;
import com.login.domain.TenantMst;
import com.login.domain.VO.FileDataVO;
import com.login.domain.VO.FileUploadRequestVO;
import com.login.domain.VO.FileUploadResponseVO;
import com.login.domain.basics.AccessObject;
import com.login.services.CapabilityMstService;
import com.login.services.LookupMstService;
import com.login.services.PracticeMstService;
import com.login.services.PracticeWorkProductMstService;
import com.login.services.ProjectMstService;

@RequestMapping("/fileUpload")
@RestController
public class FileUploadController {

@Autowired
PracticeMstService practiceMstService;

@Autowired
PracticeWorkProductMstService practiceWorkProductMstService;


@Autowired
ProjectMstService projectMstService;

@Autowired
LookupMstService lookupMstService;

	
@RequestMapping(value="/uploadSummary1",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		
		accessObject.setMsg("SUCCESS~Capability Details Created / Updated Successfully.");
		return new ModelAndView(
	    	       new RedirectView("/capabilityMstCrud", true),
	    	       model);
	}


public AccessObject getAccessObject(String tenantId)
{
	
AccessObject accessObject=new AccessObject();
	accessObject.setLoginId("SYSTEM");
	accessObject.setTenantId(tenantId);
	
	return accessObject; 

}



@RequestMapping(value="/uploadFile",method=RequestMethod.POST)
public ModelAndView uploadFile(HttpServletRequest request,@RequestParam("file") MultipartFile file,
        RedirectAttributes redirectAttributes) throws IOException
{
	AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	String UPLOADED_FOLDER=ApplicationConstants.UPLOAD_PATH;
    if (file.isEmpty()) {
    	accessObject.setMsg("ERROR~Please select a file to upload");
    	ModelMap model = new ModelMap();
    	
    	return new ModelAndView(
        	       new RedirectView("/uploadFile", true),
        	       model);	
    	
    	 
    }

    if(!file.getOriginalFilename().contains("xlsx"))
    {
    	accessObject.setMsg("ERROR~Format of File should be xlsx");
    	ModelMap model = new ModelMap();
    	
    	return new ModelAndView(
        	       new RedirectView("/uploadFile", true),
        	       model);	
    	

    }
    
   byte[] bytes = file.getBytes();
   Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
   Files.write(path, bytes);

	String tenantId=accessObject.getTenantId();
	String fileLocation=UPLOADED_FOLDER + file.getOriginalFilename();
	List<FileDataVO> fileDataList=new ArrayList<FileDataVO>();
	FileInputStream files = new FileInputStream(new File(fileLocation));
	Workbook workbook = new XSSFWorkbook(files);
	Sheet sheet = workbook.getSheetAt(0);
	Map<Integer, List<String>> data = new HashMap();
	int i = 0;
	for (Row row : sheet) {
		System.out.println("RECORD NUMBER FOR UPLOAD"+i);
	    
		if(i<2)
		{
			i++;
			continue;
		}
	FileDataVO fileData=new FileDataVO();
		   
	    for(int j=0;j<14;j++)
	    {
	    Cell cell=row.getCell(j);
	    
	    if(null!=cell && j==0)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setProjectCode(cell.toString());    	
		    }
		    	
	    }
	    
	    if(null!=cell && j==1)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setCategory(cell.toString());    	
			        	
		    }
		    	
	    }
	    
	    if(null!=cell && j==2)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setCapabilityArea(cell.toString());    	
			        	
		    }
		    	
	    }
	    
	    if(null!=cell && j==3)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setPracticeArea(cell.toString());    	
			        	
		    }
		    	
	    }
	    
	    if(null!=cell && j==4)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setPracticeGroup(cell.toString());    	
			        	
		    }
		    	
	    }
	    
	    
	    if(null!=cell && j==5)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setPractice(cell.toString());    	
			        	
		    }
		    	
	    }
	    
	    if(null!=cell && j==6)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setPracticeDetails(cell.toString());    	
			        	
		    }
		    	
	    }
	    
	    if(null!=cell && j==7)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setValue(cell.toString());    	
			        	
		    }
		    	
	    }
	    
	    if(null!=cell && j==8)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setExampleActivities(cell.toString());    	
			        	
		    }
		    	
	    }
	    
	    if(null!=cell && j==9)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setExampleWorkProduct(cell.toString());    	
			        	
		    }
		    	
	    }
	    
	    if(null!=cell && j==10)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setCurrentPractice(cell.toString());    	
			        	
		    }
		    	
	    }
	    
	    if(null!=cell && j==11)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setCompliance(cell.toString());    	
			        	
		    }
		    	
	    }
	    
	    if(null!=cell && j==12)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setGaps(cell.toString());    	
			        	
		    }
	    }
	    
	    if(null!=cell && j==13)
	    {
	    	if(ApplicationUtility.isValidData(cell.toString()))
		    {
	    		fileData.setArtifect(cell.toString());    	
			        	
		    }
	    }
	    	
	    }
	    fileDataList.add(fileData);
		    
	    i++;
	}
	
if(null!=fileDataList && fileDataList.size()>0)
{
for(FileDataVO fileData:fileDataList)
{

PracticeMst practiceMst=practiceMstService.getByTenantProjectAndPractice(tenantId, fileData.getProjectCode(), fileData.getPractice(),accessObject.getCmmiVersion());
if(null!=practiceMst)
{

if(ApplicationUtility.isValidData(fileData.getCurrentPractice()))
{
	practiceMst.setCurrentPractice(fileData.getCurrentPractice());
	
}

if(ApplicationUtility.isValidData(fileData.getCompliance()))
{
	String compliance=fileData.getCompliance();
	Integer complianceValue=0;
	{
		
		
		List<LookupMst> complianceLookup=new ArrayList<LookupMst>();
		complianceLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_COMPLIANCEREPORTLOOKUP);
		if(null!=complianceLookup)
		{
			for(LookupMst lookup:complianceLookup)
			{
				if(lookup.getDisplayValue().equalsIgnoreCase(compliance))
				{
					practiceMst.setPracticeCompliance(Double.valueOf(lookup.getLookupValue()));
					break;
				}
			}
		}
	}
	
	practiceMst.setCurrentPractice(fileData.getCurrentPractice());
	
}

if(ApplicationUtility.isValidData(fileData.getGaps()))
{
	practiceMst.setCurrentGapNptes(fileData.getGaps());
}

if(ApplicationUtility.isValidData(fileData.getArtifect()))
{
List<PracticeWorkProductMst> PracticeWorkProductMstList=new ArrayList<PracticeWorkProductMst>();
PracticeWorkProductMstList=practiceWorkProductMstService.getByPracticeCode(practiceMst.getTenantId(),practiceMst.getPracticeCode(),
		practiceMst.getOrganizationUnit(),practiceMst.getBusinessUnit(),practiceMst.getProject(),accessObject.getCmmiVersion());

if(null!=PracticeWorkProductMstList && PracticeWorkProductMstList.size()>0)
{
for(PracticeWorkProductMst practiceWorkProductMst:PracticeWorkProductMstList)
{
	if(ApplicationUtility.isValidData(fileData.getArtifect()))
	{
	practiceWorkProductMst.setQ1("Y");	
	practiceWorkProductMst.setQ2("100");
	practiceWorkProductMst.setQ3(fileData.getArtifect());
	fileData.setArtifect("");
	practiceWorkProductMstService.saveOrUpDate(practiceWorkProductMst, accessObject,accessObject.getCmmiVersion());
	}
	else
	{
	practiceWorkProductMst.setQ1("");	
	practiceWorkProductMst.setQ2("");
	practiceWorkProductMst.setQ3("");	
	practiceWorkProductMstService.saveOrUpDate(practiceWorkProductMst, accessObject,accessObject.getCmmiVersion());
	}
}

}
}



practiceMstService.saveOrUpDate(practiceMst, accessObject);
accessObject.setMsg("SUCCESS~File UPloaded Successfully.");	
}

}
}
	ModelMap model = new ModelMap();
	
	return new ModelAndView(
    	       new RedirectView("/uploadFile", true),
    	       model);	
	
}


@PostMapping("/uploadSummary")
public String uploadMonthlyBalance(HttpServletRequest httpRequest,@RequestBody String request, Locale locale) {
	try {
		AccessObject accessObject=(AccessObject) httpRequest.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		
		FileUploadRequestVO req = (FileUploadRequestVO) ApplicationUtility.jsonStringToObject(FileUploadRequestVO.class, request);
		FileUploadResponseVO res=new FileUploadResponseVO();
		List<FileDataVO> fileDataList=new ArrayList<FileDataVO>();
		String fileLocation=req.getFileLocation();
		String tenantId=req.getTenantId();
		
		FileInputStream file = new FileInputStream(new File(fileLocation));
		Workbook workbook = new XSSFWorkbook(file);
		Sheet sheet = workbook.getSheetAt(0);
		Map<Integer, List<String>> data = new HashMap();
		int i = 0;
		for (Row row : sheet) {
			if(i<2)
			{
				i++;
				continue;
			}
		FileDataVO fileData=new FileDataVO();
			   
		    for(int j=0;j<13;j++)
		    {
		    Cell cell=row.getCell(j);
		    
		    if(null!=cell && j==0)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setProjectCode(cell.toString());    	
			    }
			    	
		    }
		    
		    if(null!=cell && j==1)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setCategory(cell.toString());    	
				        	
			    }
			    	
		    }
		    
		    if(null!=cell && j==2)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setCapabilityArea(cell.toString());    	
				        	
			    }
			    	
		    }
		    
		    if(null!=cell && j==3)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setPracticeArea(cell.toString());    	
				        	
			    }
			    	
		    }
		    
		    if(null!=cell && j==4)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setPracticeGroup(cell.toString());    	
				        	
			    }
			    	
		    }
		    
		    
		    if(null!=cell && j==5)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setPractice(cell.toString());    	
				        	
			    }
			    	
		    }
		    
		    if(null!=cell && j==6)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setPracticeDetails(cell.toString());    	
				        	
			    }
			    	
		    }
		    
		    if(null!=cell && j==7)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setValue(cell.toString());    	
				        	
			    }
			    	
		    }
		    
		    if(null!=cell && j==8)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setExampleActivities(cell.toString());    	
				        	
			    }
			    	
		    }
		    
		    if(null!=cell && j==9)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setExampleWorkProduct(cell.toString());    	
				        	
			    }
			    	
		    }
		    
		    if(null!=cell && j==10)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setCurrentPractice(cell.toString());    	
				        	
			    }
			    	
		    }
		    
		    if(null!=cell && j==11)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setCompliance(cell.toString());    	
				        	
			    }
			    	
		    }
		    
		    if(null!=cell && j==12)
		    {
		    	if(ApplicationUtility.isValidData(cell.toString()))
			    {
		    		fileData.setGaps(cell.toString());    	
				        	
			    }
		    }
		    	
		    }
		    fileDataList.add(fileData);
			    
		    i++;
		}
		
if(null!=fileDataList && fileDataList.size()>0)
{
for(FileDataVO fileData:fileDataList)
{

PracticeMst practiceMst=practiceMstService.getByTenantProjectAndPractice(tenantId, fileData.getProjectCode(), fileData.getPractice(),accessObject.getCmmiVersion());
if(null!=practiceMst)
{
	
	if(ApplicationUtility.isValidData(fileData.getCurrentPractice()))
	{
		practiceMst.setCurrentPractice(fileData.getCurrentPractice());
		
	}

	if(ApplicationUtility.isValidData(fileData.getCompliance()))
	{
		String compliance=fileData.getCompliance();
		Integer complianceValue=0;
		{
			
			
			List<LookupMst> complianceLookup=new ArrayList<LookupMst>();
			complianceLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_COMPLIANCEREPORTLOOKUP);
			if(null!=complianceLookup)
			{
				for(LookupMst lookup:complianceLookup)
				{
					if(lookup.getDisplayValue().equalsIgnoreCase(compliance))
					{
						practiceMst.setPracticeCompliance(Double.valueOf(lookup.getLookupValue()));
						break;
					}
				}
			}
		}
		
		practiceMst.setCurrentPractice(fileData.getCurrentPractice());
		
	}
	
	if(ApplicationUtility.isValidData(fileData.getGaps()))
	{
		practiceMst.setCurrentGapNptes(fileData.getGaps());
	}

	practiceMstService.saveOrUpDate(practiceMst, getAccessObject(tenantId));

}

}
}


return ApplicationUtility.objectToJSONString(res);
	} catch (Exception e) {
		e.printStackTrace();
		return "";
	}
}

	

	
}
