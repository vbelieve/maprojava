package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.CapabilityVO;
import com.login.common.exceptions.WorkProductVO;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.PracticeMst;
import com.login.domain.WorkProductMst;
import com.login.domain.basics.AccessObject;
import com.login.services.WorkProductMstService;

@RequestMapping("/workProductMst")
//@CrossOrigin(origins=GlobalConstants.angularPort)
//@CrossOrigin(origins="http://localhost:4200")
@RestController
public class WorkProductMstController {
	
	@Autowired
	WorkProductMstService service;
	
	@Autowired
	private HttpServletRequest request;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

	
//	@PostMapping("/save")
//	public WorkProductMst save(@RequestBody WorkProductMst object)
//	{
//		AccessObject accessObject=new AccessObject();
//		object.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
//		object.setIsActive(1);
//		object.setTenantId(accessObject.getTenantId());
//		
//		return service.saveOrUpDate(object);
//			
//	}
//	
	
	public String validateCategory(WorkProductMst object)
	{
		String response="SUCCESS";
		String message="";
		
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase("") || object.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Work Product. TenantId  is Blank";
		}
		if(null==object.getWorkProductName() || object.getWorkProductName().equalsIgnoreCase("") || object.getWorkProductName().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Work Product. Work Product  is Blank";
		}
		
		return response+"~"+message;
	}
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		String tenantId=request.getParameter("tenantId");
		String workProductId=request.getParameter("workProductId");
		String workProductName=request.getParameter("workProductName");
		String mode=request.getParameter("mode");
		WorkProductMst workProductMst=new WorkProductMst();
		workProductMst.setTenantId(accessObject.getTenantId());
		workProductMst.setWorkProductId(workProductId);
		workProductMst.setWorkProductName(workProductName);
		workProductMst.setProcess(accessObject.getProcess());
		workProductMst.setOrganizationUnit(accessObject.getOrganizationUnit());
		workProductMst.setBusinessUnit(accessObject.getBusinessUnit());
		workProductMst.setProject(accessObject.getProject());
		workProductMst.setCmmiVersion(accessObject.getCmmiVersion());
		if(null!=mode && mode.equalsIgnoreCase("DELETE"))
		{
			String resp="";
			resp=service.delete(workProductMst, accessObject,accessObject.getCmmiVersion());
			if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
				accessObject.setMsg("SUCCESS~Work Product Deleted Successfully.");
				return new ModelAndView(
			    	       new RedirectView("/workProductMstCrud", true),
			    	       model);
			}
			else
			{
				accessObject.setMsg(resp);
				return new ModelAndView(
			    	       new RedirectView("/workProductMstCrud", true),
			    	       model);
			}
		}
		
		String validationResp="";
		validationResp=validateCategory(workProductMst);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
		service.saveOrUpDate(workProductMst,accessObject,accessObject.getCmmiVersion());
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~Work Product Created Successfully.");	
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~Work Product Updated Successfully.");
		}
		
		return new ModelAndView(
	    	       new RedirectView("/workProductMstCrud", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/workProductMstCrud", true),
		    	       model);	
		}
		
		

	
		
	}

	
	

	@GetMapping("/{tenantId}")
	public List<WorkProductMst> getByName(@PathVariable("tenantId")String tenantId){
		AccessObject accessObject=getAccessObject();
		List<WorkProductMst> workProductMst=new ArrayList<WorkProductMst>();
		workProductMst=service.getAll(tenantId,accessObject.getCmmiVersion());
		return workProductMst; 
	}
	
	@RequestMapping(value = "/getAjax", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public WorkProductVO getAjax() {
		WorkProductVO vo=new WorkProductVO();
		AccessObject accessObject=getAccessObject();
		String workProductId = "";
		workProductId= request.getParameter("workProductId");
		WorkProductMst workProduct=service.getUnique(accessObject.getTenantId(),
				workProductId, accessObject.getProcess(),
				accessObject.getOrganizationUnit(),
				accessObject.getBusinessUnit(),
				accessObject.getProject(),accessObject.getCmmiVersion());
		if(null!=workProduct)
		{
			vo.setWorkProductId(workProduct.getWorkProductId());
			vo.setWorkProductName(workProduct.getWorkProductName());
		}
		return vo;
	}
	
	
	
}
