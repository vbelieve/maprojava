package com.login.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.ApplicationConstants;
import com.login.domain.PracticeMst;
import com.login.domain.TenantMst;
import com.login.domain.basics.AccessObject;
import com.login.services.SequenceMstService;
import com.login.services.TenantMstService;

@RequestMapping("/tenant")
@RestController
public class TenantMstController {

	@Autowired
	TenantMstService tenantMstService;

	@Autowired
	SequenceMstService sequenceMstService;

//	@PostMapping("/save")
//	public TenantMst save(@RequestBody TenantMst tenantId)
//	{
//		
//		return tenantMstService.SaveOrUpdate(tenantId);
//	}
//	
	
	
	public String validateCategory(TenantMst object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getOrganizationName() || object.getOrganizationName().equalsIgnoreCase("") || object.getOrganizationName().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Organization details. Organization Name Code is Blank";
		}
		if(null==object.getBusinessModel() || object.getBusinessModel().equalsIgnoreCase("") || object.getBusinessModel().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Organization details. Business Model  is Blank";
		}
		if(null==object.getPracticeLevel() || object.getPracticeLevel()==0)
		{
			response="ERROR";
			message="Unable to update Organization details. Practice Level is Blank";
		}
		
		return response+"~"+message;
	}
	
	

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request) throws ParseException {
		AccessObject accessObject = (AccessObject) request.getSession()
				.getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		String mode=request.getParameter("mode");
		String authStatus = request.getParameter("authStatus");
		String businessUnit = request.getParameter("businessUnit");
		String isActive = request.getParameter("isActive");
		String organizationUnit = request.getParameter("organizationUnit");
		String process = request.getParameter("process");
		String project = request.getParameter("project");
		String tenantId = request.getParameter("tenantId");
		String address = request.getParameter("address");
		String businessModel = request.getParameter("businessModel");
		String organizationCode = request.getParameter("organizationCode");
		String organizationName = request.getParameter("organizationName");
		String practiceLevel = request.getParameter("practiceLevel");
		String telephone1 = request.getParameter("telephone1");
		String licenseExpiryDate = request.getParameter("licenseExpiryDate");
		TenantMst tenantMst = new TenantMst();
		tenantMst.setAuthStatus(authStatus);
		tenantMst.setBusinessUnit(businessUnit);
		tenantMst.setOrganizationUnit(organizationUnit);
		tenantMst.setProcess(process);
		tenantMst.setProject(project);
		tenantMst.setTenantId(tenantId);
		tenantMst.setAddress(address);
		tenantMst.setBusinessModel(businessModel);
		tenantMst.setOrganizationCode(organizationCode);
		tenantMst.setOrganizationName(organizationName);
		tenantMst.setPracticeLevel(Integer.parseInt(practiceLevel));
		tenantMst.setTelephone1(telephone1);
		tenantMst.setLicenseExpiryDate(licenseExpiryDate);

		if (null != mode && mode.equalsIgnoreCase("DELETE")) {
			String resp = "";
			resp = tenantMstService.delete(tenantMst, accessObject);
			if (resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS)) {
				accessObject.setMsg("SUCCESS~Organization Deleted Successfully.");
				
				return new ModelAndView(new RedirectView("/tenantMstCrud", true), model);
			} else {
				accessObject.setMsg(resp);
				return new ModelAndView(new RedirectView("/tenantMstCrud", true), model);
			}
		}

		String validationResp="";
		validationResp=validateCategory(tenantMst);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
			tenantMstService.SaveOrUpdate(tenantMst, accessObject);
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~Organization Created Successfully.");
			
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~Organization Updated Successfully.");
			
		}
		
		return new ModelAndView(
	    	       new RedirectView("/tenantMstCrud", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/tenantMstCrud", true),
		    	       model);	
		}
		
		
	}

	@RequestMapping(value = "/organizationProfileSave", method = RequestMethod.POST)
	public ModelAndView organizationProfileSave(HttpServletRequest request) {
		AccessObject accessObject = (AccessObject) request.getSession()
				.getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		String mode=request.getParameter("mode");
		String authStatus = request.getParameter("authStatus");
		String businessUnit = request.getParameter("businessUnit");
		String isActive = request.getParameter("isActive");
		String organizationUnit = request.getParameter("organizationUnit");
		String process = request.getParameter("process");
		String project = request.getParameter("project");
		String tenantId = request.getParameter("tenantId");
		String address = request.getParameter("address");
		String businessModel = request.getParameter("businessModel");
		String organizationCode = request.getParameter("organizationCode");
		String organizationName = request.getParameter("organizationName");
		String practiceLevel = request.getParameter("practiceLevel");
		String telephone1 = request.getParameter("telephone1");
		String licenseExpiryDate = request.getParameter("licenseExpiryDate");
		TenantMst tenantMst = new TenantMst();
		tenantMst.setAuthStatus(authStatus);
		tenantMst.setBusinessUnit(businessUnit);
		tenantMst.setIsActive(Integer.parseInt(isActive));
		tenantMst.setOrganizationUnit(organizationUnit);
		tenantMst.setProcess(process);
		tenantMst.setProject(project);
		tenantMst.setTenantId(tenantId);
		tenantMst.setAddress(address);
		tenantMst.setBusinessModel(businessModel);
		tenantMst.setOrganizationCode(organizationCode);
		tenantMst.setOrganizationName(organizationName);
		tenantMst.setPracticeLevel(Integer.parseInt(practiceLevel));
		tenantMst.setTelephone1(telephone1);
		tenantMst.setLicenseExpiryDate(licenseExpiryDate);

		if (null != mode && mode.equalsIgnoreCase("DELETE")) {
			String resp = "";
			resp = tenantMstService.delete(tenantMst, accessObject);
			if (resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS)) {
				accessObject.setMsg("SUCCESS~Organization Deleted Successfully.");
				return new ModelAndView(new RedirectView("/organizationProfile", true), model);
			} else {
				accessObject.setMsg(resp);
				return new ModelAndView(new RedirectView("/organizationProfile", true), model);
			}
		}

		String validationResp="";
		validationResp=validateCategory(tenantMst);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
			tenantMstService.SaveOrUpdate(tenantMst, accessObject);
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~Organization Created Successfully.");	
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~Organization Updated Successfully.");
		}
		
		return new ModelAndView(
	    	       new RedirectView("/organizationProfile", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/organizationProfile", true),
		    	       model);	
		}
		
		

		
	}

	@GetMapping("/all")
	public List<TenantMst> getallUsers() {
		return tenantMstService.getAll();
	}

	@GetMapping("/{tenantId}")
	public TenantMst getTenant(@PathVariable("tenantId") String tenantId) {
		TenantMst tenantMst = new TenantMst();
		tenantMst = tenantMstService.getTenant(tenantId);
		return tenantMst;
	}

}
