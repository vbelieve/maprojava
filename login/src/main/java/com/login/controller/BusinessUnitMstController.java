package com.login.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.ApplicationConstants;
import com.login.domain.BusinessUnitMst;
import com.login.domain.OrganizationUnitMst;
import com.login.domain.basics.AccessObject;
import com.login.services.BusinessUnitMstService;

@RequestMapping("/businessUnit")
@RestController
public class BusinessUnitMstController {
	
	@Autowired
	BusinessUnitMstService service;
	
//	@PostMapping("/save")
//	public BusinessUnitMst save(@RequestBody BusinessUnitMst object)
//	{
//		return service.saveOrUpDate(object);
//			
//	}

//	@RequestMapping(value="/save",method=RequestMethod.POST)
//	public ModelAndView save(BusinessUnitMst businessUnitMst,HttpServletRequest request)
//	{
//		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
//		ModelMap model = new ModelMap();
//		service.saveOrUpDate(businessUnitMst,accessObject);
//		accessObject.setMsg("SUCCESS~BusinessUnitMst Created / Updated Successfully.");
//		return new ModelAndView(
//	    	       new RedirectView("/businessUnitMstCrud", true),
//	    	       model);
//	}

	public String validateObject(BusinessUnitMst object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase("") || object.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Business Unit. Organization Code is Blank";
		}
		if(null==object.getBusinessUnitName()|| object.getBusinessUnitName().equalsIgnoreCase("") ||object.getBusinessUnitName().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Business Unit. Name  is Blank";
		}
				
		return response+"~"+message;
	}
	
	

	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String tenantId=request.getParameter("tenantId");
		String businessUnitCode=request.getParameter("businessUnitCode");
		String businessUnitName=request.getParameter("businessUnitName");
		String authstatus=request.getParameter("authstatus");
		String isActive=request.getParameter("isActive");
		String mode=request.getParameter("mode");
		ModelMap model = new ModelMap();
		
		BusinessUnitMst object=new BusinessUnitMst();
		object.setTenantId(accessObject.getTenantId());
		object.setProcess(accessObject.getProcess());
		object.setOrganizationUnit(accessObject.getOrganizationUnit());
		object.setBusinessUnit(accessObject.getBusinessUnit());
		object.setProject(accessObject.getProject());
		object.setBusinessUnitCode(businessUnitCode);
		object.setBusinessUnitName(businessUnitName);
		object.setAuthStatus(authstatus);
		
		if(null!=mode && mode.equalsIgnoreCase("DELETE"))
		{
			String resp="";
			resp=service.delete(object, accessObject);
			if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
				accessObject.setMsg("SUCCESS~Business Unit Deleted Successfully.");
				return new ModelAndView(
			    	       new RedirectView("/businessUnitMstCrud", true),
			    	       model);
			}
			else
			{
				accessObject.setMsg(resp);
				return new ModelAndView(
			    	       new RedirectView("/businessUnitMstCrud", true),
			    	       model);
			}
		}
		
		String validationResp="";
		validationResp=validateObject(object);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
		service.saveOrUpDate(object,accessObject);
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~Business Unit Created Successfully.");	
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~Business Unit Updated Successfully.");
		}
		
		return new ModelAndView(
	    	       new RedirectView("/businessUnitMstCrud", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/businessUnitMstCrud", true),
		    	       model);	
		}
	}


	
	
	@PostMapping("/update")
	public BusinessUnitMst update(@RequestBody BusinessUnitMst object,HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		return service.saveOrUpDate(object,accessObject);
	}
	
	@GetMapping("/{tenantId}")
	public List<BusinessUnitMst> getall(@PathVariable("tenantId")String tenantId)
	{
		List<BusinessUnitMst> businessUnitMst=new ArrayList<BusinessUnitMst>();
		businessUnitMst=(List<BusinessUnitMst>) service.getAllBusinessUnit(tenantId);
						return businessUnitMst;
	}

	@GetMapping("/{tenantId}/{businessUnitCode}")
	public BusinessUnitMst getByName(@PathVariable("tenantId")String tenantId, @PathVariable("businessUnitCode") String businessUnitCode,
			@PathVariable("process") String process){
		BusinessUnitMst businessUnitMst=new BusinessUnitMst();
		businessUnitMst=service.getUnique(tenantId, businessUnitCode,process);
		return businessUnitMst; 
	}

	
	
	public List<BusinessUnitMst> getAllActive(){
		return null;
	}
	
	
	
	
}
