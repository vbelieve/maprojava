package com.login.controller;

import com.login.services.RelationMappingPracticeAreaPracticeService;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.RelationMappingPracticeAreaPractice;
import com.login.domain.basics.AccessObject;
import com.login.common.exceptions.RelationshipCategoryCapabilityVO;
import com.login.common.exceptions.RelationshipPracticeAreaPracticeVO;
import com.login.domain.ApplicationConstants;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import com.login.domain.RelationMappingCategoryCapability;

@RequestMapping("/relationMappingPracticeAreaPractice")
//@CrossOrigin(origins="http://localhost:4200")
@RestController
public class RelationMappingPracticeAreaPracticeController {
	
	@Autowired
	RelationMappingPracticeAreaPracticeService service;
	
	@Autowired
	private HttpServletRequest request;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

	
//	@PostMapping("/save")
//	public RelationMappingPracticeAreaPractice save(@RequestBody RelationMappingPracticeAreaPractice object)
//	{
//		
//		return service.saveOrUpDate(object);
//			
//	}
	
	public String validateCategory(RelationMappingPracticeAreaPractice object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getPracticeLevel())
		{
			response="ERROR";
			message="Unable to update Relation. Practice Level is Blank";
		}
		if(null==object.getPracticeAreaCode() || object.getPracticeAreaCode().equalsIgnoreCase("") || object.getPracticeAreaCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Relation. Practice Area is Blank";
		}
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase("") || object.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Relation. TenantId  is Blank";
		}
		if(null==object.getPracticeCode() || object.getPracticeCode().equalsIgnoreCase("") || object.getPracticeCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Relation. Practice is Blank";
		}
		
		return response+"~"+message;
	}
	
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();

		String tenantId=request.getParameter("tenantId");
		String practiceAreaCode=request.getParameter("practiceAreaCode");
		String practiceCode=request.getParameter("practiceCode");
		String practiceLevelCode=request.getParameter("practiceLevelCode");
		String mode=request.getParameter("mode");
		String[] allPracticeCode=practiceCode.split(",");
		
		
		for(String pCode:allPracticeCode)
		{
			RelationMappingPracticeAreaPractice relationMappingPracticeAreaPractice=new RelationMappingPracticeAreaPractice();
			relationMappingPracticeAreaPractice.setTenantId(accessObject.getTenantId());
			relationMappingPracticeAreaPractice.setPracticeAreaCode(practiceAreaCode);
			relationMappingPracticeAreaPractice.setPracticeCode(pCode);
			relationMappingPracticeAreaPractice.setProcess(accessObject.getProcess());
			relationMappingPracticeAreaPractice.setOrganizationUnit(accessObject.getOrganizationUnit());
			relationMappingPracticeAreaPractice.setBusinessUnit(accessObject.getBusinessUnit());
			relationMappingPracticeAreaPractice.setProject(accessObject.getProject());
			relationMappingPracticeAreaPractice.setPracticeLevelCode("L"+practiceLevelCode);
			relationMappingPracticeAreaPractice.setPracticeLevel(Integer.parseInt(practiceLevelCode));
			relationMappingPracticeAreaPractice.setCmmiVersion(accessObject.getCmmiVersion());
			if(null!=mode && mode.equalsIgnoreCase("DELETE"))
			{
				String resp="";
				resp=service.delete(relationMappingPracticeAreaPractice, accessObject,accessObject.getCmmiVersion());
				if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
				{
					accessObject.setMsg("SUCCESS~Relation Deleted Successfully.");
					
					return new ModelAndView(
				    	       new RedirectView("/relationMappingPracticeAreaPracticeCrud", true),
				    	       model);
				}
				else
				{
					accessObject.setMsg(resp);
					return new ModelAndView(
				    	       new RedirectView("/relationMappingPracticeAreaPracticeCrud", true),
				    	       model);
				}
			}
			
			String validationResp="";
			validationResp=validateCategory(relationMappingPracticeAreaPractice);
			if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
			service.saveOrUpDate(relationMappingPracticeAreaPractice,accessObject,accessObject.getCmmiVersion());
			if(mode.equalsIgnoreCase("CREATE"))
			{
				accessObject.setMsg("SUCCESS~Relation Created Successfully.");
				
				
			}
			if(mode.equalsIgnoreCase("EDIT"))
			{
				accessObject.setMsg("SUCCESS~Relation Updated Successfully.");
			}
			
			
			}
			else
			{
				
				accessObject.setMsg(validationResp);
				return new ModelAndView(
			    	       new RedirectView("/relationMappingPracticeAreaPracticeCrud", true),
			    	       model);	
			}
			
		}
		return new ModelAndView(
	    	       new RedirectView("/relationMappingPracticeAreaPracticeCrud", true),
	    	       model);
	}
	

	@GetMapping("/getByCapability/{tenantId}/{practiceCode:.+}")
	public List<RelationMappingPracticeAreaPractice> getByPractice(@PathVariable("tenantId")String tenantId, @PathVariable("practiceCode") String practiceCode)
	{
		AccessObject accessObject=getAccessObject();
		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
		relationMappingPracticeAreaPractice=(List<RelationMappingPracticeAreaPractice>) service.getByPractice(tenantId, practiceCode,accessObject.getCmmiVersion());
						return relationMappingPracticeAreaPractice;
	}
	@GetMapping("/getByPracticeLevel/{tenantId}/{practiceLevelCode:.+}")
	public List<RelationMappingPracticeAreaPractice> getByPracticeLevel(@PathVariable("tenantId")String tenantId, @PathVariable("practiceLevelCode") String practiceLevelCode)
	{
		AccessObject accessObject=getAccessObject();
		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
		relationMappingPracticeAreaPractice=(List<RelationMappingPracticeAreaPractice>) service.getByPracticeLevel(tenantId, practiceLevelCode,accessObject.getCmmiVersion());
						return relationMappingPracticeAreaPractice;
	}
	
	
	@GetMapping("/{tenantId}")
	public List<RelationMappingPracticeAreaPractice> getall(@PathVariable("tenantId")String tenantId)
	{
		AccessObject accessObject=getAccessObject();
		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
		relationMappingPracticeAreaPractice=(List<RelationMappingPracticeAreaPractice>) service.getAll(tenantId,accessObject.getCmmiVersion());
						return relationMappingPracticeAreaPractice;
	}

  
	@RequestMapping(value = "/getAjax", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RelationshipPracticeAreaPracticeVO getAjax() {
		RelationshipPracticeAreaPracticeVO vo=new RelationshipPracticeAreaPracticeVO();
		AccessObject accessObject=getAccessObject();
		String practiceAreaCode = "";
		String practiceCode = "";
		String practiceLevelCode = "";
		practiceAreaCode= request.getParameter("practiceAreaCode");
		practiceCode= request.getParameter("practiceCode");
		practiceLevelCode= request.getParameter("practiceLevelCode");
		RelationMappingPracticeAreaPractice relationMappingPracticeAreaPractice=service.getUnique(accessObject.getTenantId(),
				practiceAreaCode,practiceCode,practiceLevelCode, accessObject.getProcess(),
				accessObject.getOrganizationUnit(),
				accessObject.getBusinessUnit(),
				accessObject.getProject(),accessObject.getCmmiVersion());
		if(null!=relationMappingPracticeAreaPractice)
		{
			vo.setPracticeAreaCode(relationMappingPracticeAreaPractice.getPracticeAreaCode());;
			vo.setPracticeCode(relationMappingPracticeAreaPractice.getPracticeCode());
			vo.setPracticeLevelCode(relationMappingPracticeAreaPractice.getPracticeLevelCode());
			vo.setPracticeLevel(relationMappingPracticeAreaPractice.getPracticeLevel()+"");
		}
		return vo;
	}
	
	
	
}
