package com.login.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.ApplicationConstants;
import com.login.domain.CategoryMst;
import com.login.domain.TenantMst;
import com.login.domain.UserMst;
import com.login.domain.basics.AccessObject;
import com.login.services.TenantMstService;
import com.login.services.UserMstService;

@RequestMapping("/userMst")
//@CrossOrigin(origins="http://localhost:8588")
@RestController
public class UserMstController {
	
	SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
	@Autowired
	UserMstService service;
	
	@Autowired
	TenantMstService tenantMstService;
	
    @GetMapping("/loginUser")
    public String sendForm(UserMst user) {

        return "addUser";
    }
    
//	@PostMapping("/save")
//	public UserMst save(@RequestBody UserMst object)
//	{
//		String error="SUCCESS";
//		try
//		{
//			SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
//			object.setCreatedDate(new Date());
//			object.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
//			object.setIsActive(1);
//			service.saveOrUpDate(object);
//			return object;
//		}
//		catch (Exception e)
//		{
//			error="Fail";
//		}
//		return object;
//			
//	}
    
    
    public String validate(UserMst userMst)
	{
		String response="SUCCESS";
		String message="";
		if(null==userMst.getTenantId() || userMst.getTenantId().equalsIgnoreCase("") || userMst.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update User. Organization Code is Blank";
		}
		if(null==userMst.getUserName() || userMst.getUserName().equalsIgnoreCase("") || userMst.getUserName().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update User. UserName  is Blank";
		}
		if(null==userMst.getUserType() || userMst.getUserType().equalsIgnoreCase("") || userMst.getUserType().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update User. UserType  is Blank";
		}
		
		if(null==userMst.getProject() || userMst.getProject().equalsIgnoreCase("") || userMst.getProject().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update User. Project  is Blank";
		}
		
		return response+"~"+message;
	}
    
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String tenantId=request.getParameter("tenantId");
		String userName=request.getParameter("userName");
		String mode=request.getParameter("mode");
		String project=request.getParameter("project");
		String userType=request.getParameter("userType");
		
		UserMst userMst = new UserMst();
		UserMst UserMstRecord = getByName(tenantId,userName);
		String msg="";
		ModelMap model = new ModelMap();
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		if(null!=UserMstRecord)
		{
			
			UserMstRecord.setProject(project);
			UserMstRecord.setUserType(userType);
			UserMstRecord.setCmmiVersion(accessObject.getCmmiVersion());
			service.saveOrUpDate(UserMstRecord,accessObject.getLoginId());	
		}
		else
		{
			
			userMst.setTenantId(tenantId);
			userMst.setUserName(userName);
			userMst.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
			userMst.setIsActive(1);
			userMst.setProject(project);
			userMst.setUserType(userType);
			userMst.setCmmiVersion(accessObject.getCmmiVersion());
			if(null==userMst.getProject())
			{
				msg="Cannot create user. No Projects assigned.";
				accessObject.setMsg(msg);
				return new ModelAndView(
			    	       new RedirectView("/signup", true),
			    	       model);
			}
			
				userMst.setPassword(ApplicationConstants.DEFAULT_PASSWORD);
				userMst.setIsFirstTimeLogin(Boolean.TRUE);
				service.saveOrUpDate(userMst,accessObject.getLoginId());
		}
		
		
		
		accessObject.setMsg("SUCCESS~User Created Successfully.");
		
	return new ModelAndView(
	    	       new RedirectView("/signup", true),
	    	       model);
	}
	
	@RequestMapping(value="/changePassword",method=RequestMethod.POST)
	public ModelAndView changePassword(UserMst object,HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		UserMst UserMst = getByName(object.getTenantId(),object.getUserName());
		String msg="";
		if(null!=UserMst)
		{
			String oldPassword=object.getOldPassword();
			String newPassword=object.getPassword();
			if(oldPassword.equalsIgnoreCase(newPassword))
			{
				accessObject.setMsg("ERROR~New Password and Old password entered cannot be same.");	
			}
			
			if(newPassword.length()<8)
			{
				accessObject.setMsg("ERROR~New Password Length cannot be less than 8.");
			}
			if(msg.equalsIgnoreCase(""))
			{
			UserMst=new UserMst();
			UserMst=service.validateUser(accessObject.getTenantId(), object.getUserName(), oldPassword);
			if(null!=UserMst)
			{
				UserMst.setPassword(newPassword);
				UserMst.setIsFirstTimeLogin(Boolean.FALSE);
				service.saveOrUpDate(UserMst,accessObject.getLoginId());
				accessObject.setMsg("SUCCESS~Password Updated Successfully.");
				
			}
			else
			{
				accessObject.setMsg("ERROR~Old Password is incorrect.");
			}
			}
					
		}
		//accessObject.setMsg(msg);
		
	return new ModelAndView(
	    	       new RedirectView("/changePassword", true),
	    	       model);
	}
	
	@RequestMapping(value="/profileSave",method=RequestMethod.POST)
	//public ModelAndView profileSave(UserMst userMst,HttpServletRequest request)
	public ModelAndView profileSave(String tenantId,String UserName,
			String firstName,String lastName,String address,String landline,String mobile,String email,String designation,
			HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		UserMst userMst = getByName(tenantId,UserName);
		if(null!=userMst)
		{
			userMst.setFirstName(firstName);
			userMst.setLastName(lastName);
			userMst.setAddress(address);
			userMst.setLandline(landline);
			userMst.setMobile(mobile);
			userMst.setEmail(email);
			userMst.setDesignation(designation);
		}
//		userMst.setCreatedDate(new Date());
//		userMst.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
//		userMst.setIsActive(1);
//		if(null!=userMst.getPassword())
//		{
//			
//		}
//		else
//		{
//			userMst.setPassword(ApplicationConstants.DEFAULT_PASSWORD);
//		}
		service.saveOrUpDate(userMst,accessObject.getLoginId());
		String msg="SUCCESS~User Details Updated Successfully";
		accessObject.setMsg(msg);
	return new ModelAndView(
	    	       new RedirectView("/profile", true),
	    	       model);
	}
	
	@RequestMapping(value="/userAdminSave",method=RequestMethod.POST)
	public ModelAndView userAdminSave(String UserName,String action,HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		
		UserMst UserMstToUpdate = getByName(accessObject.getTenantId(),UserName);
		if(action.equalsIgnoreCase("reset"))
		{
			UserMstToUpdate.setPassword(ApplicationConstants.DEFAULT_PASSWORD);
		}
		if(action.equalsIgnoreCase("unlock"))
		{
			UserMstToUpdate.setIsUserLocked(Boolean.FALSE);;
		}
		if(action.equalsIgnoreCase("deactivate"))
		{
			UserMstToUpdate.setIsActive(0);
		}
		if(action.equalsIgnoreCase("activate"))
		{
			UserMstToUpdate.setIsActive(1);
		}
		
		service.saveOrUpDate(UserMstToUpdate,accessObject.getLoginId());
		String msg="SUCCESS~User Details Updated Successfully";
		accessObject.setMsg(msg);
	return new ModelAndView(
	    	       new RedirectView("/userAdmin", true),
	    	       model);
	}
	
	
	
	@GetMapping("/{tenantId}")
	public List<UserMst> getall(@PathVariable("tenantId")String tenantId)
	{
		List<UserMst> userMst=new ArrayList<UserMst>();
		userMst=(List<UserMst>) service.getAllUsers(tenantId);
						return userMst;
	}

	@RequestMapping("/welcome")
	public String welcome()
		{
			return "login";
		}
	
	@GetMapping("/{tenantId}/{userName:.+}")
	public UserMst getByName(@PathVariable("tenantId")String tenantId, @PathVariable("userName") String userName){
		UserMst userMst=new UserMst();
		userMst=service.getByName(tenantId, userName);
		return userMst; 
	}

	
	@GetMapping("/{tenantId}/{userName:.+}/{password:.+}")
	public AccessObject getByName(@PathVariable("tenantId")String tenantId, @PathVariable("userName") String userName,@PathVariable("password") String password){
		UserMst userMst=new UserMst();
		AccessObject accessObject=new AccessObject();
		userMst=service.validateUser(tenantId, userName,password);
		if(null!=userMst)
		{
			accessObject.setLoginId(userMst.getUserName());
			accessObject.setTenantId(userMst.getTenantId());
			accessObject.setUserType(userMst.getUserType());
			accessObject.setResponseMsg(ApplicationConstants.SUCCESS);
			TenantMst tenantMst=new TenantMst();
			tenantMst=tenantMstService.getTenant(userMst.getTenantId());
			if(null!=tenantMst)
			{
				if(null!=tenantMst.getLicenseExpiryDate() && !tenantMst.getLicenseExpiryDate().equalsIgnoreCase(""))
				{
					try {
						Date tenantExpiryDate=sdf.parse(tenantMst.getLicenseExpiryDate());
						Date systemDate=new Date();
						if(systemDate.after(tenantExpiryDate))
						{
							accessObject.setResponseMsg("License Expired on "+tenantMst.getLicenseExpiryDate()+". PLease renew License to Continue");
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					accessObject.setResponseMsg("License Expiry Date not Set for Tenant");
				}
			}
		}
		
		return accessObject; 
	}
	
	public List<UserMst> getAllActive(){
		return null;
	}
	
	
	
	
}
