package com.login.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.ApplicationConstants;
import com.login.domain.OrganizationUnitMst;
import com.login.domain.ProjectMst;
import com.login.domain.basics.AccessObject;
import com.login.services.OrganizationUnitMstService;
import com.login.services.ProjectMstService;

@RequestMapping("/project")
@RestController
public class ProjectMstController {
	
	@Autowired
	ProjectMstService service;
	
	@Autowired
	OrganizationUnitMstService organizationUnitMstService;
	
//	@PostMapping("/save")
//	public ProjectMst save(@RequestBody ProjectMst object)
//	{
//		return service.saveOrUpDate(object);
//			
//	}

	public String validateObject(ProjectMst object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase("") || object.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Project. Organization Code is Blank";
		}
		if(null==object.getProjectName() || object.getProjectName().equalsIgnoreCase("") ||object.getProjectName().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Project. Name  is Blank";
		}
		if(null==object.getOrganizationUnit() ||object.getOrganizationUnit().equalsIgnoreCase("") || object.getOrganizationUnit().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Project. Organization unit  is Blank";
		}
		
		if(null==object.getBusinessUnit() ||object.getBusinessUnit().equalsIgnoreCase("") || object.getBusinessUnit().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Project. Business unit  is Blank";
		}
		
		
		return response+"~"+message;
	}

	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String projectCode=request.getParameter("projectCode");
		String projectName=request.getParameter("projectName");
		String organizationUnit=request.getParameter("organizationUnit");
		String process=request.getParameter("process");
		String project=request.getParameter("project");
		String tenantId=request.getParameter("tenantId");
		if(null==tenantId || tenantId.equalsIgnoreCase("") || tenantId.equalsIgnoreCase(" "))
		{
			tenantId=accessObject.getTenantId();
		}
		String isActive=request.getParameter("isActive");
		String authStatus=request.getParameter("authStatus");
		String businessUnit=request.getParameter("businessUnit");
		String mode=request.getParameter("mode");
		ModelMap model = new ModelMap();
		
		ProjectMst projectMst=new ProjectMst();
		projectMst.setProjectCode(projectCode);
		projectMst.setProjectName(projectName);
		projectMst.setOrganizationUnit(organizationUnit);
		projectMst.setProcess(process);
		projectMst.setProject(project);
		projectMst.setTenantId(tenantId);
		projectMst.setAuthStatus(authStatus);
		projectMst.setBusinessUnit(businessUnit);
		projectMst.setCmmiVersion(accessObject.getCmmiVersion());
		
		OrganizationUnitMst ou=new OrganizationUnitMst();
		ou=organizationUnitMstService.getUnique(accessObject.getTenantId(), projectMst.getOrganizationUnit(), ApplicationConstants.DEFAULT_process);
		if(null!=organizationUnit)
		{
			projectMst.setOrganizationModel(ou.getOrganizationModel());
		}

		if(null!=mode && mode.equalsIgnoreCase("DELETE"))
		{
			String resp="";
			resp=service.delete(projectMst, accessObject);
			if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
				accessObject.setMsg("SUCCESS~Project Deleted Successfully.");
				return new ModelAndView(
			    	       new RedirectView("/projectMstCrud", true),
			    	       model);
			}
			else
			{
				accessObject.setMsg(resp);
				return new ModelAndView(
			    	       new RedirectView("/projectMstCrud", true),
			    	       model);
			}
		}
		
		String validationResp="";
		validationResp=validateObject(projectMst);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
		service.saveOrUpDate(projectMst,accessObject);
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~Project Created Successfully.");	
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~Project Updated Successfully.");
		}
		
		return new ModelAndView(
	    	       new RedirectView("/projectMstCrud", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/projectMstCrud", true),
		    	       model);	
		}
		
		
	}

	


	
	
	
	
	
	
}
