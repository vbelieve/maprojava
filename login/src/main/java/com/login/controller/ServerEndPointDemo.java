package com.login.controller;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/endPoint")
public class ServerEndPointDemo {
	private static final Set<Session> sessions = Collections.synchronizedSet(new HashSet<Session>());
	@OnOpen
	public void handleOpen(Session session) throws IOException, EncodeException {
	
		System.out.println("Client is not connected..");
		sessions.add(session);
	}
	
	@OnClose
	public void handleClose(Session session)
	{
		System.out.println("Client is not Disconnected..");
		sessions.remove(session);
	}
	
	@OnError
	public void handleError(Throwable t)
	{
		t.printStackTrace();
	}
	
	@OnMessage
	public void handleMessgae(String message,Session session) throws IOException 
	{
		
		for (Session sess : sessions) {
			if (!sess.equals(session)) {
				sess.getBasicRemote().sendText(message);
			}
		}
	
	}
	

}
