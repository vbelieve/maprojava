package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.login.domain.ApplicationConstants;
import com.login.domain.QuestionMst;
import com.login.domain.basics.AccessObject;
import com.login.services.QuestionMstService;

@RequestMapping("/questionMst")
//@CrossOrigin(origins=GlobalConstants.angularPort)
//@CrossOrigin(origins="http://localhost:4200")
@RestController
public class QuestionMstController {
	
	@Autowired
	QuestionMstService service;
	
	
	
	@PostMapping("/save")
	public QuestionMst save(@RequestBody QuestionMst object)
	{
		AccessObject accessObject=new AccessObject();
		object.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		object.setIsActive(1);
		object.setTenantId(accessObject.getTenantId());
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase(""))
		{
			object.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		return service.saveOrUpDate(object);
			
	}
	
	
	
	

	@GetMapping("/{tenantId}")
	public List<QuestionMst> getByName(@PathVariable("tenantId")String tenantId){
		List<QuestionMst> questionMst=new ArrayList<QuestionMst>();
		questionMst=service.getAll(tenantId);
		return questionMst; 
	}
	
	@GetMapping("/{tenantId}/{questionId}")
	public QuestionMst getUnique(@PathVariable("tenantId")String tenantId,@PathVariable("questionId")String questionId,
			@PathVariable("process")String process){
		QuestionMst questionMst=new QuestionMst();
		questionMst=service.getUnique(tenantId, questionId,process);
		return questionMst; 
	}
	
	
	
	
}
