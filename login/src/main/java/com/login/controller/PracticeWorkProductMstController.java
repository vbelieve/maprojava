package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.RelationshipPracticeAreaPracticeVO;
import com.login.common.exceptions.RelationshipPracticeWorkProductVO;
import com.login.domain.ApplicationConstants;
import com.login.domain.PracticeMst;
import com.login.domain.PracticeWorkProductMst;
import com.login.domain.ProjectMst;
import com.login.domain.RelationMappingPracticeAreaPractice;
import com.login.domain.TenantMst;
import com.login.domain.basics.AccessObject;
import com.login.services.PracticeWorkProductMstService;
import com.login.services.ProjectMstService;
import com.login.services.TenantMstService;

@RequestMapping("/practiceWorkProductMst")
//@CrossOrigin(origins=GlobalConstants.angularPort)
//@CrossOrigin(origins="http://localhost:4200")
@RestController
public class PracticeWorkProductMstController {
	
	@Autowired
	PracticeWorkProductMstService service;
	
	@Autowired
	TenantMstService tenantMstService;
	
	@Autowired
	ProjectMstService projectMstService;
	
	@Autowired
	private HttpServletRequest request;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

	
	
//	@PostMapping("/save")
//	public PracticeWorkProductMst save(@RequestBody PracticeWorkProductMst object)
//	{
//		AccessObject accessObject=new AccessObject();
//		object.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
//		object.setIsActive(1);
//		object.setTenantId(accessObject.getTenantId());
//		object.setWorkProductId(object.getWorkProductId().replace("WP", ""));
//		return service.saveOrUpDate(object);
//			
//	}
	
	public String validateCategory(PracticeWorkProductMst object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getPracticeCode() || object.getPracticeCode().equalsIgnoreCase("") || object.getPracticeCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Practice Work product. Practice Code is Blank";
		}
		if(null==object.getWorkProductName() || object.getWorkProductName().equalsIgnoreCase("") || object.getWorkProductName().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Practice Work product. Work Product Name  is Blank";
		}
		
		
		return response+"~"+message;
	}
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();

		String tenantId=request.getParameter("tenantId");
		String practiceCode=request.getParameter("practiceCode");
		String workProduct=request.getParameter("workProduct");
		String mode=request.getParameter("mode");
		PracticeWorkProductMst practiceWorkProductMst=new PracticeWorkProductMst();
		practiceWorkProductMst.setTenantId(tenantId);
		practiceWorkProductMst.setPracticeCode(practiceCode);
		practiceWorkProductMst.setWorkProductName(workProduct);
		practiceWorkProductMst.setCmmiVersion(accessObject.getCmmiVersion());
		if(null!=mode && mode.equalsIgnoreCase("DELETE"))
		{
			String resp="";
			if(accessObject.getTenantId().equalsIgnoreCase(ApplicationConstants.DEFAULT_TENANT))
			{
				List<TenantMst> allTenants=new ArrayList<TenantMst>();
				allTenants=tenantMstService.getAll();
				for(TenantMst tenant:allTenants)
				{
					if(tenant.getTenantId().equalsIgnoreCase(ApplicationConstants.DEFAULT_TENANT))
					{
						practiceWorkProductMst.setTenantId(ApplicationConstants.DEFAULT_TENANT);
						practiceWorkProductMst.setProject("");
						practiceWorkProductMst.setCmmiVersion(accessObject.getCmmiVersion());
						resp=service.delete(practiceWorkProductMst, accessObject,accessObject.getCmmiVersion());
					}
					else
					{
						List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
						allProjects=projectMstService.getAllProject(tenant.getTenantId());
								{
									if(null!=allProjects && allProjects.size()>0)
									{
							for(ProjectMst projects:allProjects)
							{
								practiceWorkProductMst.setTenantId(tenant.getTenantId());
								practiceWorkProductMst.setProject(projects.getProjectCode());
								practiceWorkProductMst.setCmmiVersion(accessObject.getCmmiVersion());
								resp=service.delete(practiceWorkProductMst, accessObject,accessObject.getCmmiVersion());
							}
									}
								}
					}
				}
			}
			
			
			if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
				accessObject.setMsg("SUCCESS~Relation Deleted Successfully.");
				
				return new ModelAndView(
			    	       new RedirectView("/relationMappingPracticeWorkProductMstCrud", true),
			    	       model);
			}
			else
			{
				accessObject.setMsg(resp);
				return new ModelAndView(
			    	       new RedirectView("/relationMappingPracticeWorkProductMstCrud", true),
			    	       model);
			}
		}
		String validationResp="";
		validationResp=validateCategory(practiceWorkProductMst);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
		if(accessObject.getTenantId().equalsIgnoreCase(ApplicationConstants.DEFAULT_TENANT))
		{
			List<TenantMst> allTenants=new ArrayList<TenantMst>();
			allTenants=tenantMstService.getAll();
			for(TenantMst tenant:allTenants)
			{
				if(tenant.getTenantId().equalsIgnoreCase(ApplicationConstants.DEFAULT_TENANT))
				{
					practiceWorkProductMst.setTenantId(ApplicationConstants.DEFAULT_TENANT);
					practiceWorkProductMst.setProject("");
					practiceWorkProductMst.setCmmiVersion(accessObject.getCmmiVersion());
					service.saveOrUpDate(practiceWorkProductMst,accessObject,accessObject.getCmmiVersion());
				}
				else
				{
					List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
					allProjects=projectMstService.getAllProject(tenant.getTenantId());
							{
								if(null!=allProjects && allProjects.size()>0)
								{
						for(ProjectMst projects:allProjects)
						{
							practiceWorkProductMst.setTenantId(tenant.getTenantId());
							practiceWorkProductMst.setProject(projects.getProjectCode());
							practiceWorkProductMst.setCmmiVersion(accessObject.getCmmiVersion());
							service.saveOrUpDate(practiceWorkProductMst,accessObject,accessObject.getCmmiVersion());
						}
								}
							}
				}
			}
		}
		else
		{
			service.saveOrUpDate(practiceWorkProductMst,accessObject,accessObject.getCmmiVersion());	
		}
		
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~Practice Work Product Created Successfully.");	
			
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~Practice Work Product Updated Successfully.");
			
		}
		
		return new ModelAndView(
	    	       new RedirectView("/relationMappingPracticeWorkProductMstCrud", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/relationMappingPracticeWorkProductMstCrud", true),
		    	       model);	
		}
	
	}
	
	
	

	@GetMapping("/{tenantId}")
	public List<PracticeWorkProductMst> getByName(@PathVariable("tenantId")String tenantId){
		AccessObject accessObject=getAccessObject();
		List<PracticeWorkProductMst> practiceWorkProductMst=new ArrayList<PracticeWorkProductMst>();
		practiceWorkProductMst=service.getAll(tenantId,accessObject.getCmmiVersion());
		return practiceWorkProductMst; 
	}
	

	@RequestMapping(value = "/getAjax", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RelationshipPracticeWorkProductVO getAjax() {
		RelationshipPracticeWorkProductVO vo=new RelationshipPracticeWorkProductVO();
		AccessObject accessObject=getAccessObject();
		String practiceCode = "";
		String workProduct = "";
		String practiceAreaCode="";
		practiceCode= request.getParameter("practiceCode");
		workProduct= request.getParameter("workProduct");
		practiceAreaCode=request.getParameter("practiceAreaCode");
		PracticeWorkProductMst practiceWorkProductMst=service.getUnique(accessObject.getTenantId(),practiceCode,
				workProduct,
				accessObject.getProject(),accessObject.getCmmiVersion());
		if(null!=practiceWorkProductMst)
		{
			vo.setPracticeCode(practiceWorkProductMst.getPracticeCode());
			vo.setWorkProduct(practiceWorkProductMst.getWorkProductName());
		}
		return vo;
	}
	
	
	
	
	
}
