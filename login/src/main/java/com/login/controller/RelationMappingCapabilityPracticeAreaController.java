package com.login.controller;

import com.login.services.RelationMappingCapabilityPracticeAreaService;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.RelationshipCapabilityPracticeAreaVO;
import com.login.common.exceptions.RelationshipCategoryCapabilityVO;
import com.login.domain.ApplicationConstants;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.basics.AccessObject;

@RequestMapping("/relationMappingCapabilityPracticeArea")
//@CrossOrigin(origins="http://localhost:4200")
@RestController
public class RelationMappingCapabilityPracticeAreaController {
	
	@Autowired
	RelationMappingCapabilityPracticeAreaService service;
	
	@Autowired
	private HttpServletRequest request;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

//	@PostMapping("/save")
//	public RelationMappingCapabilityPracticeArea save(@RequestBody RelationMappingCapabilityPracticeArea object)
//	{
//		
//		return service.saveOrUpDate(object);
//			
//	}
	
	public String validateCategory(RelationMappingCapabilityPracticeArea object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getPracticeAreaCode() || object.getPracticeAreaCode().equalsIgnoreCase("") || object.getPracticeAreaCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Relation. PracticeArea  is Blank";
		}
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase("") || object.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Relation. TenantId  is Blank";
		}
		if(null==object.getCapabilityCode() || object.getCapabilityCode().equalsIgnoreCase("") || object.getCapabilityCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Relation. Capability  is Blank";
		}
		
		return response+"~"+message;
	}
	
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		
		String tenantId=request.getParameter("tenantId");
		String capabilityCode=request.getParameter("capabilityCode");
		String practiceAreaCode=request.getParameter("practiceAreaCode");
		
		String mode=request.getParameter("mode");
		String[] allPracticeAreaCode=practiceAreaCode.split(",");
		
		
		for(String pArea:allPracticeAreaCode)
		{
			RelationMappingCapabilityPracticeArea relationMappingCapabilityPracticeArea=new RelationMappingCapabilityPracticeArea();
			relationMappingCapabilityPracticeArea.setTenantId(accessObject.getTenantId());
			relationMappingCapabilityPracticeArea.setCapabilityCode(capabilityCode);
			relationMappingCapabilityPracticeArea.setPracticeAreaCode(pArea);
			relationMappingCapabilityPracticeArea.setProcess(accessObject.getProcess());
			relationMappingCapabilityPracticeArea.setOrganizationUnit(accessObject.getOrganizationUnit());
			relationMappingCapabilityPracticeArea.setBusinessUnit(accessObject.getBusinessUnit());
			relationMappingCapabilityPracticeArea.setProject(accessObject.getProject());
			relationMappingCapabilityPracticeArea.setCmmiVersion(accessObject.getCmmiVersion());
			
			if(null!=mode && mode.equalsIgnoreCase("DELETE"))
			{
				String resp="";
				resp=service.delete(relationMappingCapabilityPracticeArea, accessObject,accessObject.getCmmiVersion());
				if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
				{
					accessObject.setMsg("SUCCESS~Relation Deleted Successfully.");
					
					return new ModelAndView(
				    	       new RedirectView("/relationMappingCapabilityPracticeAreaCrud", true),
				    	       model);
				}
				else
				{
					accessObject.setMsg(resp);
					return new ModelAndView(
				    	       new RedirectView("/relationMappingCapabilityPracticeAreaCrud", true),
				    	       model);
				}
			}
			
			String validationResp="";
			validationResp=validateCategory(relationMappingCapabilityPracticeArea);
			if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
			service.saveOrUpDate(relationMappingCapabilityPracticeArea,accessObject,accessObject.getCmmiVersion());
			if(mode.equalsIgnoreCase("CREATE"))
			{
				accessObject.setMsg("SUCCESS~Relation Created Successfully.");	
				
			}
			if(mode.equalsIgnoreCase("EDIT"))
			{
				accessObject.setMsg("SUCCESS~Relation Updated Successfully.");
				
			}
			
			
			}
			else
			{
				
				accessObject.setMsg(validationResp);
				return new ModelAndView(
			    	       new RedirectView("/relationMappingCapabilityPracticeAreaCrud", true),
			    	       model);	
			}
			
		}
		
		return new ModelAndView(
	    	       new RedirectView("/relationMappingCapabilityPracticeAreaCrud", true),
	    	       model);
		
	}
	
	
	@GetMapping("/getByPracticeArea/{tenantId}/{practiceAreaCode:.+}")
	public List<RelationMappingCapabilityPracticeArea> getByPracticeArea(@PathVariable("tenantId")String tenantId, @PathVariable("practiceAreaCode") String practiceAreaCode)
	{
		AccessObject accessObject=getAccessObject();
		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea=new ArrayList<RelationMappingCapabilityPracticeArea>();
		relationMappingCapabilityPracticeArea=(List<RelationMappingCapabilityPracticeArea>) service.getByPracticeArea(tenantId, practiceAreaCode,accessObject.getCmmiVersion());
						return relationMappingCapabilityPracticeArea;
	}

	
	
	@GetMapping("/{tenantId}")
	public List<RelationMappingCapabilityPracticeArea> getall(@PathVariable("tenantId")String tenantId)
	{
		AccessObject accessObject=getAccessObject();
		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea=new ArrayList<RelationMappingCapabilityPracticeArea>();
		relationMappingCapabilityPracticeArea=(List<RelationMappingCapabilityPracticeArea>) service.getAll(tenantId,accessObject.getCmmiVersion());
						return relationMappingCapabilityPracticeArea;
	}

	@RequestMapping(value = "/getAjax", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RelationshipCapabilityPracticeAreaVO getAjax() {
		RelationshipCapabilityPracticeAreaVO vo=new RelationshipCapabilityPracticeAreaVO();
		AccessObject accessObject=getAccessObject();
		String capabilityCode = "";
		String practiceAreaCode = "";
		capabilityCode= request.getParameter("capabilityCode");
		practiceAreaCode = request.getParameter("practiceAreaCode");
		RelationMappingCapabilityPracticeArea relationMappingCapabilityPracticeArea=service.getUnique(accessObject.getTenantId(),
				capabilityCode,		practiceAreaCode, accessObject.getProcess(),
				accessObject.getOrganizationUnit(),
				accessObject.getBusinessUnit(),
				accessObject.getProject(),accessObject.getCmmiVersion());
		if(null!=relationMappingCapabilityPracticeArea)
		{
			
			vo.setCapabilityCode(relationMappingCapabilityPracticeArea.getCapabilityCode());;
			vo.setPracticeAreaCode(relationMappingCapabilityPracticeArea.getPracticeAreaCode());;
		}
		return vo;
	}
	
	
	
}
