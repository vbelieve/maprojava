package com.login.controller;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.sl.usermodel.PaintStyle;
import org.apache.poi.sl.usermodel.TableCell.BorderEdge;
import org.apache.poi.sl.usermodel.TextParagraph.TextAlign;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTable;
import org.apache.poi.xslf.usermodel.XSLFTableCell;
import org.apache.poi.xslf.usermodel.XSLFTableRow;
import org.apache.poi.xslf.usermodel.XSLFTextBox;
import org.apache.poi.xslf.usermodel.XSLFTextParagraph;
import org.apache.poi.xslf.usermodel.XSLFTextRun;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder.BorderSide;
import org.apache.struts2.views.util.ResourceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.ApplicationConstants;
import com.login.domain.BusinessUnitMst;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.DashboardVO;
import com.login.domain.LookupMst;
import com.login.domain.OrganizationUnitMst;
import com.login.domain.PiidMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.PracticeWorkProductMst;
import com.login.domain.ProjectMst;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.RelationMappingPracticeAreaPractice;
import com.login.domain.SequenceMst;
import com.login.domain.TenantMst;
import com.login.domain.UserMst;
import com.login.domain.WorkProductMst;
import com.login.domain.basics.AccessObject;
import com.login.domain.basics.DashBoardVo;
import com.login.services.BusinessUnitMstService;
import com.login.services.CapabilityMstService;
import com.login.services.CategoryMstService;
import com.login.services.LookupMstService;
import com.login.services.OrganizationUnitMstService;
import com.login.services.PiidMstService;
import com.login.services.PracticeAreaMstService;
import com.login.services.PracticeMstService;
import com.login.services.PracticeWorkProductMstService;
import com.login.services.ProjectMstService;
import com.login.services.RelationMappingCapabilityPracticeAreaService;
import com.login.services.RelationMappingCategoryCapabilityService;
import com.login.services.RelationMappingPracticeAreaPracticeService;
import com.login.services.SequenceMstService;
import com.login.services.TenantMstService;
import com.login.services.UserMstService;
import com.login.services.WorkProductMstService;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message; 

@Controller
@Scope("session")


public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
    public static final String ACCOUNT_SID = "ACd03c253941472889ea634ee566ce5115"; 
    public static final String AUTH_TOKEN = "dca51168d0dbe8a1a770b3490c3ce48e"; 

	private static final boolean PracticeAreaMst = false;
	SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
	@Autowired
	PracticeWorkProductMstService practiceWorkProductMstService;
	
	@Autowired
	WorkProductMstService workProductMstService; 

	@Autowired
	private HttpServletRequest request;
	
	 @Autowired
	ServletContext servletContext;
	 
	@Autowired
	UserMstService userMstService;
	
	@Autowired
	PracticeMstService practiceMstService;

	@Autowired
	SequenceMstService sequenceMstService;
	

	@Autowired
	RelationMappingCategoryCapabilityService relationMappingCategoryCapabilityService;
	
	@Autowired
	RelationMappingCapabilityPracticeAreaService relationMappingCapabilityPracticeAreaService;
	
	@Autowired
	TenantMstService tenantMstService;
	
	
	
	@Autowired
	CategoryMstService categoryMstService;
	
	@Autowired
	CapabilityMstService capabilityMstService;
	
	@Autowired
	BusinessUnitMstService businessUnitMstService;
	
	@Autowired
	OrganizationUnitMstService organizationUnitMstService;
	
	@Autowired
	ProjectMstService projectMstService;
	
	
	@Autowired
	PracticeAreaMstService practiceAreaMstService;
	

	@Autowired
	PiidMstService piidMstService;
	
	@Autowired
	RelationMappingPracticeAreaPracticeService relationMappingPracticeAreaPracticeService;
	
	
	@Autowired
	LookupMstService lookupMstService;

	
	
	
	
	@RequestMapping("/")
	public String home(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=new AccessObject();
		model.put("message", "HowToDoInJava Reader !!");
		List<LookupMst> versionLookup=new ArrayList<LookupMst>();
		versionLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_CMMIVERSION);
		request.setAttribute("versionLookup",versionLookup);
		request.getSession().setAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT,accessObject);
		return "login";
	}
	
	@RequestMapping("/userProcessHomePage")
	public String userProcessHomePage(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		model.put("message", "HowToDoInJava Reader !!");
		return "HomePage";
	}

	@RequestMapping("/sendWhatsApp")
	public String sendWhatsApp(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		  Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		  Message message = Message.creator( 
	                new com.twilio.type.PhoneNumber("+919930227938"),new com.twilio.type.PhoneNumber("+16085711848"),  
	                 "This is Gap Analysis Test Message")      
	            .create(); 
	        System.out.println(message.getSid()); 

		model.put("message", "HowToDoInJava Reader !!");
		return "HomePage";
	}
	
	
	@RequestMapping("/webVideoCall")
	public String webVideoCall(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		model.put("message", "HowToDoInJava Reader !!");
		return "webVideoCall";
	}

	@RequestMapping("/webScreenShare")
	public String webScreenShare(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		model.put("message", "HowToDoInJava Reader !!");
		return "webScreenShare";
	}
	
	
	@RequestMapping("/generateGapSummaryReport")
	public void generateGapSummaryReport(String project,HttpServletRequest request,HttpServletResponse response) throws IOException {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Report");
		if(null==project)
		{
			project=accessObject.getProject();
		}
		getReport(accessObject,project,request,response,"Summary");
	}
	
	@RequestMapping("/generateProjectSummaryReport")
	public void generateProjectSummaryReport(String project,HttpServletRequest request,HttpServletResponse response) throws IOException {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		if(null==project)
		{
			project=accessObject.getProject();
		}
		getReport(accessObject,project,request,response,"Summary");
	}
	
	@RequestMapping("/generatePiidReport")
	public void generatePiidReport(String project,HttpServletRequest request,HttpServletResponse response) throws IOException {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		if(null==project)
		{
			project=accessObject.getProject();
		}
		getPIIDReport(accessObject,project,request,response);
	}
	
	@RequestMapping("/generatePPT")
	public void generatePPT(String project,HttpServletRequest request,HttpServletResponse response) throws IOException {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		if(null==project)
		{
			project=accessObject.getProject();
		}
		//generatePPT(accessObject,project,request,response);
		getReport(accessObject,project,request,response,"PPT");
	}
	
	private static void copyRowToRow(Row row, Row newRow) {
	    Iterator<Cell> cellIterator = row.cellIterator();
	    int cellIndex = 0;
	    while(cellIterator.hasNext()) {
	        Cell cell = cellIterator.next();
	        Cell newCell = newRow.createCell(cellIndex);
	        newCell.setCellValue(cell.getStringCellValue());
	        cellIndex++;
	    }
	}
	
	public String getPIIDReport(AccessObject accessObject,String project,HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		ProjectMst proj=projectMstService.getProjectByProjectCode(accessObject.getTenantId(), project);
		List<PracticeWorkProductMst> practiceAllWorkProducts=new ArrayList<PracticeWorkProductMst>();
		practiceAllWorkProducts=practiceWorkProductMstService.getByTenantAndProject(proj.getTenantId(), project,proj.getCmmiVersion());
		List<PiidMst>allWorkProducts=new ArrayList<PiidMst>();
		List<PiidMst>allWorkProductsPiid=new ArrayList<PiidMst>();
		List<PiidMst>allWorkProductsPractice=new ArrayList<PiidMst>();
		OrganizationUnitMst orgUnit=new OrganizationUnitMst();
		orgUnit=organizationUnitMstService.getUnique(proj.getTenantId(), proj.getOrganizationUnit(), accessObject.getProcess());
		List<PracticeAreaMst> practiceAreaListBase=new ArrayList<PracticeAreaMst>();
		HashMap<String,String> practiceAreaListMap=new HashMap<String,String>();
		practiceAreaListBase=accessObject.getPracticeAreaMst();
		String[] businessModel=orgUnit.getOrganizationModel().split(",");

		for(PracticeWorkProductMst pwp:practiceAllWorkProducts)
		{
		//	if(null!=pwp.getQ3() && !pwp.getQ3().equalsIgnoreCase("") && !pwp.getQ3().equalsIgnoreCase(" "))
			//{
				PiidMst piid=new PiidMst();
				piid.setTenantId(pwp.getTenantId());
				piid.setProject(pwp.getProject());
				piid.setPracticeAreaCode(pwp.getPracticeAreaCode());
				piid.setPracticeCode(pwp.getPracticeCode());
				piid.setArtifect(pwp.getQ3());
				PracticeMst pp=practiceMstService.getUnique(pwp.getTenantId(), 
						pwp.getPracticeCode(), pwp.getProcess(),
						pwp.getOrganizationUnit(), pwp.getBusinessUnit(), pwp.getProject(),accessObject.getCmmiVersion());
				if(null!=pp)
				{
					piid.setPracticeDescription(pp.getPracticeName());	
				}
				allWorkProducts.add(piid);
				
			//}
		}
		allWorkProductsPiid=piidMstService.getByTenantAndProject(proj.getTenantId(), project);
		if(null!=allWorkProductsPiid && allWorkProductsPiid.size()>0)
		{
		for(PiidMst piidList2:allWorkProductsPiid)
		{
			allWorkProducts.add(piidList2);
		}
		}
		Collections.sort(allWorkProducts, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PiidMst bal1 = (PiidMst) o1;
				final PiidMst bal2 = (PiidMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				if(c==0)
				{
					c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				}
				
				return c;
			}
		});
		
		for(String bm:businessModel)
		{
		for(PracticeAreaMst practiceArea:practiceAreaListBase)
		{
			if(practiceArea.getBusinessModel().contains(bm))
			{
				
				practiceAreaListMap.put(practiceArea.getPracticeAreaCode(),practiceArea.getPracticeAreaName());
//				String myDynamicalyCreatedName = "wp" + practiceArea.getPracticeAreaCode();
//				List<PracticeWorkProductMst> $myDynamicalyCreatedName = new ArrayList<PracticeWorkProductMst>();
//				
			}
		}
		}
		
		
		
		String TEMPLATE_PATH = servletContext.getRealPath("//templates");
		String TEMPLATE_GapSummaryReport = TEMPLATE_PATH + "PIID_TEMPLATE.xlsx";
		File destinationfile = new File(TEMPLATE_GapSummaryReport);
		FileInputStream fis= new FileInputStream((destinationfile));
		XSSFWorkbook workbook =null;
		workbook=new XSSFWorkbook (fis);

		Integer sheetNo=2;
		for(Map.Entry<String, String> set : practiceAreaListMap.entrySet())
		{
			workbook.cloneSheet(0);
		}
		for(Map.Entry<String, String> set : practiceAreaListMap.entrySet())
		{
			String worksheet=set.getKey();
			String practiceAreaName=set.getValue();
		
			
			workbook.setSheetName(sheetNo, worksheet);
			CellStyle style = workbook.createCellStyle(); //Create new style
            style.setWrapText(true);
			XSSFSheet sheet= workbook.getSheet(worksheet);
			
			int rownum=0;
			int endRow=0;
			int cellno=0;
			int endCell=0;
			Row row = sheet.createRow(rownum);
			Cell cell = row.createCell(cellno);
			setCellVaue(practiceAreaName, cell);
			rownum=3;
			cellno=0;
			for(PiidMst piidMst:allWorkProducts)
			{
			if(piidMst.getPracticeAreaCode().equalsIgnoreCase(worksheet))
			{
			//Populate List of PracticeCode
				String practiceLevelCode="";
				String practiceName="";
				practiceLevelCode=piidMst.getLevel();
				practiceName=piidMst.getPracticeDescription();
			
				row = sheet.createRow(rownum);
				cell = row.createCell(cellno);
			
				setCellVaue(practiceLevelCode, cell);
				cell.setCellStyle(style);
				cellno++;
			
				cell = row.createCell(cellno);
				setCellVaue(piidMst.getPracticeCode(), cell);
				cell.setCellStyle(style);
				cellno++;
				
				cell = row.createCell(cellno);
				setCellVaue(practiceName, cell);
				sheet.setColumnWidth(cellno, 18000);
				cell.setCellStyle(style);
				
				cellno++;
				cell = row.createCell(cellno);
				setCellVaue(piidMst.getArtifect(), cell);
				sheet.setColumnWidth(cellno, 18000);
				cell.setCellStyle(style);
				endRow=rownum;
				endCell=cellno;
				cellno=0;
				rownum++;
			//Populate List of PracticeCode	
			}
			}
//			sheet.autoSizeColumn(0);
//			sheet.autoSizeColumn(1);
//			sheet.autoSizeColumn(2);
//			sheet.autoSizeColumn(3);
//			sheet.setAutoFilter(new CellRangeAddress(2, endRow, cellno, 9));

			for(int col=0;col<3;col++)
			{
			String prevVal="";
			String updateVal="";
			int startRows=0;
			int endRows=0;
			int totalRows=sheet.getPhysicalNumberOfRows();
			for(int i=3;i<sheet.getPhysicalNumberOfRows();i++)
			{
			
				String val=sheet.getRow(i).getCell(col).getStringCellValue();
				if(val.equalsIgnoreCase(prevVal))
				{
					endRows=i;
					updateVal=val;
					if(i==totalRows-1)
					{
						if(!updateVal.equalsIgnoreCase(""))
						{
							sheet.addMergedRegion(new CellRangeAddress(startRows,endRows, col, col));
							updateVal="";
						}
					}
				}
				else
				{
					if(!updateVal.equalsIgnoreCase(""))
					{
						sheet.addMergedRegion(new CellRangeAddress(startRows,endRows, col, col));
						updateVal="";
					}
					startRows=i;
					endRows=i;
					prevVal=val;
					
				}
			}
			}
			sheetNo=sheetNo+1;
		}
		workbook.removeSheetAt(0);
		//XSSFSheet sheet= workbook.getSheet(ApplicationConstants.REPORT_PIIDSHEET);
		XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
		InputStream in = null;
		
		OutputStream outstream = null;
		PrintWriter out = null;
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("content-disposition",
				"attachment; filename=PIIDReport_"+proj.getProject()+".xlsx");
		
		outstream = response.getOutputStream();
		workbook.write(outstream);
		IOUtils.closeQuietly(outstream);
		IOUtils.closeQuietly(fis);
		IOUtils.closeQuietly(out);
		outstream.flush();
		outstream.close();
	
//		model.put("message", "HowToDoInJava Reader !!");
		return "HomePage";

	}

	
	public String generatePPT(AccessObject accessObject,String project,HttpServletRequest request,HttpServletResponse response,List<PracticeMst> practiceList) throws IOException
	{

for(PracticeMst practice:practiceList)
{
	practice.setPriority(23);
	if(practice.getPracticeAreaCode().contains("RDM"))
	{
		practice.setPriority(1);	
	}
	if(practice.getPracticeAreaCode().contains("PQA"))
	{
		practice.setPriority(2);	
	}
	if(practice.getPracticeAreaCode().contains("VV"))
	{
		practice.setPriority(3);	
	}
	if(practice.getPracticeAreaCode().contains("PR"))
	{
		practice.setPriority(4);	
	}
	if(practice.getPracticeAreaCode().contains("SDM"))
	{
		practice.setPriority(5);	
	}
	if(practice.getPracticeAreaCode().contains("STSM"))
	{
		practice.setPriority(6);	
	}
	if(practice.getPracticeAreaCode().contains("SAM"))
	{
		practice.setPriority(7);	
	}
	if(practice.getPracticeAreaCode().contains("EST"))
	{
		practice.setPriority(8);	
	}
	if(practice.getPracticeAreaCode().contains("PLAN"))
	{
		practice.setPriority(9);	
	}
	if(practice.getPracticeAreaCode().contains("MC"))
	{
		practice.setPriority(10);	
	}
	if(practice.getPracticeAreaCode().contains("RSK"))
	{
		practice.setPriority(11);	
	}
	if(practice.getPracticeAreaCode().contains("IRP"))
	{
		practice.setPriority(12);	
	}
	if(practice.getPracticeAreaCode().contains("IRP"))
	{
		practice.setPriority(13);	
	}
	if(practice.getPracticeAreaCode().contains("CONT"))
	{
		practice.setPriority(14);	
	}
	if(practice.getPracticeAreaCode().contains("OT"))
	{
		practice.setPriority(15);	
	}
	if(practice.getPracticeAreaCode().contains("CAR"))
	{
		practice.setPriority(16);	
	}
	if(practice.getPracticeAreaCode().contains("DAR"))
	{
		practice.setPriority(17);	
	}
	if(practice.getPracticeAreaCode().contains("CM"))
	{
		practice.setPriority(18);	
	}
	if(practice.getPracticeAreaCode().contains("PCM"))
	{
		practice.setPriority(19);	
	}
	if(practice.getPracticeAreaCode().contains("PAD"))
	{
		practice.setPriority(20);	
	}
	if(practice.getPracticeAreaCode().contains("MPM"))
	{
		practice.setPriority(21);	
	}
	if(practice.getPracticeAreaCode().contains("GOV"))
	{
		practice.setPriority(22);	
	}
	if(practice.getPracticeAreaCode().contains("II"))
	{
		practice.setPriority(23);	
	}

}

Collections.sort(practiceList, new Comparator() {
	public int compare(final Object o1, final Object o2) {

		final PracticeMst bal1 = (PracticeMst) o1;
		final PracticeMst bal2 = (PracticeMst) o2;

		int c;
		
		c=bal1.getPriority().compareTo(bal2.getPriority());
		
		return c;
	}
});
		
		String TEMPLATE_PATH = servletContext.getRealPath("//templates");
		String TEMPLATE_GapSummaryReport = TEMPLATE_PATH + "pptTemplate2.pptx";
		
		File destinationfile = new File(TEMPLATE_GapSummaryReport);
		FileInputStream fis= new FileInputStream((destinationfile));
		ProjectMst proj=projectMstService.getProjectByProjectCode(accessObject.getTenantId(), project);
		//List<PracticeMst> practiceList=practiceMstService.getPracticeByTenantProject(proj.getTenantId(), proj.getProjectCode(), accessObject);
		Color blue = (new java.awt.Color(51,89,157));
		Color amber = (new java.awt.Color(255,191,0));
		
		Color blue_light1 = (new java.awt.Color(142,169,219));
		Color blue_25 =(new java.awt.Color(183,200,231));
		Color orange = (new java.awt.Color(252,219,35));
		Color yellow = (new java.awt.Color(251,255,35));
		Color green = (new java.awt.Color(31,226,31));
		Color red = (new java.awt.Color(255,0,0));
		
		XMLSlideShow ppt = new XMLSlideShow();
		ppt=new XMLSlideShow (fis);
		List<XSLFSlide> slides = ppt.getSlides();
		XSLFSlide selectesdslide = slides.get(0);
		
		// Change for Label 
		XSLFTextBox shape = selectesdslide.createTextBox();
		shape.setAnchor(new Rectangle2D.Double(650,340,200 ,0));
		XSLFTextParagraph para = shape.addNewTextParagraph();
		XSLFTextRun r1 = para.addNewTextRun();
		r1.setText(accessObject.getOrgName());
		r1.setFontColor(Color.black);
		r1.setBold(Boolean.TRUE);
		r1.setFontFamily("Calibri, sans-serif");
		r1.setFontSize(18d);
		Integer slideOrder=9;
		XSLFSlide slide=ppt.createSlide();
		ppt.setSlideOrder(slide, slideOrder);
		ppt.removeSlide(slideOrder);

		slideOrder=slideOrder+1;
	//	slide.d
		Color lightBlue=new java.awt.Color(51,153,255);
		Color lightGrey=new java.awt.Color(218,227,243);
		

		XSLFTable table=slide.createTable();// Create a table

//		table.setAnchor(new Rectangle2D.Double(1,1,0 ,0));
	
		int idx=0;
int columnsSize=3;
String previousPracticeArea="";
String previousCap="";
Integer previousPracticeLevel=0;
for(PracticeMst practice:practiceList)
{
if(!practice.getPracticeAreaCode().equals(previousPracticeArea))
{
	previousPracticeArea=practice.getPracticeAreaCode();
	
	if(!practice.getCapabilityCode().equalsIgnoreCase(previousCap))
	{
//		ppt.setSlideOrder(slides.get(1),slideOrder);
	}
	  slide = ppt.createSlide();
	  ppt.setSlideOrder(slide, slideOrder);
		slideOrder=slideOrder+1;
	  table = slide.createTable();// Create a table
	
	table.setAnchor(new Rectangle2D.Double(1,1, 0,0));

	XSLFTableRow tableRow = table.addRow();
	tableRow.setHeight(30d);
	
	
	XSLFTableCell tableCell = tableRow.addCell();// Create a table cell
	tableCell.setWordWrap(Boolean.TRUE);
	
	tableCell.setFillColor(lightBlue);
	XSLFTextParagraph p = tableCell.addNewTextParagraph();
	p.setTextAlign(TextAlign.CENTER);
    XSLFTextRun tr = p.addNewTextRun();
    tr.setFontSize(20d);
    
    tr.setBold(true);
    tr.setFontFamily("Calibri (Body)");
    tr.setFontColor(Color.WHITE);
    tr.setText("Practice Area");
    
     tableCell = tableRow.addCell();// Create a table cell
     tableCell.setFillColor(lightBlue);
     p = tableCell.addNewTextParagraph();
     p.setTextAlign(TextAlign.CENTER);
     tr = p.addNewTextRun();
    tr.setFontSize(20d);
    tr.setBold(true);
    tr.setFontFamily("Calibri (Body)");
    tr.setFontColor(Color.WHITE);
    tr.setText("Capability Area");
    
     tableCell = tableRow.addCell();// Create a table cell
     tableCell.setFillColor(lightBlue);
	 p = tableCell.addNewTextParagraph();
	 p.setTextAlign(TextAlign.CENTER);
     tr = p.addNewTextRun();
    tr.setFontSize(20d);
    tr.setBold(true);
    tr.setFontFamily("Calibri (Body)");
    tr.setFontColor(Color.WHITE);
    tr.setText("Category");
    
    tableRow = table.addRow();
	tableRow.setHeight(30d);
	tableCell = tableRow.addCell();
	p = tableCell.addNewTextParagraph();
     p.setTextAlign(TextAlign.CENTER);
     tr = p.addNewTextRun();
    tr.setFontSize(14d);
    
    tr.setFontFamily("Calibri (Body)");
    tr.setText(practice.getPracticeAreaName()+" ("+practice.getPracticeAreaCode()+")");
    
    tableCell = tableRow.addCell();
	p = tableCell.addNewTextParagraph();
     p.setTextAlign(TextAlign.CENTER);
     tr = p.addNewTextRun();
    tr.setFontSize(14d);
    
    tr.setFontFamily("Calibri (Body)");
    tr.setText(practice.getCapabilityName()+" ("+practice.getCapabilityCode()+")");
    
    tableCell = tableRow.addCell();
	p = tableCell.addNewTextParagraph();
     p.setTextAlign(TextAlign.CENTER);
     tr = p.addNewTextRun();
    tr.setFontSize(14d);
    
    tr.setFontFamily("Calibri (Body)");
    tr.setText(practice.getCategoryCode());
    

    tableRow = table.addRow();
	tableRow.setHeight(30d);
	tableCell = tableRow.addCell();
	tableCell.setFillColor(lightGrey);
	p = tableCell.addNewTextParagraph();
     p.setTextAlign(TextAlign.CENTER);
     tr = p.addNewTextRun();
    tr.setFontSize(18d);
    
    tr.setFontFamily("Calibri (Body)");
    tr.setText("Intent");
    
    tableCell = tableRow.addCell();
    tableCell = tableRow.addCell();
	
    table.mergeCells(2, 2, 0,2);


    tableRow = table.addRow();
	tableRow.setHeight(30d);
	tableCell = tableRow.addCell();
	p = tableCell.addNewTextParagraph();
     p.setTextAlign(TextAlign.CENTER);
     tr = p.addNewTextRun();
    tr.setFontSize(14d);
    
    tr.setFontFamily("Calibri (Body)");
    tr.setText(practice.getDescription());
    tr.setFontColor(Color.red);
    tableCell = tableRow.addCell();
    tableCell = tableRow.addCell();

    table.mergeCells(3, 3, 0,2);

    tableRow = table.addRow();
	tableRow.setHeight(30d);
	tableCell = tableRow.addCell();
	tableCell.setFillColor(lightGrey);
	p = tableCell.addNewTextParagraph();
     p.setTextAlign(TextAlign.CENTER);
     tr = p.addNewTextRun();
    tr.setFontSize(18d);
    
    tr.setFontFamily("Calibri (Body)");
    tr.setText("As-Is Practices");
    
    tableCell = tableRow.addCell();
    tableCell = tableRow.addCell();
	
    table.mergeCells(4, 4, 0,2);

    tableRow = table.addRow();
	tableRow.setHeight(30d);
	tableCell = tableRow.addCell();
	p = tableCell.addNewTextParagraph();
     p.setTextAlign(TextAlign.CENTER);
     tr = p.addNewTextRun();
    tr.setFontSize(14d);
    
    tr.setFontFamily("Calibri (Body)");
    tr.setText(" ");
    tableCell = tableRow.addCell();
    tableCell = tableRow.addCell();

    table.mergeCells(5, 5, 0,2);


    
    table.setColumnWidth(0, 300);
    table.setColumnWidth(1, 350);
    table.setColumnWidth(2, 308);
	
    
    idx=7;
}
//if(practice.getPracticeLevelCode()!=previousPracticeLevel)
//{
//	previousPracticeLevel=practice.getPracticeLevelCode();
//	XSLFTableRow tableRow = table.addRow();
//	tableRow.setHeight(30d);
//	
//	
//	XSLFTableCell tableCell = tableRow.addCell();// Create a table cell
//	tableCell.setFillColor(lightGrey);
//	
//	
//	XSLFTextParagraph p = tableCell.addNewTextParagraph();
//	XSLFTextRun tr = p.addNewTextRun();
//    
//	p = tableCell.addNewTextParagraph();
//	tableCell.setFillColor(lightGrey);
//     p.setTextAlign(TextAlign.CENTER);
//     tr = p.addNewTextRun();
//    tr.setFontSize(18d);
//    
//    tr.setFontFamily("Calibri (Body)");
//    tr.setText("ML"+practice.getPracticeLevelCode());
//    
//    tableCell = tableRow.addCell();
//    tableCell.setFillColor(lightGrey);
//	p = tableCell.addNewTextParagraph();
//     p.setTextAlign(TextAlign.CENTER);
//     tr = p.addNewTextRun();
//    tr.setFontSize(18d);
//    
//    tr.setFontFamily("Calibri (Body)");
//    tr.setText("As-Is Work Products");
//    
//    tableCell = tableRow.addCell();
//    tableCell.setFillColor(lightGrey);
//	p = tableCell.addNewTextParagraph();
//     p.setTextAlign(TextAlign.CENTER);
//     tr = p.addNewTextRun();
//    tr.setFontSize(18d);
//    
//    tr.setFontFamily("Calibri (Body)");
//    tr.setText("Gap to be Filled");
//
//    table.setColumnWidth(0, 300);
//    table.setColumnWidth(1, 300);
//    table.setColumnWidth(2, 300);
//    idx=idx+1;
//
//}
	if(idx>7)
	{
		slide = ppt.createSlide();
		ppt.setSlideOrder(slide, slideOrder);
		slideOrder=slideOrder+1;
		table = slide.createTable();// Create a table

		table.setAnchor(new Rectangle2D.Double(1, 1, 0,0));
		
		idx=0;		
		
		XSLFTableRow tableRow = table.addRow();
		tableRow.setHeight(30d);
		
		
		XSLFTableCell tableCell = tableRow.addCell();// Create a table cell
		tableCell.setWordWrap(Boolean.TRUE);
		
		tableCell.setFillColor(lightBlue);
		XSLFTextParagraph p = tableCell.addNewTextParagraph();
		p.setTextAlign(TextAlign.CENTER);
	    XSLFTextRun tr = p.addNewTextRun();
	    tr.setFontSize(20d);
	    
	    tr.setBold(true);
	    tr.setFontFamily("Calibri (Body)");
	    tr.setFontColor(Color.WHITE);
	    tr.setText("Practice Area");
	    
	     tableCell = tableRow.addCell();// Create a table cell
	     tableCell.setFillColor(lightBlue);
	     p = tableCell.addNewTextParagraph();
	     p.setTextAlign(TextAlign.CENTER);
	     tr = p.addNewTextRun();
	    tr.setFontSize(20d);
	    tr.setBold(true);
	    tr.setFontFamily("Calibri (Body)");
	    tr.setFontColor(Color.WHITE);
	    tr.setText("Capability Area");
	    
	     tableCell = tableRow.addCell();// Create a table cell
	     tableCell.setFillColor(lightBlue);
		 p = tableCell.addNewTextParagraph();
		 p.setTextAlign(TextAlign.CENTER);
	     tr = p.addNewTextRun();
	    tr.setFontSize(20d);
	    tr.setBold(true);
	    tr.setFontFamily("Calibri (Body)");
	    tr.setFontColor(Color.WHITE);
	    tr.setText("Category");
	    
	    idx=idx+1;
	    
	    tableRow = table.addRow();
		tableRow.setHeight(30d);
		tableCell = tableRow.addCell();
		p = tableCell.addNewTextParagraph();
	     p.setTextAlign(TextAlign.CENTER);
	     tr = p.addNewTextRun();
	    tr.setFontSize(14d);
	    
	    tr.setFontFamily("Calibri (Body)");
	    tr.setText(practice.getPracticeAreaName()+" ("+practice.getPracticeAreaCode()+")");
	    
	    tableCell = tableRow.addCell();
		p = tableCell.addNewTextParagraph();
	     p.setTextAlign(TextAlign.CENTER);
	     tr = p.addNewTextRun();
	    tr.setFontSize(14d);
	    
	    tr.setFontFamily("Calibri (Body)");
	    tr.setText(practice.getCapabilityName()+" ("+practice.getCapabilityCode()+")");
	    
	    tableCell = tableRow.addCell();
		p = tableCell.addNewTextParagraph();
	     p.setTextAlign(TextAlign.CENTER);
	     tr = p.addNewTextRun();
	    tr.setFontSize(14d);
	    
	    tr.setFontFamily("Calibri (Body)");
	    tr.setText(practice.getCategoryCode());

	    idx=idx+1;

		tableRow = table.addRow();
		tableRow.setHeight(30d);
		
		
		tableCell = tableRow.addCell();// Create a table cell
		tableCell.setFillColor(lightGrey);
		
		
		p = tableCell.addNewTextParagraph();
		tr = p.addNewTextRun();

	     p.setTextAlign(TextAlign.CENTER);
	    tr.setFontSize(18d);
	    
	    tr.setFontFamily("Calibri (Body)");
	    tr.setText("ML"+practice.getPracticeLevelCode());
	    
	    tableCell = tableRow.addCell();
	    tableCell.setFillColor(lightGrey);
		p = tableCell.addNewTextParagraph();
	     p.setTextAlign(TextAlign.CENTER);
	     tr = p.addNewTextRun();
	    tr.setFontSize(18d);
	    
	    tr.setFontFamily("Calibri (Body)");
	    tr.setText("As-Is Work Products");
	    
	    tableCell = tableRow.addCell();
	    tableCell.setFillColor(lightGrey);
		p = tableCell.addNewTextParagraph();
	     p.setTextAlign(TextAlign.CENTER);
	     tr = p.addNewTextRun();
	    tr.setFontSize(18d);
	    
	    tr.setFontFamily("Calibri (Body)");
	    tr.setText("Gap to be Filled");
	    previousPracticeLevel=practice.getPracticeLevelCode();
	    idx=idx+1;
	    table.setColumnWidth(0, 300);
	    table.setColumnWidth(1, 350);
	    table.setColumnWidth(2, 308);
		   
		   

		   
	}
	if(previousPracticeLevel!=practice.getPracticeLevelCode())
	   {
		   previousPracticeLevel=practice.getPracticeLevelCode();
		   XSLFTableRow tableRow = table.addRow();
			tableRow.setHeight(30d);
			
			
			XSLFTableCell tableCell = tableRow.addCell();// Create a table cell
			tableCell.setFillColor(lightGrey);
			
			
			XSLFTextParagraph p = tableCell.addNewTextParagraph();
			XSLFTextRun tr = p.addNewTextRun();

		     p.setTextAlign(TextAlign.CENTER);
		    tr.setFontSize(18d);
		    
		    tr.setFontFamily("Calibri (Body)");
		    tr.setText("ML"+practice.getPracticeLevelCode());
		    
		    tableCell = tableRow.addCell();
		    tableCell.setFillColor(lightGrey);
			p = tableCell.addNewTextParagraph();
		     p.setTextAlign(TextAlign.CENTER);
		     tr = p.addNewTextRun();
		    tr.setFontSize(18d);
		    
		    tr.setFontFamily("Calibri (Body)");
		    tr.setText("As-Is Work Products");
		    
		    tableCell = tableRow.addCell();
		    tableCell.setFillColor(lightGrey);
			p = tableCell.addNewTextParagraph();
		     p.setTextAlign(TextAlign.CENTER);
		     tr = p.addNewTextRun();
		    tr.setFontSize(18d);
		    
		    tr.setFontFamily("Calibri (Body)");
		    tr.setText("Gap to be Filled");
		    
		    idx=idx+1;
	   }

	XSLFTableRow tableRow = table.addRow();
	tableRow.setHeight(30d);
	
	XSLFTableCell tableCell = tableRow.addCell();// Create a table cell
	tableCell.setWordWrap(Boolean.TRUE);
	
	XSLFTextParagraph p = tableCell.addNewTextParagraph();
    XSLFTextRun tr = p.addNewTextRun();

    tr.setFontSize(10.50);
    tr.setFontFamily("calibri");
    tr.setText(practice.getPracticeCode()+" "+practice.getPracticeName());
    tableCell.setFillColor(red); 
    if(practice.getPracticeCompliance()>(29))
	{
		  tableCell.setFillColor(amber); 
			
		
	}
	if(practice.getPracticeCompliance()>(69))
	{
		  tableCell.setFillColor(yellow); 
			
		
	}
	if(practice.getPracticeCompliance()>(99))
	{
		  tableCell.setFillColor(green); 
			
		
	}
	

	tableCell = tableRow.addCell();// Create a table cell
	tableCell.setWordWrap(Boolean.TRUE);
	
	p = tableCell.addNewTextParagraph();
    tr = p.addNewTextRun();
    tr.setFontSize(10.50);
   
    tr.setFontFamily("calibri");
   // tr.setText(practice.getCurrentPractice());
    if(null==practice.getCurrentPractice()||practice.getCurrentPractice().equalsIgnoreCase("")||practice.getCurrentPractice().equalsIgnoreCase(" "))
    {
        tr.setText("None");
    	
    }
    else
    {
        tr.setText(practice.getCurrentPractice());
    	
    }
	tableCell = tableRow.addCell();// Create a table cell
	tableCell.setWordWrap(Boolean.TRUE);
	p = tableCell.addNewTextParagraph();
    tr = p.addNewTextRun();
    tr.setFontSize(10.50);
   
    tr.setFontFamily("calibri");
    if(null==practice.getCurrentGapNptes()||practice.getCurrentGapNptes().equalsIgnoreCase("")||practice.getCurrentGapNptes().equalsIgnoreCase(" "))
    {
        tr.setText("None");
    	
    }
    else
    {
        tr.setText(practice.getCurrentGapNptes());
    	
    }

table.setColumnWidth(0, 300);
table.setColumnWidth(1, 350);
table.setColumnWidth(2, 308);
	   idx=idx+1;
	   	
}
		
		
	
		
		OutputStream outstream = null;
		PrintWriter out = null;
		response.setContentType("application/vnd.ms-?powerpoint");
		response.addHeader("content-disposition",
				"attachment; filename=getReport.pptx");
		
		outstream = response.getOutputStream();
		ppt.write(outstream);
		IOUtils.closeQuietly(outstream);
		IOUtils.closeQuietly(fis);
		IOUtils.closeQuietly(out);
		outstream.flush();
		outstream.close();
	
		return "HomePage";

	}

	
	public String getReport(AccessObject accessObject,String project,HttpServletRequest request,HttpServletResponse response,String type) throws IOException
	{
		ProjectMst proj=projectMstService.getProjectByProjectCode(accessObject.getTenantId(), project);
		TenantMst tenantMst=tenantMstService.getTenant(proj.getTenantId());
		accessObject.setOrgName(tenantMst.getOrganizationName());
		List<PracticeAreaMst> practiceAreaListBase=new ArrayList<PracticeAreaMst>();
		List<PracticeAreaMst> practiceAreaList=new ArrayList<PracticeAreaMst>();
		List<PracticeMst> practiceList=new ArrayList<PracticeMst>();
		
		practiceAreaListBase=accessObject.getPracticeAreaMst();
		OrganizationUnitMst orgUnit=new OrganizationUnitMst();
		orgUnit=organizationUnitMstService.getUnique(proj.getTenantId(), proj.getOrganizationUnit(), accessObject.getProcess());
		String[] businessModel=orgUnit.getOrganizationModel().split(",");
		List<PracticeMst> practiceList1=new ArrayList<PracticeMst>();

		String TEMPLATE_PATH = servletContext.getRealPath("//templates");
		String TEMPLATE_GapSummaryReport = TEMPLATE_PATH + "GapSummaryReport.xlsx";
		File destinationfile = new File(TEMPLATE_GapSummaryReport);
		FileInputStream fis= new FileInputStream((destinationfile));
		XSSFWorkbook workbook =null;
		workbook=new XSSFWorkbook (fis);
		
        XSSFCellStyle styleSubHeader = (XSSFCellStyle) workbook.createCellStyle();
        

        
        
		XSSFCellStyle cellStyle = workbook.createCellStyle();
		XSSFCellStyle cellStyle2 = workbook.createCellStyle();
		XSSFCellStyle cellStyle3 = workbook.createCellStyle();
		XSSFCellStyle cellStyle4 = workbook.createCellStyle();
		XSSFCellStyle cellStyle5 = workbook.createCellStyle();
		XSSFCellStyle cellStyle6 = workbook.createCellStyle();
		XSSFCellStyle cellStyle7 = workbook.createCellStyle();
		XSSFCellStyle cellStyle8 = workbook.createCellStyle();

        styleSubHeader.setBorderBottom(BorderStyle.THIN);
        styleSubHeader.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styleSubHeader.setBorderLeft(BorderStyle.THIN);
        styleSubHeader.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        styleSubHeader.setBorderRight(BorderStyle.THIN);
        styleSubHeader.setRightBorderColor(IndexedColors.BLACK.getIndex());
        styleSubHeader.setBorderTop(BorderStyle.THIN);
        styleSubHeader.setTopBorderColor(IndexedColors.BLACK.getIndex());

        
		XSSFColor blue = new XSSFColor(new java.awt.Color(51,89,157));
		XSSFColor amber = new XSSFColor(new java.awt.Color(255,191,0));
		
		XSSFColor blue_light1 = new XSSFColor(new java.awt.Color(142,169,219));
		XSSFColor blue_25 = new XSSFColor(new java.awt.Color(183,200,231));
		XSSFColor Orange = new XSSFColor(new java.awt.Color(252,219,35));
		XSSFColor Yellow = new XSSFColor(new java.awt.Color(251,255,35));
		XSSFColor Green = new XSSFColor(new java.awt.Color(31,226,31));
		XSSFColor Red = new XSSFColor(new java.awt.Color(255,0,0));
		
		XSSFFont fontHeader =workbook.createFont();
	    fontHeader.setFontName("Calibri");
	    fontHeader.setFontHeight(14);
	    fontHeader.setBold(true);
	    fontHeader.setColor(HSSFColor.YELLOW.index);
	    
	    XSSFFont fontHeader2 =workbook.createFont();
	    fontHeader2.setFontName("Calibri");
	    fontHeader2.setFontHeight(11);
	    fontHeader2.setBold(true);
	    fontHeader2.setColor(HSSFColor.YELLOW.index);
	    
	    XSSFFont font11 =workbook.createFont();
	    font11.setFontName("Calibri");
	    font11.setFontHeight(11);
	    font11.setBold(true);
	    font11.setColor(IndexedColors.BLACK.getIndex());

	    XSSFFont font11_RED =workbook.createFont();
	    font11_RED.setFontName("Calibri");
	    font11_RED.setFontHeight(11);
	    font11_RED.setBold(true);
	    font11_RED.setColor(HSSFColor.RED.index);
	    
	    XSSFFont font11_Normal =workbook.createFont();
	    font11.setFontName("Calibri");
	    font11.setFontHeight(11);
	    font11.setColor(IndexedColors.BLACK.getIndex());
	    
	    XSSFFont font18 =workbook.createFont();
	    font18.setFontName("Calibri");
	    font18.setFontHeight(18);
	    font18.setBold(true);
	    font18.setColor(IndexedColors.BLACK.getIndex());
	    
	    
	    XSSFCellStyle cellStyleRed = workbook.createCellStyle();
	    cellStyleRed.setAlignment(HorizontalAlignment.CENTER);
	    cellStyleRed.setBorderBottom(BorderStyle.THIN);
	    cellStyleRed.setBorderTop(BorderStyle.THIN);
	    cellStyleRed.setBorderRight(BorderStyle.THIN);
	    cellStyleRed.setBorderLeft(BorderStyle.THIN);
	    cellStyleRed.setFillForegroundColor(Red);
	    cellStyleRed.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    
        
	    XSSFCellStyle cellStyleOrange = workbook.createCellStyle();
	    cellStyleOrange.setAlignment(HorizontalAlignment.CENTER);
	    cellStyleOrange.setBorderBottom(BorderStyle.THIN);
	    cellStyleOrange.setBorderTop(BorderStyle.THIN);
	    cellStyleOrange.setBorderRight(BorderStyle.THIN);
	    cellStyleOrange.setBorderLeft(BorderStyle.THIN);
	    cellStyleOrange.setFillForegroundColor(Orange);
	    cellStyleOrange.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    
	    XSSFCellStyle cellStyleGreen = workbook.createCellStyle();
	    cellStyleGreen.setAlignment(HorizontalAlignment.CENTER);
	    cellStyleGreen.setBorderBottom(BorderStyle.THIN);
	    cellStyleGreen.setBorderTop(BorderStyle.THIN);
	    cellStyleGreen.setBorderRight(BorderStyle.THIN);
	    cellStyleGreen.setBorderLeft(BorderStyle.THIN);
	    cellStyleGreen.setFillForegroundColor(Green);
	    cellStyleGreen.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    
        
	    XSSFCellStyle cellStyleYellow = workbook.createCellStyle();
	    cellStyleYellow.setAlignment(HorizontalAlignment.CENTER);
	    cellStyleYellow.setBorderBottom(BorderStyle.THIN);
	    cellStyleYellow.setBorderTop(BorderStyle.THIN);
	    cellStyleYellow.setBorderRight(BorderStyle.THIN);
	    cellStyleYellow.setBorderLeft(BorderStyle.THIN);
	    cellStyleYellow.setFillForegroundColor(Yellow);
	    cellStyleYellow.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    
	    XSSFCellStyle cellStyleBlue = workbook.createCellStyle();
	    cellStyleBlue.setAlignment(HorizontalAlignment.CENTER);
	    cellStyleBlue.setBorderBottom(BorderStyle.THIN);
	    cellStyleBlue.setBorderTop(BorderStyle.THIN);
	    cellStyleBlue.setBorderRight(BorderStyle.THIN);
	    cellStyleBlue.setBorderLeft(BorderStyle.THIN);
	    cellStyleBlue.setFillForegroundColor(blue);
	    cellStyleBlue.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    
	    XSSFCellStyle cellStyleAmber = workbook.createCellStyle();
	    cellStyleAmber.setAlignment(HorizontalAlignment.CENTER);
	    cellStyleAmber.setBorderBottom(BorderStyle.THIN);
	    cellStyleAmber.setBorderTop(BorderStyle.THIN);
	    cellStyleAmber.setBorderRight(BorderStyle.THIN);
	    cellStyleAmber.setBorderLeft(BorderStyle.THIN);
	    cellStyleAmber.setFillForegroundColor(amber);
	    cellStyleAmber.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    
	    
        
	    
        
        
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setFillForegroundColor(blue);
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    cellStyle.setFont(fontHeader);
	    
	    
	    cellStyle3.setAlignment(HorizontalAlignment.CENTER);
		cellStyle3.setBorderBottom(BorderStyle.THIN);
		cellStyle3.setBorderTop(BorderStyle.THIN);
		cellStyle3.setBorderRight(BorderStyle.THIN);
		cellStyle3.setBorderLeft(BorderStyle.THIN);
        cellStyle3.setFillForegroundColor(blue_25);
        cellStyle3.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    cellStyle3.setFont(font11);
	    
	    cellStyle7.setAlignment(HorizontalAlignment.CENTER);
	    cellStyle7.setVerticalAlignment(VerticalAlignment.TOP);
		cellStyle7.setBorderBottom(BorderStyle.THIN);
		cellStyle7.setBorderTop(BorderStyle.THIN);
		cellStyle7.setBorderRight(BorderStyle.THIN);
		cellStyle7.setBorderLeft(BorderStyle.THIN);
        cellStyle7.setFillForegroundColor(HSSFColor.WHITE.index);
        cellStyle7.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    cellStyle7.setFont(font11);
	    
	    cellStyle8.setAlignment(HorizontalAlignment.CENTER);
	    cellStyle8.setVerticalAlignment(VerticalAlignment.TOP);
		cellStyle8.setBorderBottom(BorderStyle.THIN);
		cellStyle8.setBorderTop(BorderStyle.THIN);
		cellStyle8.setBorderRight(BorderStyle.THIN);
		cellStyle8.setBorderLeft(BorderStyle.THIN);
        cellStyle8.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyle8.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    cellStyle8.setFont(font11_RED);
	    
	    cellStyle5.setWrapText(true);
	    cellStyle5.setAlignment(HorizontalAlignment.LEFT);
		cellStyle5.setBorderBottom(BorderStyle.THIN);
		cellStyle5.setBorderTop(BorderStyle.THIN);
		cellStyle5.setBorderRight(BorderStyle.THIN);
		cellStyle5.setBorderLeft(BorderStyle.THIN);
        cellStyle5.setFillForegroundColor(blue_25);
        cellStyle5.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    cellStyle5.setFont(font11);
	    
	    cellStyle6.setWrapText(true);
	    cellStyle6.setAlignment(HorizontalAlignment.LEFT);
		cellStyle6.setBorderBottom(BorderStyle.THIN);
		cellStyle6.setBorderTop(BorderStyle.THIN);
		cellStyle6.setBorderRight(BorderStyle.THIN);
		cellStyle6.setBorderLeft(BorderStyle.THIN);
        cellStyle6.setFillForegroundColor(blue_25);
        cellStyle6.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    cellStyle6.setFont(font11_Normal);
	    
	    
	    cellStyle4.setAlignment(HorizontalAlignment.CENTER);
		cellStyle4.setBorderBottom(BorderStyle.THIN);
		cellStyle4.setBorderTop(BorderStyle.THIN);
		cellStyle4.setBorderRight(BorderStyle.THIN);
		cellStyle4.setBorderLeft(BorderStyle.THIN);
        cellStyle4.setFillForegroundColor(blue_light1);
        cellStyle4.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    cellStyle4.setFont(font18);
	    
	    
	    cellStyle2.setAlignment(HorizontalAlignment.CENTER);
		cellStyle2.setBorderBottom(BorderStyle.THIN);
		cellStyle2.setBorderTop(BorderStyle.THIN);
		cellStyle2.setBorderRight(BorderStyle.THIN);
		cellStyle2.setBorderLeft(BorderStyle.THIN);
        cellStyle2.setFillForegroundColor(blue);
        cellStyle2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    cellStyle2.setFont(fontHeader2);
	    
	    
	    

		XSSFSheet sheet= workbook.getSheet(ApplicationConstants.REPORT_SHEETGASHEET);
		
		sheet.setZoom(92);

		List<CategoryMst> categoryList=accessObject.getCategoryMst();

		List<CapabilityMst> capabilityList=accessObject.getCapabilityMst();
		List<PracticeAreaMst> practiceAreaList1=accessObject.getPracticeAreaMst();
		for(String bm:businessModel)
		{
		for(PracticeAreaMst practiceArea:practiceAreaList1)
		{
			if(practiceArea.getBusinessModel().contains(bm))
			{
				practiceAreaList.add(practiceArea);
			}
		}
		}
		
		for(PracticeAreaMst practiceArea:practiceAreaList)
		{
			for(RelationMappingCapabilityPracticeArea relCap:accessObject.getRelationMappingCapabilityPracticeArea())
			{
				if(practiceArea.getPracticeAreaCode().equalsIgnoreCase(relCap.getPracticeAreaCode()))
				{
					practiceArea.setCapabilityCode(relCap.getCapabilityCode());
					break;
				}
			}
			
			for(RelationMappingCategoryCapability relCat:accessObject.getRelationMappingCategoryCapability())
			{
				if(practiceArea.getCapabilityCode().equalsIgnoreCase(relCat.getCapabilityCode()))
				{
					practiceArea.setCategoryCode(relCat.getCategoryCode());
					break;
				}
			}
		}
		List<PracticeMst> practiceList3=practiceMstService.getPracticeByTenantProject(proj.getTenantId(), 
				proj.getProjectCode(), accessObject,accessObject.getCmmiVersion());
		List<PracticeWorkProductMst> practiceWorkProductMst=new ArrayList<PracticeWorkProductMst>();
		int rowum=0;
		int cellno=0;
		Row row = sheet.createRow(rowum);
		sheet.addMergedRegion(new CellRangeAddress(rowum,rowum, 0, 10));

		
		Cell cell = row.createCell(cellno);
		fontHeader.setFontHeight(18);
		cell.setCellStyle(cellStyle4);
		setCellVaue("CMMI v2.0 FOR DEV & SVC (Combined) - GAP ANALYSIS SHEET", cell);

		rowum++;
		
		row = sheet.createRow(rowum);
		cell = row.createCell(cellno);
		row = sheet.createRow(rowum);
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Project Code", cell);
		sheet.setColumnWidth(cellno, 18000);
		cellno++;
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Category", cell);
		sheet.setColumnWidth(cellno, 18000);
		cellno++;
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Capability Area", cell);
		sheet.setColumnWidth(cellno, 18000);
		cellno++;
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Practice Area", cell);
		sheet.setColumnWidth(cellno, 18000);
		cellno++;
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Practice group", cell);
        cellno++;
		
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Practice", cell);
        cellno++;
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Practice Areas, Intent/Practices", cell);
		sheet.setColumnWidth(cellno, 18000);
		cellno++;
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Value", cell);
		sheet.setColumnWidth(cellno, 18000);
		cellno++;
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Example Activities", cell);
		sheet.setColumnWidth(cellno, 18000);
		cellno++;
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Example Work Products.", cell);
		sheet.setColumnWidth(cellno, 18000);
		cellno++;
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Current Practices/Work Products", cell);
		sheet.setColumnWidth(cellno, 18000);
		cellno++;
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Compliance", cell);
		sheet.setColumnWidth(cellno, 10000);
		cellno++;
		cell = row.createCell(cellno);
		cell.setCellStyle(cellStyle3);
		setCellVaue("Gaps Identified (to be filled)", cell);
		sheet.setColumnWidth(cellno, 18000);
		cellno++;
		rowum++;
		if(type.equalsIgnoreCase("PPT"))
		{
		generatePPT(accessObject, project, request, response,practiceList3);
		}
		for(PracticeMst prac:practiceList3)
		{
			
			Boolean proceedToPrint=Boolean.TRUE;
			String practiceCode=prac.getPracticeCode();
						
			String practiceComplianceLevel="NY";
			if(proceedToPrint){
				String compliance=Math.round(prac.getPracticeCompliance())+"";
				
				
				List<LookupMst> complianceLookup=new ArrayList<LookupMst>();
				complianceLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_COMPLIANCEREPORTLOOKUP);
				if(null!=complianceLookup)
				{
					for(LookupMst lookup:complianceLookup)
					{
						if(lookup.getLookupValue().equalsIgnoreCase(compliance))
						{
							practiceComplianceLevel=lookup.getDisplayValue();
							prac.setPracticeComplianceLevel(practiceComplianceLevel);
							break;
						}
					}
				}
			}
			if(proceedToPrint)
			{
				practiceList1.add(prac);
			cellno=0;
			practiceWorkProductMst=practiceWorkProductMstService.getByPracticeCode(
					prac.getTenantId(),
					prac.getPracticeCode(),
					prac.getOrganizationUnit(),
					prac.getBusinessUnit(),
					prac.getProject(),prac.getCmmiVersion());
			String workProduct="";
			if(null!=practiceWorkProductMst)
			{
				for(PracticeWorkProductMst wp:practiceWorkProductMst)
				{
					workProduct=workProduct+"\n-"+wp.getWorkProductName();
				}
			}
			
				
			row = sheet.createRow(rowum);
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(prac.getProject(),cell);
			cellno++;
		
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(prac.getCategoryCode(),cell);
			cellno++;
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(prac.getCapabilityCode(),cell);
			cellno++;
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(prac.getPracticeAreaCode(),cell);
			cellno++;
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(prac.getPracticeLevelCode(),cell);
			cellno++;
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(prac.getPracticeCode(),cell);
			cellno++;
		
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(prac.getPracticeCode()+" "+prac.getPracticeName(),cell);
			cellno++;
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(prac.getPracticeValue(),cell);
			cellno++;
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(prac.getPracticeExamples(),cell);
			cellno++;
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(workProduct,cell);
			cellno++;
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(prac.getCurrentPractice(),cell);
			cellno++;
			cell = row.createCell(cellno);
			if(practiceComplianceLevel.equalsIgnoreCase("NY"))
			{
				cell.setCellStyle(cellStyleRed);
			}

			if(practiceComplianceLevel.equalsIgnoreCase("DM"))
			{
				cell.setCellStyle(cellStyleRed);
				
			}
			if(practiceComplianceLevel.equalsIgnoreCase("PM"))
			{
				cell.setCellStyle(cellStyleAmber);
				
			}
			if(practiceComplianceLevel.equalsIgnoreCase("LM"))
			{
				cell.setCellStyle(cellStyleYellow);
				
			}
			if(practiceComplianceLevel.equalsIgnoreCase("FM"))
			{
				cell.setCellStyle(cellStyleGreen);
				
			}

			
			setCellVaue(practiceComplianceLevel,cell);
			cellno++;
			cell = row.createCell(cellno);
			cell.setCellStyle(cellStyle6);
			setCellVaue(prac.getCurrentGapNptes(),cell);
			rowum++;
		}
		}
		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		

		sheet = workbook.getSheet(ApplicationConstants.REPORT_SHEETGASUMMARY);
		sheet.setZoom(80);

		Integer rownum = 1;
		Row row1 = sheet.createRow(0);
		Row row2 = sheet.createRow(1);
		Row row3 = sheet.createRow(2);
		Row row4 = sheet.createRow(3);
		Row row5 = sheet.createRow(4);
		Row row6 = sheet.createRow(5);
		Row row7 = sheet.createRow(6);
		Row row8 = sheet.createRow(7);
		Row row9 = sheet.createRow(8);
		Row row10 = sheet.createRow(9);
		Row row11 = sheet.createRow(10);
		Row row12 = sheet.createRow(11);
		Row row13 = sheet.createRow(12);
		Row row14 = sheet.createRow(13);
		Row row15 = sheet.createRow(14);
		Row row16 = sheet.createRow(15);
		Row row17 = sheet.createRow(16);
		Row row18 = sheet.createRow(17);
		Row row19 = sheet.createRow(18);
		Row row20 = sheet.createRow(19);
		Row row21 = sheet.createRow(20);
		Row row22 = sheet.createRow(21);
		Row row23 = sheet.createRow(22);
		Row row24 = sheet.createRow(23);
		Row row25 = sheet.createRow(24);
		Row row26 = sheet.createRow(25);
		Row row27 = sheet.createRow(26);
		Row row28 = sheet.createRow(27);
		Row row29 = sheet.createRow(28);
		Row row30 = sheet.createRow(29);
		Integer tempcellNum = 0;
		Cell tempCela = row1.createCell(tempcellNum++);
			    
	    
	    
	    
	    
	    
	    
		sheet.addMergedRegion(new CellRangeAddress(0,0, 0, 26));
		setCellVaue("CMMI V2.0 - DEV & SVC - GAP ANALYSIS SUMMARY", tempCela);

		Collections.sort(practiceAreaList, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeAreaMst bal1 = (PracticeAreaMst) o1;
				final PracticeAreaMst bal2 = (PracticeAreaMst) o2;

				int c;
				
				c=bal1.getCategoryCode().compareTo(bal2.getCategoryCode());
				if(c==0)
				{
					c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				}
				if(c==0)
				{
					c=bal1.getPracticeAreaCode().toUpperCase().compareTo(bal2.getPracticeAreaCode().toUpperCase());
				}
				
				return c;
			}
		});
	
List<LookupMst> lookups=new ArrayList<LookupMst>();
lookups=lookupMstService.getLookupMst(ApplicationConstants.DEFAULT_TENANT, ApplicationConstants.LOOKUP_COMPLIANCELOOKUP);
cellStyle.setFont(fontHeader2);

		tempCela.setCellStyle(cellStyle);
		tempcellNum = 0;
		tempCela = row2.createCell(0);
		setCellVaue("Category", tempCela);
		tempCela.setCellStyle(cellStyle);
//		{
//			Integer startNo=2;
//			Integer endNo=2;
//			String previousCAP="";
//			for (PracticeAreaMst practiceA:practiceAreaList)
//			{
//				tempCela = row2.createCell(endNo);
//				setCellVaue(practiceA.getCategoryCode(), tempCela);
//				tempCela.setCellStyle(cellStyle);
//				endNo=endNo+1;
//				
//			}
//			
//			
//		}
		
		tempCela = row3.createCell(0);
		setCellVaue("Capability Area", tempCela);
		tempCela.setCellStyle(cellStyle);
		
//		{
//			Integer startNo=2;
//			Integer endNo=2;
//			String previousCAP="";
//			for (PracticeAreaMst practiceA:practiceAreaList)
//			{
//				tempCela = row3.createCell(endNo);
//				setCellVaue(practiceA.getCapabilityCode(), tempCela);
//				tempCela.setCellStyle(cellStyle);
//				endNo=endNo+1;
//				previousCAP=practiceA.getCapabilityCode();
//			}
//		}
		
		tempCela = row4.createCell(0);
		tempCela.setCellStyle(cellStyle);
		
		
		setCellVaue("Practice Area", tempCela);
		tempCela.setCellStyle(cellStyle);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				tempCela = row2.createCell(counter);
				setCellVaue(practiceA.getCategoryCode(), tempCela);
				tempCela.setCellStyle(cellStyle);
				tempCela = row3.createCell(counter);
				setCellVaue(practiceA.getCapabilityCode(), tempCela);
				tempCela.setCellStyle(cellStyle);
				tempCela = row4.createCell(counter);
				setCellVaue(practiceA.getPracticeAreaCode(), tempCela);
				tempCela.setCellStyle(cellStyle);
				counter++;
			}
		}
		
		sheet.autoSizeColumn(0);
		sheet.addMergedRegion(new CellRangeAddress(4,6, 0, 0));
		sheet.addMergedRegion(new CellRangeAddress(7,14, 0, 0));
		sheet.addMergedRegion(new CellRangeAddress(15,21, 0, 0));
		sheet.addMergedRegion(new CellRangeAddress(22,26, 0, 0));
		sheet.addMergedRegion(new CellRangeAddress(27,29, 0, 0));
		sheet.addMergedRegion(new CellRangeAddress(1,1, 0, 1));
		sheet.addMergedRegion(new CellRangeAddress(2,2, 0, 1));
		sheet.addMergedRegion(new CellRangeAddress(3,3, 0, 1));
		tempCela = row5.createCell(0);
		setCellVaue("ML1", tempCela);
		tempCela.setCellStyle(cellStyle2);
		tempCela = row8.createCell(0);
		setCellVaue("ML2", tempCela);
		tempCela.setCellStyle(cellStyle2);
		tempCela = row16.createCell(0);
		setCellVaue("ML3", tempCela);
		tempCela.setCellStyle(cellStyle2);
		tempCela = row23.createCell(0);
		setCellVaue("ML4", tempCela);
		tempCela.setCellStyle(cellStyle2);
		tempCela = row28.createCell(0);
		setCellVaue("ML5", tempCela);
		tempCela.setCellStyle(cellStyle2);
		tempCela = row5.createCell(1);
		setCellVaue("1.1", tempCela);
		tempCela.setCellStyle(cellStyle2);
		
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("1.1"))
						{
							
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row5.createCell(counter);
									tempCela.setCellStyle(cellStyleBlue);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
		
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}
							
								
						}
						
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row6.createCell(1);
		setCellVaue("1.2", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("1.2"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row6.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}

									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row7.createCell(1);
		setCellVaue("1.3", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("1.3"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row7.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}

								}
							}
								
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row8.createCell(1);
		setCellVaue("2.1", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("2.1"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row8.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row9.createCell(1);
		setCellVaue("2.2", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("2.2"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row9.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row10.createCell(1);
		setCellVaue("2.3", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("2.3"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row10.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row11.createCell(1);
		setCellVaue("2.4", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("2.4"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row11.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row12.createCell(1);
		setCellVaue("2.5", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("2.5"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row12.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row13.createCell(1);
		setCellVaue("2.6", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("2.6"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row13.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleBlue);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row14.createCell(1);
		setCellVaue("2.7", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("2.7"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row14.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row15.createCell(1);
		setCellVaue("2.8", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("2.8"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row15.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row16.createCell(1);
		setCellVaue("3.1", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("3.1"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row16.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row17.createCell(1);
		setCellVaue("3.2", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("3.2"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row17.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row18.createCell(1);
		setCellVaue("3.3", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("3.3"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row18.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row19.createCell(1);
		setCellVaue("3.4", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("3.4"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row19.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row20.createCell(1);
		setCellVaue("3.5", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("3.5"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row20.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row21.createCell(1);
		setCellVaue("3.6", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("3.6"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row21.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row22.createCell(1);
		setCellVaue("3.7", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("3.7"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row22.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row23.createCell(1);
		setCellVaue("4.1", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("4.1"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row23.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row24.createCell(1);
		setCellVaue("4.2", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("4.2"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row24.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row25.createCell(1);
		setCellVaue("4.3", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("4.3"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row25.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		
		tempCela = row26.createCell(1);
		setCellVaue("4.4", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("4.4"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row26.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		
		tempCela = row27.createCell(1);
		setCellVaue("4.5", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("4.5"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row27.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row28.createCell(1);
		setCellVaue("5.1", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("5.1"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row28.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}		
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row29.createCell(1);
		setCellVaue("5.2", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("5.2"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row29.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		tempCela = row30.createCell(1);
		setCellVaue("5.3", tempCela);
		tempCela.setCellStyle(cellStyle2);
		{
			Integer counter=2;
			for (PracticeAreaMst practiceA:practiceAreaList)
			{
				for (PracticeMst practiceB:practiceList1)
				{
					if(practiceB.getPracticeAreaCode().equalsIgnoreCase(practiceA.getPracticeAreaCode()))
					{
						if(practiceB.getPracticeCode().contains("5.3"))
						{
							for(LookupMst look:lookups)
							{
								if(look.getLookupValue().equalsIgnoreCase(Math.round(practiceB.getPracticeCompliance())+""))
								{
									tempCela = row30.createCell(counter);
									setCellVaue(practiceB.getPracticeComplianceLevel(), tempCela);
									
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("NY"))
									{
										tempCela.setCellStyle(cellStyle8);
									}

									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("DM"))
									{
										tempCela.setCellStyle(cellStyleRed);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("PM"))
									{
										tempCela.setCellStyle(cellStyleAmber);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("LM"))
									{
										tempCela.setCellStyle(cellStyleYellow);
										
									}
									if(practiceB.getPracticeComplianceLevel().equalsIgnoreCase("FM"))
									{
										tempCela.setCellStyle(cellStyleGreen);
										
									}
									
								}
							}	
						}
					}
					else
					{
						continue;
					}
					
				}
				counter++;		
			}
		}
		
		
		
		//sheet= workbook.createSheet(ApplicationConstants.REPORT_SHEETCOMPLIANCESHEET);
		sheet= workbook.getSheet(ApplicationConstants.REPORT_SHEETCOMPLIANCESHEET);
		rowum=0;
		cellno=0;
		
		List<DashBoardVo> dashBoardVoPracticeArea=new ArrayList<DashBoardVo>();
		List<DashBoardVo> dashBoardVoCategory=new ArrayList<DashBoardVo>();
		List<DashBoardVo> dashBoardVoCapability=new ArrayList<DashBoardVo>();
		
		
		
		for(PracticeMst practice:practiceList1)
		{
			
			String practiceArea=practice.getPracticeAreaCode();
			Boolean recordFound=Boolean.FALSE;
			if(dashBoardVoPracticeArea.size()>0)
			{
				for(DashBoardVo dbvo:dashBoardVoPracticeArea)
				{
					if(dbvo.getProjectCode().equalsIgnoreCase(practice.getProject()))
					{
						
						if(dbvo.getPracticeAreaCode().equalsIgnoreCase(practice.getPracticeAreaCode()))
						{
							dbvo.setComplianceScore(dbvo.getComplianceScore()+practice.getPracticeCompliance());
							dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
							recordFound=Boolean.TRUE;
						}
						
					}
					
				}
				if(!recordFound)
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
					dashBoardVo.setPracticeCode(practice.getPracticeCode());
					dashBoardVo.setCategoryCode(practice.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
					dashBoardVo.setProjectCode(practice.getProject());
					dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoPracticeArea.add(dashBoardVo);		
				}
			}
			else
			{
				DashBoardVo dashBoardVo=new DashBoardVo();
				dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
				dashBoardVo.setPracticeCode(practice.getPracticeCode());
				dashBoardVo.setCategoryCode(practice.getCategoryCode());
				dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
				dashBoardVo.setProjectCode(practice.getProject());
				dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
				dashBoardVo.setTotalComplianceScore(100.00);
				dashBoardVoPracticeArea.add(dashBoardVo);
			}
		}
		
		for(PracticeMst practice:practiceList1)
		{
			
			String category=practice.getCategoryCode();
			Boolean recordFound=Boolean.FALSE;
			if(dashBoardVoCategory.size()>0)
			{
				for(DashBoardVo dbvo:dashBoardVoCategory)
				{
					if(dbvo.getProjectCode().equalsIgnoreCase(practice.getProject()))
					{
						
						if(dbvo.getCategoryCode().equalsIgnoreCase(category))
						{
							dbvo.setComplianceScore(dbvo.getComplianceScore()+practice.getPracticeCompliance());
							dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
							recordFound=Boolean.TRUE;
						}
						
					}
					
				}
				if(!recordFound)
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
					dashBoardVo.setProjectName(practice.getProject());
					dashBoardVo.setCategoryCode(practice.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
					dashBoardVo.setPracticeCode(practice.getPracticeCode());
					dashBoardVo.setProjectCode(practice.getProject());
					dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoCategory.add(dashBoardVo);		
				}
			}
			else
			{
				DashBoardVo dashBoardVo=new DashBoardVo();
				dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
				dashBoardVo.setProjectName(practice.getProject());
				dashBoardVo.setCategoryCode(practice.getCategoryCode());
				dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
				dashBoardVo.setPracticeCode(practice.getPracticeCode());
				dashBoardVo.setProjectCode(practice.getProject());
				dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
				dashBoardVo.setTotalComplianceScore(100.00);
				dashBoardVoCategory.add(dashBoardVo);
			}
		}
		
		for(PracticeMst practice:practiceList1)
		{
			
			String capability=practice.getCapabilityCode();
			Boolean recordFound=Boolean.FALSE;
			if(dashBoardVoCapability.size()>0)
			{
				for(DashBoardVo dbvo:dashBoardVoCapability)
				{
					if(dbvo.getProjectCode().equalsIgnoreCase(practice.getProject()))
					{
						
						if(dbvo.getCapabilityCode().equalsIgnoreCase(capability))
						{
							dbvo.setComplianceScore(dbvo.getComplianceScore()+practice.getPracticeCompliance());
							dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
							recordFound=Boolean.TRUE;
						}
						
					}
					
				}
				if(!recordFound)
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
					dashBoardVo.setProjectName(practice.getProject());
					dashBoardVo.setCategoryCode(practice.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
					dashBoardVo.setPracticeCode(practice.getPracticeCode());
					dashBoardVo.setProjectCode(practice.getProject());
					dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoCapability.add(dashBoardVo);		
				}
			}
			else
			{
				DashBoardVo dashBoardVo=new DashBoardVo();
				dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
				dashBoardVo.setProjectName(practice.getProject());
				dashBoardVo.setCategoryCode(practice.getCategoryCode());
				dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
				dashBoardVo.setPracticeCode(practice.getPracticeCode());
				dashBoardVo.setProjectCode(practice.getProject());
				dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
				dashBoardVo.setTotalComplianceScore(100.00);
				dashBoardVoCapability.add(dashBoardVo);
			}
		}
		
		
		
		
		
		for(DashBoardVo dbc:dashBoardVoCategory)
		{
			if(dbc.getComplianceScore()>0)
			{
				Double totalComplianceScore=(double) Math.round(((double) dbc.getComplianceScore()*100)/dbc.getTotalComplianceScore());
				//Double totalComplianceScore=(double) Math.round((( dbc.getComplianceScore()*100)/dbc.getTotalComplianceScore())*100)/100;
				dbc.setComplianceScore(totalComplianceScore);
			}
			
		}
		
		for(DashBoardVo dbcap:dashBoardVoCapability)
		{
			if(dbcap.getComplianceScore()>0)
			{
				Double totalComplianceScore=(double) Math.round(((double) dbcap.getComplianceScore()*100)/dbcap.getTotalComplianceScore());
//				Double totalComplianceScore=(double) Math.round((( dbcap.getComplianceScore()*100)/dbcap.getTotalComplianceScore())*100)/100;
				dbcap.setComplianceScore(totalComplianceScore);
			}
			
		}
		
		for(DashBoardVo dPA:dashBoardVoPracticeArea)
		{
			if(dPA.getComplianceScore()>0)
			{
				Double totalComplianceScore=(double) Math.round(((double) dPA.getComplianceScore()*100)/dPA.getTotalComplianceScore());
				dPA.setComplianceScore(totalComplianceScore);
			}
			
		}
		rowum=0;
		cellno=0;
		row = sheet.createRow(rowum);
		cell = row.createCell(cellno);
		setCellVaue("Category", cell);
		cellno++;
		cell = row.createCell(cellno);
		setCellVaue("Capability", cell);
		cellno++;
		cell = row.createCell(cellno);
		setCellVaue("Practice Area", cell);
		cellno++;
		cell = row.createCell(cellno);
		setCellVaue("Practice", cell);
		cellno++;
		cell = row.createCell(cellno);
		setCellVaue("Complaince", cell);
		rowum++;
		for(PracticeMst practice:practiceList1)
		{

			cellno=0;
			row = sheet.createRow(rowum);
			cell = row.createCell(cellno);
			setCellVaue(practice.getCategoryCode(), cell);
			cellno++;
			cell = row.createCell(cellno);
			setCellVaue(practice.getCapabilityCode(), cell);
			cellno++;
			cell = row.createCell(cellno);
			setCellVaue(practice.getPracticeAreaCode(), cell);
			cellno++;
			cell = row.createCell(cellno);
			setCellVaue(practice.getPracticeCode(), cell);
			cellno++;
			cell = row.createCell(cellno);
			setCellVaue(practice.getPracticeCompliance(), cell);
			rowum++;
			
		}
		rowum=0;
		row = sheet.getRow(rowum);
		cellno=5;
		cell = row.createCell(cellno);
		setCellVaue("Practice Area", cell);
		cellno++;
		cell = row.createCell(cellno);
		setCellVaue("Compliance", cell);
		rowum++;
		for(DashBoardVo dPA:dashBoardVoPracticeArea)
		{
			row = sheet.getRow(rowum);
			cellno=5;
			cell = row.createCell(cellno);
			setCellVaue(dPA.getPracticeAreaCode(), cell);
			cellno++;
			cell = row.createCell(cellno);
			setCellVaue(dPA.getComplianceScore(), cell);
			rowum++;
			
			
		}
		rowum=0;
		row = sheet.getRow(rowum);
		cellno=7;
		cell = row.createCell(cellno);
		setCellVaue("Capability", cell);
		cellno++;
		cell = row.createCell(cellno);
		setCellVaue("Compliance", cell);
		rowum++;
		for(DashBoardVo dbcap:dashBoardVoCapability)
		{
			row = sheet.getRow(rowum);
			cellno=7;
			cell = row.createCell(cellno);
			setCellVaue(dbcap.getCapabilityCode(), cell);
			cellno++;
			cell = row.createCell(cellno);
			setCellVaue(dbcap.getComplianceScore(), cell);
			rowum++;
			
		}
		rowum=0;
		row = sheet.getRow(rowum);
		cellno=9;
		cell = row.createCell(cellno);
		setCellVaue("Category", cell);
		cellno++;
		cell = row.createCell(cellno);
		setCellVaue("Compliance", cell);
		rowum++;
		for(DashBoardVo dbc:dashBoardVoCategory)
		{
			row = sheet.getRow(rowum);
			cellno=9;
			cell = row.createCell(cellno);
			setCellVaue(dbc.getCategoryCode(), cell);
			cellno++;
			cell = row.createCell(cellno);
			setCellVaue(dbc.getComplianceScore(), cell);
			rowum++;	
			
		}
		
		
		
		
		 XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);

		
		InputStream in = null;
		
		OutputStream outstream = null;
		PrintWriter out = null;
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("content-disposition",
				"attachment; filename=GapSummaryReport_"+accessObject.getProject()+".xlsx");
		
		outstream = response.getOutputStream();
		workbook.write(outstream);
		IOUtils.closeQuietly(outstream);
		IOUtils.closeQuietly(fis);
		IOUtils.closeQuietly(out);
		outstream.flush();
		outstream.close();
	
//		model.put("message", "HowToDoInJava Reader !!");
		return "HomePage";
	}
	
	@RequestMapping("/wip")
	public String wip(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		model.put("message", "HowToDoInJava Reader !!");
		return "WIP";
	}
	
	@RequestMapping("/user")
	public String user(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String msg=accessObject.getMsg();
		accessObject.setMsg("");
		TenantMst tenant=tenantMstService.getTenant(accessObject.getTenantId());
		request.setAttribute("tenant",tenant);
		request.setAttribute("msg", msg);
		model.put("message", "HowToDoInJava Reader !!");
		return "userHomePage";
	}
	
	@RequestMapping("/manager")
	public String manager(Map<String, Object> model) {
		model.put("message", "HowToDoInJava Reader !!");
		return "Manager";
	}
	
	@RequestMapping("/practices/{practiceCode}")
	public String practice(Map<String, Object> model) {
		model.put("message", "HowToDoInJava Reader !!");
		return "PracticeDetails";
	}
	
	@RequestMapping(value="/processSelect",method=RequestMethod.POST)
	public ModelAndView selectProcecss(String process,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setProcess(process);
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/userScreen1", true),
	    	       model);
	}
	
	@RequestMapping(value="/complianceProjectSelect",method=RequestMethod.POST)
	public ModelAndView complianceProjectSelect(String project,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ProjectMst projects=new ProjectMst();
		projects=projectMstService.getProjectByProjectCode(accessObject.getTenantId(), project);
		TenantMst tenantMst=tenantMstService.getTenant(projects.getTenantId());
		OrganizationUnitMst orgUnit=new OrganizationUnitMst();
		orgUnit=organizationUnitMstService.getUnique(projects.getTenantId(), projects.getOrganizationUnit(), projects.getProcess());
		String[] businessModel=orgUnit.getOrganizationModel().split(",");
		List<PracticeAreaMst> allPracticeAreaExisting=accessObject.getPracticeAreaMst();
		List<PracticeAreaMst> allPracticeAreaNew=new ArrayList<PracticeAreaMst>();
		for(String bm:businessModel)
		{
		for(PracticeAreaMst practiceArea:allPracticeAreaExisting)
		{
			if(practiceArea.getBusinessModel().contains(bm))
			{
				allPracticeAreaNew.add(practiceArea);
			}
		}
		}
		if(null!=projects)
		{
			accessObject.setBusinessUnit(projects.getBusinessUnit());
			accessObject.setProject(projects.getProjectCode());
			accessObject.setOrganizationUnit(projects.getOrganizationUnit());
			accessObject.setBusinessModel(projects.getOrganizationModel());
			accessObject.setSelectedProject(projects.getProjectCode());
			accessObject.setSelectedProjectName(projects.getProjectName());
			accessObject.setPracticeAreaMst(allPracticeAreaNew);
		
		}
		
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/categories", true),
	    	       model);
	}
	
	
	
	@RequestMapping(value="/complianceCategorySelect",method=RequestMethod.POST)
	public ModelAndView complianceCategorySelect(String category,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setCategory(category);
				
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/capabilities", true),
	    	       model);
	}
	
	@RequestMapping(value="/dropDownCategorySelect",method=RequestMethod.POST)
	public ModelAndView dropDownCategorySelect(String categoryDropdown,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setCategory(categoryDropdown);
				
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/capabilities", true),
	    	       model);
	}
	
	@RequestMapping(value="/complianceCapabilitySelect",method=RequestMethod.POST)
	public ModelAndView complianceCapabilitySelect(String capability,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setCapability(capability);
				
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/practiceAreas", true),
	    	       model);
	}
	
	@RequestMapping(value="/dropdownCapabilitySelect",method=RequestMethod.POST)
	public ModelAndView dropdownCapabilitySelect(String capabilityDropdown,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setCapability(capabilityDropdown);
				
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/practiceAreas", true),
	    	       model);
	}
	
	@RequestMapping(value="/compliancePracticeAreaSelect",method=RequestMethod.POST)
	public ModelAndView compliancePracticeAreaSelect(String practiceArea,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setPracticeArea(practiceArea);
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/practices", true),
	    	       model);
	}
	
	
	@RequestMapping(value="/dropdownParcticeAreaSelect",method=RequestMethod.POST)
	public ModelAndView dropdownParcticeAreaSelect(String parcticeAreaDropdown,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setPracticeArea(parcticeAreaDropdown);
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/singlepracticeAreaDetails", true),
	    	       model);
	}
	
	@RequestMapping(value="/piidSelect",method=RequestMethod.POST)
	public ModelAndView piidSelect(String parcticeAreaDropdown,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setPracticeArea(parcticeAreaDropdown);
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/piiDDetails", true),
	    	       model);
	}
	
	
	
	
	@RequestMapping(value="/practiceAreaDetails",method=RequestMethod.POST)
	public ModelAndView practiceAreaDetails(String practiceArea,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setPracticeArea(practiceArea);
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/practiceAreaDetails", true),
	    	       model);
	}
	
	
	@RequestMapping(value="/compliancePracticeSelect",method=RequestMethod.POST)
	public ModelAndView compliancePracticeSelect(String practice,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setPractice(practice);
		
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/practiceDetails", true),
	    	       model);
	}
	
	@RequestMapping(value="/practiceDetailsSubmit",method=RequestMethod.POST)
	public ModelAndView practiceDetailsSubmit(PracticeWorkProductMst practice,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		List<PracticeWorkProductMst> practiceWorkProductMst=new ArrayList<PracticeWorkProductMst>();
		practiceWorkProductMst=practiceWorkProductMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess(),accessObject.getCmmiVersion());
		for(PracticeWorkProductMst practiceWorkProductMstList:practiceWorkProductMst)
		{
			if(practiceWorkProductMstList.getPracticeCode().equalsIgnoreCase(accessObject.getPractice()))
			{
				String q1=request.getParameter("q1"+practiceWorkProductMstList.getId());
				String q2=request.getParameter("q2"+practiceWorkProductMstList.getId());
				String q3=request.getParameter("q3"+practiceWorkProductMstList.getId());
				practiceWorkProductMstList.setQ1(q1);
				practiceWorkProductMstList.setQ2(q2);
				practiceWorkProductMstList.setQ3(q3);
				practiceWorkProductMstService.saveOrUpDate(practiceWorkProductMstList,accessObject.getCmmiVersion());
			}
		}
		String currentGap=request.getParameter("currentGapNptes");
		String practiceCompliance=request.getParameter("practiceCompliance");
		PracticeMst practicea=new PracticeMst();
		practicea=practiceMstService.getUnique(accessObject.getTenantId(), accessObject.getPractice(), 
				accessObject.getProcess(),accessObject.getOrganizationUnit(),
				accessObject.getBusinessUnit(),accessObject.getProject(),accessObject.getCmmiVersion());
		if(practiceCompliance.equalsIgnoreCase("Li"))
		{
			practicea.setPracticeComplianceLevel("Li");	
			practicea.setPracticeCompliance(0d);
		}
		if(practiceCompliance.equalsIgnoreCase("Di"))
		{
			practicea.setPracticeComplianceLevel("Di");	
			practicea.setPracticeCompliance(0.25);	
		}
		if(practiceCompliance.equalsIgnoreCase("Gi"))
		{
			practicea.setPracticeComplianceLevel("Gi");	
			practicea.setPracticeCompliance(0.75);		
		}
		if(practiceCompliance.equalsIgnoreCase("Mi"))
		{
			practicea.setPracticeComplianceLevel("Mi");	
			practicea.setPracticeCompliance(1.00);	
		}
		
		if(practiceCompliance.equalsIgnoreCase("0"))
		{
			practicea.setPracticeComplianceLevel("Li");	
		}
		if (practiceCompliance.equalsIgnoreCase("0.25"))
		{
			practicea.setPracticeComplianceLevel("Di");
		}
		if (practiceCompliance.equalsIgnoreCase("0.75"))
		{
			practicea.setPracticeComplianceLevel("Gi");
		}
		if (practiceCompliance.equalsIgnoreCase("1"))
		{
			practicea.setPracticeComplianceLevel("Mi");
		}


		
		practicea.setCurrentGapNptes(currentGap);
		practiceMstService.saveOrUpDate(practicea,accessObject);
		
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/userScreen6", true),
	    	       model);
	}
	
	@RequestMapping(value="/categorySelect",method=RequestMethod.POST)
	public ModelAndView categorySelect(String category,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setCategory(category);
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/userScreen2", true),
	    	       model);
	}
	
	@RequestMapping(value="/practiceSelect",method=RequestMethod.POST)
	public ModelAndView practiceSelect(String practice,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setPractice(practice);
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/userScreen6", true),
	    	       model);
	}
	
	@RequestMapping(value="/capabilitySelect",method=RequestMethod.POST)
	public ModelAndView capabilitySelect(String capability,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setCapability(capability);
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/userScreen3", true),
	    	       model);
	}
	@RequestMapping(value="/practiceAreaSelect",method=RequestMethod.POST)
	public ModelAndView practiceAreaSelect(String practiceArea,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setPracticeArea(practiceArea);
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/userScreen4", true),
	    	       model);
	}
	
	@RequestMapping(value="/practiceAreaPracticeSelect",method=RequestMethod.POST)
	public ModelAndView practiceAreaPracticeSelect(String practiceArea,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setPracticeArea(practiceArea);
		ModelMap model = new ModelMap();
		return new ModelAndView(
	    	       new RedirectView("/userScreen5", true),
	    	       model);
	}
	
	
	@RequestMapping(value="/loginUser",method=RequestMethod.POST)
    public ModelAndView sendForm(String UserName,String password,String tenantId, HttpServletRequest request) {
		logger.info("Initialted request for Login User");
		AccessObject accessObject=new AccessObject();
		request.getSession().setAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT,accessObject);
		
		ModelMap model = new ModelMap();
		UserMst userMst=new UserMst();
		String msg="";
		
		
		userMst=userMstService.validateUser(tenantId, UserName,password);
		if(null==userMst)
		{
			   model.put("erm","02");
				return new ModelAndView(
			    	       new RedirectView("/?tid="+tenantId, true),
			    	       model);
		}
		
		if(null!=userMst)
		{
			
			
			TenantMst tenant=new TenantMst();
			tenant=tenantMstService.getTenant(userMst.getTenantId());
			if(null!=tenant.getLicenseExpiryDate())
			{
				try {
					Date tenantExpiryDate=sdf.parse(tenant.getLicenseExpiryDate());
					Date systemDate=new Date();
					if(systemDate.after(tenantExpiryDate))
					{
						model.put("erm","03");
						return new ModelAndView(
					    	       new RedirectView("/?tid="+tenantId, true),
					    	       model);
						
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			{
				model.put("erm","04");
				return new ModelAndView(
			    	       new RedirectView("/?tid="+tenantId, true),
			    	       model);
			}
			accessObject.setCmmiVersion(userMst.getCmmiVersion());
			List<CategoryMst> categoryMst=new ArrayList<CategoryMst>();
			List<CapabilityMst> capabilityMst=new ArrayList<CapabilityMst>();
			List<PracticeAreaMst> practiceAreaMst=new ArrayList<PracticeAreaMst>();
			List<RelationMappingCategoryCapability> relationMappingCategoryCapability=new ArrayList<RelationMappingCategoryCapability>();
			List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea=new ArrayList<RelationMappingCapabilityPracticeArea>();
			List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
			relationMappingCategoryCapability=relationMappingCategoryCapabilityService.getAll("0",accessObject.getCmmiVersion());
			relationMappingCapabilityPracticeArea=relationMappingCapabilityPracticeAreaService.getAll("0",accessObject.getCmmiVersion());
			relationMappingPracticeAreaPractice=relationMappingPracticeAreaPracticeService.getAll("0",accessObject.getCmmiVersion());
			categoryMst=categoryMstService.getBaseCategory(accessObject.getCmmiVersion());
			capabilityMst=capabilityMstService.getBase(accessObject.getCmmiVersion());
			practiceAreaMst=practiceAreaMstService.getBase(accessObject.getCmmiVersion());
			accessObject.setCategoryMst(categoryMst);
			accessObject.setCapabilityMst(capabilityMst);
			accessObject.setPracticeAreaMst(practiceAreaMst);
			accessObject.setRelationMappingCapabilityPracticeArea(relationMappingCapabilityPracticeArea);
			accessObject.setRelationMappingCategoryCapability(relationMappingCategoryCapability);
			accessObject.setRelationMappingPracticeAreaPractice(relationMappingPracticeAreaPractice);
			accessObject.setLoginId(userMst.getUserName());
			accessObject.setTenantId(userMst.getTenantId());
			accessObject.setUserType(userMst.getUserType());
			accessObject.setResponseMsg(ApplicationConstants.SUCCESS);
			accessObject.setOrgName(tenant.getOrganizationName());
			accessObject.setProcess(userMst.getProcess());
			accessObject.setUserFirstName(userMst.getFirstName());
			accessObject.setAllProcess(tenant.getProcess());
			accessObject.setProject(userMst.getProject());
			accessObject.setPracticeLevel(tenant.getPracticeLevel());
			request.getSession().setAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT,accessObject);
			if(userMst.getIsFirstTimeLogin())
			{
				return new ModelAndView(
			    	       new RedirectView("/changePassword", true),
			    	       model);
				

			}
		}
		
		userMst.setLastLoginDate(new Date());
		userMstService.saveOrUpDate(userMst,accessObject.getLoginId());
		if(userMst.getUserType().equalsIgnoreCase("SUPERADMIN"))
		{
			return new ModelAndView(
		    	       new RedirectView("/versionSelect", true),
		    	       model);
				
		}
		return new ModelAndView(
	    	       new RedirectView("/home", true),
	    	       model);
		
		
//		if(userMst.getUserType().equalsIgnoreCase("SUPERADMIN"))
//		{
////		    return new ModelAndView(
////		    	       new RedirectView("/superAdmin", true),
////		    	       model);
//		    return new ModelAndView(
//		    	       new RedirectView("/admin", true),
//		    	       model);
//
//		}
//		if(userMst.getUserType().equalsIgnoreCase("ADMIN"))
//		{
//			return new ModelAndView(
//		    	       new RedirectView("/admin", true),
//		    	       model);
//		}
//		if(userMst.getUserType().equalsIgnoreCase("USER"))
//		{
//			return new ModelAndView(
//		    	       new RedirectView("/user", true),
//		    	       model);
//		}
//		if(userMst.getUserType().equalsIgnoreCase("MANAGER"))
//		{
//			return new ModelAndView(
//		    	       new RedirectView("/admin", true),
//		    	       model);
//		}
//		return null;

    }

	@RequestMapping("/gotoDashboard")
	public ModelAndView gotoDashboard(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		if(accessObject.getUserType().equalsIgnoreCase("SUPERADMIN"))
		{
			return new ModelAndView(
		    	       new RedirectView("/dashboardSuperAdmin", true),
		    	       model);
		}
		else if(accessObject.getUserType().equalsIgnoreCase("USER")||accessObject.getUserType().equalsIgnoreCase("ADMIN")||accessObject.getUserType().equalsIgnoreCase("MANAGER"))
		{
			return new ModelAndView(
		    	       new RedirectView("/dashboardHome", true),
		    	       model);
		}
		else
		{
			return new ModelAndView(
		    	       new RedirectView("/dashboard", true),
		    	       model);
		}
		
	}
	
	@RequestMapping("/dashboardSuperAdmin")
	public String dashboardSuperAdmin(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Dashboard");
		if(accessObject.getUserType().equalsIgnoreCase("SUPERADMIN"))
		{
			String msg="";
			List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
			List<ProjectMst> listOfProjects=new ArrayList<ProjectMst>();
			List<PracticeMst> allPractices=new ArrayList<PracticeMst>();
			List<TenantMst> allTenantList=new ArrayList<TenantMst>();
			List<TenantMst> tenantList=new ArrayList<TenantMst>();
			allTenantList=tenantMstService.getAll();
			for(TenantMst tenant:allTenantList)
			{
				if(tenant.getIsActive().equals(1))
				{
					tenantList.add(tenant);
				}
			}
			
			listOfProjects=projectMstService.getAllActiveProjects();
			for(ProjectMst projectCode:listOfProjects)
			{
			ProjectMst project=new ProjectMst();
			project=projectMstService.getProjectByProjectCode(projectCode.getTenantId(), projectCode.getProjectCode());
			if(null!=project)
			{
				TenantMst tenantMst=new TenantMst();
				tenantMst=tenantMstService.getTenant(project.getTenantId());
				Integer totalPracticeComplianceScore=0;
				Integer totalPracticeScore=0;
				Integer totalComplianceScore=0;
				if(tenantMst==null)
				{
					continue;
				}
				
				Integer practiceLevel=tenantMst.getPracticeLevel();
//				allPractices=practiceMstService.getPracticeByTenantOrganizationBusinesssProject(
//						project.getTenantId(), project.getOrganizationUnit(),
//						project.getBusinessUnit(), project.getProjectCode());
				allPractices=practiceMstService.getPracticeByTenantProject(project.getTenantId(),
						project.getProjectCode(), accessObject,accessObject.getCmmiVersion());
				
				
				for(PracticeMst practice:allPractices)
				{
//					if(!practice.getIsSaved())
//					{
//						continue;
//					}
//					
					totalPracticeComplianceScore=totalPracticeComplianceScore+((int)Math.round(practice.getPracticeCompliance()));
					totalPracticeScore=totalPracticeScore+100;
				}
				
if(totalPracticeComplianceScore>0)
{
	totalComplianceScore=Math.round(((totalPracticeComplianceScore*100)/totalPracticeScore));
}
				
				project.setComplianceScore(totalComplianceScore);
				allProjects.add(project);
			}
			}
			
			for(TenantMst tenant:tenantList)
			{
				for(ProjectMst project:allProjects)
				{
					
					if(project.getTenantId().equalsIgnoreCase(tenant.getTenantId()))
					{
						project.setDescription(tenant.getOrganizationName());
						tenant.setComplianceScore(tenant.getComplianceScore()+(int)Math.round(project.getComplianceScore()));
						tenant.setTotalComplianceScore(tenant.getTotalComplianceScore()+100);
					}
				}
				if(tenant.getTotalComplianceScore().equals(0))
				{
					tenant.setTotalComplianceScore(100);
				}
				tenant.setComplianceScore(((tenant.getComplianceScore()*100)/tenant.getTotalComplianceScore()));
				
			}
			request.setAttribute("tenantList",tenantList);
			request.setAttribute("allProjects",allProjects);
			request.setAttribute("msg", msg);
			return "dashboardSuperAdmin";
		}
		model.put("message", "You are in new page !!");
		return "dashboardhome";
		
	}
	
	@RequestMapping("/dashboard")
	public String dashboard(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<OrganizationUnitMst> allOrgUnits=new ArrayList<OrganizationUnitMst>();
		List<BusinessUnitMst> allBusinessUnit=new ArrayList<BusinessUnitMst>();
		List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
		List<PracticeMst> allPractice=new ArrayList<PracticeMst>();
		List<PracticeMst> selectedPractice=new ArrayList<PracticeMst>();
		List<CategoryMst> allCategory=new ArrayList<CategoryMst>();
		DashboardVO dashboardVo=new DashboardVO();
		dashboardVo.setTenantId(accessObject.getTenantId());
		dashboardVo.setOrganizationUnit(accessObject.getOrganizationUnit());
		dashboardVo.setProject(accessObject.getProject());
		TenantMst allTenants=new TenantMst();
		if(null==dashboardVo.getTenantId())
		{
			dashboardVo.setTenantId(accessObject.getTenantId());
			
		}
		allProjects=projectMstService.getAllProject(dashboardVo.getTenantId());
		allTenants=tenantMstService.getTenant(dashboardVo.getTenantId());
		allOrgUnits=organizationUnitMstService.getAllOrganizationUnit(dashboardVo.getTenantId());
		allBusinessUnit=businessUnitMstService.getAllBusinessUnit(dashboardVo.getTenantId());
		allPractice=practiceMstService.getAll(accessObject.getTenantId(),accessObject.getCmmiVersion());
		
		request.setAttribute("allProjects",allProjects);
		request.setAttribute("allTenants",allTenants);
		request.setAttribute("allPractice",allPractice);
		request.setAttribute("allOrgUnits",allOrgUnits);
		request.setAttribute("selectedPractice",selectedPractice);
		request.setAttribute("allBusinessUnit",allBusinessUnit);
		request.setAttribute("msg", msg);
		return "dashboard";
		
	}
	
	@RequestMapping(value="/selectDashboardProject",method=RequestMethod.POST)
	public ModelAndView save(String project,HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		ProjectMst projects=new ProjectMst();
		accessObject.setProject(project);
		projects=projectMstService.getProjectByProjectCode(accessObject.getTenantId(), accessObject.getProject());
		accessObject.setOrganizationUnit(projects.getOrganizationUnit());
		accessObject.setBusinessUnit(projects.getBusinessUnit());
	return new ModelAndView(
	    	       new RedirectView("/dashboardProject", true),
	    	       model);
	}
	
	@RequestMapping("/dashboardWidget")
	public String dashboardWidget(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		ProjectMst project=new ProjectMst();
		project=projectMstService.getProjectByProjectCode(accessObject.getTenantId(), accessObject.getProject());
		List<CategoryMst> allCategory=new ArrayList<CategoryMst>();
		//allCategory=categoryMstService.getByTenantAndProject(project.getTenantId(), project.getProjectCode());
		allCategory=accessObject.getCategoryMst();
	//	List<CapabilityMst> allCapability=new ArrayList<CapabilityMst>();
		//allCapability=capabilityMstService.getByTenantAndProject(project.getTenantId(), project.getProjectCode());
		
		List<PracticeAreaMst> allPracticeArea=new ArrayList<PracticeAreaMst>();
		List<PracticeAreaMst> allPracticeAreaBase=new ArrayList<PracticeAreaMst>();
		allPracticeAreaBase=accessObject.getPracticeAreaMst();
		TenantMst tenantMst=tenantMstService.getTenant(project.getTenantId());
		OrganizationUnitMst orgUnit=new OrganizationUnitMst();
		orgUnit=organizationUnitMstService.getUnique(project.getTenantId(), project.getOrganizationUnit(), project.getProcess());
		String[] businessModel=orgUnit.getOrganizationModel().split(",");
		
		for(String bm:businessModel)
		{
		for(PracticeAreaMst practiceArea:allPracticeAreaBase)
		{
			if(practiceArea.getBusinessModel().contains(bm))
			{
				allPracticeArea.add(practiceArea);
			}
		}
		}
		List<RelationMappingCategoryCapability> relationMappingCategoryCapability=new ArrayList<RelationMappingCategoryCapability>();
		relationMappingCategoryCapability=accessObject.getRelationMappingCategoryCapability();
		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea=new ArrayList<RelationMappingCapabilityPracticeArea>();
		relationMappingCapabilityPracticeArea=accessObject.getRelationMappingCapabilityPracticeArea();
		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
		relationMappingPracticeAreaPractice=accessObject.getRelationMappingPracticeAreaPractice();
		List<PracticeMst> allPractices=new ArrayList<PracticeMst>();
		List<PracticeMst> projectPractices=new ArrayList<PracticeMst>();
		allPractices=practiceMstService.getPracticeByTenantOrganizationBusinesssProject(
				project.getTenantId(), project.getOrganizationUnit(),
				project.getBusinessUnit(), project.getProjectCode(),accessObject.getCmmiVersion());
		for(PracticeMst practice:allPractices)
		{
			if(!practice.getIsSaved())
			{
				continue;
			}
			
			Boolean ignoreRecord=Boolean.FALSE;
			for(RelationMappingPracticeAreaPractice rel:relationMappingPracticeAreaPractice)
			{
				if(rel.getPracticeCode().equalsIgnoreCase(practice.getPracticeCode()))
				{
					if(rel.getPracticeLevel()>tenantMst.getPracticeLevel())
					{
						ignoreRecord=Boolean.TRUE;
					}
					practice.setPracticeAreaCode(rel.getPracticeAreaCode());
					practice.setPracticeLevelCode(rel.getPracticeLevel());
					break;
				}
				
			}
			if(ignoreRecord)
			{
				continue;
			}
			
			for(RelationMappingCapabilityPracticeArea rel:relationMappingCapabilityPracticeArea)
			{
				if(rel.getPracticeAreaCode().equalsIgnoreCase(practice.getPracticeAreaCode()))
				{
					practice.setCapabilityCode(rel.getCapabilityCode());
					break;
				}
			}
			for(RelationMappingCategoryCapability rel:relationMappingCategoryCapability)
			{
				if(rel.getCapabilityCode().equalsIgnoreCase(practice.getCapabilityCode()))
				{
					practice.setCategoryCode(rel.getCategoryCode());
				}
			}
			for(CategoryMst cat:allCategory)
			{
				if(cat.getCategoryCode().equalsIgnoreCase(practice.getCategoryCode()))
				{
					practice.setCategoryName(cat.getCategoryName());
					break;
				}
			}
			for(CapabilityMst cap:accessObject.getCapabilityMst())
			{
				if(cap.getCapabilityCode().equalsIgnoreCase(practice.getCapabilityCode()))
				{
					practice.setCapabilityName(cap.getCapabilityName());
				}
			}
			for(PracticeAreaMst pa:allPracticeArea)
			{
				if(practice.getPracticeAreaCode().equalsIgnoreCase(pa.getPracticeAreaCode()))
				{
					practice.setPracticeAreaName(pa.getPracticeAreaName());
				}
			}
			projectPractices.add(practice);
			
		}
		
		request.setAttribute("msg", msg);
		request.setAttribute("projectPractices", projectPractices);
		request.setAttribute("allCategory", allCategory);
		request.setAttribute("allCapability", accessObject.getCapabilityMst());
		request.setAttribute("allPracticeArea", allPracticeArea);
		request.setAttribute("projectName", project.getProjectName());
		request.setAttribute("projectCode", project.getProjectCode());
		
		
		return "dashboardProjectTest";
		
	}
	
	@RequestMapping("/dashboardProject")
	public String dashboardProject(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		ProjectMst project=new ProjectMst();
		project=projectMstService.getProjectByProjectCode(accessObject.getTenantId(), accessObject.getProject());
		List<CategoryMst> allCategory=new ArrayList<CategoryMst>();
		//allCategory=categoryMstService.getByTenantAndProject(project.getTenantId(), project.getProjectCode());
		allCategory=accessObject.getCategoryMst();
		
		List<PracticeAreaMst> allPracticeArea=new ArrayList<PracticeAreaMst>();
		List<PracticeAreaMst> allPracticeAreaBase=new ArrayList<PracticeAreaMst>();
		allPracticeAreaBase=accessObject.getPracticeAreaMst();
		TenantMst tenantMst=tenantMstService.getTenant(project.getTenantId());
		OrganizationUnitMst orgUnit=new OrganizationUnitMst();
		orgUnit=organizationUnitMstService.getUnique(project.getTenantId(), project.getOrganizationUnit(), project.getProcess());
		String[] businessModel=orgUnit.getOrganizationModel().split(",");
		
		for(String bm:businessModel)
		{
		for(PracticeAreaMst practiceArea:allPracticeAreaBase)
		{
			if(practiceArea.getBusinessModel().contains(bm))
			{
				allPracticeArea.add(practiceArea);
			}
		}
		}
		
		List<PracticeMst> allPractices=new ArrayList<PracticeMst>();
		List<PracticeMst> projectPractices=new ArrayList<PracticeMst>();
		List<DashBoardVo> dashBoardVoPracticeArea=new ArrayList<DashBoardVo>();
		List<DashBoardVo> dashBoardVoCategory=new ArrayList<DashBoardVo>();
		List<DashBoardVo> dashBoardVoCapability=new ArrayList<DashBoardVo>();
		allPractices=practiceMstService.getPracticeByTenantProject(project.getTenantId(), project.getProjectCode(), accessObject,accessObject.getCmmiVersion());
		for(PracticeMst practice:allPractices)
		{
			for(CategoryMst cat:allCategory)
			{
				if(cat.getCategoryCode().equalsIgnoreCase(practice.getCategoryCode()))
				{
					practice.setCategoryName(cat.getCategoryName());
					break;
				}
			}
			for(CapabilityMst cap:accessObject.getCapabilityMst())
			{
				if(cap.getCapabilityCode().equalsIgnoreCase(practice.getCapabilityCode()))
				{
					practice.setCapabilityName(cap.getCapabilityName());
				}
			}
			for(PracticeAreaMst pa:allPracticeArea)
			{
				if(practice.getPracticeAreaCode().equalsIgnoreCase(pa.getPracticeAreaCode()))
				{
					practice.setPracticeAreaName(pa.getPracticeAreaName());
				}
			}
			projectPractices.add(practice);
			
		}
		
		for(PracticeMst practice:allPractices)
		{
			
			String practiceArea=practice.getPracticeAreaCode();
			Boolean recordFound=Boolean.FALSE;
			if(dashBoardVoPracticeArea.size()>0)
			{
				for(DashBoardVo dbvo:dashBoardVoPracticeArea)
				{
					if(dbvo.getProjectCode().equalsIgnoreCase(practice.getProject()))
					{
						
						if(dbvo.getPracticeAreaCode().equalsIgnoreCase(practice.getPracticeAreaCode()))
						{
							dbvo.setComplianceScore(dbvo.getComplianceScore()+practice.getPracticeCompliance());
							dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
							recordFound=Boolean.TRUE;
						}
						
					}
					
				}
				if(!recordFound)
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
					dashBoardVo.setPracticeCode(practice.getPracticeCode());
					dashBoardVo.setCategoryCode(practice.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
					dashBoardVo.setProjectCode(practice.getProject());
					dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoPracticeArea.add(dashBoardVo);		
				}
			}
			else
			{
				DashBoardVo dashBoardVo=new DashBoardVo();
				dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
				dashBoardVo.setPracticeCode(practice.getPracticeCode());
				dashBoardVo.setCategoryCode(practice.getCategoryCode());
				dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
				dashBoardVo.setProjectCode(practice.getProject());
				dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
				dashBoardVo.setTotalComplianceScore(100.00);
				dashBoardVoPracticeArea.add(dashBoardVo);
			}
		}
		
		for(PracticeMst practice:allPractices)
		{
			
			String category=practice.getCategoryCode();
			Boolean recordFound=Boolean.FALSE;
			if(dashBoardVoCategory.size()>0)
			{
				for(DashBoardVo dbvo:dashBoardVoCategory)
				{
					if(dbvo.getProjectCode().equalsIgnoreCase(practice.getProject()))
					{
						
						if(dbvo.getCategoryCode().equalsIgnoreCase(category))
						{
							dbvo.setComplianceScore(dbvo.getComplianceScore()+practice.getPracticeCompliance());
							dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
							recordFound=Boolean.TRUE;
						}
						
					}
					
				}
				if(!recordFound)
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
					dashBoardVo.setProjectName(practice.getProject());
					dashBoardVo.setCategoryCode(practice.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
					dashBoardVo.setPracticeCode(practice.getPracticeCode());
					dashBoardVo.setProjectCode(practice.getProject());
					dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoCategory.add(dashBoardVo);		
				}
			}
			else
			{
				DashBoardVo dashBoardVo=new DashBoardVo();
				dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
				dashBoardVo.setProjectName(practice.getProject());
				dashBoardVo.setCategoryCode(practice.getCategoryCode());
				dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
				dashBoardVo.setPracticeCode(practice.getPracticeCode());
				dashBoardVo.setProjectCode(practice.getProject());
				dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
				dashBoardVo.setTotalComplianceScore(100.00);
				dashBoardVoCategory.add(dashBoardVo);
			}
		}
		
		for(PracticeMst practice:allPractices)
		{
			
			String capability=practice.getCapabilityCode();
			Boolean recordFound=Boolean.FALSE;
			if(dashBoardVoCapability.size()>0)
			{
				for(DashBoardVo dbvo:dashBoardVoCapability)
				{
					if(dbvo.getProjectCode().equalsIgnoreCase(practice.getProject()))
					{
						
						if(dbvo.getCapabilityCode().equalsIgnoreCase(capability))
						{
							dbvo.setComplianceScore(dbvo.getComplianceScore()+practice.getPracticeCompliance());
							dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
							recordFound=Boolean.TRUE;
						}
						
					}
					
				}
				if(!recordFound)
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
					dashBoardVo.setProjectName(practice.getProject());
					dashBoardVo.setCategoryCode(practice.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
					dashBoardVo.setPracticeCode(practice.getPracticeCode());
					dashBoardVo.setProjectCode(practice.getProject());
					dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoCapability.add(dashBoardVo);		
				}
			}
			else
			{
				DashBoardVo dashBoardVo=new DashBoardVo();
				dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
				dashBoardVo.setProjectName(practice.getProject());
				dashBoardVo.setCategoryCode(practice.getCategoryCode());
				dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
				dashBoardVo.setPracticeCode(practice.getPracticeCode());
				dashBoardVo.setProjectCode(practice.getProject());
				dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
				dashBoardVo.setTotalComplianceScore(100.00);
				dashBoardVoCapability.add(dashBoardVo);
			}
		}
		
		
		
		
		
		for(DashBoardVo dbc:dashBoardVoCategory)
		{
			if(dbc.getComplianceScore()>0)
			{
				Double totalComplianceScore=(double) Math.round(((double) dbc.getComplianceScore()*100)/dbc.getTotalComplianceScore());
				//Double totalComplianceScore=(double) Math.round((( dbc.getComplianceScore()*100)/dbc.getTotalComplianceScore())*100)/100;
				dbc.setComplianceScore(totalComplianceScore);
			}
			
		}
		
		for(DashBoardVo dbcap:dashBoardVoCapability)
		{
			if(dbcap.getComplianceScore()>0)
			{
				Double totalComplianceScore=(double) Math.round(((double) dbcap.getComplianceScore()*100)/dbcap.getTotalComplianceScore());
//				Double totalComplianceScore=(double) Math.round((( dbcap.getComplianceScore()*100)/dbcap.getTotalComplianceScore())*100)/100;
				dbcap.setComplianceScore(totalComplianceScore);
			}
			
		}
		
		for(DashBoardVo dPA:dashBoardVoPracticeArea)
		{
			if(dPA.getComplianceScore()>0)
			{
				Double totalComplianceScore=(double) Math.round(((double) dPA.getComplianceScore()*100)/dPA.getTotalComplianceScore());
				dPA.setComplianceScore(totalComplianceScore);
			}
			
		}
		
		request.setAttribute("msg", msg);
		request.setAttribute("dashBoardVoPracticeArea", dashBoardVoPracticeArea);
		request.setAttribute("dashBoardVoCapability", dashBoardVoCapability);
		request.setAttribute("dashBoardVoCategory", dashBoardVoCategory);
		request.setAttribute("projectPractices", projectPractices);
		request.setAttribute("allCategory", allCategory);
		request.setAttribute("allCapability", accessObject.getCapabilityMst());
		request.setAttribute("allPracticeArea", allPracticeArea);
		request.setAttribute("projectName", project.getProjectName());
		request.setAttribute("projectCode", project.getProjectCode());
		
		
		return "dashboardProject";
		
	}
	
	@RequestMapping("/superAdmin")
	public String superAdmin(Map<String, Object> model,HttpServletRequest request) {
		List<DashBoardVo> dashBoardVoList=practiceMstService.getPracticeComplianceByTenant();
		Map<String,Double> complianceListForAll=new HashMap<String,Double>(); 
		complianceListForAll=getComplianceForTenantAndProcess();
		request.setAttribute("dashBoardVoList", dashBoardVoList);
		request.setAttribute("complianceListForAll", complianceListForAll);
		return "SuperAdmin";
	}
	
	
	
	public Map<String,Double> getComplianceForTenant(String tenantId)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		
		Map<String,Double> complianceList=new HashMap<String,Double>();
		TenantMst tenant=new TenantMst();
		tenant=tenantMstService.getTenant(tenantId);
		
		String[] processes=null;
		if(null!=tenant.getProcess())
		{
			processes=tenant.getProcess().split(",");
		}
		if(processes.length>0)
		{
			for(String process:processes)
			{
				Double result=practiceMstService.getPracticeComplianceByTenantAndProcess(tenantId,accessObject.getCmmiVersion());
				complianceList.put(process, result);
			}
		}
		
		return complianceList;
		
	}
	
	public Map<String,Double> getComplianceForTenantAndProcess()
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		
		Map<String,Double> complianceList=new HashMap<String,Double>();
		List<TenantMst> tenant=new ArrayList<TenantMst>();
		tenant=tenantMstService.getAll();
		if(null!=tenant)
		{
			for(TenantMst tid:tenant)
			{
				Double result=practiceMstService.getPracticeComplianceByTenantAndProcess(tid.getTenantId(),accessObject.getCmmiVersion());
				complianceList.put(tid.getOrganizationName(), result);
			}
		}
		return complianceList;
	}
	
	
    	
	@RequestMapping("/signup")
	public String signup(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Setup");
		String msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
		allProjects=projectMstService.getAllProject(accessObject.getTenantId());
		if(null==allProjects)
		{
			msg="No Projects Created. Please create Projects First. Cannot create User";
			request.setAttribute("msg", msg);
			return "signup";
		}
		List<UserMst> users=userMstService.getAllUsers(accessObject.getTenantId());
		request.setAttribute("allProjects", allProjects);
		request.setAttribute("users", users);
		request.setAttribute("msg", msg);
		model.put("message", "You are in new page !!");
		return "signup";
	}
	
	@RequestMapping("/userAdmin")
	public String userAdmin(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Setup");
		String msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<UserMst> users=userMstService.getAllUsersForAdmin(accessObject.getTenantId());
		request.setAttribute("users", users);
		request.setAttribute("msg", msg);
		model.put("message", "You are in new page !!");
		return "userAdmin";
	}

	@RequestMapping("/viewLicense")
	public String viewLicense(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("About");
		String msg=accessObject.getMsg();
		accessObject.setMsg("");
		TenantMst tenantDetails=new TenantMst();
		tenantDetails=tenantMstService.getTenant(accessObject.getTenantId());
		request.setAttribute("expiryDate", tenantDetails.getLicenseExpiryDate());
		request.setAttribute("msg", msg);
		model.put("message", "You are in new page !!");
		return "ViewLicense";
	}

	@RequestMapping("/about")
	public String about(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("About");
		String msg=accessObject.getMsg();
		accessObject.setMsg("");
		TenantMst tenantDetails=new TenantMst();
		tenantDetails=tenantMstService.getTenant(accessObject.getTenantId());
		request.setAttribute("expiryDate", tenantDetails.getLicenseExpiryDate());
		request.setAttribute("msg", msg);
		model.put("message", "You are in new page !!");
		return "About";
	}
	
	
	
	@RequestMapping("/profile")
	public String profile(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Profile");
		String msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<UserMst> usersAll=userMstService.getAllUsers(accessObject.getTenantId());
		List<UserMst> users=new ArrayList<UserMst>();
		if(null!=usersAll)
		{
			for(UserMst user:usersAll)
			{
				if(user.getUserName().equalsIgnoreCase(accessObject.getLoginId()))
				{
					users.add(user);
				}
			}
		}
		request.setAttribute("users", users);
		request.setAttribute("msg", msg);
		model.put("message", "You are in new page !!");
		return "profile";
	}
	
	@RequestMapping("/adminUserMst")
	public String adminUserMst(Map<String, Object> model) {
		model.put("message", "You are in new page !!");
		return "AdminUserMst";
	}
	
	@RequestMapping("/admin")
	public String admin(Map<String, Object> model) {
		model.put("message", "You are in new page !!");
		return "Admin";
	}
	
	@RequestMapping("/versionSelect")
	public String versionSelect(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Home");
		if(accessObject.getUserType().equalsIgnoreCase("SUPERADMIN"))
		{
			List<LookupMst> cmmiVersionLookup=new ArrayList<LookupMst>();
			cmmiVersionLookup=lookupMstService.getLookupMst(accessObject.getTenantId(),ApplicationConstants.LOOKUP_CMMIVERSION);
			request.setAttribute("cmmiVersionLookup", cmmiVersionLookup);
			
					return "versionSelect";
		}
		model.put("message", "You are in new page !!");
		return "home";
	}
	
	@RequestMapping("/versionSelected")
	public ModelAndView versionSelected(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Home");
		
		UserMst userMst=userMstService.getByName(accessObject.getTenantId(),accessObject.getLoginId());
		if(userMst.getUserType().equalsIgnoreCase("SUPERADMIN"))
		{
		String versionSelected=request.getParameter("versionSelected");
		accessObject.setCmmiVersion(versionSelected);
		}
		TenantMst tenant=tenantMstService.getTenant(accessObject.getTenantId());
		List<CategoryMst> categoryMst=new ArrayList<CategoryMst>();
		List<CapabilityMst> capabilityMst=new ArrayList<CapabilityMst>();
		List<PracticeAreaMst> practiceAreaMst=new ArrayList<PracticeAreaMst>();
		List<RelationMappingCategoryCapability> relationMappingCategoryCapability=new ArrayList<RelationMappingCategoryCapability>();
		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea=new ArrayList<RelationMappingCapabilityPracticeArea>();
		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
		relationMappingCategoryCapability=relationMappingCategoryCapabilityService.getAll("0",accessObject.getCmmiVersion());
		relationMappingCapabilityPracticeArea=relationMappingCapabilityPracticeAreaService.getAll("0",accessObject.getCmmiVersion());
		relationMappingPracticeAreaPractice=relationMappingPracticeAreaPracticeService.getAll("0",accessObject.getCmmiVersion());
		categoryMst=categoryMstService.getBaseCategory(accessObject.getCmmiVersion());
		capabilityMst=capabilityMstService.getBase(accessObject.getCmmiVersion());
		practiceAreaMst=practiceAreaMstService.getBase(accessObject.getCmmiVersion());
		accessObject.setCategoryMst(categoryMst);
		accessObject.setCapabilityMst(capabilityMst);
		accessObject.setPracticeAreaMst(practiceAreaMst);
		accessObject.setRelationMappingCapabilityPracticeArea(relationMappingCapabilityPracticeArea);
		accessObject.setRelationMappingCategoryCapability(relationMappingCategoryCapability);
		accessObject.setRelationMappingPracticeAreaPractice(relationMappingPracticeAreaPractice);
		accessObject.setLoginId(userMst.getUserName());
		accessObject.setTenantId(userMst.getTenantId());
		accessObject.setUserType(userMst.getUserType());
		accessObject.setResponseMsg(ApplicationConstants.SUCCESS);
		accessObject.setOrgName(tenant.getOrganizationName());
		accessObject.setProcess(userMst.getProcess());
		accessObject.setUserFirstName(userMst.getFirstName());
		accessObject.setAllProcess(tenant.getProcess());
		accessObject.setProject(userMst.getProject());
		if(userMst.getUserType().equalsIgnoreCase("SUPERADMIN"))
		{
			
		}
		else
		{
		accessObject.setCmmiVersion(userMst.getCmmiVersion());
		}
		accessObject.setPracticeLevel(tenant.getPracticeLevel());
		
		request.getSession().setAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT,accessObject);

		return new ModelAndView(
	    	       new RedirectView("/home", true),
	    	       model);
		
	}
	
	
	@RequestMapping("/home")
	public String homePage(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Home");
		if(accessObject.getUserType().equalsIgnoreCase("USER"))
		{
			accessObject.setSelectedProject("");
			accessObject.setSelectedProjectName("");
			UserMst userMst=new UserMst();
			userMst=userMstService.getByName(accessObject.getTenantId(), accessObject.getLoginId());
			String msg="";
			msg=accessObject.getMsg();
			accessObject.setMsg("");
			String[] userProjects=userMst.getProject().split(",");
			List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
			for(String projectCode:userProjects)
			{
			ProjectMst project=new ProjectMst();
			project=projectMstService.getProjectByProjectCode(accessObject.getTenantId(), projectCode);
			if(null!=project)
			{
				allProjects.add(project);
			}
			}
//			accessObject.setBusinessUnit("");
//			accessObject.setOrganizationUnit("");
//			accessObject.setBusinessModel("");
			
			request.setAttribute("allProjects",allProjects);
			request.setAttribute("msg", msg);
			return "compliance";
		}
		model.put("message", "You are in new page !!");
		return "home";
	}
	
	@RequestMapping(value="/superAdmindashboardHome",method=RequestMethod.POST)
	public String superAdmindashboardHome(String selectedtenantId,HttpServletRequest request)
	{

		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String msg="";
		{
			List<DashBoardVo> dashBoardVoCategoryAll=new ArrayList<DashBoardVo>();
			List<DashBoardVo> dashBoardVoCategory=new ArrayList<DashBoardVo>();
			List<DashBoardVo> dashBoardVoCapability=new ArrayList<DashBoardVo>();
			List<DashBoardVo> dashBoardVoPracticeArea=new ArrayList<DashBoardVo>();
			List<ProjectMst> orgProjects=new ArrayList<ProjectMst>();
			List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
			List<PracticeMst> allPractices=new ArrayList<PracticeMst>();

				UserMst userMst=new UserMst();
				userMst=userMstService.getByName(accessObject.getTenantId(), accessObject.getLoginId());
				TenantMst tenantMst=new TenantMst();
				tenantMst=tenantMstService.getTenant(userMst.getTenantId());
		
				Integer practiceLevel=tenantMst.getPracticeLevel();
				String[] userProjects=userMst.getProject().split(",");	
				
				
				orgProjects=projectMstService.getAllProject(selectedtenantId);
				if(!accessObject.getUserType().equalsIgnoreCase("USER"))
				{
					userProjects=new String[orgProjects.size()];
					for(int i=0;i<orgProjects.size();i++)
					{
						userProjects[i]=orgProjects.get(i).getProjectCode();
					}
				}
//			allPractices=practiceMstService.getAll(selectedtenantId);
			for(String projectCode:userProjects)
			{
			ProjectMst project=new ProjectMst();
			project=projectMstService.getProjectByProjectCode(selectedtenantId, projectCode);
			allPractices=practiceMstService.getPracticeByTenantProject(project.getTenantId(), project.getProjectCode(), accessObject,accessObject.getCmmiVersion());
			List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea=new ArrayList<RelationMappingCapabilityPracticeArea>();
			relationMappingCapabilityPracticeArea=accessObject.getRelationMappingCapabilityPracticeArea();
			
			List<RelationMappingCategoryCapability> relationMappingCategoryCapability=new ArrayList<RelationMappingCategoryCapability>();
			relationMappingCategoryCapability=accessObject.getRelationMappingCategoryCapability();
			
			List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
			relationMappingPracticeAreaPractice=accessObject.getRelationMappingPracticeAreaPractice();
			
			Integer totalPracticeComplianceScore=0;
			Integer totalComplianceScore=0;
			Integer  totalPracticeScore=0;
			
			if(null!=project)
			{
				for(PracticeMst practice:allPractices)
				{
//					if(!practice.getIsSaved())
//					{
//						continue;
//					}
					 
					
					
					if(practice.getProject().equalsIgnoreCase(project.getProjectCode()))
					{
						DashBoardVo dashBoardVo=new DashBoardVo();
						dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
						dashBoardVo.setProjectName(project.getProjectName());
						dashBoardVo.setPracticeCode(practice.getPracticeCode());
						dashBoardVo.setCategoryCode(practice.getCategoryCode());
						dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
						dashBoardVo.setProjectCode(practice.getProject());
						dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
						dashBoardVoCategoryAll.add(dashBoardVo);
						totalPracticeComplianceScore=totalPracticeComplianceScore+((int)Math.round(practice.getPracticeCompliance()));
						totalPracticeScore=totalPracticeScore+100;
						
					}
				}
				totalComplianceScore=Math.round(((totalPracticeComplianceScore*100)/totalPracticeScore));
				project.setComplianceScore(totalComplianceScore);
				allProjects.add(project);
			}
			}
			
			for(DashBoardVo db:dashBoardVoCategoryAll)
			{
				String project=db.getProjectCode();
				String category=db.getCategoryCode();
				Boolean recordFound=Boolean.FALSE;
				if(dashBoardVoCategory.size()>0)
				{
					for(DashBoardVo dbvo:dashBoardVoCategory)
					{
						if(dbvo.getProjectCode().equalsIgnoreCase(db.getProjectCode()))
						{
							
							if(dbvo.getCategoryCode().equalsIgnoreCase(db.getCategoryCode()))
							{
								dbvo.setComplianceScore(dbvo.getComplianceScore()+db.getComplianceScore());
								dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
								recordFound=Boolean.TRUE;
							}
							
						}
						
					}
					if(!recordFound)
					{
						DashBoardVo dashBoardVo=new DashBoardVo();
						dashBoardVo.setCapabilityCode(db.getCapabilityCode());
						dashBoardVo.setProjectName(db.getProjectName());
						dashBoardVo.setPracticeCode(db.getPracticeCode());
						dashBoardVo.setCategoryCode(db.getCategoryCode());
						dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
						dashBoardVo.setProjectCode(db.getProjectCode());
						dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
						dashBoardVo.setTotalComplianceScore(100.00);
						dashBoardVoCategory.add(dashBoardVo);		
					}
				}
				else
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(db.getCapabilityCode());
					dashBoardVo.setProjectName(db.getProjectName());
					dashBoardVo.setPracticeCode(db.getPracticeCode());
					dashBoardVo.setCategoryCode(db.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
					dashBoardVo.setProjectCode(db.getProjectCode());
					dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoCategory.add(dashBoardVo);
				}
			}
			
			for(DashBoardVo db:dashBoardVoCategoryAll)
			{
				String project=db.getProjectCode();
				String capability=db.getCapabilityCode();
				Boolean recordFound=Boolean.FALSE;
				if(dashBoardVoCapability.size()>0)
				{
					for(DashBoardVo dbvo:dashBoardVoCapability)
					{
						if(dbvo.getProjectCode().equalsIgnoreCase(db.getProjectCode()))
						{
							
							if(dbvo.getCapabilityCode().equalsIgnoreCase(db.getCapabilityCode()))
							{
								dbvo.setComplianceScore(dbvo.getComplianceScore()+db.getComplianceScore());
								dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
								recordFound=Boolean.TRUE;
							}
							
						}
						
					}
					if(!recordFound)
					{
						DashBoardVo dashBoardVo=new DashBoardVo();
						dashBoardVo.setCapabilityCode(db.getCapabilityCode());
						dashBoardVo.setProjectName(db.getProjectName());
						dashBoardVo.setPracticeCode(db.getPracticeCode());
						dashBoardVo.setCategoryCode(db.getCategoryCode());
						dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
						dashBoardVo.setProjectCode(db.getProjectCode());
						dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
						dashBoardVo.setTotalComplianceScore(100.00);
						dashBoardVoCapability.add(dashBoardVo);		
					}
				}
				else
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(db.getCapabilityCode());
					dashBoardVo.setProjectName(db.getProjectName());
					dashBoardVo.setPracticeCode(db.getPracticeCode());
					dashBoardVo.setCategoryCode(db.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
					dashBoardVo.setProjectCode(db.getProjectCode());
					dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoCapability.add(dashBoardVo);
				}
			}
			
			for(DashBoardVo db:dashBoardVoCategoryAll)
			{
				String project=db.getProjectCode();
				String practiceArea=db.getPracticeAreaCode();
				Boolean recordFound=Boolean.FALSE;
				if(dashBoardVoPracticeArea.size()>0)
				{
					for(DashBoardVo dbvo:dashBoardVoPracticeArea)
					{
						if(dbvo.getProjectCode().equalsIgnoreCase(db.getProjectCode()))
						{
							
							if(dbvo.getPracticeAreaCode().equalsIgnoreCase(db.getPracticeAreaCode()))
							{
								dbvo.setComplianceScore(dbvo.getComplianceScore()+db.getComplianceScore());
								dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
								recordFound=Boolean.TRUE;
							}
							
						}
						
					}
					if(!recordFound)
					{
						DashBoardVo dashBoardVo=new DashBoardVo();
						dashBoardVo.setCapabilityCode(db.getCapabilityCode());
						dashBoardVo.setProjectName(db.getProjectName());
						dashBoardVo.setPracticeCode(db.getPracticeCode());
						dashBoardVo.setCategoryCode(db.getCategoryCode());
						dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
						dashBoardVo.setProjectCode(db.getProjectCode());
						dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
						dashBoardVo.setTotalComplianceScore(100.00);
						dashBoardVoPracticeArea.add(dashBoardVo);		
					}
				}
				else
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(db.getCapabilityCode());
					dashBoardVo.setProjectName(db.getProjectName());
					dashBoardVo.setPracticeCode(db.getPracticeCode());
					dashBoardVo.setCategoryCode(db.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
					dashBoardVo.setProjectCode(db.getProjectCode());
					dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoPracticeArea.add(dashBoardVo);
				}
			}
			
			
			
			for(DashBoardVo dbc:dashBoardVoCategory)
			{
				if(dbc.getComplianceScore()>0)
				{
					//Double totalComplianceScore=(double) Math.round(( dbc.getComplianceScore()*100.00)/dbc.getTotalComplianceScore());
					Double totalComplianceScore=(double) Math.round(((double) dbc.getComplianceScore()*100)/dbc.getTotalComplianceScore());
					dbc.setComplianceScore(totalComplianceScore);
				}
				
			}
			
			for(DashBoardVo dbcap:dashBoardVoCapability)
			{
				if(dbcap.getComplianceScore()>0)
				{
//					Double totalComplianceScore=(double) Math.round((dbcap.getComplianceScore()*100.00)/dbcap.getTotalComplianceScore());
					Double totalComplianceScore=(double) Math.round(((double) dbcap.getComplianceScore()*100)/dbcap.getTotalComplianceScore());
					dbcap.setComplianceScore(totalComplianceScore);
				}
				
			}
			
			for(DashBoardVo dPA:dashBoardVoPracticeArea)
			{
				if(dPA.getComplianceScore()>0)
				{
//					Double totalComplianceScore=Math.round( dPA.getComplianceScore()*100.00)/dPA.getTotalComplianceScore();
					Double totalComplianceScore=(double) Math.round(((double) dPA.getComplianceScore()*100)/dPA.getTotalComplianceScore());
					dPA.setComplianceScore(totalComplianceScore);
				}
				
			}
			request.setAttribute("dashBoardVoCategory",dashBoardVoCategory);
			request.setAttribute("dashBoardVoCapability",dashBoardVoCapability);
			request.setAttribute("dashBoardVoPracticeArea",dashBoardVoPracticeArea);
			request.setAttribute("dashBoardVoAll",dashBoardVoCategoryAll);
			request.setAttribute("allProjects",allProjects);
			request.setAttribute("selectedtenantId",selectedtenantId);
			request.setAttribute("msg", msg);
			return "dashboardhome";
	
		}
		
	}
	
	@RequestMapping("/dashboardHome")
	public String dashboardhomePage(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Dashboard");
		String msg="";
		if(accessObject.getUserType().equalsIgnoreCase("USER1"))
		{
			List<DashBoardVo> dashBoardVoAll=new ArrayList<DashBoardVo>();
			List<DashBoardVo> dashBoardVoAll1=new ArrayList<DashBoardVo>();
			List<DashBoardVo> dashBoardVoAll2=new ArrayList<DashBoardVo>();
			List<DashBoardVo> dashBoardVoAll3=new ArrayList<DashBoardVo>();
			List<DashBoardVo> dashBoardVoCategoryAll=new ArrayList<DashBoardVo>();
			List<DashBoardVo> dashBoardVoCategory=new ArrayList<DashBoardVo>();
			List<DashBoardVo> dashBoardVoCapability=new ArrayList<DashBoardVo>();
			List<DashBoardVo> dashBoardVoPracticeArea=new ArrayList<DashBoardVo>();
			List<ProjectMst> orgProjects=new ArrayList<ProjectMst>();
			List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
			List<PracticeMst> allPractices=new ArrayList<PracticeMst>();
			
				UserMst userMst=new UserMst();
				userMst=userMstService.getByName(accessObject.getTenantId(), accessObject.getLoginId());
				TenantMst tenantMst=new TenantMst();
				tenantMst=tenantMstService.getTenant(userMst.getTenantId());
				String selectedtenantId=userMst.getTenantId();
				Integer practiceLevel=tenantMst.getPracticeLevel();
				
				
				String[] userProjects=userMst.getProject().split(",");
				orgProjects=projectMstService.getAllProject(selectedtenantId);
			
			for(String projectCode:userProjects)
			{
			ProjectMst project=new ProjectMst();
			project=projectMstService.getProjectByProjectCode(selectedtenantId, projectCode);
			
			List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea=new ArrayList<RelationMappingCapabilityPracticeArea>();
			relationMappingCapabilityPracticeArea=accessObject.getRelationMappingCapabilityPracticeArea();
			
			List<RelationMappingCategoryCapability> relationMappingCategoryCapability=new ArrayList<RelationMappingCategoryCapability>();
			relationMappingCategoryCapability=accessObject.getRelationMappingCategoryCapability();
			
			List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
			relationMappingPracticeAreaPractice=accessObject.getRelationMappingPracticeAreaPractice();
			
			Integer totalPracticeComplianceScore=0;
			Integer totalComplianceScore=0;
			Integer  totalPracticeScore=0;
			
			if(null!=project)
			{
				allPractices=practiceMstService.getPracticeByTenantProject(project.getTenantId(), 
						project.getProjectCode(), accessObject,accessObject.getCmmiVersion());
				for(PracticeMst practice:allPractices)
				{
					
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
					dashBoardVo.setProjectName(project.getProjectName());
					dashBoardVo.setCategoryCode(practice.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
					dashBoardVo.setPracticeCode(practice.getPracticeCode());
					dashBoardVo.setProjectCode(practice.getProject());
					dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoAll.add(dashBoardVo);
					dashBoardVoAll1.add(dashBoardVo);
					dashBoardVoAll2.add(dashBoardVo);
					dashBoardVoAll3.add(dashBoardVo);
					totalPracticeComplianceScore=totalPracticeComplianceScore+((int)Math.round(practice.getPracticeCompliance()));
					totalPracticeScore=totalPracticeScore+100;
					
				}
				totalComplianceScore=Math.round(((totalPracticeComplianceScore*100)/totalPracticeScore));
				project.setComplianceScore(totalComplianceScore);
				allProjects.add(project);
			}
			}

			for(PracticeMst practice:allPractices)
			{
				String project=practice.getProject();
				String practiceArea=practice.getPracticeAreaCode();
				Boolean recordFound=Boolean.FALSE;
				if(dashBoardVoPracticeArea.size()>0)
				{
					for(DashBoardVo dbvo:dashBoardVoPracticeArea)
					{
						if(dbvo.getProjectCode().equalsIgnoreCase(project))
						{
							
							if(dbvo.getPracticeAreaCode().equalsIgnoreCase(practiceArea))
							{
								dbvo.setComplianceScore(dbvo.getComplianceScore()+practice.getPracticeCompliance());
								dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
								recordFound=Boolean.TRUE;
								break;
							}
							
						}
						
					}
					if(!recordFound)
					{
						DashBoardVo dashBoardVo=new DashBoardVo();
						dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
						dashBoardVo.setProjectName(practice.getProject());
						dashBoardVo.setCategoryCode(practice.getCategoryCode());
						dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
						dashBoardVo.setPracticeCode(practice.getPracticeCode());
						dashBoardVo.setProjectCode(practice.getProject());
						dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
						dashBoardVo.setTotalComplianceScore(100.00);
						
						dashBoardVoPracticeArea.add(dashBoardVo);		
					}
				}
				else
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
					dashBoardVo.setProjectName(practice.getProject());
					dashBoardVo.setCategoryCode(practice.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
					dashBoardVo.setPracticeCode(practice.getPracticeCode());
					dashBoardVo.setProjectCode(practice.getProject());
					dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
					dashBoardVo.setTotalComplianceScore(100.00);
					
					dashBoardVoPracticeArea.add(dashBoardVo);
					
				}
			}

			
			
			for(PracticeMst practice:allPractices)
			{
				String project=practice.getProject();
				String category=practice.getCategoryCode();
				Boolean recordFound=Boolean.FALSE;
				if(dashBoardVoCategory.size()>0)
				{
					for(DashBoardVo dbvo:dashBoardVoCategory)
					{
						if(dbvo.getProjectCode().equalsIgnoreCase(project))
						{
							
							if(dbvo.getCategoryCode().equalsIgnoreCase(category))
							{
								dbvo.setComplianceScore(dbvo.getComplianceScore()+practice.getPracticeCompliance());
								dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
								recordFound=Boolean.TRUE;
							}
							
						}
						
					}
					if(!recordFound)
					{
						DashBoardVo dashBoardVo=new DashBoardVo();
						dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
						dashBoardVo.setProjectName(practice.getProject());
						dashBoardVo.setCategoryCode(practice.getCategoryCode());
						dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
						dashBoardVo.setPracticeCode(practice.getPracticeCode());
						dashBoardVo.setProjectCode(practice.getProject());
						dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
						dashBoardVo.setTotalComplianceScore(100.00);
						dashBoardVoCategory.add(dashBoardVo);		
					}
				}
				else
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
					dashBoardVo.setProjectName(practice.getProject());
					dashBoardVo.setCategoryCode(practice.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
					dashBoardVo.setPracticeCode(practice.getPracticeCode());
					dashBoardVo.setProjectCode(practice.getProject());
					dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoCategory.add(dashBoardVo);
				}
			}
			
			for(PracticeMst practice:allPractices)
			{
				String project=practice.getProject();
				String capability=practice.getCapabilityCode();
				Boolean recordFound=Boolean.FALSE;
				if(dashBoardVoCapability.size()>0)
				{
					for(DashBoardVo dbvo:dashBoardVoCapability)
					{
						if(dbvo.getProjectCode().equalsIgnoreCase(project))
						{
							
							if(dbvo.getCapabilityCode().equalsIgnoreCase(capability))
							{
								dbvo.setComplianceScore(dbvo.getComplianceScore()+practice.getPracticeCompliance());
								dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
								recordFound=Boolean.TRUE;
							}
							
						}
						
					}
					if(!recordFound)
					{
						DashBoardVo dashBoardVo=new DashBoardVo();
						dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
						dashBoardVo.setProjectName(practice.getProject());
						dashBoardVo.setCategoryCode(practice.getCategoryCode());
						dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
						dashBoardVo.setPracticeCode(practice.getPracticeCode());
						dashBoardVo.setProjectCode(practice.getProject());
						dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
						dashBoardVo.setTotalComplianceScore(100.00);
						dashBoardVoCapability.add(dashBoardVo);		
					}
				}
				else
				{
					DashBoardVo dashBoardVo=new DashBoardVo();
					dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
					dashBoardVo.setProjectName(practice.getProject());
					dashBoardVo.setCategoryCode(practice.getCategoryCode());
					dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
					dashBoardVo.setPracticeCode(practice.getPracticeCode());
					dashBoardVo.setProjectCode(practice.getProject());
					dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
					dashBoardVo.setTotalComplianceScore(100.00);
					dashBoardVoCapability.add(dashBoardVo);
				}
			}
			
			
			
			
			
			for(DashBoardVo dbc:dashBoardVoCategory)
			{
				if(dbc.getComplianceScore()>0)
				{
					Double totalComplianceScore=(double) Math.round(((double) dbc.getComplianceScore()*100)/dbc.getTotalComplianceScore());
					//Double totalComplianceScore=(double) Math.round((( dbc.getComplianceScore()*100)/dbc.getTotalComplianceScore())*100)/100;
					dbc.setComplianceScore(totalComplianceScore);
				}
				
			}
			
			for(DashBoardVo dbcap:dashBoardVoCapability)
			{
				if(dbcap.getComplianceScore()>0)
				{
					Double totalComplianceScore=(double) Math.round(((double) dbcap.getComplianceScore()*100)/dbcap.getTotalComplianceScore());
//					Double totalComplianceScore=(double) Math.round((( dbcap.getComplianceScore()*100)/dbcap.getTotalComplianceScore())*100)/100;
					dbcap.setComplianceScore(totalComplianceScore);
				}
				
			}
			
			for(DashBoardVo dPA:dashBoardVoPracticeArea)
			{
				if(dPA.getComplianceScore()>0)
				{
					Double totalComplianceScore=(double) Math.round(((double) dPA.getComplianceScore()*100)/dPA.getTotalComplianceScore());
//					Double totalComplianceScore=(double) Math.round((( dPA.getComplianceScore()*100)/dPA.getTotalComplianceScore())*100)/100;
					dPA.setComplianceScore(totalComplianceScore);
				}
				
			}
			request.setAttribute("dashBoardVoCategory",dashBoardVoCategory);
			request.setAttribute("dashBoardVoCapability",dashBoardVoCapability);
			request.setAttribute("dashBoardVoPracticeArea",dashBoardVoPracticeArea);
			request.setAttribute("dashBoardVoAll",dashBoardVoAll);
			request.setAttribute("allProjects",allProjects);
			request.setAttribute("selectedtenantId",selectedtenantId);
			request.setAttribute("msg", msg);
			return "dashboardhome";
			
				}
		else if(accessObject.getUserType().equalsIgnoreCase("USER")||accessObject.getUserType().equalsIgnoreCase("ADMIN")||accessObject.getUserType().equalsIgnoreCase("MANAGER"))
		{
				List<DashBoardVo> dashBoardVoCategoryAll=new ArrayList<DashBoardVo>();
				List<DashBoardVo> dashBoardVoCategory=new ArrayList<DashBoardVo>();
				List<DashBoardVo> dashBoardVoCapability=new ArrayList<DashBoardVo>();
				List<DashBoardVo> dashBoardVoPracticeArea=new ArrayList<DashBoardVo>();
				List<ProjectMst> orgProjects=new ArrayList<ProjectMst>();
				List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
				List<PracticeMst> allPractices=new ArrayList<PracticeMst>();

					UserMst userMst=new UserMst();
					userMst=userMstService.getByName(accessObject.getTenantId(), accessObject.getLoginId());
					TenantMst tenantMst=new TenantMst();
					tenantMst=tenantMstService.getTenant(userMst.getTenantId());
					String selectedtenantId=userMst.getTenantId();
					Integer practiceLevel=tenantMst.getPracticeLevel();
					String[] userProjects=userMst.getProject().split(",");	
					
					
					orgProjects=projectMstService.getAllProject(selectedtenantId);
					if(!accessObject.getUserType().equalsIgnoreCase("USER"))
					{
						userProjects=new String[orgProjects.size()];
						for(int i=0;i<orgProjects.size();i++)
						{
							userProjects[i]=orgProjects.get(i).getProjectCode();
						}
					}
//				allPractices=practiceMstService.getAll(selectedtenantId);
				for(String projectCode:userProjects)
				{
				ProjectMst project=new ProjectMst();
				project=projectMstService.getProjectByProjectCode(selectedtenantId, projectCode);
				allPractices=practiceMstService.getPracticeByTenantProject(project.getTenantId(), 
						project.getProjectCode(), accessObject,accessObject.getCmmiVersion());
				List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea=new ArrayList<RelationMappingCapabilityPracticeArea>();
				relationMappingCapabilityPracticeArea=accessObject.getRelationMappingCapabilityPracticeArea();
				
				List<RelationMappingCategoryCapability> relationMappingCategoryCapability=new ArrayList<RelationMappingCategoryCapability>();
				relationMappingCategoryCapability=accessObject.getRelationMappingCategoryCapability();
				
				List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
				relationMappingPracticeAreaPractice=accessObject.getRelationMappingPracticeAreaPractice();
				
				Integer totalPracticeComplianceScore=0;
				Integer totalComplianceScore=0;
				Integer  totalPracticeScore=0;
				
				if(null!=project)
				{
					for(PracticeMst practice:allPractices)
					{
//						if(!practice.getIsSaved())
//						{
//							continue;
//						}
						 
						
						
						if(practice.getProject().equalsIgnoreCase(project.getProjectCode()))
						{
							DashBoardVo dashBoardVo=new DashBoardVo();
							dashBoardVo.setCapabilityCode(practice.getCapabilityCode());
							dashBoardVo.setProjectName(project.getProjectName());
							dashBoardVo.setPracticeCode(practice.getPracticeCode());
							dashBoardVo.setCategoryCode(practice.getCategoryCode());
							dashBoardVo.setPracticeAreaCode(practice.getPracticeAreaCode());
							dashBoardVo.setProjectCode(practice.getProject());
							dashBoardVo.setComplianceScore((double) Math.round(practice.getPracticeCompliance()));
							dashBoardVoCategoryAll.add(dashBoardVo);
							totalPracticeComplianceScore=totalPracticeComplianceScore+((int)Math.round(practice.getPracticeCompliance()));
							totalPracticeScore=totalPracticeScore+100;
							
						}
					}
					totalComplianceScore=Math.round(((totalPracticeComplianceScore*100)/totalPracticeScore));
					project.setComplianceScore(totalComplianceScore);
					allProjects.add(project);
				}
				}
				
				for(DashBoardVo db:dashBoardVoCategoryAll)
				{
					String project=db.getProjectCode();
					String category=db.getCategoryCode();
					Boolean recordFound=Boolean.FALSE;
					if(dashBoardVoCategory.size()>0)
					{
						for(DashBoardVo dbvo:dashBoardVoCategory)
						{
							if(dbvo.getProjectCode().equalsIgnoreCase(db.getProjectCode()))
							{
								
								if(dbvo.getCategoryCode().equalsIgnoreCase(db.getCategoryCode()))
								{
									dbvo.setComplianceScore(dbvo.getComplianceScore()+db.getComplianceScore());
									dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
									recordFound=Boolean.TRUE;
								}
								
							}
							
						}
						if(!recordFound)
						{
							DashBoardVo dashBoardVo=new DashBoardVo();
							dashBoardVo.setCapabilityCode(db.getCapabilityCode());
							dashBoardVo.setProjectName(db.getProjectName());
							dashBoardVo.setPracticeCode(db.getPracticeCode());
							dashBoardVo.setCategoryCode(db.getCategoryCode());
							dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
							dashBoardVo.setProjectCode(db.getProjectCode());
							dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
							dashBoardVo.setTotalComplianceScore(100.00);
							dashBoardVoCategory.add(dashBoardVo);		
						}
					}
					else
					{
						DashBoardVo dashBoardVo=new DashBoardVo();
						dashBoardVo.setCapabilityCode(db.getCapabilityCode());
						dashBoardVo.setProjectName(db.getProjectName());
						dashBoardVo.setPracticeCode(db.getPracticeCode());
						dashBoardVo.setCategoryCode(db.getCategoryCode());
						dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
						dashBoardVo.setProjectCode(db.getProjectCode());
						dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
						dashBoardVo.setTotalComplianceScore(100.00);
						dashBoardVoCategory.add(dashBoardVo);
					}
				}
				
				for(DashBoardVo db:dashBoardVoCategoryAll)
				{
					String project=db.getProjectCode();
					String capability=db.getCapabilityCode();
					Boolean recordFound=Boolean.FALSE;
					if(dashBoardVoCapability.size()>0)
					{
						for(DashBoardVo dbvo:dashBoardVoCapability)
						{
							if(dbvo.getProjectCode().equalsIgnoreCase(db.getProjectCode()))
							{
								
								if(dbvo.getCapabilityCode().equalsIgnoreCase(db.getCapabilityCode()))
								{
									dbvo.setComplianceScore(dbvo.getComplianceScore()+db.getComplianceScore());
									dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
									recordFound=Boolean.TRUE;
								}
								
							}
							
						}
						if(!recordFound)
						{
							DashBoardVo dashBoardVo=new DashBoardVo();
							dashBoardVo.setCapabilityCode(db.getCapabilityCode());
							dashBoardVo.setProjectName(db.getProjectName());
							dashBoardVo.setPracticeCode(db.getPracticeCode());
							dashBoardVo.setCategoryCode(db.getCategoryCode());
							dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
							dashBoardVo.setProjectCode(db.getProjectCode());
							dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
							dashBoardVo.setTotalComplianceScore(100.00);
							dashBoardVoCapability.add(dashBoardVo);		
						}
					}
					else
					{
						DashBoardVo dashBoardVo=new DashBoardVo();
						dashBoardVo.setCapabilityCode(db.getCapabilityCode());
						dashBoardVo.setProjectName(db.getProjectName());
						dashBoardVo.setPracticeCode(db.getPracticeCode());
						dashBoardVo.setCategoryCode(db.getCategoryCode());
						dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
						dashBoardVo.setProjectCode(db.getProjectCode());
						dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
						dashBoardVo.setTotalComplianceScore(100.00);
						dashBoardVoCapability.add(dashBoardVo);
					}
				}
				
				for(DashBoardVo db:dashBoardVoCategoryAll)
				{
					String project=db.getProjectCode();
					String practiceArea=db.getPracticeAreaCode();
					Boolean recordFound=Boolean.FALSE;
					if(dashBoardVoPracticeArea.size()>0)
					{
						for(DashBoardVo dbvo:dashBoardVoPracticeArea)
						{
							if(dbvo.getProjectCode().equalsIgnoreCase(db.getProjectCode()))
							{
								
								if(dbvo.getPracticeAreaCode().equalsIgnoreCase(db.getPracticeAreaCode()))
								{
									dbvo.setComplianceScore(dbvo.getComplianceScore()+db.getComplianceScore());
									dbvo.setTotalComplianceScore(dbvo.getTotalComplianceScore()+100);
									recordFound=Boolean.TRUE;
								}
								
							}
							
						}
						if(!recordFound)
						{
							DashBoardVo dashBoardVo=new DashBoardVo();
							dashBoardVo.setCapabilityCode(db.getCapabilityCode());
							dashBoardVo.setProjectName(db.getProjectName());
							dashBoardVo.setPracticeCode(db.getPracticeCode());
							dashBoardVo.setCategoryCode(db.getCategoryCode());
							dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
							dashBoardVo.setProjectCode(db.getProjectCode());
							dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
							dashBoardVo.setTotalComplianceScore(100.00);
							dashBoardVoPracticeArea.add(dashBoardVo);		
						}
					}
					else
					{
						DashBoardVo dashBoardVo=new DashBoardVo();
						dashBoardVo.setCapabilityCode(db.getCapabilityCode());
						dashBoardVo.setProjectName(db.getProjectName());
						dashBoardVo.setPracticeCode(db.getPracticeCode());
						dashBoardVo.setCategoryCode(db.getCategoryCode());
						dashBoardVo.setPracticeAreaCode(db.getPracticeAreaCode());
						dashBoardVo.setProjectCode(db.getProjectCode());
						dashBoardVo.setComplianceScore((double) Math.round(db.getComplianceScore()));
						dashBoardVo.setTotalComplianceScore(100.00);
						dashBoardVoPracticeArea.add(dashBoardVo);
					}
				}
				
				
				
				for(DashBoardVo dbc:dashBoardVoCategory)
				{
					if(dbc.getComplianceScore()>0)
					{
						//Double totalComplianceScore=(double) Math.round(( dbc.getComplianceScore()*100.00)/dbc.getTotalComplianceScore());
						Double totalComplianceScore=(double) Math.round(((double) dbc.getComplianceScore()*100)/dbc.getTotalComplianceScore());
						dbc.setComplianceScore(totalComplianceScore);
					}
					
				}
				
				for(DashBoardVo dbcap:dashBoardVoCapability)
				{
					if(dbcap.getComplianceScore()>0)
					{
//						Double totalComplianceScore=(double) Math.round((dbcap.getComplianceScore()*100.00)/dbcap.getTotalComplianceScore());
						Double totalComplianceScore=(double) Math.round(((double) dbcap.getComplianceScore()*100)/dbcap.getTotalComplianceScore());
						dbcap.setComplianceScore(totalComplianceScore);
					}
					
				}
				
				for(DashBoardVo dPA:dashBoardVoPracticeArea)
				{
					if(dPA.getComplianceScore()>0)
					{
//						Double totalComplianceScore=Math.round( dPA.getComplianceScore()*100.00)/dPA.getTotalComplianceScore();
						Double totalComplianceScore=(double) Math.round(((double) dPA.getComplianceScore()*100)/dPA.getTotalComplianceScore());
						dPA.setComplianceScore(totalComplianceScore);
					}
					
				}
				request.setAttribute("dashBoardVoCategory",dashBoardVoCategory);
				request.setAttribute("dashBoardVoCapability",dashBoardVoCapability);
				request.setAttribute("dashBoardVoPracticeArea",dashBoardVoPracticeArea);
				request.setAttribute("dashBoardVoAll",dashBoardVoCategoryAll);
				request.setAttribute("allProjects",allProjects);
				request.setAttribute("selectedtenantId",selectedtenantId);
				request.setAttribute("msg", msg);
				return "dashboardhome";
		
			}		
		request.setAttribute("msg", msg);
		return "dashboardhome";
	}
	
	
	
	@RequestMapping("/categoryMstCrud")
	public String categoryMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Master");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<CategoryMst> categoryList=new ArrayList<CategoryMst>();
		categoryList=categoryMstService.getAllCategory(accessObject.getTenantId(),accessObject.getCmmiVersion());
		//categoryList=accessObject.getCategoryMst();
		request.setAttribute("categoryList",categoryList);
		request.setAttribute("msg", msg);
		return "CategoryMstCrud";
	}
	
	@RequestMapping("/lookupMstCrud")
	public String lookupMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Configure");
		String msg="";
		msg=accessObject.getMsg();
		List<LookupMst> lookups=new ArrayList<LookupMst>();
		lookups=lookupMstService.getAllByTenant("0");
		accessObject.setMsg("");
		request.setAttribute("lookups",lookups);
		request.setAttribute("msg", msg);
		return "LookupMstCrud";
	}
	
	
	@RequestMapping("/sequenceMstCrud")
	public String sequenceMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Configure");
		String msg="";
		msg=accessObject.getMsg();
		List<SequenceMst> objectList=new ArrayList<SequenceMst>();
		objectList=sequenceMstService.getAll("0");
		accessObject.setMsg("");
		request.setAttribute("objectList",objectList);
		request.setAttribute("msg", msg);
		return "SequenceMstCrud";
	}
	
	
	
	
	@RequestMapping("/businessUnitMstCrud")
	public String businessUnitMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Setup");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<BusinessUnitMst> objectList=new ArrayList<BusinessUnitMst>();
		List<OrganizationUnitMst> allOrganizationUnit=new ArrayList<OrganizationUnitMst>();
		allOrganizationUnit=organizationUnitMstService.getAllOrganizationUnit(accessObject.getTenantId());
		objectList=businessUnitMstService.getAllBusinessUnit(accessObject.getTenantId());
		TenantMst tenantDetails=new TenantMst();
		tenantDetails=tenantMstService.getTenant(accessObject.getTenantId());
		request.setAttribute("objectList",objectList);
		request.setAttribute("allOrganizationUnit",allOrganizationUnit);
		request.setAttribute("tenantDetails",tenantDetails);
		request.setAttribute("msg", msg);
		return "BusinessUnitMstCrud";
	}
	
	@RequestMapping("/organizationUnitMstCrud")
	public String organizationUnitMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Setup");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<OrganizationUnitMst> objectList=new ArrayList<OrganizationUnitMst>();
		objectList=organizationUnitMstService.getAllOrganizationUnit(accessObject.getTenantId());
		TenantMst tenantDetails=new TenantMst();
		tenantDetails=tenantMstService.getTenant(accessObject.getTenantId());
		request.setAttribute("objectList",objectList);
		request.setAttribute("tenantDetails",tenantDetails);
		request.setAttribute("msg", msg);
		return "OrganizationUnitMstCrud";
	}
	
	@RequestMapping("/projectMstCrud")
	public String projectMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Setup");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<ProjectMst> objectList=new ArrayList<ProjectMst>();
		objectList=projectMstService.getAllProject(accessObject.getTenantId());
		List<OrganizationUnitMst> organizationUnitList=new ArrayList<OrganizationUnitMst>();
		organizationUnitList=organizationUnitMstService.getAllOrganizationUnit(accessObject.getTenantId());
		List<BusinessUnitMst> businessUnitList=new ArrayList<BusinessUnitMst>();
		businessUnitList=businessUnitMstService.getAllBusinessUnit(accessObject.getTenantId());
		TenantMst tenantDetails=new TenantMst();
		tenantDetails=tenantMstService.getTenant(accessObject.getTenantId());
		request.setAttribute("objectList",objectList);
		request.setAttribute("businessUnitList",businessUnitList);
		request.setAttribute("organizationUnitList",organizationUnitList);
		request.setAttribute("tenantDetails",tenantDetails);
		request.setAttribute("msg", msg);
		return "ProjectMstCrud";
	}
	

	@RequestMapping("/capabilityMstCrud")
	public String capabilityMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Master");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<CapabilityMst> objectList=new ArrayList<CapabilityMst>();
		objectList=capabilityMstService.getAllCapability("0",accessObject.getCmmiVersion());
//		objectList=accessObject.getCapabilityMst();
		request.setAttribute("objectList",objectList);
		request.setAttribute("msg", msg);
		return "CapabilityMstCrud";
	}
	
	
	
@RequestMapping("/practiceAreaMstCrud")
	public String practiceAreaMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Master");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<PracticeAreaMst> objectList=new ArrayList<PracticeAreaMst>();
		List<LookupMst> businessModelLookup=new ArrayList<LookupMst>();
		businessModelLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_BUSINESSMODEL);
		//objectList=accessObject.getPracticeAreaMst();
		objectList=practiceAreaMstService.getAll("0",accessObject.getCmmiVersion());
		request.setAttribute("objectList",objectList);
		request.setAttribute("businessModelLookup",businessModelLookup);
		request.setAttribute("msg", msg);
		return "PracticeAreaMstCrud";
	}
	
	@RequestMapping("/practiceMstCrud")
	public String practiceMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Master");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<PracticeMst> objectList=new ArrayList<PracticeMst>();
		List<LookupMst> practiceLevelLookup=new ArrayList<LookupMst>();
		practiceLevelLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_PRACTICELEVEL);
		objectList=practiceMstService.getAll(accessObject.getTenantId(),accessObject.getCmmiVersion());
		request.setAttribute("objectList",objectList);
		request.setAttribute("practiceLevelLookup",practiceLevelLookup);
		request.setAttribute("msg", msg);
		return "PracticeMstCrud";
	}
	
	@RequestMapping("/workProductMstCrud")
	public String workProductMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Master");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<WorkProductMst> objectList=new ArrayList<WorkProductMst>();
		objectList=workProductMstService.getAll(accessObject.getTenantId(),accessObject.getCmmiVersion());
		request.setAttribute("objectList",objectList);
		request.setAttribute("msg", msg);
		return "WorkProductMstCrud";
	}
	
	@RequestMapping("/changePassword")
	public String changePassword(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Password Management");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		request.setAttribute("msg", msg);
		return "ChangePassword";
	}
	
	
	@RequestMapping("/relationMappingCategoryCapabilityCrud")
	public String relationMappingCategoryCapabilityCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Relationship Mapping");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<RelationMappingCategoryCapability> objectList=new ArrayList<RelationMappingCategoryCapability>();
		List<CategoryMst>categoryList=new ArrayList<CategoryMst>();
		List<CapabilityMst>capabilityList=new ArrayList<CapabilityMst>();
		//categoryList=categoryMstService.getAllCategory(accessObject.getTenantId());
		categoryList=categoryMstService.getAllCategory("0",accessObject.getCmmiVersion());
		capabilityList=capabilityMstService.getAllCapability("0",accessObject.getCmmiVersion());
		//objectList=accessObject.getRelationMappingCategoryCapability();
		objectList=relationMappingCategoryCapabilityService.getAll("0",accessObject.getCmmiVersion());
		request.setAttribute("objectList",objectList);
		request.setAttribute("categoryList",categoryList);
		request.setAttribute("capabilityList",capabilityList);
		request.setAttribute("msg", msg);
		return "RelationMappingCategoryCapabilityCrud";
	}
	
	@RequestMapping("/relationMappingCapabilityPracticeAreaCrud")
	public String relationMappingCapabilityPracticeAreaCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Relationship Mapping");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<RelationMappingCapabilityPracticeArea> objectList=new ArrayList<RelationMappingCapabilityPracticeArea>();
		List<PracticeAreaMst>practiceList=new ArrayList<PracticeAreaMst>();
		List<CapabilityMst>capabilityList=new ArrayList<CapabilityMst>();
		practiceList=practiceAreaMstService.getAll("0",accessObject.getCmmiVersion());
		//capabilityList=capabilityMstService.getAllCapability(accessObject.getTenantId());
		capabilityList=capabilityMstService.getAllCapability("0",accessObject.getCmmiVersion());
		objectList=relationMappingCapabilityPracticeAreaService.getAll("0",accessObject.getCmmiVersion());
		request.setAttribute("objectList",objectList);
		request.setAttribute("practiceList",practiceList);
		request.setAttribute("capabilityList",capabilityList);
		request.setAttribute("msg", msg);
		return "RelationMappingCapabilityPracticeAreaCrud";
	}
	
	@RequestMapping("/relationMappingPracticeAreaPracticeCrud")
	public String relationMappingPracticeAreaPracticeCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Relationship Mapping");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<RelationMappingPracticeAreaPractice> objectList=new ArrayList<RelationMappingPracticeAreaPractice>();
		List<PracticeAreaMst>practiceAreaList=new ArrayList<PracticeAreaMst>();
		List<PracticeMst>practiceList=new ArrayList<PracticeMst>();
		practiceList=practiceMstService.getAll(accessObject.getTenantId(),accessObject.getCmmiVersion());
		practiceAreaList=practiceAreaMstService.getAll("0",accessObject.getCmmiVersion());
		objectList=relationMappingPracticeAreaPracticeService.getAll("0",accessObject.getCmmiVersion());
		request.setAttribute("objectList",objectList);
		request.setAttribute("practiceList",practiceList);
		request.setAttribute("practiceAreaList",practiceAreaList);
		request.setAttribute("msg", msg);
		return "RelationMappingPracticeAreaPracticeCrud";
	}
	

	
	@RequestMapping("/tenantMstCrud")
	public String tenantMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Setup");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<TenantMst> tenantList=new ArrayList<TenantMst>();
		List<LookupMst> processLookup=new ArrayList<LookupMst>();
		List<LookupMst> practiceLevelLookup=new ArrayList<LookupMst>();
		processLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_PROCESSAREALOOKUP);
		List<LookupMst> businessModelLookup=new ArrayList<LookupMst>();
		businessModelLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_BUSINESSMODEL);
		practiceLevelLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_PRACTICELEVEL);
		tenantList=tenantMstService.getAll();
		request.setAttribute("processLookup", processLookup);
		request.setAttribute("businessModelLookup", businessModelLookup);
		request.setAttribute("tenantList", tenantList);
		request.setAttribute("practiceLevelLookup", practiceLevelLookup);
		request.setAttribute("msg", msg);
		return "TenantMstCrud";
	}
	
	@RequestMapping("/organizationProfile")
	public String organizationProfile(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Setup");
		String msg="";
		msg=accessObject.getMsg();
		accessObject.setMsg("");
		List<TenantMst> tenantListAll=new ArrayList<TenantMst>();
		List<TenantMst> tenantList=new ArrayList<TenantMst>();
		List<LookupMst> processLookup=new ArrayList<LookupMst>();
		processLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_PROCESSAREALOOKUP);
		tenantListAll=tenantMstService.getAll();
		for(TenantMst tenant:tenantListAll)
		{
			if(tenant.getTenantId().equalsIgnoreCase(accessObject.getTenantId()))
			{
				tenantList.add(tenant);
			}
		}
		request.setAttribute("processLookup", processLookup);
		request.setAttribute("tenantList", tenantList);
		request.setAttribute("msg", msg);
		return "organizationProfile";
	}
	

	
	@RequestMapping("/questionMstCrud")
	public String questionMstCrud(Map<String, Object> model) {
		model.put("message", "You are in new page !!");
		return "QuestionMstCrud";
	}
	
	
	@RequestMapping("/relationMappingPracticeWorkProductMstCrud")
	public String practiceWorkProductMstCrud(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Relationship Mapping");
		List<WorkProductMst> workProductList=new ArrayList<WorkProductMst>();
		List<PracticeAreaMst> practiceAreaList=new ArrayList<PracticeAreaMst>();
		List<PracticeMst> practiceList=new ArrayList<PracticeMst>();
		workProductList=workProductMstService.getAll(accessObject.getTenantId(),accessObject.getCmmiVersion());
		practiceAreaList=accessObject.getPracticeAreaMst();
		practiceList=practiceMstService.getAll("0",accessObject.getCmmiVersion());
		List<PracticeWorkProductMst> practiceWorkProductMstList=new ArrayList<PracticeWorkProductMst>();
		practiceWorkProductMstList=practiceWorkProductMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess(),accessObject.getCmmiVersion());
		request.setAttribute("practiceWorkProductMstList", practiceWorkProductMstList);
		request.setAttribute("practiceList", practiceList);
		request.setAttribute("workProductList", workProductList);
		request.setAttribute("practiceAreaList", practiceAreaList);
		return "PracticeWorkProductMstCrud";
	}

	@RequestMapping("/userScreen1")
	public String screen1(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		List<CategoryMst> categories=new ArrayList<CategoryMst>();
		List<PracticeAreaMst> practiceArea=new ArrayList<PracticeAreaMst>();
		practiceArea=accessObject.getPracticeAreaMst();
		//categories=categoryMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess());
		categories=accessObject.getCategoryMst();
		request.setAttribute("categories", categories);
		request.setAttribute("practiceArea", practiceArea);
		model.put("message", "HowToDoInJava Reader !!");
		return "screen1";
	}
	
	@RequestMapping("/categories")
	public String compliance2(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Category");
	List<CategoryMst> allCategories=new ArrayList<CategoryMst>();
	//allCategories=categoryMstService.getByTenantAndProject(accessObject.getTenantId(), accessObject.getProject());
	allCategories=accessObject.getCategoryMst();
	request.setAttribute("allCategories", allCategories);
		model.put("message", "HowToDoInJava Reader !!");
		return "compliance2";
	}
	
	@RequestMapping("/capabilities")
	public String compliance3(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Capability");
		String category=accessObject.getCategory();
		List<RelationMappingCategoryCapability> relationCatCap=new ArrayList<RelationMappingCategoryCapability>();
		for(RelationMappingCategoryCapability rel:accessObject.getRelationMappingCategoryCapability())
		{
			if(rel.getCategoryCode().equalsIgnoreCase(category))
			{
				relationCatCap.add(rel);
			}
		}
		
		
		List<CapabilityMst> allCapabilities=new ArrayList<CapabilityMst>();
		
		for(RelationMappingCategoryCapability rel:relationCatCap)
		{
			for(CapabilityMst cap:accessObject.getCapabilityMst())
			{
				if(cap.getCapabilityCode().equalsIgnoreCase(rel.getCapabilityCode()))
				{
					allCapabilities.add(cap);	
				}
			}
			
		}
		request.setAttribute("allCapabilities", allCapabilities);
		model.put("message", "HowToDoInJava Reader !!");
		return "compliance3";
	}
	
	@RequestMapping("/practiceAreas")
	public String compliance4(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Practice Area");
		String capability=accessObject.getCapability();
		List<RelationMappingCapabilityPracticeArea> relationCapPracticeArea=new ArrayList<RelationMappingCapabilityPracticeArea>();
		for(RelationMappingCapabilityPracticeArea rel:accessObject.getRelationMappingCapabilityPracticeArea())
		{
			if(rel.getCapabilityCode().equalsIgnoreCase(capability))
			{
				relationCapPracticeArea.add(rel);
			}
		}
		
		List<PracticeAreaMst> allPracticeArea=new ArrayList<PracticeAreaMst>();
		
		
		for(RelationMappingCapabilityPracticeArea rel:relationCapPracticeArea)
		{
			for(PracticeAreaMst pa:accessObject.getPracticeAreaMst())
			{
				if(pa.getPracticeAreaCode().equalsIgnoreCase(rel.getPracticeAreaCode()))
				{
					String[] paBusinessModel=pa.getBusinessModel().split(",");
					for(String domainView:paBusinessModel)
					{
						if(domainView.equalsIgnoreCase(accessObject.getBusinessModel()))
						{
							allPracticeArea.add(pa);		
						}
					}	
				}
			}
			
		}
		String msg="";
		if(allPracticeArea.isEmpty())
		{
			msg=("ERROR~Selected Capability "+capability+" does not have any practice Area mapped for "+accessObject.getBusinessModel());
		}
		request.setAttribute("allPracticeArea", allPracticeArea);
		request.setAttribute("msg", msg);
		
		model.put("message", "HowToDoInJava Reader !!");
		return "compliance4";
	}
	
	@RequestMapping("/practiceAreaDetails")
	public String practiceAreaDetails(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("PracticeArea Details");
		String practiceArea=accessObject.getPracticeArea();
		String capability=accessObject.getCapability();
		List<RelationMappingCapabilityPracticeArea> relationCapPracticeArea=new ArrayList<RelationMappingCapabilityPracticeArea>();
		for(RelationMappingCapabilityPracticeArea rel:accessObject.getRelationMappingCapabilityPracticeArea())
		{
			if(rel.getCapabilityCode().equalsIgnoreCase(capability))
			{
				relationCapPracticeArea.add(rel);
			}
		}
		
		List<PracticeAreaMst> allPracticeArea=new ArrayList<PracticeAreaMst>();
		PracticeAreaMst practiceAreas=new PracticeAreaMst();
		for(RelationMappingCapabilityPracticeArea rel:relationCapPracticeArea)
		{
			for(PracticeAreaMst pa:accessObject.getPracticeAreaMst())
			{
				if(pa.getPracticeAreaCode().equalsIgnoreCase(rel.getPracticeAreaCode()))
				{
					
							allPracticeArea.add(pa);		
					
						
				}
				if(pa.getPracticeAreaCode().equalsIgnoreCase(practiceArea))
				{
					practiceAreas=pa;
				}
			}
		}
		
		
		request.setAttribute("allPracticeArea", allPracticeArea);
		request.setAttribute("practiceAreas", practiceAreas);
		model.put("message", "HowToDoInJava Reader !!");
		return "PracticeAreaDetails";
	}
	
	@RequestMapping("/piiDDetails")
	public String piiDDetails(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("PIID");
		String msg=accessObject.getMsg();
		accessObject.setMsg("");
		String practiceArea=accessObject.getPracticeArea();
		
		List<PracticeAreaMst> allPracticeArea=new ArrayList<PracticeAreaMst>();
		List<PiidMst> allPiid=new ArrayList<PiidMst>();
		allPiid=piidMstService.getByProjectPracticeArea(accessObject.getTenantId(),
				accessObject.getProject(), practiceArea);
		
		PracticeAreaMst practiceAreas=new PracticeAreaMst();
		for(PracticeAreaMst pa:accessObject.getPracticeAreaMst())
		{
			if(pa.getPracticeAreaCode().equalsIgnoreCase(practiceArea))
			{
				practiceAreas=pa;
				allPracticeArea.add(practiceAreas);
			}
		}
	
		List<RelationMappingPracticeAreaPractice> rel=new ArrayList<RelationMappingPracticeAreaPractice>();
		for(RelationMappingPracticeAreaPractice obj:accessObject.getRelationMappingPracticeAreaPractice())
		{
			if(obj.getPracticeAreaCode().equalsIgnoreCase(practiceArea))
			{
				rel.add(obj);
			}
		}
		
		List<PracticeMst> allPractices=new ArrayList<PracticeMst>();
		for(RelationMappingPracticeAreaPractice relp:rel)
		{
			PracticeMst pr=new PracticeMst();
			pr=practiceMstService.getUnique(accessObject.getTenantId(), relp.getPracticeCode(), 
					ApplicationConstants.DEFAULT_process,
					accessObject.getOrganizationUnit(), accessObject.getBusinessUnit(), accessObject.getProject(),accessObject.getCmmiVersion());
			if(null==pr)
			{
				msg="Error~Practice Code "+relp.getPracticeCode()+" Mapped in relationShipPracticeAreaPractice but not found in practice Master for Project";
				request.setAttribute("allPractices", allPractices);
				request.setAttribute("msg", msg);
				
				model.put("message", "HowToDoInJava Reader !!");
				return "PiidDetails";
			}
			pr.setPracticeLevelCode(relp.getPracticeLevel());
			if(null!=pr)
			{
				if(relp.getPracticeLevel()>accessObject.getPracticeLevel())
				{
					
				}
				else
				{
					allPractices.add(pr);	
				}
				
			}
		}
		request.setAttribute("allPractices", allPractices);
		request.setAttribute("allPiid", allPiid);
		request.setAttribute("allPracticeArea", allPracticeArea);
		request.setAttribute("practiceAreas", practiceAreas);
		request.setAttribute("msg", msg);
		model.put("message", "HowToDoInJava Reader !!");
		return "PiidDetails";
	}
	
	


//	@GetMapping("/practiceDetailsAjax")
//	public PracticeMst PracticeMst(@PathVariable("practiceCode")String practiceCode,@PathVariable("tenantId")String tenantId,
//			@PathVariable("project")String project){
//		PracticeMst practiceMst=new PracticeMst();
//		practiceMst=practiceMstService.getByTenantProjectAndPractice(tenantId,project, practiceCode);
//		if(null!=practiceMst)
//		{
//			return practiceMst;
//		}
//		
//		return practiceMst;
//	}
	
	
	@RequestMapping(value = "/practiceDetailsAjax",produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PracticeMst getBaseBranchforLoginId(Model model) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		
		String tenantId = (String) (request.getParameter("tenantId"));
		String practiceCode = (String) request.getParameter("practiceCode");
		String project = (String) request.getParameter("project");
		PracticeMst practiceMst=new PracticeMst();
		practiceMst=practiceMstService.getByTenantProjectAndPractice(tenantId,project, practiceCode,accessObject.getCmmiVersion());
		if(null!=practiceMst)
		{
			return practiceMst;
		}
		
		return practiceMst;
	}
	
	@RequestMapping("/singlepracticeAreaDetails")
	public String singlepracticeAreaDetails(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Practice Area Details");
		String practiceArea=accessObject.getPracticeArea();
		
		List<PracticeAreaMst> allPracticeArea=new ArrayList<PracticeAreaMst>();
		
		
		PracticeAreaMst practiceAreas=new PracticeAreaMst();
		for(PracticeAreaMst pa:accessObject.getPracticeAreaMst())
		{
			if(pa.getPracticeAreaCode().equalsIgnoreCase(practiceArea))
			{
				practiceAreas=pa;
				allPracticeArea.add(practiceAreas);
			}
		}
	
		request.setAttribute("allPracticeArea", allPracticeArea);
		request.setAttribute("practiceAreas", practiceAreas);
		model.put("message", "HowToDoInJava Reader !!");
		return "PracticeAreaDetails";
	}
	
	
	
	@RequestMapping("/practices")
	public String compliance5(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Practice");
		String practiceArea=accessObject.getPracticeArea();
		String msg="";
		if(null!=accessObject.getMsg())
		{
			msg=accessObject.getMsg();
			accessObject.setMsg("");
		}
		List<RelationMappingPracticeAreaPractice> rel=new ArrayList<RelationMappingPracticeAreaPractice>();
		for(RelationMappingPracticeAreaPractice obj:accessObject.getRelationMappingPracticeAreaPractice())
		{
			if(obj.getPracticeAreaCode().equalsIgnoreCase(practiceArea))
			{
				rel.add(obj);
			}
		}
		
		List<PracticeMst> allPractices=new ArrayList<PracticeMst>();
		for(RelationMappingPracticeAreaPractice relp:rel)
		{
			PracticeMst pr=new PracticeMst();
			pr=practiceMstService.getUnique(accessObject.getTenantId(), relp.getPracticeCode(), ApplicationConstants.DEFAULT_process,
					accessObject.getOrganizationUnit(), accessObject.getBusinessUnit(), accessObject.getProject(),accessObject.getCmmiVersion());
			if(null==pr)
			{
				msg="Error~Practice Code "+relp.getPracticeCode()+" Mapped in relationShipPracticeAreaPractice but not found in practice Master for Project";
				request.setAttribute("allPractices", allPractices);
				request.setAttribute("msg", msg);
				
				model.put("message", "HowToDoInJava Reader !!");
				return "compliance5";
			}
			pr.setPracticeLevelCode(relp.getPracticeLevel());
			if(null!=pr)
			{
				if(relp.getPracticeLevel()>accessObject.getPracticeLevel())
				{
					
				}
				else
				{
					allPractices.add(pr);	
				}
				
			}
		}
		request.setAttribute("allPractices", allPractices);
		request.setAttribute("msg", msg);
		
		model.put("message", "HowToDoInJava Reader !!");
		return "compliance5";
	}
	
	
	@RequestMapping("/viewText")
	public String viewtext(HttpServletRequest request) throws FileNotFoundException, IOException
	{
		HttpSession session = request.getSession();
        ServletContext sc = session.getServletContext();
        String x = sc.getRealPath("/");

        String path=x+"reports\\testing.txt";
        System.out.println("Real Path "+x);
        System.out.println(x+"reports\\testing.txt");
		return op(path);
		
	}
	
	public static String op(String path) throws IOException,FileNotFoundException
	{
		File file=new File(path);
		String content= new String(Files.readAllBytes(file.toPath()));
		return content;
		
	}
	
	
	
	@RequestMapping("/practiceDetails")
	public String compliance6(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		accessObject.setMenu("Practice Details");
		String practice=accessObject.getPractice();
		List<PracticeWorkProductMst> allWorkProducts=new ArrayList<PracticeWorkProductMst>();
		PracticeMst practices=new PracticeMst();
//		List<LookupMst> complianceLookup=new ArrayList<LookupMst>();
		Map<String,String> complianceLookup=new HashMap<String,String>();
		Map<String,String> coverageLookup=new HashMap<String,String>();
		//List<LookupMst> coverageLookup=new ArrayList<LookupMst>();
//		complianceLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_COMPLIANCELOOKUP);
	
		complianceLookup.put("100", "Fully Meets (FM)");
		complianceLookup.put("70", "Largely Meets (LM)");
		complianceLookup.put("30", "Partially Meets (PM)");
		complianceLookup.put("0", "Does Not Meet (DM)");
		complianceLookup.put("N", "Not Yet (NY)");
	String complianceArray="100,Fully Meets (FM)~70,Largely Meets (LM)~30,Partially Meets (PM)~0,Does Not Meet (DM)~0,Not Yet (NY)";
	
		
		//coverageLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_COVERAGE);
		coverageLookup.put("5", "5");
		coverageLookup.put("15", "15");
		coverageLookup.put("25", "25");
		coverageLookup.put("50", "50");
		coverageLookup.put("75", "75");
		coverageLookup.put("100", "100");
		String coverageArray="5,5~15,15~25,25~50,50~75,75~100,100";
		
		allWorkProducts=practiceWorkProductMstService.getByPracticeCode(accessObject.getTenantId(), practice,
				accessObject.getOrganizationUnit(), accessObject.getBusinessUnit(), accessObject.getProject(),accessObject.getCmmiVersion());
		practices=practiceMstService.getUnique(accessObject.getTenantId(), practice,ApplicationConstants.DEFAULT_process,
				accessObject.getOrganizationUnit(), accessObject.getBusinessUnit(), accessObject.getProject(),accessObject.getCmmiVersion());
		
		request.setAttribute("practices", practices);
		request.setAttribute("allWorkProducts", allWorkProducts);
		request.setAttribute("complianceLookup", complianceLookup);
		request.setAttribute("coverageLookup", coverageLookup);
		request.setAttribute("complianceArray", complianceArray);
		request.setAttribute("coverageArray", coverageArray);
		
		
		
		model.put("message", "HowToDoInJava Reader !!");
		return "compliance6";
	}
	
	
	
	@RequestMapping("/db2")
	public String screendb2(Map<String, Object> model) {
		model.put("message", "HowToDoInJava Reader !!");
		return "db2";
	}
	
	@RequestMapping("/db1")
	public String screendb1(Map<String, Object> model) {
		model.put("message", "HowToDoInJava Reader !!");
		return "db1";
	}
	
	@RequestMapping("/2")
	public String screen2(Map<String, Object> model) {
		model.put("message", "HowToDoInJava Reader !!");
		return "screen2";
	}
	
	@RequestMapping("/userScreen2")
	public String screen2(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		List<CategoryMst> categories=new ArrayList<CategoryMst>();
		List<PracticeAreaMst> practiceArea=new ArrayList<PracticeAreaMst>();
		List<CapabilityMst> capabilities=new ArrayList<CapabilityMst>();
		List<CapabilityMst> capabilitiesMapped=new ArrayList<CapabilityMst>();
		List<RelationMappingCategoryCapability> relationMappingCategoryCapabilities=new ArrayList<RelationMappingCategoryCapability>();
		relationMappingCategoryCapabilities=accessObject.getRelationMappingCategoryCapability();
//		capabilities=capabilityMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess());
		practiceArea=accessObject.getPracticeAreaMst();
		//categories=categoryMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess());
		categories=accessObject.getCategoryMst();
		for(RelationMappingCategoryCapability relationMappingCategoryCapability:relationMappingCategoryCapabilities)
		{
			if(relationMappingCategoryCapability.getCategoryCode().equalsIgnoreCase(accessObject.getCategory()))
			{
			for(CapabilityMst cap:capabilities)
			{
				if(cap.getCapabilityCode().equalsIgnoreCase(relationMappingCategoryCapability.getCapabilityCode()))
				{
					capabilitiesMapped.add(cap);
				}
			}
			}
		}
		request.setAttribute("categories", categories);
		request.setAttribute("practiceArea", practiceArea);
		request.setAttribute("capabilities", capabilities);
		request.setAttribute("capabilitiesMapped", capabilitiesMapped);
		model.put("message", "HowToDoInJava Reader !!");
		return "UserScreen2";
	}
	
	@RequestMapping("/userScreen3")
	public String screen3(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		List<CategoryMst> categories=new ArrayList<CategoryMst>();
		List<PracticeAreaMst> practiceArea=new ArrayList<PracticeAreaMst>();
		List<PracticeAreaMst> practiceAreaMapped=new ArrayList<PracticeAreaMst>();
		List<CapabilityMst> capabilities=new ArrayList<CapabilityMst>();
		List<CapabilityMst> capabilitiesMapped=new ArrayList<CapabilityMst>();
		List<RelationMappingCategoryCapability> relationMappingCategoryCapabilities=new ArrayList<RelationMappingCategoryCapability>();
		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeAreas=new ArrayList<RelationMappingCapabilityPracticeArea>();
		relationMappingCategoryCapabilities=accessObject.getRelationMappingCategoryCapability();
		relationMappingCapabilityPracticeAreas=accessObject.getRelationMappingCapabilityPracticeArea();
//		capabilities=capabilityMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess());
		practiceArea=accessObject.getPracticeAreaMst();
		//categories=categoryMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess());
		categories=accessObject.getCategoryMst();
		for(RelationMappingCategoryCapability relationMappingCategoryCapability:relationMappingCategoryCapabilities)
		{
			if(relationMappingCategoryCapability.getCategoryCode().equalsIgnoreCase(accessObject.getCategory()))
			{
			for(CapabilityMst cap:capabilities)
			{
				if(cap.getCapabilityCode().equalsIgnoreCase(relationMappingCategoryCapability.getCapabilityCode()))
				{
					capabilitiesMapped.add(cap);
				}
			}
			}
		}
		
		for(RelationMappingCapabilityPracticeArea RelationMappingCapabilityPracticeArea:relationMappingCapabilityPracticeAreas)
		{
			if(RelationMappingCapabilityPracticeArea.getCapabilityCode().equalsIgnoreCase(accessObject.getCapability()))
			{
				for(PracticeAreaMst pa:practiceArea)
				{
					if(pa.getPracticeAreaCode().equalsIgnoreCase(RelationMappingCapabilityPracticeArea.getPracticeAreaCode()))
					{
						practiceAreaMapped.add(pa);
					}
				}
			}
		}
		request.setAttribute("categories", categories);
		request.setAttribute("practiceArea", practiceArea);
		request.setAttribute("capabilities", capabilities);
		request.setAttribute("capabilitiesMapped", capabilitiesMapped);
		request.setAttribute("practiceAreaMapped", practiceAreaMapped);
		model.put("message", "HowToDoInJava Reader !!");
		return "UserScreen3";
	}
	
	@RequestMapping("/userScreen4")
	public String screen4(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		List<CategoryMst> categories=new ArrayList<CategoryMst>();
		List<PracticeAreaMst> practiceArea=new ArrayList<PracticeAreaMst>();
		List<PracticeAreaMst> practiceAreaMapped=new ArrayList<PracticeAreaMst>();
		List<CapabilityMst> capabilities=new ArrayList<CapabilityMst>();
		List<CapabilityMst> capabilitiesMapped=new ArrayList<CapabilityMst>();
		List<RelationMappingCategoryCapability> relationMappingCategoryCapabilities=new ArrayList<RelationMappingCategoryCapability>();
		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeAreas=new ArrayList<RelationMappingCapabilityPracticeArea>();
		relationMappingCategoryCapabilities=accessObject.getRelationMappingCategoryCapability();
		relationMappingCapabilityPracticeAreas=accessObject.getRelationMappingCapabilityPracticeArea();
//		capabilities=capabilityMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess());
		practiceArea=accessObject.getPracticeAreaMst();
		//categories=categoryMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess());
		categories=accessObject.getCategoryMst();
		for(RelationMappingCategoryCapability relationMappingCategoryCapability:relationMappingCategoryCapabilities)
		{
			if(relationMappingCategoryCapability.getCategoryCode().equalsIgnoreCase(accessObject.getCategory()))
			{
			for(CapabilityMst cap:capabilities)
			{
				if(cap.getCapabilityCode().equalsIgnoreCase(relationMappingCategoryCapability.getCapabilityCode()))
				{
					capabilitiesMapped.add(cap);
				}
			}
			}
		}
		
		for(RelationMappingCapabilityPracticeArea RelationMappingCapabilityPracticeArea:relationMappingCapabilityPracticeAreas)
		{
			if(RelationMappingCapabilityPracticeArea.getCapabilityCode().equalsIgnoreCase(accessObject.getCapability()))
			{
				for(PracticeAreaMst pa:practiceArea)
				{
					if(pa.getPracticeAreaCode().equalsIgnoreCase(RelationMappingCapabilityPracticeArea.getPracticeAreaCode()))
					{
						practiceAreaMapped.add(pa);
					}
				}
			}
		}
		request.setAttribute("categories", categories);
		request.setAttribute("practiceArea", practiceArea);
		request.setAttribute("capabilities", capabilities);
		request.setAttribute("capabilitiesMapped", capabilitiesMapped);
		request.setAttribute("practiceAreaMapped", practiceAreaMapped);
		model.put("message", "HowToDoInJava Reader !!");
		return "UserScreen4";
	}
	
	@RequestMapping("/userScreen5")
	public String screen5(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		List<CategoryMst> categories=new ArrayList<CategoryMst>();
		List<PracticeAreaMst> practiceArea=new ArrayList<PracticeAreaMst>();
		List<PracticeMst> practices=new ArrayList<PracticeMst>();
		List<PracticeMst> practicesMapped=new ArrayList<PracticeMst>();
		List<PracticeAreaMst> practiceAreaMapped=new ArrayList<PracticeAreaMst>();
		List<CapabilityMst> capabilities=new ArrayList<CapabilityMst>();
		List<CapabilityMst> capabilitiesMapped=new ArrayList<CapabilityMst>();
		List<RelationMappingCategoryCapability> relationMappingCategoryCapabilities=new ArrayList<RelationMappingCategoryCapability>();
		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeAreas=new ArrayList<RelationMappingCapabilityPracticeArea>();
		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
		relationMappingCategoryCapabilities=accessObject.getRelationMappingCategoryCapability();
		relationMappingCapabilityPracticeAreas=accessObject.getRelationMappingCapabilityPracticeArea();
//		capabilities=capabilityMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess());
		practiceArea=accessObject.getPracticeAreaMst();
		//categories=categoryMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess());
		categories=accessObject.getCategoryMst();
		practices=practiceMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess(),accessObject.getCmmiVersion());
		relationMappingPracticeAreaPractice=accessObject.getRelationMappingPracticeAreaPractice();
		for(RelationMappingCategoryCapability relationMappingCategoryCapability:relationMappingCategoryCapabilities)
		{
			if(relationMappingCategoryCapability.getCategoryCode().equalsIgnoreCase(accessObject.getCategory()))
			{
			for(CapabilityMst cap:capabilities)
			{
				if(cap.getCapabilityCode().equalsIgnoreCase(relationMappingCategoryCapability.getCapabilityCode()))
				{
					capabilitiesMapped.add(cap);
				}
			}
			}
		}
		
		for(RelationMappingCapabilityPracticeArea RelationMappingCapabilityPracticeArea:relationMappingCapabilityPracticeAreas)
		{
			if(RelationMappingCapabilityPracticeArea.getCapabilityCode().equalsIgnoreCase(accessObject.getCapability()))
			{
				for(PracticeAreaMst pa:practiceArea)
				{
					if(pa.getPracticeAreaCode().equalsIgnoreCase(RelationMappingCapabilityPracticeArea.getPracticeAreaCode()))
					{
						practiceAreaMapped.add(pa);
					}
				}
			}
		}
		
		for(RelationMappingPracticeAreaPractice relationMappingPracticeAreaPractices:relationMappingPracticeAreaPractice)
		{
			if(relationMappingPracticeAreaPractices.getPracticeAreaCode().equalsIgnoreCase(accessObject.getPracticeArea()))
			{
				for(PracticeMst practice:practices)
				{
					if(practice.getPracticeCode().equalsIgnoreCase(relationMappingPracticeAreaPractices.getPracticeCode()))
					{
						practicesMapped.add(practice);
					}
				}
			}
		}
		request.setAttribute("categories", categories);
		request.setAttribute("practiceArea", practiceArea);
		request.setAttribute("capabilities", capabilities);
		request.setAttribute("capabilitiesMapped", capabilitiesMapped);
		request.setAttribute("practiceAreaMapped", practiceAreaMapped);
		request.setAttribute("practices", practices);
		request.setAttribute("practicesMapped", practicesMapped);
		
		model.put("message", "HowToDoInJava Reader !!");
		return "UserScreen5";
	}
	
	@RequestMapping("/userScreen6")
	public String screen6(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		List<CategoryMst> categories=new ArrayList<CategoryMst>();
		List<PracticeAreaMst> practiceArea=new ArrayList<PracticeAreaMst>();
		List<PracticeMst> practices=new ArrayList<PracticeMst>();
		List<PracticeMst> practicesMapped=new ArrayList<PracticeMst>();
		List<PracticeWorkProductMst> practiceWorkProductMst=new ArrayList<PracticeWorkProductMst>();
		List<PracticeWorkProductMst> practiceWorkProductMstMapped=new ArrayList<PracticeWorkProductMst>();
		List<WorkProductMst> workProducts=new ArrayList<WorkProductMst>();
		List<WorkProductMst> workProductsMapped=new ArrayList<WorkProductMst>();
		List<PracticeAreaMst> practiceAreaMapped=new ArrayList<PracticeAreaMst>();
		List<CapabilityMst> capabilities=new ArrayList<CapabilityMst>();
		List<CapabilityMst> capabilitiesMapped=new ArrayList<CapabilityMst>();
		
		List<RelationMappingCategoryCapability> relationMappingCategoryCapabilities=new ArrayList<RelationMappingCategoryCapability>();
		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeAreas=new ArrayList<RelationMappingCapabilityPracticeArea>();
		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
		workProducts=workProductMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess(),accessObject.getCmmiVersion());
		practiceWorkProductMst=practiceWorkProductMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess(),accessObject.getCmmiVersion());
		relationMappingCategoryCapabilities=accessObject.getRelationMappingCategoryCapability();
		relationMappingCapabilityPracticeAreas=accessObject.getRelationMappingCapabilityPracticeArea();
//		capabilities=capabilityMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess());
		practiceArea=accessObject.getPracticeAreaMst();
		//categories=categoryMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess());
		categories=accessObject.getCategoryMst();
		practices=practiceMstService.getByTenantAndProcess(accessObject.getTenantId(), accessObject.getProcess(),accessObject.getCmmiVersion());
		relationMappingPracticeAreaPractice=accessObject.getRelationMappingPracticeAreaPractice();
		for(PracticeWorkProductMst practiceWorkProducts:practiceWorkProductMst)
		{
		if(practiceWorkProducts.getPracticeCode().equalsIgnoreCase(accessObject.getPractice()))
		{
			practiceWorkProductMstMapped.add(practiceWorkProducts);
		}
		}
		for(RelationMappingCategoryCapability relationMappingCategoryCapability:relationMappingCategoryCapabilities)
		{
			if(relationMappingCategoryCapability.getCategoryCode().equalsIgnoreCase(accessObject.getCategory()))
			{
			for(CapabilityMst cap:capabilities)
			{
				if(cap.getCapabilityCode().equalsIgnoreCase(relationMappingCategoryCapability.getCapabilityCode()))
				{
					capabilitiesMapped.add(cap);
				}
			}
			}
		}
		
		for(RelationMappingCapabilityPracticeArea RelationMappingCapabilityPracticeArea:relationMappingCapabilityPracticeAreas)
		{
			if(RelationMappingCapabilityPracticeArea.getCapabilityCode().equalsIgnoreCase(accessObject.getCapability()))
			{
				for(PracticeAreaMst pa:practiceArea)
				{
					if(pa.getPracticeAreaCode().equalsIgnoreCase(RelationMappingCapabilityPracticeArea.getPracticeAreaCode()))
					{
						practiceAreaMapped.add(pa);
					}
				}
			}
		}
		
		for(RelationMappingPracticeAreaPractice relationMappingPracticeAreaPractices:relationMappingPracticeAreaPractice)
		{
			if(relationMappingPracticeAreaPractices.getPracticeAreaCode().equalsIgnoreCase(accessObject.getPracticeArea()))
			{
				for(PracticeMst practice:practices)
				{
					if(practice.getPracticeCode().equalsIgnoreCase(relationMappingPracticeAreaPractices.getPracticeCode()))
					{
						practicesMapped.add(practice);
					}
				}
			}
		}
		request.setAttribute("categories", categories);
		request.setAttribute("practiceArea", practiceArea);
		request.setAttribute("capabilities", capabilities);
		request.setAttribute("capabilitiesMapped", capabilitiesMapped);
		request.setAttribute("practiceAreaMapped", practiceAreaMapped);
		request.setAttribute("practices", practices);
		request.setAttribute("practicesMapped", practicesMapped);
		request.setAttribute("practiceWorkProductMstMapped", practiceWorkProductMstMapped);
		
		model.put("message", "HowToDoInJava Reader !!");
		return "UserScreen7";
	}
	
	@RequestMapping("/3")
	public String screen3(Map<String, Object> model) {
		model.put("message", "HowToDoInJava Reader !!");
		return "screen3";
	}
	
	@RequestMapping("/4")
	public String screen4(Map<String, Object> model) {
		model.put("message", "HowToDoInJava Reader !!");
		return "screen4";
	}
	
	@RequestMapping("/5")
	public String screen5(Map<String, Object> model) {
		model.put("message", "HowToDoInJava Reader !!");
		return "screen5";
	}
	@RequestMapping("/sampleAdmin")
	public String sampleAdmin(Map<String, Object> model) {
		model.put("message", "HowToDoInJava Reader !!");
		return "SuperAdmin_bak";
	}
	
	public Cell setCellVaue(Object obj, Cell cell) {

		if (obj instanceof String)
			cell.setCellValue((String) obj);
		else if (obj instanceof Integer)
			cell.setCellValue((Integer) obj);

		else if (obj instanceof Double)
			cell.setCellValue((Double) obj);

		return cell;
	}
	
	public String getComplianceCode(double complianceValue)
	{
		String complianceCode="NI";
		if(complianceValue==0)
		{
			return "NI";
		}
		if(complianceValue==25)
		{
			return "PI";
		}
		if(complianceValue==75)
		{
			return "LI";
		}
		if(complianceValue==100)
		{
			return "FI";
		}
		return complianceCode;
	}

	public String getCellStyle(String compliance)
	{
		if(compliance.equalsIgnoreCase("NY"))
		{
			return "cellStyle8";
		}

		if(compliance.equalsIgnoreCase("DM"))
		{
			return "cellStyleRed";
			
		}
		if(compliance.equalsIgnoreCase("PM"))
		{
			return "cellStyleAmber";
			
		}
		if(compliance.equalsIgnoreCase("LM"))
		{
			return "cellStyleYellow";
			
		}
		if(compliance.equalsIgnoreCase("FM"))
		{
			return "cellStyleGreen";
			
		}
		
		return "cellStyleRed";
	}

	@RequestMapping("/uploadFile")
	public String uploadFile(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String msg=accessObject.getMsg();
		accessObject.setMsg("");
		request.setAttribute("msg", msg);
		model.put("message", "HowToDoInJava Reader !!");
		return "UploadFileScreen";
	}

	@RequestMapping("/developer")
	public String developer(Map<String, Object> model,HttpServletRequest request) {
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String msg=accessObject.getMsg();
		accessObject.setMsg("SUCCESS~Developed By Karan S. Gupta");
		request.setAttribute("msg", msg);
		model.put("message", "HowToDoInJava Reader !!");
		return "home";
	}

	
}