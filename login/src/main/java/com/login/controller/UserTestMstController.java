package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.CategoryVO;
import com.login.common.exceptions.PracticeVO;
import com.login.domain.AnswerMst;
import com.login.domain.ApplicationConstants;
import com.login.domain.CategoryMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.TenantMst;
import com.login.domain.TestMst;
import com.login.domain.UserTestMst;
import com.login.domain.basics.AccessObject;
import com.login.services.AnswerMstService;
import com.login.services.CategoryMstService;
import com.login.services.PracticeAreaMstService;
import com.login.services.UserTestMstService;

@RequestMapping("/userTestMst")
@RestController
public class UserTestMstController {
    private static final Logger logger = LoggerFactory.getLogger(UserTestMstController.class);
	
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private UserTestMstService userTestMstService;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

	private AccessObject getAccessObjectApi() {
		AccessObject accessObject=new AccessObject();
		accessObject.setLoginId("API");
		return accessObject;
	}
	
	@RequestMapping(value = "/getAll", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<UserTestMst> getAll() {
		String tenantId = "";
		tenantId= request.getParameter("tenantId");
		
		String authStatus = "";
		authStatus = request.getParameter("authStatus");
		List<UserTestMst> userTestList=null;
		userTestList=userTestMstService.getAll(tenantId,authStatus);
		
		
		if(null!=userTestList && userTestList.size()>0)
		{
			
			return userTestList;
		}
		
		return userTestList;
	}
	
	@RequestMapping(value="/saveObject",method=RequestMethod.POST)
	public String saveObject(HttpServletRequest request)
	{
		
		String tenantId = "";
		String testName = "";
		String questionId = "";
		String answerId = "";
		String score = "";
		
		tenantId= request.getParameter("tenantId");
		testName= request.getParameter("testName");
		questionId = request.getParameter("questionId");
		Long questionIdLong = Long.parseLong(questionId);
		answerId = request.getParameter("answerId");
		Long answerIdLong = Long.parseLong(answerId);
		score= request.getParameter("score");
		Integer scoreInt=Integer.parseInt(score);	
		
		
		UserTestMst userTestMst = new UserTestMst();
		userTestMst.setTenantId(tenantId);
		userTestMst.setTestName(testName);
		userTestMst.setQuestionId(questionIdLong);
		userTestMst.setAnswereid(answerIdLong);
		userTestMst.setScore(scoreInt);
		userTestMst=userTestMstService.saveOrUpDate(userTestMst, getAccessObjectApi());
		return "Success";
	}
	
	
}
