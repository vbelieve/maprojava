package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.ApplicationConstants;
import com.login.domain.CategoryMst;
import com.login.domain.OrganizationUnitMst;
import com.login.domain.basics.AccessObject;
import com.login.services.OrganizationUnitMstService;

@RequestMapping("/organizationUnit")
@RestController
public class OrganizationUnitMstController {
	
	@Autowired
	OrganizationUnitMstService service;
	
//	@PostMapping("/save")
//	public OrganizationUnitMst save(@RequestBody OrganizationUnitMst object)
//	{
//		return service.saveOrUpDate(object);
//			
//	}

//	@RequestMapping(value="/save",method=RequestMethod.POST)
//	public ModelAndView save(OrganizationUnitMst organizationUnitMst,HttpServletRequest request)
//	{
//		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
//		ModelMap model = new ModelMap();
//		service.saveOrUpDate(organizationUnitMst,accessObject);
//		accessObject.setMsg("SUCCESS~OrganizationUnitMst Created / Updated Successfully.");
//		return new ModelAndView(
//	    	       new RedirectView("/organizationUnitMstCrud", true),
//	    	       model);
//	}

	public String validateObject(OrganizationUnitMst object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase("") || object.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Organization Unit. Organization Code is Blank";
		}
		if(null==object.getOrganizationModel() || object.getOrganizationModel().equalsIgnoreCase("") ||object.getOrganizationModel().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Organization Unit. Model  is Blank";
		}
		if(null==object.getOrganizationUnitName() ||object.getOrganizationUnitName().equalsIgnoreCase("") || object.getOrganizationUnitName().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Organization Unit. Name  is Blank";
		}
		
		return response+"~"+message;
	}
	
	

	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String tenantId=request.getParameter("tenantId");
		String organizationUnitCode=request.getParameter("organizationUnitCode");
		String organizationUnitName=request.getParameter("organizationUnitName");
		String organizationModel=request.getParameter("organizationModel");
		String mode=request.getParameter("mode");
		ModelMap model = new ModelMap();
		
		OrganizationUnitMst object=new OrganizationUnitMst();
		object.setTenantId(accessObject.getTenantId());
		object.setOrganizationUnitCode(organizationUnitCode);
		object.setOrganizationUnitName(organizationUnitName);
		object.setOrganizationModel(organizationModel);
		object.setProcess(accessObject.getProcess());
		object.setOrganizationUnit(accessObject.getOrganizationUnit());
		object.setBusinessUnit(accessObject.getBusinessUnit());
		object.setProject(accessObject.getProject());
		if(null!=mode && mode.equalsIgnoreCase("DELETE"))
		{
			String resp="";
			resp=service.delete(object, accessObject);
			if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
				accessObject.setMsg("SUCCESS~Organization Unit Deleted Successfully.");
				return new ModelAndView(
			    	       new RedirectView("/organizationUnitMstCrud", true),
			    	       model);
			}
			else
			{
				accessObject.setMsg(resp);
				return new ModelAndView(
			    	       new RedirectView("/organizationUnitMstCrud", true),
			    	       model);
			}
		}
		
		String validationResp="";
		validationResp=validateObject(object);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
		service.saveOrUpDate(object,accessObject);
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~Category Created Successfully.");	
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~Category Updated Successfully.");
		}
		
		return new ModelAndView(
	    	       new RedirectView("/organizationUnitMstCrud", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/organizationUnitMstCrud", true),
		    	       model);	
		}
	}

	
	
	
	@PostMapping("/update")
	public OrganizationUnitMst update(@RequestBody OrganizationUnitMst object,HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		return service.saveOrUpDate(object,accessObject);
	}
	
	@GetMapping("/{tenantId}")
	public List<OrganizationUnitMst> getall(@PathVariable("tenantId")String tenantId)
	{
		List<OrganizationUnitMst> organizationUnitMst=new ArrayList<OrganizationUnitMst>();
		organizationUnitMst=(List<OrganizationUnitMst>) service.getAllOrganizationUnit(tenantId);
						return organizationUnitMst;
	}

	@GetMapping("/{tenantId}/{organizationUnitCode}")
	public OrganizationUnitMst getByName(@PathVariable("tenantId")String tenantId, @PathVariable("organizationUnitCode") String organizationUnitCode,
			@PathVariable("process") String process){
		OrganizationUnitMst organizationUnitMst=new OrganizationUnitMst();
		organizationUnitMst=service.getUnique(tenantId, organizationUnitCode,process);
		return organizationUnitMst; 
	}

	
	
	public List<OrganizationUnitMst> getAllActive(){
		return null;
	}
	
	
	
	
}
