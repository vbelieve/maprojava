package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.CategoryVO;
import com.login.common.exceptions.PracticeVO;
import com.login.domain.ApplicationConstants;
import com.login.domain.CategoryMst;
import com.login.domain.LookupMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.PracticeWorkProductMst;
import com.login.domain.TenantMst;
import com.login.domain.TestMst;
import com.login.domain.basics.AccessObject;
import com.login.services.CategoryMstService;
import com.login.services.PracticeAreaMstService;
import com.login.services.TestMstService;

@RequestMapping("/testMst")
@RestController
public class TestMstController {
    private static final Logger logger = LoggerFactory.getLogger(TestMstController.class);
	
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private TestMstService testMstService;
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

	private AccessObject getAccessObjectApi() {
		AccessObject accessObject=new AccessObject();
		accessObject.setLoginId("API");
		return accessObject;
	}
	
	@RequestMapping(value = "/getAll", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<TestMst> getAll() {
		String tenantId = "";
		tenantId= request.getParameter("tenantId");
		String authStatus = "";
		authStatus = request.getParameter("authStatus");
		List<TestMst> testList=null;
		testList=testMstService.getAll(tenantId,authStatus);
		if(null!=testList && testList.size()>0)
		{
			
			return testList;
		}
		
		return testList;
	}
	
	
	@RequestMapping(value="/saveObject",method=RequestMethod.POST)
	public String saveObject(HttpServletRequest request)
	{
		
		String tenantId = "";
		String testName = "";
		String testGroup = "";
		String questionGroup = "";
		
		tenantId= request.getParameter("tenantId");
		testName= request.getParameter("testName");
		testGroup= request.getParameter("testGroup");
		questionGroup= request.getParameter("questionGroup");
		TestMst testMst = new TestMst();
		testMst.setTenantId(tenantId);
		testMst.setTestName(testName);
		testMst.setTestGroup(testGroup);
		testMst.setQuestionGroup(questionGroup);
		testMst=testMstService.saveOrUpDate(testMst, getAccessObjectApi());
		return "Success";
	}

}
