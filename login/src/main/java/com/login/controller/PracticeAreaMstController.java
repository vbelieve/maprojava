package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.CapabilityVO;
import com.login.common.exceptions.PracticeAreaVO;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.basics.AccessObject;
import com.login.services.PracticeAreaMstService;

@ResponseBody
@RequestMapping("/practiceArea")
@RestController
public class PracticeAreaMstController {
	
	@Autowired
	PracticeAreaMstService service;
	
	@Autowired
	private HttpServletRequest request;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

	
//	@PostMapping("/save")
//	public PracticeAreaMst save(@RequestBody PracticeAreaMst object)
//	{
//		return service.saveOrUpDate(object);
//			
//	}
//	
	
	public String validateCategory(PracticeAreaMst object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getPracticeAreaCode() || object.getPracticeAreaCode().equalsIgnoreCase("") || object.getPracticeAreaCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Practice Area. Practice Area Code is Blank";
		}
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase("") || object.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Practice Area. TenantId  is Blank";
		}
		if(null==object.getPracticeAreaName() || object.getPracticeAreaName().equalsIgnoreCase("") || object.getPracticeAreaName().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Practice Area. Practice Area Name is Blank";
		}
		if(null==object.getBusinessModel() || object.getBusinessModel().equalsIgnoreCase("") || object.getBusinessModel().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Practice Area. Domain View Not selected  ";
		}
		return response+"~"+message;
	}
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String tenantId=request.getParameter("tenantId");
		String practiceAreaCode=request.getParameter("practiceAreaCode");
		String practiceAreaName=request.getParameter("practiceAreaName");
		String authstatus=request.getParameter("authstatus");
		String practiceAreaAdditionalInfo=request.getParameter("practiceAreaAdditionalInfo");
		String practiceAreaIntent=request.getParameter("practiceAreaIntent");
		String practiceAreaValue=request.getParameter("practiceAreaValue");
		String capabilityCode=request.getParameter("capabilityCode");
		String categoryCode=request.getParameter("categoryCode");
		String organizationUnit=request.getParameter("organizationUnit");
		String process=request.getParameter("process");
		String project=request.getParameter("project");
		String mode=request.getParameter("mode");
		ModelMap model = new ModelMap();
		String businessModel=request.getParameter("businessModel");
		PracticeAreaMst practiceAreaMst=new PracticeAreaMst();
		practiceAreaMst.setTenantId(accessObject.getTenantId());
		practiceAreaMst.setPracticeAreaCode(practiceAreaCode);
		if(null!=authstatus)
		{
			practiceAreaMst.setAuthStatus(authstatus);
		}
		practiceAreaMst.setCategoryCode(categoryCode);
		practiceAreaMst.setCapabilityCode(capabilityCode);
		practiceAreaMst.setPracticeAreaValue(practiceAreaValue);
		practiceAreaMst.setPracticeAreaIntent(practiceAreaIntent);
		practiceAreaMst.setPracticeAreaAdditionalInfo(practiceAreaAdditionalInfo);
		practiceAreaMst.setPracticeAreaName(practiceAreaName);
		practiceAreaMst.setBusinessModel(businessModel);
		practiceAreaMst.setProcess(accessObject.getProcess());
		practiceAreaMst.setOrganizationUnit(accessObject.getOrganizationUnit());
		practiceAreaMst.setBusinessUnit(accessObject.getBusinessUnit());
		practiceAreaMst.setProject(accessObject.getProject());
		practiceAreaMst.setCmmiVersion(accessObject.getCmmiVersion());
		if(null!=mode && mode.equalsIgnoreCase("DELETE"))
		{
			String resp="";
			resp=service.delete(practiceAreaMst, accessObject,accessObject.getCmmiVersion());
			if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
				accessObject.setMsg("SUCCESS~Practice Area Deleted Successfully.");
				
				return new ModelAndView(
			    	       new RedirectView("/practiceAreaMstCrud", true),
			    	       model);
			}
			else
			{
				accessObject.setMsg(resp);
				return new ModelAndView(
			    	       new RedirectView("/practiceAreaMstCrud", true),
			    	       model);
			}
		}
		
		String validationResp="";
		validationResp=validateCategory(practiceAreaMst);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
		service.saveOrUpDate(practiceAreaMst,accessObject,accessObject.getCmmiVersion());
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~Practice Area Created Successfully.");	
			
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~Practice Area Updated Successfully");
		}
		
		return new ModelAndView(
	    	       new RedirectView("/practiceAreaMstCrud", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/practiceAreaMstCrud", true),
		    	       model);	
		}
		
	
	}
	
	
	@GetMapping("/getBycategory")
	public List<PracticeAreaMst> getByCategory(@RequestParam String tid,@RequestParam Integer catid)
	{
		AccessObject accessObject=getAccessObject();
		List<PracticeAreaMst> practiceAreaMst=new ArrayList<PracticeAreaMst>();
		practiceAreaMst=(List<PracticeAreaMst>) service.getByCategory(tid, catid,accessObject.getCmmiVersion());
						return practiceAreaMst;
	}

	@GetMapping("/getByCapability")
	public List<PracticeAreaMst> getByCapability(@RequestParam String tid,@RequestParam Integer capid)
	{
		AccessObject accessObject=getAccessObject();
		List<PracticeAreaMst> practiceAreaMst=new ArrayList<PracticeAreaMst>();
		practiceAreaMst=(List<PracticeAreaMst>) service.getByCapability(tid, capid,accessObject.getCmmiVersion());
						return practiceAreaMst;
	}
	
	@RequestMapping("/login")
	public String login()
	{
		return "test";
	}
	
	@GetMapping("/getBycategoryAndCapability")
	public List<PracticeAreaMst> getByCategoryAndCapability(@RequestParam String tid,@RequestParam Integer catid,@RequestParam Integer capid)
	{
		AccessObject accessObject=getAccessObject();
		List<PracticeAreaMst> practiceAreaMst=new ArrayList<PracticeAreaMst>();
		practiceAreaMst=(List<PracticeAreaMst>) service.getByCategoryAndCapability(tid, catid,capid,accessObject.getCmmiVersion());
						return practiceAreaMst;
	}
	
	
	@GetMapping("/{tenantId}")
	public List<PracticeAreaMst> getall(@PathVariable("tenantId")String tenantId)
	{
		AccessObject accessObject=getAccessObject();
		List<PracticeAreaMst> practiceAreaMst=new ArrayList<PracticeAreaMst>();
		practiceAreaMst=(List<PracticeAreaMst>) service.getAll(tenantId,accessObject.getCmmiVersion());
						return practiceAreaMst;
	}

	
	public List<PracticeAreaMst> getAllActive(){
		return null;
	}
	
	@RequestMapping(value = "/getAjax", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PracticeAreaVO getAjax() {
		PracticeAreaVO vo=new PracticeAreaVO();
		AccessObject accessObject=getAccessObject();
		String practiceAreaCode = "";
		practiceAreaCode= request.getParameter("practiceAreaCode");
		PracticeAreaMst practiceArea=service.getUnique(accessObject.getTenantId(),
				practiceAreaCode, accessObject.getProcess(),
				accessObject.getOrganizationUnit(),
				accessObject.getBusinessUnit(),
				accessObject.getProject(),accessObject.getCmmiVersion());
		if(null!=practiceArea)
		{
			vo.setPracticeAreaCode(practiceArea.getPracticeAreaCode());
			vo.setPracticeAreaName(practiceArea.getPracticeAreaName());
			vo.setPracticeAreaIntent(practiceArea.getPracticeAreaIntent());
			vo.setPracticeAreaAdditionalInfo(practiceArea.getPracticeAreaAdditionalInfo());
			vo.setPracticeAreaValue(practiceArea.getPracticeAreaValue());
			vo.setBusinessModel(practiceArea.getBusinessModel());
		}
		return vo;
	}
	
	
	
	
}
