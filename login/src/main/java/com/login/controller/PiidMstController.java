package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.PiidMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.basics.AccessObject;
import com.login.services.PiidMstService;
import com.login.services.PracticeAreaMstService;

@ResponseBody
@RequestMapping("/piid")
@RestController
public class PiidMstController {
	
	@Autowired
	PiidMstService service;
	
//	@PostMapping("/save")
//	public PracticeAreaMst save(@RequestBody PracticeAreaMst object)
//	{
//		return service.saveOrUpDate(object);
//			
//	}
//	
	
	public String validateCategory(PiidMst object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getPracticeAreaCode() || object.getPracticeAreaCode().equalsIgnoreCase("") || object.getPracticeAreaCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Practice Area. Practice Area Code is Blank";
		}
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase("") || object.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Practice Area. TenantId  is Blank";
		}
		if(null==object.getLevel() || object.getLevel().equalsIgnoreCase("") || object.getLevel().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update PIID. Level is Blank";
		}
		if(null==object.getPracticeAreaCode() || object.getPracticeAreaCode().equalsIgnoreCase("") || object.getPracticeAreaCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update PIID. Practice Area is Blank";
		}
		if(null==object.getPracticeCode() || object.getPracticeCode().equalsIgnoreCase("") || object.getPracticeCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update PIID. Practice Code is Blank";
		}
		if(null==object.getArtifect() || object.getArtifect().equalsIgnoreCase("") || object.getArtifect().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update PIID. Artifect is Blank";
		}
		
		return response+"~"+message;
	}
	
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String tenantId=accessObject.getTenantId();
		String level=request.getParameter("level");
		String practiceAreaCode=request.getParameter("practiceAreaCode");
		String practiceCode=request.getParameter("practiceCode");
		String artifect=request.getParameter("artifect");
		String practiceDescription=request.getParameter("practiceDescription");
		String mode=request.getParameter("mode");
		ModelMap model = new ModelMap();
		PiidMst piid=new PiidMst();
		piid.setTenantId(accessObject.getTenantId());
		piid.setLevel(level);
		piid.setPracticeAreaCode(practiceAreaCode);
		piid.setPracticeCode(practiceCode);
		piid.setPracticeDescription(practiceDescription);
		piid.setArtifect(artifect);
		piid.setProject(accessObject.getProject());
		if(null!=mode && mode.equalsIgnoreCase("DELETE"))
		{
			String resp="";
			resp=service.delete(piid, accessObject);
			if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
				accessObject.setMsg("SUCCESS~PIID Deleted Successfully.");
				
				return new ModelAndView(
			    	       new RedirectView("/piiDDetails", true),
			    	       model);
			}
			else
			{
				accessObject.setMsg(resp);
				return new ModelAndView(
			    	       new RedirectView("/piiDDetails", true),
			    	       model);
			}
		}
		
		String validationResp="";
		validationResp=validateCategory(piid);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
		service.saveOrUpDate(piid,accessObject);
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~PIID Created Successfully.");	
			
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~PIID Updated Successfully");
		}
		
		return new ModelAndView(
	    	       new RedirectView("/piiDDetails", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/piiDDetails", true),
		    	       model);	
		}
		
	
	}
	
	
	
	
	
	
}
