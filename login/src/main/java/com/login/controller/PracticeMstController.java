
package com.login.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.login.common.exceptions.PracticeAreaVO;
import com.login.common.exceptions.PracticeVO;
import com.login.domain.ApplicationConstants;
import com.login.domain.LookupMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.PracticeWorkProductMst;
import com.login.domain.ProjectMst;
import com.login.domain.TenantMst;
import com.login.domain.basics.AccessObject;
import com.login.services.LookupMstService;
import com.login.services.PracticeMstService;
import com.login.services.PracticeWorkProductMstService;
import com.login.services.ProjectMstService;
import com.login.services.TenantMstService;

@RequestMapping("/practice")
@RestController
public class PracticeMstController {
	
	@Autowired
	PracticeMstService service;
	
	@Autowired
	PracticeWorkProductMstService practiceWorkProductMstService;
	
	@Autowired
	TenantMstService tenantMstService;
	
	@Autowired
	ProjectMstService projectMstService;
	
	
	
	@Autowired
	LookupMstService lookupMstService;
	
	@Autowired
	private HttpServletRequest request;
	
	
	private AccessObject getAccessObject() {
		return (AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
	}

	
//	@PostMapping("/save")
//	public PracticeMst save(@RequestBody PracticeMst object)
//	{
//		return service.saveOrUpDate(object);
//			
//	}

	public String validateCategory(PracticeMst object)
	{
		String response="SUCCESS";
		String message="";
		if(null==object.getPracticeCode() || object.getPracticeCode().equalsIgnoreCase("") || object.getPracticeCode().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Practice. Practice Code is Blank";
		}
		if(null==object.getTenantId() || object.getTenantId().equalsIgnoreCase("") || object.getTenantId().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Practice. TenantId  is Blank";
		}
		if(null==object.getPracticeName() || object.getPracticeName().equalsIgnoreCase("") || object.getPracticeName().equalsIgnoreCase(" "))
		{
			response="ERROR";
			message="Unable to update Practice. Practice  is Blank";
		}
		
		return response+"~"+message;
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public ModelAndView save(HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		ModelMap model = new ModelMap();
		String tenantId=request.getParameter("tenantId");
		String practiceCode=request.getParameter("practiceCode");
		String practiceName=request.getParameter("practiceName");
		String practiceDescription=request.getParameter("practiceDescription");
		String practiceValue=request.getParameter("practiceValue");
		String practiceExamples=request.getParameter("practiceExamples");
		String mode=request.getParameter("mode");
		String authStatus =request.getParameter("authStatus");
		String businessUnit =request.getParameter("businessUnit");
		String organizationUnit =request.getParameter("organizationUnit");
		String process =request.getParameter("process");
		String project =request.getParameter("project");
		String capabilityCode =request.getParameter("capabilityCode");
		String categoryCode =request.getParameter("categoryCode");
		String currentGapNptes =request.getParameter("currentGapNptes");
		String currentPractice =request.getParameter("currentPractice");
		String isSaved =request.getParameter("isSaved");
		String practiceAreaCode =request.getParameter("practiceAreaCode");
		String practiceBusinessModel =request.getParameter("practiceBusinessModel");
		String practiceBusinessUnit =request.getParameter("practiceBusinessUnit");
		String practiceCompliance =request.getParameter("practiceCompliance");
		String practiceComplianceLevel =request.getParameter("practiceComplianceLevel");
		String practiceLevelCode =request.getParameter("practiceLevelCode");
Integer practiceLevel=Integer.parseInt(practiceLevelCode);		

		
		PracticeMst practiceMst=new PracticeMst();
		practiceMst.setTenantId(accessObject.getTenantId());
		practiceMst.setPracticeCode(practiceCode);
		practiceMst.setPracticeName(practiceName);
		practiceMst.setPracticeDescription(practiceDescription);
		practiceMst.setPracticeValue(practiceValue);
		practiceMst.setPracticeExamples(practiceExamples);
		practiceMst.setProcess(accessObject.getProcess());
		practiceMst.setOrganizationUnit(accessObject.getOrganizationUnit());
		practiceMst.setBusinessUnit(accessObject.getBusinessUnit());
		practiceMst.setProject(accessObject.getProject());
		practiceMst.setAuthStatus(authStatus);
		practiceMst.setBusinessUnit(businessUnit);
		practiceMst.setOrganizationUnit(organizationUnit);
		practiceMst.setProcess(process);
		practiceMst.setProject(project);
		practiceMst.setCapabilityCode(capabilityCode);
		practiceMst.setCategoryCode(categoryCode);
		practiceMst.setCurrentGapNptes(currentGapNptes);
		practiceMst.setCurrentPractice(currentPractice);
		practiceMst.setIsSaved(Boolean.parseBoolean(isSaved));
		practiceMst.setPracticeAreaCode(practiceAreaCode);
		practiceMst.setPracticeBusinessModel(practiceBusinessModel);
		practiceMst.setPracticeBusinessUnit(practiceBusinessUnit);
		if(null==practiceCompliance||practiceCompliance.equalsIgnoreCase("")||practiceCompliance.equalsIgnoreCase(" "))
		{
			practiceCompliance="0";
		}
		practiceMst.setPracticeCompliance(Double.valueOf(practiceCompliance));
		practiceMst.setPracticeComplianceLevel(practiceComplianceLevel);
		practiceMst.setPracticeLevelCode(practiceLevel);
		practiceMst.setPracticeValue(practiceValue);
		practiceMst.setCmmiVersion(accessObject.getCmmiVersion());
		
		if(null!=mode && mode.equalsIgnoreCase("DELETE"))
		{
			String resp="";
			resp=service.delete(practiceMst, accessObject,accessObject.getCmmiVersion());
			if(resp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
			{
				accessObject.setMsg("SUCCESS~Practice Deleted Successfully.");
				
				return new ModelAndView(
			    	       new RedirectView("/practiceMstCrud", true),
			    	       model);
			}
			else
			{
				accessObject.setMsg(resp);
				return new ModelAndView(
			    	       new RedirectView("/practiceMstCrud", true),
			    	       model);
			}
		}
		
		String validationResp="";
		validationResp=validateCategory(practiceMst);
		if(validationResp.equalsIgnoreCase(ApplicationConstants.RESPONSE_SUCCESS))
		{
		if(accessObject.getTenantId().equalsIgnoreCase(ApplicationConstants.DEFAULT_TENANT))
		{
			List<TenantMst> allTenants=new ArrayList<TenantMst>();
			allTenants=tenantMstService.getAll();
			for(TenantMst tenant:allTenants)
			{
				if(tenant.getTenantId().equalsIgnoreCase(ApplicationConstants.DEFAULT_TENANT))
				{
					practiceMst.setTenantId(ApplicationConstants.DEFAULT_TENANT);
					practiceMst.setProject("");
					service.saveOrUpDate(practiceMst,accessObject);
				}
				else
				{
					List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
					allProjects=projectMstService.getAllProject(tenant.getTenantId());
							{
								if(null!=allProjects && allProjects.size()>0)
								{
						for(ProjectMst projects:allProjects)
						{
							practiceMst.setTenantId(tenant.getTenantId());
							practiceMst.setProject(projects.getProjectCode());
							service.saveOrUpDate(practiceMst,accessObject);
						}
								}
							}
				}
			}
		}
		else
		{
			service.saveOrUpDate(practiceMst,accessObject);	
		}
		
		if(mode.equalsIgnoreCase("CREATE"))
		{
			accessObject.setMsg("SUCCESS~Practice Created Successfully.");	
			
		}
		if(mode.equalsIgnoreCase("EDIT"))
		{
			accessObject.setMsg("SUCCESS~Practice Updated Successfully.");
			
		}
		
		return new ModelAndView(
	    	       new RedirectView("/practiceMstCrud", true),
	    	       model);
		}
		else
		{
			
			accessObject.setMsg(validationResp);
			return new ModelAndView(
		    	       new RedirectView("/practiceMstCrud", true),
		    	       model);	
		}
		
		
	}

	@RequestMapping(value="/saveCompliance",method=RequestMethod.POST)
	public ModelAndView saveCompliance(String compliance,String currentPractice,String currentGapNptes,HttpServletRequest request)
	{
		AccessObject accessObject=(AccessObject) request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT);
		String practiceComplianceLevel="NY";
		List<LookupMst> complianceLookup=new ArrayList<LookupMst>();
		complianceLookup=lookupMstService.getLookupMst("0", ApplicationConstants.LOOKUP_COMPLIANCEREPORTLOOKUP);
		if(null!=complianceLookup)
		{
			for(LookupMst lookup:complianceLookup)
			{
				if(lookup.getLookupValue().equalsIgnoreCase(compliance))
				{
					practiceComplianceLevel=lookup.getDisplayValue();
				}
			}
		}
		ModelMap model = new ModelMap();
		PracticeMst pratice=new PracticeMst();
		pratice=service.getUnique(accessObject.getTenantId(), accessObject.getPractice(), ApplicationConstants.DEFAULT_process,
				accessObject.getOrganizationUnit(), accessObject.getBusinessUnit(), accessObject.getProject(),accessObject.getCmmiVersion());
		if(null!=pratice)
		{
			pratice.setPracticeCompliance(Double.parseDouble(compliance));
			pratice.setPracticeComplianceLevel(practiceComplianceLevel);
			pratice.setPracticeAreaCode(accessObject.getPracticeArea());
			pratice.setCategoryCode(accessObject.getCategory());
			pratice.setCapabilityCode(accessObject.getCapability());
			pratice.setCurrentGapNptes(currentGapNptes);
			pratice.setCurrentPractice(currentPractice);
			pratice.setIsSaved(Boolean.TRUE);
			
		}
		service.saveOrUpDate(pratice,accessObject);
		List<PracticeWorkProductMst> practiceWorkProductMst=new ArrayList<PracticeWorkProductMst>();
		practiceWorkProductMst=practiceWorkProductMstService.getByPracticeCode(accessObject.getTenantId(), pratice.getPracticeCode(),
				accessObject.getOrganizationUnit(), accessObject.getBusinessUnit(), accessObject.getProject(),accessObject.getCmmiVersion());
		for(PracticeWorkProductMst practiceWorkProductMstList:practiceWorkProductMst)
		{
			if(practiceWorkProductMstList.getPracticeCode().equalsIgnoreCase(accessObject.getPractice()))
			{
				
				String q1=request.getParameter("q1"+practiceWorkProductMstList.getId());
				String q2=request.getParameter("q2"+practiceWorkProductMstList.getId());
				String q3=request.getParameter("q3"+practiceWorkProductMstList.getId());
				practiceWorkProductMstList.setQ1(q1);
				practiceWorkProductMstList.setQ2(q2);
				practiceWorkProductMstList.setQ3(q3);
				practiceWorkProductMstService.saveOrUpDate(practiceWorkProductMstList,accessObject.getCmmiVersion());
			}
		}
		accessObject.setMsg("SUCCESS~Compliance Updated Successfully for practice :"+pratice.getPracticeCode()+" .");
		return new ModelAndView(
	    	       new RedirectView("/practices", true),
	    	       model);
	}
	
	
	
	@GetMapping("/getBycategory")
	public List<PracticeMst> getByCategory(@RequestParam String tid,@RequestParam Integer catid)
	{
		AccessObject accessObject=getAccessObject();
		List<PracticeMst> practiceMst=new ArrayList<PracticeMst>();
		practiceMst=(List<PracticeMst>) service.getByCategory(tid, catid,accessObject.getCmmiVersion());
						return practiceMst;
	}

	@GetMapping("/getByCapability")
	public List<PracticeMst> getByCapability(@RequestParam String tid,@RequestParam Integer capid)
	{
		AccessObject accessObject=getAccessObject();
		List<PracticeMst> practiceMst=new ArrayList<PracticeMst>();
		practiceMst=(List<PracticeMst>) service.getByCapability(tid, capid,accessObject.getCmmiVersion());
						return practiceMst;
	}
	
	@GetMapping("/getBycategoryAndCapability")
	public List<PracticeMst> getByCategoryAndCapability(@RequestParam String tid,@RequestParam Integer catid,@RequestParam Integer capid)
	{
		AccessObject accessObject=getAccessObject();
		List<PracticeMst> practiceMst=new ArrayList<PracticeMst>();
		practiceMst=(List<PracticeMst>) service.getByCategoryAndCapability(tid, catid,capid,accessObject.getCmmiVersion());
						return practiceMst;
	}
	
	
	@GetMapping("/{tenantId}")
	public List<PracticeMst> getall(@PathVariable("tenantId")String tenantId)
	{
		AccessObject accessObject=getAccessObject();
		List<PracticeMst> practiceMst=new ArrayList<PracticeMst>();
		practiceMst=(List<PracticeMst>) service.getAll(tenantId,accessObject.getCmmiVersion());
						return practiceMst;
	}


	
	
	public List<PracticeMst> getAllActive(){
		return null;
	}
	
	
	@RequestMapping(value = "/getAjax", method = { RequestMethod.POST,
			RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PracticeVO getAjax() {
		PracticeVO vo=new PracticeVO();
		AccessObject accessObject=getAccessObject();
		String practiceCode = "";
		practiceCode= request.getParameter("practiceCode");
		
		PracticeMst practiceMst=service.getUnique(accessObject.getTenantId(),
				practiceCode, accessObject.getProcess(),
				accessObject.getOrganizationUnit(),
				accessObject.getBusinessUnit(),
				accessObject.getProject(),accessObject.getCmmiVersion());
		if(null!=practiceMst)
		{
			vo.setPracticeCode(practiceMst.getPracticeCode());
			vo.setPracticeName(practiceMst.getPracticeName());
			vo.setPracticeDescription(practiceMst.getPracticeDescription());
			vo.setPracticeValue(practiceMst.getPracticeValue());
			vo.setPracticeExamples(practiceMst.getPracticeExamples());
			vo.setPracticeLevelCode(practiceMst.getPracticeLevelCode());
			


			
		}
		return vo;
	}
	
	
	
}
