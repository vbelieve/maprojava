package com.login.services;

import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.CapabilityMst;
import com.login.domain.basics.AccessObject;



public interface CapabilityMstService {
	CapabilityMst saveOrUpDate(CapabilityMst entity,AccessObject accessObject,String cmmiVersion);
	List<CapabilityMst> getAllCapability(String tenantId,String cmmiVersion);
	List<CapabilityMst> getByCategory(String tenantId,Integer categoryId,String cmmiVersion);
//	List<CapabilityMst> getByCategoryAndProject(String tenantId,String categoryCode,String organizationUnit,String businessUnit,String project );
	CapabilityMst getByName(String tenantId,String capabilityName,String cmmiVersion);
	public CapabilityMst getUnique(String tenantId,String capabilityCode,
			String process,String organizationUnit,String businessUnit,String project,String cmmiVersion);
	public List<CapabilityMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion);
	public String delete(CapabilityMst entity,AccessObject accessObject,String cmmiVersion)throws BaseException;
	public List<CapabilityMst> getBase(String cmmiVersion);
	
}
