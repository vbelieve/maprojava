package com.login.services.basic;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import com.login.common.exceptions.BaseException;

public interface CoreService <Long, E>{
	public void setEntityManagerOnDao(EntityManager entityManager)
			throws Exception;

	public E find(long id) throws BaseException;

	public List<E> findAll() throws BaseException;

	public List<E> find(int startFrom, int maxResults) throws BaseException;

	// public List<E> find(String tenantId, int startFrom, int maxResults)
	// throws BaseException;

	public List<E> findAllByTenant(String tenantId) throws BaseException;

	public List<E> findAllByTenantPg(String tenantId, Integer startFrom,
			Integer maxResults) throws BaseException;

	public List<E> findDisabled(String tenantId) throws BaseException;

	public List<E> findDisabledPg(String tenantId, Integer startFrom,
			Integer maxResults) throws BaseException;

	public void save(E entity) throws BaseException;

	public E update(E entity) throws BaseException;

	public E saveOrUpdate(E entity) throws BaseException;

	public E saveOrUpdate(String loginId, Object entity) throws BaseException;

	/**
	 * Will throw an exception if entity to be deleted is not found.
	 * 
	 * @param id
	 * @throws BaseException
	 */
	public void delete(long id) throws BaseException;

	/**
	 * Will not throw an exception if entity to be deleted is not found. Will
	 * return gracefully.
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void deleteIfExisting(long id) throws Exception;

	/**
	 * Find using a named query.
	 * 
	 * @param queryName
	 *            the name of the query
	 * @param params
	 *            the query parameters
	 * 
	 * @return the list of entities
	 */
	public List<E> findByNamedQueryAndNamedParams(final String queryName,
			final Map<String, ? extends Object> params);

	public List<E> findByNamedQueryAndNamedParams(final String queryName,
			final Map<String, ? extends Object> params, int startFrom,
			int maxResults);

	public int recordCount(final String name,
			final Map<String, ? extends Object> params);

	/**
	 * Save the list of entities in batch mode. Simultaneously, it removes the
	 * entities from the session.
	 * 
	 * @param loginId
	 *            Login id
	 * @param entities
	 *            List of entities
	 */
	void batchSaveOrUpdate(String loginId, List<Object> entities)
			throws BaseException;


}
