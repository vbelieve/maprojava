package com.login.services.basic;

import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.login.common.exceptions.BaseException;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.basics.Base;

public class CoreServiceImpl<K, E> implements CoreService {

	private CustomDao dao;

	protected Class<E> entityClass;
	
	public static final Logger logger = Logger
			.getLogger(CoreServiceImpl.class);

	@SuppressWarnings("unchecked")
	public CoreServiceImpl() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass()
				.getGenericSuperclass();
		this.entityClass = (Class<E>) genericSuperclass
				.getActualTypeArguments()[1];
	}
	

	public void setEntityManagerOnDao(EntityManager entityManager) {
		dao.setEntityManager(entityManager);

	}

	public void setDAO(CustomDao<K,E> dAO) {
		this.dao = dAO;
	}
    
	
	public E find(long id) throws BaseException {
		try {
			return (E) dao.findById(id);
		} catch (Exception e) {
			throw new BaseException(entityClass.getName() + " - not found ",
					e);
		}
	}

	public List<E> findAll() throws BaseException {
		try {
			return dao.findAll();
		} catch (Exception e) {
			throw new BaseException(entityClass.getName() + " - ", e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<E> findAllByTenant(String tenantId) throws BaseException {
		try {
			return dao.findAllByTenant(tenantId);
		} catch (Exception e) {
			throw new BaseException(entityClass.getName() + " - ", e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<E> findAllByTenantPg(String tenantId, Integer startFrom,
			Integer maxResults) throws BaseException {
		try {
			int i = 9;
			i = i + 1;
			return dao.findAllByTenantPg(tenantId, startFrom, maxResults);
		} catch (Exception e) {
			throw new BaseException(entityClass.getName() + " - ", e);
		}
	}

	public List<E> findDisabled(String tenantId) throws BaseException {
		try {
			return dao.findDisabled(tenantId);
		} catch (Exception e) {
			throw new BaseException(entityClass.getName() + " - ", e);
		}
	}

	public List<E> findDisabledPg(String tenantId, Integer startFrom,
			Integer maxResults) throws BaseException {
		try {
			return dao.findDisabledPg(tenantId, startFrom, maxResults);
		} catch (Exception e) {
			throw new BaseException(entityClass.getName() + " - ", e);
		}
	}

	public List<E> find(int startFrom, int maxResults) throws BaseException {
		try {
			return dao.find(startFrom, maxResults);
		} catch (Exception e) {
			throw new BaseException(entityClass.getName() + " - ", e);
		}
	}

	public void save(Object entity) throws BaseException {
		try {
			dao.persist(entity);
		} catch (Exception e) {
			logger.error(e.getStackTrace().toString());
			throw new BaseException(entityClass.getName() + " - ", e);
		}

	}

	public E update(Object entity) throws BaseException {
		try {
			dao.merge(entity);
			return (E) dao.flush(entity);
		} catch (Exception e) {
			logger.error(e.getStackTrace().toString());
			throw new BaseException(entityClass.getName() + " - ", e);
		}
	}

	public E saveOrUpdate(Object entity) throws BaseException {
		try {
			entity = dao.merge(entity);
			
			/*
			 * //-------------------------------------- String tenantId="";
			 * String tableName=""; Long id=0L; String etlProcessStatus="";
			 * String etlCompletedStatus="";
			 * 
			 * tenantId =
			 * entity.getClass().getMethod("getTenantId").invoke(null)
			 * .toString(); tableName = entity.getClass().getName(); id =
			 * Long.parseLong
			 * (entity.getClass().getMethod("getId").invoke(null).toString());
			 * System.out.println("tenantId >> " + tenantId);
			 * System.out.println("tableName >> " + tableName);
			 * System.out.println("id >> " + id); //-----------------------
			 */

			return (E) dao.flush(entity);
		} catch (Exception e) {
			logger.error(e.getStackTrace().toString());
			throw new BaseException(entityClass.getName() + " - ", e);
		}

	}

	public void delete(long id) throws BaseException {
		E e = null;
		try {
			e = find(id);
			if (e == null) {
				throw new BaseException(entityClass.getName() + " - ");
			}
		} catch (Exception e1) {
			throw new BaseException(entityClass.getName() + " - ", e1);
		}

		try {
			if (e != null) {
				dao.remove(e);
				dao.flush(e);
			}
		} catch (Exception e1) {
			throw new BaseException(entityClass.getName() + " - ", e1);
		}

	}

	public void deleteIfExisting(long id) throws Exception {
		try {
			E e = find(id);
			if (e != null) {
				dao.remove(e);
				dao.flush(e);
			}
		} catch (Exception e) {
			throw new BaseException(entityClass.getName() + " - ", e);
		}

	}

	@SuppressWarnings("unchecked")
	public List findByNamedQueryAndNamedParams(String queryName, Map params) {
		// TODO Auto-generated method stub
		return dao.findByNamedQueryAndNamedParams(queryName, params);
	}

	public List findByNamedQueryAndNamedParams(String queryName, Map params,
			int startFrom, int maxResults) {
		return dao.findByNamedQueryAndNamedParams(queryName, params, startFrom,
				maxResults);
	}

	public int recordCount(String name, Map params) {
		return dao.recordCount(name, params);
	}

	public E saveOrUpdate(String loginId, Object entity) throws BaseException {
		try {
			createUpdateUserDetails(loginId, entity);
			entity = dao.merge(entity);
			return (E) dao.flush(entity);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("", e);
			throw new BaseException(entityClass.getName() + " - ", e);
		}
	}

	protected void createUpdateUserDetails(String loginId, Object entity) {
		if (loginId == null || loginId.isEmpty()) {
			return;
		}

		if (!(entity instanceof Base)) {
			return;
		}

		Base base = (Base) entity;
		if (base.getId() == null || base.getId() == 0) {
			base.setCreatedBy(loginId);
			base.setCreatedDate(new Date());
			base.setLastModifiedBy(loginId);
			base.setLastModifiedDate(new Date());
			base.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
			base.setIsActive(1);
		}
		base.setCreatedBy(base.getCreatedBy());
		base.setCreatedDate(base.getCreatedDate());
		base.setLastModifiedBy(loginId);
		base.setLastModifiedDate(new Date());
		if(null==base.getAuthStatus() || base.getAuthStatus().equalsIgnoreCase(""))
		{
			base.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
			
		}
		if(null!=base.getIsActive())
		{
			
		}
		else
		{
			base.setIsActive(1);
		}
	}
	public void batchSaveOrUpdate(String loginId, List entities)
			throws BaseException {

		if (null == entities || entities.isEmpty()) {
			return;
		}
		
		try {
			for (Object entity : entities) {
				createUpdateUserDetails(loginId, entity);
				dao.merge(entity);
			}
			dao.flush();
		} catch (Exception e) {
			logger.error("", e);
			throw new BaseException(entityClass.getName() + " - ", e);
		}
		
	}

}
