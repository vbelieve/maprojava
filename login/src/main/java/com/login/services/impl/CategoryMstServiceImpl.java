package com.login.services.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.CategoryMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.CategoryMst;
import com.login.domain.LookupMst;
import com.login.domain.basics.AccessObject;
import com.login.services.CategoryMstService;
import com.login.services.SequenceMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("categoryMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class CategoryMstServiceImpl extends CoreServiceImpl<Long, CategoryMst> implements CategoryMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	CategoryMstDao categoryMstDao;

	@Autowired
	SequenceMstService sequenceMstService;

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) categoryMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		categoryMstDao.setEntityManager(entityManager);
	}


	public String delete(CategoryMst entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		CategoryMst categoryMst = getUniqueCmmiVersion(entity.getTenantId(),entity.getCategoryCode(),entity.getProcess(),
				entity.getOrganizationUnit(),
				entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (categoryMst == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = categoryMst.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	
	public CategoryMst saveOrUpDate(CategoryMst entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {
		String categorySeqNo="0";
		try {
			categorySeqNo=sequenceMstService.getNextSeqNo(ApplicationConstants.DEFAULT_TENANT, ApplicationConstants.SEQ_CATEGORYSEQ);
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		
		if(null==entity.getTenantId())
		{
			entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);	
		}
		
		String loginId=accessObject.getLoginId();
//		CategoryMst categoryMst = getUnique(entity.getTenantId(),entity.getCategoryCode(),
//				entity.getProcess(),entity.getOrganizationUnit(),
//				entity.getBusinessUnit(),entity.getProject());
		CategoryMst categoryMst = getUniqueCmmiVersion(entity.getTenantId(),entity.getCategoryCode(),
				entity.getProcess(),entity.getOrganizationUnit(),
				entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (categoryMst == null) {
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = categoryMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, categoryMst);
			} catch (Exception e) {
				
			}
			categoryMst.setId(id);
			return super.saveOrUpdate(loginId, categoryMst);
		}
		
	}

	

	
	
	public CategoryMst getUnique(String tenantId,String categoryCode,String process,String organizationUnit,String businessUnit,
			String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("categoryCode", categoryCode);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<CategoryMst> categoryMst = (List<CategoryMst>) categoryMstDao.findByNamedQueryAndNamedParams("categoryMst.getUnique",queryParams);
		if (categoryMst == null || categoryMst.size() == 0) {
			return null;
		}
		return categoryMst.get(0);
	}	

@Override
	public CategoryMst getUniqueCmmiVersion(String tenantId,String categoryCode,String process,String organizationUnit,String businessUnit,
			String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("categoryCode", categoryCode);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<CategoryMst> categoryMst = (List<CategoryMst>) categoryMstDao.findByNamedQueryAndNamedParams("categoryMst.getUniqueCmmiVersion",queryParams);
		if (categoryMst == null || categoryMst.size() == 0) {
			return null;
		}
		return categoryMst.get(0);
	}	
	
	
	public CategoryMst getByName(String tenantId,String name,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("categoryName", name);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<CategoryMst> categoryMst = (List<CategoryMst>) categoryMstDao.findByNamedQueryAndNamedParams("categoryMst.getByName",queryParams);
		if (categoryMst == null || categoryMst.size() == 0) {
			return null;
		}
		return categoryMst.get(0);
	}

	public List<CategoryMst> getAllCategory(String tenantId,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<CategoryMst> categoryMst = (List<CategoryMst>) categoryMstDao.findByNamedQueryAndNamedParams("categoryMst.getAll",queryParams);
		if (categoryMst == null || categoryMst.size() == 0) {
			return null;
		}
		Collections.sort(categoryMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final CategoryMst bal1 = (CategoryMst) o1;
				final CategoryMst bal2 = (CategoryMst) o2;

				int c;
				
				c=bal1.getCategoryCode().compareTo(bal2.getCategoryCode());
				
				
				return c;
			}
		});
		return categoryMst;
		
		
	}
	
	public List<CategoryMst> getByTenantAndProcess(String tenantId,String process,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("process", process);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<CategoryMst> categoryMst = (List<CategoryMst>) categoryMstDao.findByNamedQueryAndNamedParams("categoryMst.getByTenantAndProcess",queryParams);
		if (categoryMst == null || categoryMst.size() == 0) {
			return null;
		}
		Collections.sort(categoryMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final CategoryMst bal1 = (CategoryMst) o1;
				final CategoryMst bal2 = (CategoryMst) o2;

				int c;
				
				c=bal1.getCategoryCode().compareTo(bal2.getCategoryCode());
				
				
				return c;
			}
		});
		return categoryMst;
		
		
	}

	public List<CategoryMst> getBaseCategory(String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", "0");
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<CategoryMst> categoryMst = (List<CategoryMst>) categoryMstDao.findByNamedQueryAndNamedParams("categoryMst.getBaseCategory",queryParams);
		if (categoryMst == null || categoryMst.size() == 0) {
			return null;
		}
		Collections.sort(categoryMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final CategoryMst bal1 = (CategoryMst) o1;
				final CategoryMst bal2 = (CategoryMst) o2;

				int c;
				
				c=bal1.getCategoryCode().compareTo(bal2.getCategoryCode());
				
				
				return c;
			}
		});
		return categoryMst;
	}
		
	public List<CategoryMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<CategoryMst> categoryMst = (List<CategoryMst>) categoryMstDao.findByNamedQueryAndNamedParams("categoryMst.getByTenantAndProject",queryParams);
		if (categoryMst == null || categoryMst.size() == 0) {
			return null;
		}
		Collections.sort(categoryMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final CategoryMst bal1 = (CategoryMst) o1;
				final CategoryMst bal2 = (CategoryMst) o2;

				int c;
				
				c=bal1.getCategoryCode().compareTo(bal2.getCategoryCode());
				
				
				return c;
			}
		});
		return categoryMst;
		
		
	}

}