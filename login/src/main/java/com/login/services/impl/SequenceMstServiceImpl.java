package com.login.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.SequenceMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.SequenceMst;
import com.login.domain.basics.AccessObject;
import com.login.services.SequenceMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("sequenceMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class SequenceMstServiceImpl extends CoreServiceImpl<Long, SequenceMst> implements SequenceMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	SequenceMstDao sequenceMstDao;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) sequenceMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
//	
	public List<SequenceMst> getAllList() {
		return super.findAll();
	}
	
	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		sequenceMstDao.setEntityManager(entityManager);
	}

	
	public SequenceMst saveOrUpDate(SequenceMst entity,AccessObject accessObject)
			throws BaseException {
		
		String loginId=accessObject.getLoginId();
		if(null==entity.getTenantId() ||entity.getTenantId().equalsIgnoreCase(""))
		{
			entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		
		
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		SequenceMst sequenceMst = getSequenceMst(entity.getTenantId(),entity.getSequenceType());
		if (sequenceMst == null) {
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = sequenceMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, sequenceMst);
			} catch (Exception e) {
				
			}
			sequenceMst.setId(id);

			return super.saveOrUpdate(loginId, sequenceMst);
		}
		
	}

	
	public SequenceMst getSequenceMst(String tenantId,String sequenceType) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("sequenceType", sequenceType);

		List<SequenceMst> sequenceMst = (List<SequenceMst>) sequenceMstDao.findByNamedQueryAndNamedParams("sequenceMst.getSequenceBySequenceType",queryParams);
		if (sequenceMst == null || sequenceMst.size() == 0) {
			return null;
		}
		return sequenceMst.get(0);
	}

	public List<SequenceMst> getAll(String tenantId) {
		Map<String, String> queryParams = new HashMap<String, String>();
		
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		

		List<SequenceMst> sequenceMst = (List<SequenceMst>) sequenceMstDao.findByNamedQueryAndNamedParams("sequenceMst.getAll",queryParams);
		if (sequenceMst == null || sequenceMst.size() == 0) {
			return null;
		}
		return sequenceMst;
	}
	

	public String getNextSeqNo(String tenantId, String seqType)
			throws Exception {

		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		if (null == seqType) {
			throw new Exception("Sequence Type not exist.");
		}

		Map<String, String> params = new HashMap<String, String>();
		params.put("tenantId", tenantId);
		params.put("sequenceType", seqType);

		
		SequenceMst entity = new SequenceMst();

		List<SequenceMst> sequenceNoMstList = (List<SequenceMst>)sequenceMstDao.findByNamedQueryAndNamedParams(
				"sequenceMst.getSequenceBySequenceType", params);

		if (null == sequenceNoMstList || sequenceNoMstList.isEmpty()) {
			throw new Exception(
					"No Application Configuration Exists with sequence Type :"
							+ seqType + "for Tenant : " + tenantId);

		}
		entity = sequenceNoMstList.get(0);

		
		Integer nextNumber = 1;
		if (null == entity.getNextSeqNo()) {
			nextNumber = 1;
		} else {
			nextNumber = entity.getNextSeqNo();
		}
		Integer storeNumber = nextNumber + 1;
		entity.setNextSeqNo(storeNumber);
		saveOrUpdate(entity);

		String str = "";
		String nextNumberStr = nextNumber.toString();
		
		return nextNumberStr;
	}

}
