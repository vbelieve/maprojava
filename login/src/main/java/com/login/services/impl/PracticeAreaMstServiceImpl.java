package com.login.services.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.PracticeAreaMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.basics.AccessObject;
import com.login.services.PracticeAreaMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("practiceAreaMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class PracticeAreaMstServiceImpl extends CoreServiceImpl<Long, PracticeAreaMst> implements PracticeAreaMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	PracticeAreaMstDao practiceAreaMstDao;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) practiceAreaMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		practiceAreaMstDao.setEntityManager(entityManager);
	}

	public String delete(PracticeAreaMst entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		PracticeAreaMst practiceAreaMst = getUnique(entity.getTenantId(),
				entity.getPracticeAreaCode(),entity.getProcess(),entity.getOrganizationUnit(),
				entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (practiceAreaMst == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = practiceAreaMst.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	
	public PracticeAreaMst saveOrUpDate(PracticeAreaMst entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {
		String loginId=accessObject.getLoginId();
		entity.setCreatedDate(new Date());
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		if(entity.getTenantId()==null)
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		entity.setVersion(0);
		
		PracticeAreaMst practiceAreaMst = getUnique(entity.getTenantId(),entity.getPracticeAreaCode(),
				entity.getProcess(),entity.getOrganizationUnit(),
				entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (practiceAreaMst == null) {
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = practiceAreaMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, practiceAreaMst);
			} catch (Exception e) {
				
			}
			practiceAreaMst.setId(id);
			
			return super.saveOrUpdate(loginId, practiceAreaMst);
		}
		
	}

	

	
	
	

	public PracticeAreaMst getUnique(String tenantId,String name,String process,String organizationUnit,String businessUnit,String project
			,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", "0");
		queryParams.put("practiceAreaCode", name);
		queryParams.put("cmmiVersion", cmmiVersion);
		
//		queryParams.put("organizationUnit", organizationUnit);
//		queryParams.put("businessUnit", businessUnit);
//		queryParams.put("project", project);

		List<PracticeAreaMst> practiceAreaMst = (List<PracticeAreaMst>) practiceAreaMstDao.findByNamedQueryAndNamedParams("practiceAreaMst.getUnique",queryParams);
		if (practiceAreaMst == null || practiceAreaMst.size() == 0) {
			return null;
		}
		return practiceAreaMst.get(0);
	}
	
	
	public PracticeAreaMst getByName(String tenantId,String name,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("practiceAreaId", name);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeAreaMst> practiceAreaMst = (List<PracticeAreaMst>) practiceAreaMstDao.findByNamedQueryAndNamedParams("practiceAreaMst.getByName",queryParams);
		if (practiceAreaMst == null || practiceAreaMst.size() == 0) {
			return null;
		}
		return practiceAreaMst.get(0);
	}
	
	
	public List<PracticeAreaMst> getByCategory(String tenantId,Integer categoryId,String cmmiVersion)
	{
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("categoryId", categoryId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeAreaMst> practiceAreaMst = (List<PracticeAreaMst>) practiceAreaMstDao.findByNamedQueryAndNamedParams("practiceAreaMst.getByCategory",queryParams);
		if (practiceAreaMst == null || practiceAreaMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceAreaMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeAreaMst bal1 = (PracticeAreaMst) o1;
				final PracticeAreaMst bal2 = (PracticeAreaMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				
				
				return c;
			}
		});
		
		return practiceAreaMst;
		
	}

	public List<PracticeAreaMst> getByTenantAndProcess(String tenantId,String process,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("process", process);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeAreaMst> practiceAreaMst = (List<PracticeAreaMst>) practiceAreaMstDao.findByNamedQueryAndNamedParams("practiceAreaMst.getByTenantAndProcess",queryParams);
		if (practiceAreaMst == null || practiceAreaMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceAreaMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeAreaMst bal1 = (PracticeAreaMst) o1;
				final PracticeAreaMst bal2 = (PracticeAreaMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				
				
				return c;
			}
		});
		
		return practiceAreaMst;
		
		
	}
	
	public List<PracticeAreaMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
//		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeAreaMst> practiceAreaMst = (List<PracticeAreaMst>) practiceAreaMstDao.findByNamedQueryAndNamedParams("practiceAreaMst.getByTenantAndProject",queryParams);
		if (practiceAreaMst == null || practiceAreaMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceAreaMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeAreaMst bal1 = (PracticeAreaMst) o1;
				final PracticeAreaMst bal2 = (PracticeAreaMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				
				
				return c;
			}
		});
		
		return practiceAreaMst;
		
		
	}
	
	public List<PracticeAreaMst> getBase(String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", "0");
		queryParams.put("cmmiVersion",cmmiVersion);
		
		
		List<PracticeAreaMst> practiceAreaMst = (List<PracticeAreaMst>) practiceAreaMstDao.findByNamedQueryAndNamedParams("practiceAreaMst.getBase",queryParams);
		if (practiceAreaMst == null || practiceAreaMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceAreaMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeAreaMst bal1 = (PracticeAreaMst) o1;
				final PracticeAreaMst bal2 = (PracticeAreaMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				
				
				return c;
			}
		});
		
		return practiceAreaMst;
		
		
	}

	public PracticeAreaMst getByProjectPracticeArea(String tenantId,String project,String practiceAreaCode,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("practiceAreaCode", practiceAreaCode);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeAreaMst> practiceAreaMst = (List<PracticeAreaMst>) practiceAreaMstDao.findByNamedQueryAndNamedParams("practiceAreaMst.getByProjectPracticeArea",queryParams);
		if (practiceAreaMst == null || practiceAreaMst.size()==0) {
			return null;
		}
		return practiceAreaMst.get(0);
		
		
	}
	
	public List<PracticeAreaMst> getAll(String tenantId,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeAreaMst> practiceAreaMst = (List<PracticeAreaMst>) practiceAreaMstDao.findByNamedQueryAndNamedParams("practiceAreaMst.getAll",queryParams);
		if (practiceAreaMst == null || practiceAreaMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceAreaMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeAreaMst bal1 = (PracticeAreaMst) o1;
				final PracticeAreaMst bal2 = (PracticeAreaMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				
				
				return c;
			}
		});
		
		return practiceAreaMst;
		
		
	}

	@Override
	public List<PracticeAreaMst> getByCapability(String tenantId, Integer capabilityId,String cmmiVersion) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("capabilityId", capabilityId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeAreaMst> practiceAreaMst = (List<PracticeAreaMst>) practiceAreaMstDao.findByNamedQueryAndNamedParams("practiceAreaMst.getByCapability",queryParams);
		if (practiceAreaMst == null || practiceAreaMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceAreaMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeAreaMst bal1 = (PracticeAreaMst) o1;
				final PracticeAreaMst bal2 = (PracticeAreaMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				
				
				return c;
			}
		});
		
		return practiceAreaMst;

	
	}

	@Override
	public List<PracticeAreaMst> getByCategoryAndCapability(String tenantId, Integer categoryId, Integer capabilityId,String cmmiVersion) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("categoryId", categoryId);
		queryParams.put("capabilityId", capabilityId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeAreaMst> practiceAreaMst = (List<PracticeAreaMst>) practiceAreaMstDao.findByNamedQueryAndNamedParams("practiceAreaMst.getByCategoryAndCapability",queryParams);
		if (practiceAreaMst == null || practiceAreaMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceAreaMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeAreaMst bal1 = (PracticeAreaMst) o1;
				final PracticeAreaMst bal2 = (PracticeAreaMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				
				
				return c;
			}
		});
		
		return practiceAreaMst;

	}

}