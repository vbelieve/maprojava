package com.login.services.impl;


import com.login.dao.RelationMappingCategoryCapabilityDao;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.WorkProductMst;
import com.login.domain.basics.AccessObject;
import com.login.services.RelationMappingCategoryCapabilityService;
import com.login.services.basic.CoreServiceImpl;

@Service("relationMappingCategoryCapabilityService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class RelationMappingCategoryCapabilityServiceImpl extends CoreServiceImpl<Long, RelationMappingCategoryCapability> implements RelationMappingCategoryCapabilityService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	RelationMappingCategoryCapabilityDao relationMappingCategoryCapabilityDao;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) relationMappingCategoryCapabilityDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		relationMappingCategoryCapabilityDao.setEntityManager(entityManager);
	}

	public String delete(RelationMappingCategoryCapability entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		RelationMappingCategoryCapability relationMappingCategoryCapability = getUnique(entity.getTenantId(),entity.getCategoryCode(),entity.getCapabilityCode(),
				entity.getProcess(),entity.getOrganizationUnit(),entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (relationMappingCategoryCapability == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = relationMappingCategoryCapability.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	
	public RelationMappingCategoryCapability saveOrUpDate(RelationMappingCategoryCapability entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {
		String loginId=accessObject.getLoginId();
		if(entity.getTenantId().equalsIgnoreCase(""))
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		
		RelationMappingCategoryCapability relationMappingCategoryCapability = getUnique(entity.getTenantId(),entity.getCategoryCode(),
				entity.getCapabilityCode(),entity.getProcess(),entity.getOrganizationUnit(),entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (relationMappingCategoryCapability == null) {
			entity.setCreatedDate(new Date());
			entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = relationMappingCategoryCapability.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, relationMappingCategoryCapability);
			} catch (Exception e) {
				
			}
			relationMappingCategoryCapability.setId(id);
			return super.saveOrUpdate(loginId, relationMappingCategoryCapability);
		}
		
	}

	

	
	
	


	public RelationMappingCategoryCapability getUnique(String tenantId,String categoryCode,String capabilityCode,String process,String organizationUnit,
			String businessUnit,String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("categoryCode", categoryCode);
		queryParams.put("capabilityCode", capabilityCode);
		queryParams.put("cmmiVersion", cmmiVersion);
//		queryParams.put("organizationUnit", organizationUnit);
//		queryParams.put("businessUnit", businessUnit);
//		queryParams.put("project", project);

		

		List<RelationMappingCategoryCapability> relationMappingCategoryCapability = (List<RelationMappingCategoryCapability>) relationMappingCategoryCapabilityDao.
				findByNamedQueryAndNamedParams("relationMappingCategoryCapability.getUnique",queryParams);
		if (relationMappingCategoryCapability == null || relationMappingCategoryCapability.size() == 0) {
			return null;
		}
		return relationMappingCategoryCapability.get(0);
	}

	
	@Override
	public List<RelationMappingCategoryCapability> getAll(String tenantId,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("cmmiVersion", cmmiVersion);
		

		List<RelationMappingCategoryCapability> relationMappingCategoryCapability = (List<RelationMappingCategoryCapability>)
				relationMappingCategoryCapabilityDao.findByNamedQueryAndNamedParams("relationMappingCategoryCapability.getAll",queryParams);
		if (relationMappingCategoryCapability == null || relationMappingCategoryCapability.size() == 0) {
			return null;
		}
		Collections.sort(relationMappingCategoryCapability, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingCategoryCapability bal1 = (RelationMappingCategoryCapability) o1;
				final RelationMappingCategoryCapability bal2 = (RelationMappingCategoryCapability) o2;

				int c;
				
				c=bal1.getCategoryCode().compareTo(bal2.getCategoryCode());
				if(c==0)
				{
					c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				}
				
				
				return c;
			}
		});
		return relationMappingCategoryCapability;


	}
	
	public List<RelationMappingCategoryCapability> getByTenantAndProcess(String tenantId,String process,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("process", process);
		queryParams.put("cmmiVersion", cmmiVersion);
		List<RelationMappingCategoryCapability> relationMappingCategoryCapability = (List<RelationMappingCategoryCapability>) relationMappingCategoryCapabilityDao.findByNamedQueryAndNamedParams("relationMappingCategoryCapability.getByTenantAndProcess",queryParams);
		if (relationMappingCategoryCapability == null || relationMappingCategoryCapability.size() == 0) {
			return null;
		}
		Collections.sort(relationMappingCategoryCapability, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingCategoryCapability bal1 = (RelationMappingCategoryCapability) o1;
				final RelationMappingCategoryCapability bal2 = (RelationMappingCategoryCapability) o2;

				int c;
				
				c=bal1.getCategoryCode().compareTo(bal2.getCategoryCode());
				if(c==0)
				{
					c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				}
				
				
				return c;
			}
		});
		return relationMappingCategoryCapability;
		
		
	}

	
	public List<RelationMappingCategoryCapability> getByCapability(String tenantId, String capabilityCode,String organizationUnit,String businessUnit,String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("capabilityCode", capabilityCode);
		queryParams.put("cmmiVersion", cmmiVersion);
//		queryParams.put("organizationUnit", organizationUnit);
//		queryParams.put("businessUnit", businessUnit);
//		queryParams.put("project", project);

		List<RelationMappingCategoryCapability> relationMappingCategoryCapability = (List<RelationMappingCategoryCapability>) relationMappingCategoryCapabilityDao.findByNamedQueryAndNamedParams("relationMappingCategoryCapability.getCapability",queryParams);
		if (relationMappingCategoryCapability == null || relationMappingCategoryCapability.size() == 0) {
			return null;
		}
		Collections.sort(relationMappingCategoryCapability, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingCategoryCapability bal1 = (RelationMappingCategoryCapability) o1;
				final RelationMappingCategoryCapability bal2 = (RelationMappingCategoryCapability) o2;

				int c;
				
				c=bal1.getCategoryCode().compareTo(bal2.getCategoryCode());
				if(c==0)
				{
					c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				}
				
				
				return c;
			}
		});
		return relationMappingCategoryCapability;


	}

	public RelationMappingCategoryCapability getByCapabilityByProcess(String tenantId, String capabilityCode,String organizationUnit,String businessUnit,String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("capabilityCode", capabilityCode);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		RelationMappingCategoryCapability relationMappingCategoryCapability = (RelationMappingCategoryCapability) relationMappingCategoryCapabilityDao.findByNamedQueryAndNamedParams("relationMappingCategoryCapability.getCapability",queryParams);
		if (relationMappingCategoryCapability == null) {
			return null;
		}

		return relationMappingCategoryCapability;


	}
	@Override
	public List<RelationMappingCategoryCapability> getByCategory(String tenantId, String categoryCode,String organizationUnit,String businessUnit,String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("categoryCode", categoryCode);
		queryParams.put("cmmiVersion", cmmiVersion);
		//queryParams.put("organizationUnit", organizationUnit);
		//queryParams.put("businessUnit", businessUnit);
		//queryParams.put("project", project);

		List<RelationMappingCategoryCapability> relationMappingCategoryCapability = (List<RelationMappingCategoryCapability>) relationMappingCategoryCapabilityDao.findByNamedQueryAndNamedParams("relationMappingCategoryCapability.getCategory",queryParams);
		if (relationMappingCategoryCapability == null || relationMappingCategoryCapability.size() == 0) {
			return null;
		}
		Collections.sort(relationMappingCategoryCapability, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingCategoryCapability bal1 = (RelationMappingCategoryCapability) o1;
				final RelationMappingCategoryCapability bal2 = (RelationMappingCategoryCapability) o2;

				int c;
				
				c=bal1.getCategoryCode().compareTo(bal2.getCategoryCode());
				if(c==0)
				{
					c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				}
				
				
				return c;
			}
		});
		return relationMappingCategoryCapability;
}

	@Override
	public List<RelationMappingCategoryCapability> getByTenantAndProject(String tenantId, String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
//		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<RelationMappingCategoryCapability> relationMappingCategoryCapability = (List<RelationMappingCategoryCapability>) relationMappingCategoryCapabilityDao.findByNamedQueryAndNamedParams("relationMappingCategoryCapability.getByTenantAndProject",queryParams);
		if (relationMappingCategoryCapability == null || relationMappingCategoryCapability.size() == 0) {
			return null;
		}
		Collections.sort(relationMappingCategoryCapability, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingCategoryCapability bal1 = (RelationMappingCategoryCapability) o1;
				final RelationMappingCategoryCapability bal2 = (RelationMappingCategoryCapability) o2;

				int c;
				
				c=bal1.getCategoryCode().compareTo(bal2.getCategoryCode());
				if(c==0)
				{
					c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				}
				
				
				return c;
			}
		});
		return relationMappingCategoryCapability;
		
	}

	
	
	


}