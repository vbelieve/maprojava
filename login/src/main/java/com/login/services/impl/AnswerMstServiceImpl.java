package com.login.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.login.common.exceptions.BaseException;
import com.login.dao.AnswerMstDao;
import com.login.dao.PracticeMstDao;
import com.login.dao.TestMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.AnswerMst;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.ProjectMst;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.RelationMappingPracticeAreaPractice;
import com.login.domain.TenantMst;
import com.login.domain.TestMst;
import com.login.domain.basics.AccessObject;
import com.login.domain.basics.DashBoardVo;
import com.login.services.AnswerMstService;
import com.login.services.PracticeMstService;
import com.login.services.ProjectMstService;
import com.login.services.TenantMstService;
import com.login.services.TestMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("answerMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class AnswerMstServiceImpl extends CoreServiceImpl<Long, AnswerMst> implements AnswerMstService{

	@PersistenceContext
	EntityManager em;
	

	
	@Autowired
	AnswerMstDao answerMstDao;
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) answerMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		answerMstDao.setEntityManager(entityManager);
	}

	
	public List<AnswerMst> getAll(String tenantId,String authStatus) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("authStatus", authStatus);
		

		List<AnswerMst> answerMstList = (List<AnswerMst>) answerMstDao.findByNamedQueryAndNamedParams(""
				+ "answerMst.getAll",queryParams);
		if (answerMstList == null || answerMstList.size() == 0) {
			return null;
		}		
		return answerMstList;
	}
	
	@Override
	public AnswerMst getUnique(String tenantId,Long answerId) {
	Map<String, Object> queryParams = new HashMap<String, Object>();
	queryParams.put("tenantId", tenantId);
	queryParams.put("answerId", answerId);
	

	List<AnswerMst> answerMstList = (List<AnswerMst>) answerMstDao.findByNamedQueryAndNamedParams(""
			+ "answerMst.getUnique",queryParams);
	if (answerMstList == null || answerMstList.size() == 0) {
		return null;
	}		
	return answerMstList.get(0);
	}
	
	@Override
	public AnswerMst saveOrUpDate(AnswerMst entity,AccessObject accessObject)
			throws BaseException {
		String loginId=accessObject.getLoginId();
		if(entity.getTenantId()=="")
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		
		entity.setCreatedDate(new Date());
		AnswerMst answerMst = getUnique(entity.getTenantId(), entity.getAnswerId());
		if (answerMst == null) {
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = answerMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, answerMst);
			} catch (Exception e) {
				
			}
			answerMst.setId(id);
			
			return super.saveOrUpdate(loginId, answerMst);
		}
		
	}

}