package com.login.services.impl;


import com.login.services.SequenceMstService;
import com.login.services.WorkProductMstService;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.QuestionMstDao;
import com.login.dao.WorkProductMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.QuestionMst;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.SequenceMst;
import com.login.domain.WorkProductMst;
import com.login.domain.basics.AccessObject;
import com.login.services.QuestionMstService;
import com.login.services.basic.CoreServiceImpl;

@Service("workProductMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class WorkProductMstServiceImpl extends CoreServiceImpl<Long, WorkProductMst> implements WorkProductMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	WorkProductMstDao workProductMstDao;

	@Autowired
	SequenceMstService sequenceMstService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) workProductMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		workProductMstDao.setEntityManager(entityManager);
	}

	public String delete(WorkProductMst entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		WorkProductMst workProductMst = getUnique(entity.getTenantId(),entity.getWorkProductId(),
				entity.getProcess(),entity.getOrganizationUnit(),entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (workProductMst == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = workProductMst.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	
	public WorkProductMst saveOrUpDate(WorkProductMst entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {
		
		String seq="";
		
		String loginId=accessObject.getLoginId();
		if(entity.getTenantId().equalsIgnoreCase(""))
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
//		entity.setWorkProductId(seq);
		WorkProductMst workProductMst = getUnique(entity.getTenantId(),entity.getWorkProductId(),
				entity.getProcess(),entity.getOrganizationUnit(),entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (workProductMst == null) {
			try {
				seq=sequenceMstService.getNextSeqNo(ApplicationConstants.DEFAULT_TENANT,
						ApplicationConstants.SEQ_WORKPRODUCTSEQ);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			entity.setWorkProductId(seq);
			entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
			entity.setIsActive(1);
			entity.setCreatedDate(new Date());
			
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = workProductMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, workProductMst);
			} catch (Exception e) {
				
			}
			workProductMst.setId(id);
	
			return super.saveOrUpdate(loginId, workProductMst);
		}
		
	}

	

	
	
	


	public WorkProductMst getUnique(String tenantId,String workProductId,String process,
			String organizationUnit,String businessUnit,String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("workProductId", workProductId);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		
		
		

		List<WorkProductMst> workProductMst = (List<WorkProductMst>) workProductMstDao.findByNamedQueryAndNamedParams("WorkProductMst.getUnique",queryParams);
		if (workProductMst == null || workProductMst.size() == 0) {
			return null;
		}
		return workProductMst.get(0);
	}

	
	@Override
	public List<WorkProductMst> getAll(String tenantId,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		

		List<WorkProductMst> workProductMst = (List<WorkProductMst>) workProductMstDao.findByNamedQueryAndNamedParams("WorkProductMst.getAll",queryParams);
		if (workProductMst == null || workProductMst.size() == 0) {
			return null;
		}
		Collections.sort(workProductMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final WorkProductMst bal1 = (WorkProductMst) o1;
				final WorkProductMst bal2 = (WorkProductMst) o2;

				int c;
				
				c=bal1.getWorkProductName().compareTo(bal2.getWorkProductName());
				
				
				return c;
			}
		});

		return workProductMst;


	}
	
	public List<WorkProductMst> getByTenantAndProcess(String tenantId,String process,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("process", process);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<WorkProductMst> workProductMst = (List<WorkProductMst>) workProductMstDao.findByNamedQueryAndNamedParams("WorkProductMst.getByTenantAndProcess",queryParams);
		if (workProductMst == null || workProductMst.size() == 0) {
			return null;
		}
		Collections.sort(workProductMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final WorkProductMst bal1 = (WorkProductMst) o1;
				final WorkProductMst bal2 = (WorkProductMst) o2;

				int c;
				
				c=bal1.getWorkProductName().compareTo(bal2.getWorkProductName());
				
				
				return c;
			}
		});
		return workProductMst;
		
		
	}

	public List<WorkProductMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<WorkProductMst> workProductMst = (List<WorkProductMst>) workProductMstDao.findByNamedQueryAndNamedParams("WorkProductMst.getByTenantAndProject",queryParams);
		if (workProductMst == null || workProductMst.size() == 0) {
			return null;
		}
		Collections.sort(workProductMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final WorkProductMst bal1 = (WorkProductMst) o1;
				final WorkProductMst bal2 = (WorkProductMst) o2;

				int c;
				
				c=bal1.getWorkProductName().compareTo(bal2.getWorkProductName());
				
				
				return c;
			}
		});
		return workProductMst;
		
		
	}
	



		


}