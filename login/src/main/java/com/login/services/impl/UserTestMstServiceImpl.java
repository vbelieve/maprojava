package com.login.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.login.common.exceptions.BaseException;
import com.login.dao.AnswerMstDao;
import com.login.dao.PracticeMstDao;
import com.login.dao.TestMstDao;
import com.login.dao.UserTestMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.AnswerMst;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.ProjectMst;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.RelationMappingPracticeAreaPractice;
import com.login.domain.TenantMst;
import com.login.domain.TestMst;
import com.login.domain.UserTestMst;
import com.login.domain.basics.AccessObject;
import com.login.domain.basics.DashBoardVo;
import com.login.services.AnswerMstService;
import com.login.services.PracticeMstService;
import com.login.services.ProjectMstService;
import com.login.services.TenantMstService;
import com.login.services.TestMstService;
import com.login.services.UserTestMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("userTestMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class UserTestMstServiceImpl extends CoreServiceImpl<Long, UserTestMst> implements UserTestMstService{

	@PersistenceContext
	EntityManager em;
	

	
	@Autowired
	UserTestMstDao userTestMstDao;
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) userTestMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		userTestMstDao.setEntityManager(entityManager);
	}

	
	public List<UserTestMst> getAll(String tenantId,String authStatus) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("authStatus", authStatus);
		

		List<UserTestMst> userTestMstList = (List<UserTestMst>) userTestMstDao.findByNamedQueryAndNamedParams(""
				+ "userTestMst.getAll",queryParams);
		if (userTestMstList == null || userTestMstList.size() == 0) {
			return null;
		}		
		return userTestMstList;
	}
	
	@Override
	public UserTestMst getUnique(String tenantId,String testName,Long questionId) {
	Map<String, Object> queryParams = new HashMap<String, Object>();
	queryParams.put("tenantId", tenantId);
	queryParams.put("testName", testName);
	queryParams.put("questionId", questionId);

	List<UserTestMst> userTestMstList = (List<UserTestMst>) userTestMstDao.findByNamedQueryAndNamedParams(""
			+ "userTestMst.getUnique",queryParams);
	if (userTestMstList == null || userTestMstList.size() == 0) {
		return null;
	}		
	return userTestMstList.get(0);
	}
	
	@Override
	public UserTestMst saveOrUpDate(UserTestMst entity,AccessObject accessObject)
			throws BaseException {
		String loginId=accessObject.getLoginId();
		if(entity.getTenantId()=="")
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		
		entity.setCreatedDate(new Date());
		UserTestMst userTestMst = getUnique(entity.getTenantId(),entity.getTestName(), entity.getQuestionId());
		if (userTestMst == null) {
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = userTestMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, userTestMst);
			} catch (Exception e) {
				
			}
			userTestMst.setId(id);
			
			return super.saveOrUpdate(loginId, userTestMst);
		}
		
	}

}