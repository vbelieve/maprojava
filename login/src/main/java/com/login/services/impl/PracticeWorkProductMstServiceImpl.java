package com.login.services.impl;


import com.login.services.SequenceMstService;
import com.login.services.PracticeWorkProductMstService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.QuestionMstDao;
import com.login.dao.PracticeWorkProductMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.QuestionMst;
import com.login.domain.RelationMappingPracticeAreaPractice;
import com.login.domain.SequenceMst;
import com.login.domain.WorkProductMst;
import com.login.domain.basics.AccessObject;
import com.login.domain.PracticeWorkProductMst;
import com.login.services.QuestionMstService;
import com.login.services.basic.CoreServiceImpl;

@Service("practiceWorkProductMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class PracticeWorkProductMstServiceImpl extends CoreServiceImpl<Long, PracticeWorkProductMst> implements PracticeWorkProductMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	PracticeWorkProductMstDao practiceWorkProductMstDao;

	@Autowired
	SequenceMstService sequenceMstService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) practiceWorkProductMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		practiceWorkProductMstDao.setEntityManager(entityManager);
	}
	
	public String delete(PracticeWorkProductMst entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		PracticeWorkProductMst practiceWorkProductMst = getUnique(entity.getTenantId(),entity.getPracticeCode(),entity.getWorkProductName(),entity.getProject(),entity.getCmmiVersion());
		if (practiceWorkProductMst == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = practiceWorkProductMst.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	

	public PracticeWorkProductMst saveOrUpDate(PracticeWorkProductMst entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {
		String loginId=accessObject.getLoginId();
		if(entity.getTenantId()=="")
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		
		entity.setCreatedDate(new Date());
		PracticeWorkProductMst practiceWorkProductMst = getUnique(entity.getTenantId(),entity.getPracticeCode(),
				entity.getWorkProductName(),entity.getProject(),entity.getCmmiVersion());
		if (practiceWorkProductMst == null) {
			entity.setIsActive(1);
			List<RelationMappingPracticeAreaPractice> relation=new ArrayList<RelationMappingPracticeAreaPractice>();
			relation=accessObject.getRelationMappingPracticeAreaPractice();
			for(RelationMappingPracticeAreaPractice rel:relation)
			{
				if(entity.getPracticeCode().equalsIgnoreCase(rel.getPracticeCode()))
				{
					entity.setPracticeAreaCode(rel.getPracticeAreaCode());
					break;
				}
			}
			
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = practiceWorkProductMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, practiceWorkProductMst);
			} catch (Exception e) {
				
			}
			practiceWorkProductMst.setId(id);
			
			return super.saveOrUpdate(loginId, practiceWorkProductMst);
		}
		
	}
	
	public PracticeWorkProductMst saveOrUpDate(PracticeWorkProductMst entity,String cmmiVersion)
			throws BaseException {
		
//		String seq="";
//		try {
//			seq=sequenceMstService.getNextSeqNo(ApplicationConstants.DEFAULT_TENANT,
//					ApplicationConstants.SEQ_WORKPRODUCTSEQ);
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		String loginId=entity.getCreatedBy();
		if(entity.getTenantId().equalsIgnoreCase(""))
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
//		entity.setPracticeWorkProductId(seq);
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		entity.setIsActive(1);
		entity.setCreatedDate(new Date());
		PracticeWorkProductMst practiceWorkProductMst = getUnique(entity.getTenantId(),entity.getPracticeAreaCode(),
				entity.getWorkProductName(),
				entity.getProject(),entity.getCmmiVersion());
		if (practiceWorkProductMst == null) {
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = practiceWorkProductMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, practiceWorkProductMst);
			} catch (Exception e) {
				
			}
			practiceWorkProductMst.setId(id);
			practiceWorkProductMst.setIsActive(1);
			return super.saveOrUpdate(loginId, practiceWorkProductMst);
		}
		
	}

	public List<PracticeWorkProductMst> getByPracticeCode(String tenantId,String practiceCode,String organizationUnit,String businessUnit,String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("practiceCode", practiceCode);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		
		

		List<PracticeWorkProductMst> practiceWorkProductMst = (List<PracticeWorkProductMst>) practiceWorkProductMstDao.findByNamedQueryAndNamedParams("PracticeWorkProductMst.getByPracticeCode",queryParams);
		if (practiceWorkProductMst == null || practiceWorkProductMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceWorkProductMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeWorkProductMst bal1 = (PracticeWorkProductMst) o1;
				final PracticeWorkProductMst bal2 = (PracticeWorkProductMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				if(c==0)
				{
					c=bal1.getWorkProductName().compareTo(bal2.getWorkProductName());
				}
				
				return c;
			}
		});
		return practiceWorkProductMst;
	}	

	
	
	

@Override
	public PracticeWorkProductMst getUnique(String tenantId,String practiceCode,String workProductName,String project,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("practiceCode", practiceCode);
		queryParams.put("workProductName", workProductName);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeWorkProductMst> practiceWorkProductMst = (List<PracticeWorkProductMst>) practiceWorkProductMstDao.findByNamedQueryAndNamedParams("PracticeWorkProductMst.getUnique",queryParams);
		if (practiceWorkProductMst == null || practiceWorkProductMst.size() == 0) {
			return null;
		}
		return practiceWorkProductMst.get(0);
	}

	
	@Override
	public List<PracticeWorkProductMst> getAll(String tenantId,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		

		List<PracticeWorkProductMst> practiceWorkProductMst = (List<PracticeWorkProductMst>) practiceWorkProductMstDao.findByNamedQueryAndNamedParams("PracticeWorkProductMst.getAll",queryParams);
		if (practiceWorkProductMst == null || practiceWorkProductMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceWorkProductMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeWorkProductMst bal1 = (PracticeWorkProductMst) o1;
				final PracticeWorkProductMst bal2 = (PracticeWorkProductMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				if(c==0)
				{
					c=bal1.getWorkProductName().compareTo(bal2.getWorkProductName());
				}
				
				return c;
			}
		});
		return practiceWorkProductMst;


	}
	

	public List<PracticeWorkProductMst> getByTenantAndProcess(String tenantId,String process,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("process", process);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeWorkProductMst> practiceWorkProductMst = (List<PracticeWorkProductMst>) practiceWorkProductMstDao.findByNamedQueryAndNamedParams("PracticeWorkProductMst.getByTenantAndProcess",queryParams);
		if (practiceWorkProductMst == null || practiceWorkProductMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceWorkProductMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeWorkProductMst bal1 = (PracticeWorkProductMst) o1;
				final PracticeWorkProductMst bal2 = (PracticeWorkProductMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				if(c==0)
				{
					c=bal1.getWorkProductName().compareTo(bal2.getWorkProductName());
				}
				
				return c;
			}
		});
		return practiceWorkProductMst;
		
		
	}

	public List<PracticeWorkProductMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeWorkProductMst> practiceWorkProductMst = (List<PracticeWorkProductMst>) practiceWorkProductMstDao.
				findByNamedQueryAndNamedParams("PracticeWorkProductMst.getByTenantAndProject",queryParams);
		if (practiceWorkProductMst == null || practiceWorkProductMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceWorkProductMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeWorkProductMst bal1 = (PracticeWorkProductMst) o1;
				final PracticeWorkProductMst bal2 = (PracticeWorkProductMst) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				if(c==0)
				{
					c=bal1.getWorkProductName().compareTo(bal2.getWorkProductName());
				}
				
				return c;
			}
		});
		return practiceWorkProductMst;
		
		
	}



}