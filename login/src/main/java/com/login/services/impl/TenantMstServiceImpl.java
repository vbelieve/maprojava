package com.login.services.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.TenantMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.PracticeMst;
import com.login.domain.TenantMst;
import com.login.domain.basics.AccessObject;
import com.login.services.CreateOrgGapDetailsService;
import com.login.services.SequenceMstService;
import com.login.services.TenantMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("tenantMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class TenantMstServiceImpl extends CoreServiceImpl<Long, TenantMst> implements TenantMstService {

	SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
	@PersistenceContext
	EntityManager em;


	@Autowired
	TenantMstDao tenantMstDao;

	@Autowired
	CreateOrgGapDetailsService createOrgGapDetailsService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })

	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) tenantMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		tenantMstDao.setEntityManager(entityManager);
	}

	
	@Autowired
	SequenceMstService sequenceMstService;
	

	public String delete(TenantMst entity,AccessObject accessObject)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		TenantMst tenantMst = getUnique(entity.getTenantId(),entity.getOrganizationCode());
		if (tenantMst == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = tenantMst.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	


	public TenantMst SaveOrUpdate(TenantMst entity,AccessObject accessObject) {
		String tenantSeqNo="0";
		String orgCode="0";
		String loginId=accessObject.getLoginId();
		
		TenantMst tenantMst = getUnique(entity.getTenantId(),entity.getOrganizationCode());
		if (tenantMst == null) {
			try {
				tenantSeqNo=sequenceMstService.getNextSeqNo(entity.getTenantId(), ApplicationConstants.SEQ_TENANTIDSEQ);
				orgCode=sequenceMstService.getNextSeqNo(entity.getTenantId(), ApplicationConstants.SEQ_ORGCODESEQ);
			} catch (Exception e) {

				e.printStackTrace();
			}
			entity.setCreatedDate(new Date());
			entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
			entity.setVersion(0);
			entity.setIsActive(1);
			entity.setTenantId(tenantSeqNo);
			entity.setOrganizationCode(orgCode);
			if(null==entity.getLicenseExpiryDate() || entity.getLicenseExpiryDate().equalsIgnoreCase(""))
			{
				entity.setLicenseExpiryDate(sdf.format(new Date()));
			}
			super.saveOrUpdate(loginId, entity);
			//createOrgGapDetailsService.CreateOrgGapDetails(entity);
			createOrgGapDetailsService.CreateOrgAdminUser(entity);
			return entity;
		} else {
			Long id = tenantMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, tenantMst);
			} catch (Exception e) {
				logger.info(e);
			}
			tenantMst.setId(id);
			
			return super.saveOrUpdate(loginId, tenantMst);
		}
	}


	public List<TenantMst> getAll()
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		List<TenantMst> tenantMst = (List<TenantMst>) tenantMstDao.findByNamedQueryAndNamedParams("tenantMst.getAll",queryParams);
		if (tenantMst == null || tenantMst.size() == 0) {
			return null;
		}
		return tenantMst;
		
		
	}

	public TenantMst getTenant(String tenantId) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		

		List<TenantMst> tenantMst = (List<TenantMst>) tenantMstDao.findByNamedQueryAndNamedParams("tenantMst.getTenant",queryParams);
		if (tenantMst == null || tenantMst.size() == 0) {
			return null;
		}
		return tenantMst.get(0);
	}
	
	public TenantMst getUnique(String tenantId,String organizationCode) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("organizationCode", organizationCode);

		List<TenantMst> tenantMst = (List<TenantMst>) tenantMstDao.findByNamedQueryAndNamedParams("tenantMst.getUnique",queryParams);
		if (tenantMst == null || tenantMst.size() == 0) {
			return null;
		}
		return tenantMst.get(0);
	}
	
}
