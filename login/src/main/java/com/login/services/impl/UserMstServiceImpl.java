package com.login.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.UserMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.UserMst;
import com.login.services.UserMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("userMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class UserMstServiceImpl extends CoreServiceImpl<Long, UserMst> implements UserMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	UserMstDao userMstDao;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) userMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		userMstDao.setEntityManager(entityManager);
	}

	
	public UserMst saveOrUpDate(UserMst entity,String loginId)
			throws BaseException {
		UserMst UserMst = getByName(entity.getTenantId(),entity.getUserName());
		if (UserMst == null) {
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = UserMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, UserMst);
			} catch (Exception e) {
				
			}
			UserMst.setId(id);
			return super.saveOrUpdate(loginId, UserMst);
		}
		
	}

	

	
	
	


	
	public UserMst getUserMst(String tenantId,String sequenceType) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("sequenceType", sequenceType);

		List<UserMst> userMst = (List<UserMst>) userMstDao.findByNamedQueryAndNamedParams("UserMst.getSequenceBySequenceType",queryParams);
		if (userMst == null || userMst.size() == 0) {
			return null;
		}
		return userMst.get(0);
	}

	public UserMst getByName(String tenantId,String userName) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("userName", userName);

		List<UserMst> userMst = (List<UserMst>) userMstDao.findByNamedQueryAndNamedParams("userMst.getUserByUserName",queryParams);
		if (userMst == null || userMst.size() == 0) {
			return null;
		}
		return userMst.get(0);
	}

	public List<UserMst> getAllUsers(String tenantId)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		List<UserMst> userMst = (List<UserMst>) userMstDao.findByNamedQueryAndNamedParams("userMst.getAllUser",queryParams);
		if (userMst == null || userMst.size() == 0) {
			return null;
		}
		return userMst;
		
		
	}
	
	public List<UserMst> getAllUsersForAdmin(String tenantId)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		List<UserMst> userMst = (List<UserMst>) userMstDao.findByNamedQueryAndNamedParams("userMst.getAllUserForAdmin",queryParams);
		if (userMst == null || userMst.size() == 0) {
			return null;
		}
		return userMst;
		
		
	}
	
	public UserMst validateUser(String tenantId,String userName,String password) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("userName", userName);
		queryParams.put("password", password);

		List<UserMst> userMst = (List<UserMst>) userMstDao.findByNamedQueryAndNamedParams("userMst.getUser",queryParams);
		if (userMst == null || userMst.size() == 0) {
			return null;
		}
		return userMst.get(0);
	}

}
