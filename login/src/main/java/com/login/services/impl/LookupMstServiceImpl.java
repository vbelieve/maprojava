package com.login.services.impl;

import com.login.domain.basics.AccessObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.LookupMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.LookupMst;
import com.login.domain.PracticeAreaMst;
import com.login.services.LookupMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("lookupMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class LookupMstServiceImpl extends CoreServiceImpl<Long, LookupMst> implements LookupMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	LookupMstDao lookupMstDao;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) lookupMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
//	
	public List<LookupMst> getAllList() {
		return super.findAll();
	}
	
	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		lookupMstDao.setEntityManager(entityManager);
	}

	
	public LookupMst saveOrUpDate(LookupMst entity,AccessObject accessObject)
			throws BaseException {
		String loginId=accessObject.getLoginId();
		LookupMst lookupMst = getLookupByLookupTypeAndValue(accessObject.getTenantId(),entity.getLookupType(),entity.getLookupValue());
		if (lookupMst == null) {
//			entity.setIsActive(true);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = lookupMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, lookupMst);
			} catch (Exception e) {
				
			}
			lookupMst.setId(id);
//			lookupMst.setIsActive(true);
			return super.saveOrUpdate(loginId, lookupMst);
		}
		
	}

	

	
	
	@Override
	public LookupMst getById(Integer id) {
		return null;
	}

	@Override
	public List<LookupMst> getAllActiveList() {
		return null;
	}

	@Override
	public LookupMst Save(LookupMst obj) {
		return null;
	}


	@Override
	public LookupMst updateStatus(LookupMst obj) {
//		return dao.save(obj);
		return null;
	}

	
	public Map<String,String> getAllLookupType(String tenantId)
	{
		Map<String,String> lookupTypeMap=new HashMap<String,String>();
		List<LookupMst> lookupsByTenant=getAllLookupMst(tenantId);
		if(null!=lookupsByTenant)
		{
			for(LookupMst lookupObject:lookupsByTenant)
			{
				String lookupType=lookupObject.getLookupType();
				if(lookupTypeMap.containsKey(lookupType))
				{
					continue;
				}
				else
				{
					lookupTypeMap.put(lookupType,lookupType);
				
				}
			}		
		}
		return lookupTypeMap;
	}
	
	public List<LookupMst> getAllLookupMst(String tenantId) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase("")||tenantId.equalsIgnoreCase(" "))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		
		List<LookupMst> lookupMst = (List<LookupMst>) lookupMstDao.findByNamedQueryAndNamedParams("lookupMst.getAll",queryParams);
		if (lookupMst == null || lookupMst.size() == 0) {
			return null;
		}
		Collections.sort(lookupMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final LookupMst bal1 = (LookupMst) o1;
				final LookupMst bal2 = (LookupMst) o2;

				int c;
				
				c=bal1.getLookupType().compareTo(bal2.getLookupType());
				if(c==0)
				{
					c=bal1.getDisplayValue().compareTo(bal2.getDisplayValue());
				}
				
				return c;
			}
		});

		return lookupMst;
	}
	
	
	public List<LookupMst> getLookupMst(String tenantId,String lookupType) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase("")||tenantId.equalsIgnoreCase(" "))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("lookupType", lookupType);

		List<LookupMst> lookupMst = (List<LookupMst>) lookupMstDao.findByNamedQueryAndNamedParams("lookupMst.getLookupByLookupType",queryParams);
		if (lookupMst == null || lookupMst.size() == 0) {
			return null;
		}
		Collections.sort(lookupMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final LookupMst bal1 = (LookupMst) o1;
				final LookupMst bal2 = (LookupMst) o2;

				int c;
				
				c=bal1.getDisplayValue().compareTo(bal2.getDisplayValue());
				
				return c;
			}
		});

		return lookupMst;
	}

	public List<LookupMst> getAllByTenant(String tenantId) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase("")||tenantId.equalsIgnoreCase(" "))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		
		

		List<LookupMst> lookupMst = (List<LookupMst>) lookupMstDao.findByNamedQueryAndNamedParams("lookupMst.getAllByTenant",queryParams);
		if (lookupMst == null || lookupMst.size() == 0) {
			return null;
		}
		
		Collections.sort(lookupMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final LookupMst bal1 = (LookupMst) o1;
				final LookupMst bal2 = (LookupMst) o2;

				int c;
				
				c=bal1.getLookupType().compareTo(bal2.getLookupType());
				if(c==0)
				{
					c=bal1.getDisplayValue().compareTo(bal2.getDisplayValue());
				}
				
				return c;
			}
		});

		
		return lookupMst;
	}

	@Override
	public LookupMst getLookupByLookupTypeAndValue(String tenantId, String LookupType,String lookupValue) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase("")||tenantId.equalsIgnoreCase(" "))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("lookupType", LookupType);
		queryParams.put("lookupValue", lookupValue);

		List<LookupMst> lookupMst = (List<LookupMst>) lookupMstDao.findByNamedQueryAndNamedParams("lookupMst.getLookupByLookupTypeAndValue",queryParams);
		if (lookupMst == null || lookupMst.size() == 0) {
			return null;
		}
		return lookupMst.get(0);
	}

	@Override
	public LookupMst getByName(String tenantId, String name) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
