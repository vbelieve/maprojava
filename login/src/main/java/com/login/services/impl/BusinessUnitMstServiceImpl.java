package com.login.services.impl;

import com.login.domain.basics.AccessObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.BusinessUnitMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.BusinessUnitMst;
import com.login.domain.OrganizationUnitMst;
import com.login.services.BusinessUnitMstService;
import com.login.services.SequenceMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("businessUnitMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class BusinessUnitMstServiceImpl extends CoreServiceImpl<Long, BusinessUnitMst> implements BusinessUnitMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	BusinessUnitMstDao businessUnitMstDao;

	@Autowired
	SequenceMstService sequenceMstService;

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) businessUnitMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		businessUnitMstDao.setEntityManager(entityManager);
	}


	public String delete(BusinessUnitMst entity,AccessObject accessObject)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		BusinessUnitMst businessUnitMst = getUnique(entity.getTenantId(),entity.getBusinessUnitCode(),entity.getProcess());
		if (businessUnitMst == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = businessUnitMst.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	
	
	public BusinessUnitMst saveOrUpDate(BusinessUnitMst entity,AccessObject accessObject)
			throws BaseException {
		String businessUnitSeqNo="0";
		try {
			businessUnitSeqNo=sequenceMstService.getNextSeqNo(ApplicationConstants.DEFAULT_TENANT, ApplicationConstants.SEQ_CATEGORYSEQ);
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		if(null!=entity.getBusinessUnitCode() || !entity.getBusinessUnitCode().equalsIgnoreCase(""))
		{
			
		}
		else
		{
			entity.setBusinessUnitCode(businessUnitSeqNo);
		}
				if(null==entity.getTenantId())
		{
			
			entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);	
		}
		
		String loginId=accessObject.getLoginId();
		BusinessUnitMst businessUnitMst = getUnique(entity.getTenantId(),entity.getBusinessUnitCode(),entity.getProcess());
		if (businessUnitMst == null) {
			entity.setBusinessUnitCode(businessUnitSeqNo);
			entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = businessUnitMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, businessUnitMst);
			} catch (Exception e) {
				
			}
			businessUnitMst.setId(id);
			if(null==businessUnitMst.getAuthStatus())
			{
				businessUnitMst.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
			}
			if(null==businessUnitMst.getIsActive())
			{
				businessUnitMst.setIsActive(1);
			}
			
			return super.saveOrUpdate(loginId, businessUnitMst);
		}
		
	}

	

	
	
	public BusinessUnitMst getUnique(String tenantId,String businessUnitCode,String process) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("businessUnitCode", businessUnitCode);
		
		List<BusinessUnitMst> businessUnitMst = (List<BusinessUnitMst>) businessUnitMstDao.findByNamedQueryAndNamedParams("businessUnitMst.getUnique",queryParams);
		if (businessUnitMst == null || businessUnitMst.size() == 0) {
			return null;
		}
		return businessUnitMst.get(0);
	}	


	
	
	public BusinessUnitMst getByName(String tenantId,String name) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("businessUnitName", name);

		List<BusinessUnitMst> businessUnitMst = (List<BusinessUnitMst>) businessUnitMstDao.findByNamedQueryAndNamedParams("businessUnitMst.getByName",queryParams);
		if (businessUnitMst == null || businessUnitMst.size() == 0) {
			return null;
		}
		return businessUnitMst.get(0);
	}

	public List<BusinessUnitMst> getAllActiveBusinessUnit()
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		List<BusinessUnitMst> businessUnitMst = (List<BusinessUnitMst>) businessUnitMstDao.findByNamedQueryAndNamedParams("businessUnitMst.getAllActiveBusinessUnit",queryParams);
		if (businessUnitMst == null || businessUnitMst.size() == 0) {
			return null;
		}
		return businessUnitMst;
		
		
	}
	
	public List<BusinessUnitMst> getAllBusinessUnit(String tenantId)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		List<BusinessUnitMst> businessUnitMst = (List<BusinessUnitMst>) businessUnitMstDao.findByNamedQueryAndNamedParams("businessUnitMst.getAll",queryParams);
		if (businessUnitMst == null || businessUnitMst.size() == 0) {
			return null;
		}
		return businessUnitMst;
		
		
	}
	
	public List<BusinessUnitMst> getByTenantAndProcess(String tenantId,String process)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("process", process);
		List<BusinessUnitMst> businessUnitMst = (List<BusinessUnitMst>) businessUnitMstDao.findByNamedQueryAndNamedParams("businessUnitMst.getByTenantAndProcess",queryParams);
		if (businessUnitMst == null || businessUnitMst.size() == 0) {
			return null;
		}
		return businessUnitMst;
		
		
	}

}