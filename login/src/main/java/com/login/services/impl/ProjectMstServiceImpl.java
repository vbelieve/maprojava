package com.login.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.ProjectMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.OrganizationUnitMst;
import com.login.domain.ProjectMst;
import com.login.domain.basics.AccessObject;
import com.login.services.CreateOrgGapDetailsService;
import com.login.services.ProjectMstService;
import com.login.services.SequenceMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("projectMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class ProjectMstServiceImpl extends CoreServiceImpl<Long, ProjectMst> implements ProjectMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	ProjectMstDao projectMstDao;

	@Autowired
	SequenceMstService sequenceMstService;

	@Autowired
	CreateOrgGapDetailsService createOrgGapDetailsService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) projectMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		projectMstDao.setEntityManager(entityManager);
	}

	
	public String delete(ProjectMst entity,AccessObject accessObject)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		ProjectMst projectMst = getUnique(entity.getTenantId(),entity.getProjectCode(),entity.getProcess(),entity.getBusinessUnitCode());
		if (projectMst == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = projectMst.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	

	
	public ProjectMst saveOrUpDate(ProjectMst entity,AccessObject accessObject)
			throws BaseException {
		String projectSeqNo="0";
		try {
			projectSeqNo=sequenceMstService.getNextSeqNo(ApplicationConstants.DEFAULT_TENANT, ApplicationConstants.SEQ_CATEGORYSEQ);
		} catch (Exception e) {

			e.printStackTrace();
		}
		if(null!=entity.getProjectCode() || !entity.getProjectCode().equalsIgnoreCase(""))
		{
			
		}
		else
		{
			entity.setProjectCode(projectSeqNo);
		}
		
		if(null==entity.getTenantId())
		{
			entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);	
		}
		
		String loginId=accessObject.getLoginId();
		ProjectMst projectMst = getUnique(entity.getTenantId(),entity.getProjectCode(),entity.getProcess(),entity.getBusinessUnitCode());
		if (projectMst == null) {
			entity.setProjectCode(projectSeqNo);
			entity.setIsActive(1);
			createOrgGapDetailsService.newProjectMigration(
					entity.getTenantId(), 
					entity.getOrganizationUnit(),
					entity.getBusinessUnit(),
					entity.getProjectCode(),accessObject);
			super.saveOrUpdate(loginId, entity);
			return entity;
		} else {
			Long id = projectMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, projectMst);
			} catch (Exception e) {
				
			}
			projectMst.setId(id);
			if(null!=projectMst.getIsActive())
			{
				
			}
			else
			{
				projectMst.setIsActive(1);
			}
			
			return super.saveOrUpdate(loginId, projectMst);
		}
		
	}

	

	
	
	public ProjectMst getUnique(String tenantId,String projectCode,String process,String businessUnitCode) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("projectCode", projectCode);
		queryParams.put("businessUnitCode", businessUnitCode);
		
		List<ProjectMst> projectMst = (List<ProjectMst>) projectMstDao.findByNamedQueryAndNamedParams("projectMst.getUnique",queryParams);
		if (projectMst == null || projectMst.size() == 0) {
			return null;
		}
		return projectMst.get(0);
	}	


	
	
	public ProjectMst getByName(String tenantId,String name) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("projectName", name);

		List<ProjectMst> projectMst = (List<ProjectMst>) projectMstDao.findByNamedQueryAndNamedParams("projectMst.getByName",queryParams);
		if (projectMst == null || projectMst.size() == 0) {
			return null;
		}
		return projectMst.get(0);
	}

	public List<ProjectMst> getAllProject(String tenantId)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		List<ProjectMst> projectMst = (List<ProjectMst>) projectMstDao.findByNamedQueryAndNamedParams("projectMst.getAll",queryParams);
		if (projectMst == null || projectMst.size() == 0) {
			return null;
		}
		return projectMst;
		
		
	}
	
	public List<ProjectMst> getAllActiveProjects()
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		
		List<ProjectMst> projectMst = (List<ProjectMst>) projectMstDao.findByNamedQueryAndNamedParams("projectMst.getAllActiveProjects",queryParams);
		if (projectMst == null || projectMst.size() == 0) {
			return null;
		}
		return projectMst;
		
		
	}
	
	public List<ProjectMst> getByTenantAndProcess(String tenantId,String process)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("process", process);
		List<ProjectMst> projectMst = (List<ProjectMst>) projectMstDao.findByNamedQueryAndNamedParams("projectMst.getByTenantAndProcess",queryParams);
		if (projectMst == null || projectMst.size() == 0) {
			return null;
		}
		return projectMst;
		
		
	}
	
	public ProjectMst getProjectByProjectCode(String tenantId,String projectCode)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("projectCode", projectCode);
		List<ProjectMst> projectMst = (List<ProjectMst>) projectMstDao.findByNamedQueryAndNamedParams("projectMst.getProjectByProjectCode",queryParams);
		if (projectMst == null || projectMst.size()==0) {
			return null;
		}
		return projectMst.get(0);
		
		
	}

}