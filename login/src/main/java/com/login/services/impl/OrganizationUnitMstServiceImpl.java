package com.login.services.impl;

import com.login.domain.basics.AccessObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.OrganizationUnitMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.CategoryMst;
import com.login.domain.OrganizationUnitMst;
import com.login.services.OrganizationUnitMstService;
import com.login.services.SequenceMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("organizationUnitMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class OrganizationUnitMstServiceImpl extends CoreServiceImpl<Long, OrganizationUnitMst> implements OrganizationUnitMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	OrganizationUnitMstDao organizationUnitMstDao;

	@Autowired
	SequenceMstService sequenceMstService;

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) organizationUnitMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		organizationUnitMstDao.setEntityManager(entityManager);
	}

	public String delete(OrganizationUnitMst entity,AccessObject accessObject)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		OrganizationUnitMst organizationUnitMst = getUnique(entity.getTenantId(),entity.getOrganizationUnitCode(),entity.getProcess());
		if (organizationUnitMst == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = organizationUnitMst.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	
	
	
	public OrganizationUnitMst saveOrUpDate(OrganizationUnitMst entity,AccessObject accessObject)
			throws BaseException {
		String organizationUnitSeqNo="0";
		try {
			organizationUnitSeqNo=sequenceMstService.getNextSeqNo(ApplicationConstants.DEFAULT_TENANT, ApplicationConstants.SEQ_CATEGORYSEQ);
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		if(null!=entity.getOrganizationUnitCode() || !entity.getOrganizationUnitCode().equalsIgnoreCase(""))
		{
			
		}
		else
		{
			entity.setOrganizationUnitCode(organizationUnitSeqNo);
		}
				if(null==entity.getTenantId())
		{
			
			entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);	
		}
		
		String loginId=accessObject.getLoginId();
		OrganizationUnitMst organizationUnitMst = getUnique(entity.getTenantId(),entity.getOrganizationUnitCode(),entity.getProcess());
		if (organizationUnitMst == null) {
			entity.setOrganizationUnitCode(organizationUnitSeqNo);
			entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = organizationUnitMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, organizationUnitMst);
			} catch (Exception e) {
				
			}
			organizationUnitMst.setId(id);
			if(null==organizationUnitMst.getAuthStatus())
			{
				organizationUnitMst.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
			}
			if(null==organizationUnitMst.getIsActive())
			{
				organizationUnitMst.setIsActive(1);
			}
			
			return super.saveOrUpdate(loginId, organizationUnitMst);
		}
		
	}

	

	
	
	public OrganizationUnitMst getUnique(String tenantId,String organizationUnitCode,String process) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("organizationUnitCode", organizationUnitCode);
		
		List<OrganizationUnitMst> organizationUnitMst = (List<OrganizationUnitMst>) organizationUnitMstDao.findByNamedQueryAndNamedParams("organizationUnitMst.getUnique",queryParams);
		if (organizationUnitMst == null || organizationUnitMst.size() == 0) {
			return null;
		}
		return organizationUnitMst.get(0);
	}	


	
	
	public OrganizationUnitMst getByName(String tenantId,String name) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("organizationUnitName", name);

		List<OrganizationUnitMst> organizationUnitMst = (List<OrganizationUnitMst>) organizationUnitMstDao.findByNamedQueryAndNamedParams("organizationUnitMst.getByName",queryParams);
		if (organizationUnitMst == null || organizationUnitMst.size() == 0) {
			return null;
		}
		return organizationUnitMst.get(0);
	}

	public List<OrganizationUnitMst> getAllActiveOrganizationUnit()
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		List<OrganizationUnitMst> organizationUnitMst = (List<OrganizationUnitMst>) organizationUnitMstDao.findByNamedQueryAndNamedParams("organizationUnitMst.getAllActiveOrganizationUnit",queryParams);
		if (organizationUnitMst == null || organizationUnitMst.size() == 0) {
			return null;
		}
		return organizationUnitMst;
		
		
	}
	
	public List<OrganizationUnitMst> getAllOrganizationUnit(String tenantId)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		List<OrganizationUnitMst> organizationUnitMst = (List<OrganizationUnitMst>) organizationUnitMstDao.findByNamedQueryAndNamedParams("organizationUnitMst.getAll",queryParams);
		if (organizationUnitMst == null || organizationUnitMst.size() == 0) {
			return null;
		}
		return organizationUnitMst;
		
		
	}
	
	public List<OrganizationUnitMst> getByTenantAndProcess(String tenantId,String process)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("process", process);
		List<OrganizationUnitMst> organizationUnitMst = (List<OrganizationUnitMst>) organizationUnitMstDao.findByNamedQueryAndNamedParams("organizationUnitMst.getByTenantAndProcess",queryParams);
		if (organizationUnitMst == null || organizationUnitMst.size() == 0) {
			return null;
		}
		return organizationUnitMst;
		
		
	}

}