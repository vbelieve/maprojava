package com.login.services.impl;


import com.login.services.SequenceMstService;
import com.login.services.WorkProductMstService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.PracticeQAMstDao;
import com.login.dao.QuestionMstDao;
import com.login.dao.WorkProductMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.PracticeQAMst;
import com.login.domain.QuestionMst;
import com.login.domain.SequenceMst;
import com.login.domain.WorkProductMst;
import com.login.services.PracticeQAMstService;
import com.login.services.QuestionMstService;
import com.login.services.basic.CoreServiceImpl;

@Service("practiceQAMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class PracticeQAMstServiceImpl extends CoreServiceImpl<Long, PracticeQAMst> implements PracticeQAMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	PracticeQAMstDao practiceQAMstDao;

	@Autowired
	SequenceMstService sequenceMstService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) practiceQAMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		practiceQAMstDao.setEntityManager(entityManager);
	}

	
	public PracticeQAMst saveOrUpDate(PracticeQAMst entity)
			throws BaseException {
		
//		String seq="";
//		try {
//			seq=sequenceMstService.getNextSeqNo(ApplicationConstants.DEFAULT_TENANT,
//					ApplicationConstants.SEQ_WORKPRODUCTSEQ);
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		String loginId=entity.getCreatedBy();
		if(entity.getTenantId().equalsIgnoreCase(""))
		{
			entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);	
		}
		
//		entity.setWorkProductId(seq);
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		entity.setIsActive(1);
		entity.setCreatedDate(new Date());
		PracticeQAMst practiceQAMst = getUnique(entity.getTenantId(),entity.getPracticeQAId(),entity.getProcess());
		if (practiceQAMst == null) {
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = practiceQAMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, practiceQAMst);
			} catch (Exception e) {
				
			}
			practiceQAMst.setId(id);
			practiceQAMst.setIsActive(1);
			return super.saveOrUpdate(loginId, practiceQAMst);
		}
		
	}

	

	
	
	


	public PracticeQAMst getUnique(String tenantId,String practiceQAId,String process) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("practiceQAId", practiceQAId);
		queryParams.put("process", process);
		
		
		

		List<PracticeQAMst> practiceQAMst = (List<PracticeQAMst>) practiceQAMstDao.findByNamedQueryAndNamedParams("practiceQAMst.getUnique",queryParams);
		if (practiceQAMst == null || practiceQAMst.size() == 0) {
			return null;
		}
		return practiceQAMst.get(0);
	}

	
	@Override
	public List<PracticeQAMst> getAll(String tenantId) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		
		

		List<PracticeQAMst> practiceQAMst = (List<PracticeQAMst>) practiceQAMstDao.findByNamedQueryAndNamedParams("practiceQAMst.getAll",queryParams);
		if (practiceQAMst == null || practiceQAMst.size() == 0) {
			return null;
		}
		return practiceQAMst;


	}
	



		


}