package com.login.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.PracticeMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.ProjectMst;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.RelationMappingPracticeAreaPractice;
import com.login.domain.TenantMst;
import com.login.domain.basics.AccessObject;
import com.login.domain.basics.DashBoardVo;
import com.login.services.PracticeMstService;
import com.login.services.ProjectMstService;
import com.login.services.TenantMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("practiceMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class PracticeMstServiceImpl extends CoreServiceImpl<Long, PracticeMst> implements PracticeMstService{

	@PersistenceContext
	EntityManager em;
	

	
	@Autowired
	PracticeMstDao practiceMstDao;
	
	@Autowired
	TenantMstService tenantMstService;
	
	@Autowired
	ProjectMstService projectMstService;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) practiceMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		practiceMstDao.setEntityManager(entityManager);
	}

	
	public String delete(PracticeMst entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		PracticeMst practiceMst = getUnique(entity.getTenantId(),entity.getPracticeCode(),
				entity.getProcess(),entity.getOrganizationUnit(),entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (practiceMst == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = practiceMst.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	
	public PracticeMst saveOrUpDate(PracticeMst entity,AccessObject accessObject)
			throws BaseException {
		String loginId=accessObject.getLoginId();
		if(entity.getTenantId()=="")
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		
		entity.setCreatedDate(new Date());
		PracticeMst practiceMst = getUnique(entity.getTenantId(),entity.getPracticeCode(),entity.getProcess(),
				entity.getOrganizationUnit(),entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (practiceMst == null) {
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = practiceMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, practiceMst);
			} catch (Exception e) {
				
			}
			practiceMst.setId(id);
			
			return super.saveOrUpdate(loginId, practiceMst);
		}
		
	}

	
public List<DashBoardVo> getPracticeComplianceByTenant()
{
	List<DashBoardVo> dashBoardVoList=new ArrayList<DashBoardVo>();
	
	 System.out.println("-- Employee MAX Salary --");
	 Query query = em.createQuery(
			 "SELECT (sum(b.practiceCompliance)*100/(count(a.practiceCode))),sum(b.practiceCompliance),count(a.practiceCode),t.organizationName,a.practiceAreaCode,a.tenantId,a.process FROM RelationMappingPracticeAreaPractice a , PracticeMst b ,TenantMst t WHERE a.tenantId=b.tenantId AND a.practiceCode=b.practiceCode AND a.process=b.process AND a.tenantId=t.tenantId GROUP BY a.tenantId,a.process,a.practiceAreaCode,t.organizationName");
	 List<Object[]> results = query.getResultList();
	 for (Object[] result : results) {
		 DashBoardVo dashBoardVo=new DashBoardVo();
		 dashBoardVo.setTenantProcessPracticeAreaPercent(result[0]+"");
		 dashBoardVo.setTenantProcessPracticeAreaSum(result[1]+"");
		 dashBoardVo.setTenantProcessPracticeAreaPracticeCount(result[2]+"");
		 dashBoardVo.setTenantName(result[3]+"");
		 dashBoardVo.setTenantProcessPracticeAreaCode(result[4]+"");
		 dashBoardVo.setTenantId(result[5]+"");
		 dashBoardVo.setProcess(result[6]+"");
		 dashBoardVoList.add(dashBoardVo);
	 }
     
     em.close();
	return dashBoardVoList;
}
	

public Double getPracticeComplianceByTenantAndProcess(String tenantId,String cmmiVersion)
{
	 
	 Query query = em.createQuery("SELECT (sum(practiceCompliance)*100/(count(*))) FROM PracticeMst where tenantId=:tenantId");
	 query.setParameter("tenantId", tenantId);
	 Double result = (Double) query.getSingleResult();

     em.close();
	return result;
}


	


	public PracticeMst getUnique(String tenantId,String practiceCode,String process,String organizationUnit,String businessUnit,
			String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("practiceCode", practiceCode);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		

		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getUnique",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		return practiceMst.get(0);
	}	

	public List<PracticeMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion",cmmiVersion);
		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getByTenantAndProject",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeMst bal1 = (PracticeMst) o1;
				final PracticeMst bal2 = (PracticeMst) o2;

				int c;
				
				c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				
				
				return c;
			}
		});
		
		return practiceMst;
	}

	public PracticeMst getByTenantProjectAndPractice(String tenantId,String project,String practiceCode,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("practiceCode", practiceCode);
		queryParams.put("cmmiVersion", cmmiVersion);
		

		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getByTenantProjectAndPractice",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}		
		return practiceMst.get(0);
	}
	
	public PracticeMst getByName(String tenantId,String name,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("capabilityName", name);
		queryParams.put("cmmiVersion", cmmiVersion);
		

		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getByName",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		return practiceMst.get(0);
	}
	
	
	public List<PracticeMst> getByCategory(String tenantId,Integer categoryId,String cmmiVersion)
	{
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("categoryId", categoryId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getByCategory",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeMst bal1 = (PracticeMst) o1;
				final PracticeMst bal2 = (PracticeMst) o2;

				int c;
				
				c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				
				
				return c;
			}
		});
		return practiceMst;
		
	}

	public List<PracticeMst> getAll(String tenantId,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getAll",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeMst bal1 = (PracticeMst) o1;
				final PracticeMst bal2 = (PracticeMst) o2;

				int c;
				
				c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				
				
				return c;
			}
		});
		return practiceMst;
		
		
	}
	
	public List<PracticeMst> getPracticeByTenantOrganization(String tenantId,String organizationUnit,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getPracticeByTenantOrganization",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeMst bal1 = (PracticeMst) o1;
				final PracticeMst bal2 = (PracticeMst) o2;

				int c;
				
				c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				
				
				return c;
			}
		});
		return practiceMst;
		
		
	}
	
	
	public List<PracticeMst> getPracticeByTenantOrganizationBusinesss(String tenantId,String organizationUnit,String businessUnit,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getPracticeByTenantOrganizationBusinesss",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeMst bal1 = (PracticeMst) o1;
				final PracticeMst bal2 = (PracticeMst) o2;

				int c;
				
				c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				
				
				return c;
			}
		});
		return practiceMst;
		
		
	}
	
	public List<PracticeMst> getPracticeByTenantOrganizationBusinesssProject(String tenantId,
			String organizationUnit,String businessUnit,String project,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
//		queryParams.put("tenantId", tenantId);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getPracticeByTenantOrganizationBusinesssProject",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeMst bal1 = (PracticeMst) o1;
				final PracticeMst bal2 = (PracticeMst) o2;

				int c;
				
				c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				
				
				return c;
			}
		});
		return practiceMst;
	}
	

	public List<PracticeMst> getPracticeByTenantProject(String tenantId,String project,AccessObject accessObject,String cmmiVersion)
	{
		TenantMst tenantMst=new TenantMst();
		ProjectMst projectMst=new ProjectMst();
		tenantMst=tenantMstService.getTenant(tenantId);
		projectMst=projectMstService.getProjectByProjectCode(tenantId, project);
		Integer practiceLevelCode=tenantMst.getPracticeLevel();
		String businessModel=projectMst.getOrganizationModel();
		
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", accessObject.getCmmiVersion());
		List<PracticeMst> practiceMstReturn = new ArrayList<PracticeMst>();
		
		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getPracticeByTenantProject",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		
		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea=new ArrayList<RelationMappingCapabilityPracticeArea>();
		relationMappingCapabilityPracticeArea=accessObject.getRelationMappingCapabilityPracticeArea();
		
		List<RelationMappingCategoryCapability> relationMappingCategoryCapability=new ArrayList<RelationMappingCategoryCapability>();
		relationMappingCategoryCapability=accessObject.getRelationMappingCategoryCapability();
		
		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice=new ArrayList<RelationMappingPracticeAreaPractice>();
		relationMappingPracticeAreaPractice=accessObject.getRelationMappingPracticeAreaPractice();
		
		List<PracticeAreaMst> practiceAreaMst=accessObject.getPracticeAreaMst();
		
		for(PracticeMst practice:practiceMst)
		{
			Boolean skiprecord=Boolean.TRUE;
			
			if(practice.getPracticeLevelCode()>practiceLevelCode)
			{
				continue;
			}
			
			for(RelationMappingPracticeAreaPractice relationMappingPracticeAreaPractices:relationMappingPracticeAreaPractice)
			{
				if(practice.getPracticeCode().equalsIgnoreCase(relationMappingPracticeAreaPractices.getPracticeCode()))
				{
					practice.setPracticeAreaCode(relationMappingPracticeAreaPractices.getPracticeAreaCode());
					for(PracticeAreaMst practiceArea:practiceAreaMst)
					{
						if(practiceArea.getPracticeAreaCode().equalsIgnoreCase(practice.getPracticeAreaCode()))
						{
							practice.setPracticeAreaName(practiceArea.getPracticeAreaName());
							practice.setDescription(practiceArea.getPracticeAreaIntent());
							if(practiceArea.getBusinessModel().contains(businessModel))
							{
								skiprecord=Boolean.FALSE;
								break;
							}
						}
					}
					break;
				}
			}
			
			if(skiprecord)
			{
				continue;
			}
			
			for(RelationMappingCapabilityPracticeArea relationMappingCapabilityPracticeAreas:relationMappingCapabilityPracticeArea)
			{
				if(practice.getPracticeAreaCode().equalsIgnoreCase(relationMappingCapabilityPracticeAreas.getPracticeAreaCode()))
				{
					practice.setCapabilityCode(relationMappingCapabilityPracticeAreas.getCapabilityCode());
					for(CapabilityMst capa:accessObject.getCapabilityMst())
					{
						if(capa.getCapabilityCode().contentEquals(practice.getCapabilityCode()))
						{
							practice.setCapabilityName(capa.getCapabilityName());
						}
					}
					break;
				}
			}
			
			for(RelationMappingCategoryCapability relationMappingCategoryCapabilities:relationMappingCategoryCapability)
			{
				if(practice.getCapabilityCode().equalsIgnoreCase(relationMappingCategoryCapabilities.getCapabilityCode()))
				{
					practice.setCategoryCode(relationMappingCategoryCapabilities.getCategoryCode());
					for(CategoryMst cate:accessObject.getCategoryMst())
					{
						if(cate.getCategoryCode().equalsIgnoreCase(practice.getCategoryCode()))
						{
							practice.setCategoryName(cate.getCategoryName());
						}
					}
					break;
				}
			}
			
			practiceMstReturn.add(practice);
		}
		
		Collections.sort(practiceMstReturn, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeMst bal1 = (PracticeMst) o1;
				final PracticeMst bal2 = (PracticeMst) o2;

				int c;
				
				c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				
				
				return c;
			}
		});
		return practiceMstReturn;
	}
	
	
	public List<PracticeMst> getAllActive()
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getAllActive",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeMst bal1 = (PracticeMst) o1;
				final PracticeMst bal2 = (PracticeMst) o2;

				int c;
				
				c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				
				
				return c;
			}
		});
		return practiceMst;
		
		
	}
	
	public List<PracticeMst> getByTenantAndProcess(String tenantId,String process,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("process", process);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getByTenantAndProcess",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeMst bal1 = (PracticeMst) o1;
				final PracticeMst bal2 = (PracticeMst) o2;

				int c;
				
				c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				
				
				return c;
			}
		});
		return practiceMst;
		
		
	}

	@Override
	public List<PracticeMst> getByCapability(String tenantId, Integer capabilityId,String cmmiVersion) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("capabilityId", capabilityId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getByCapability",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeMst bal1 = (PracticeMst) o1;
				final PracticeMst bal2 = (PracticeMst) o2;

				int c;
				
				c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				
				
				return c;
			}
		});
		return practiceMst;

	
	}

	@Override
	public List<PracticeMst> getByCategoryAndCapability(String tenantId, Integer categoryId, Integer capabilityId,String cmmiVersion) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("categoryId", categoryId);
		queryParams.put("capabilityId", capabilityId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<PracticeMst> practiceMst = (List<PracticeMst>) practiceMstDao.findByNamedQueryAndNamedParams("practiceMst.getByCategoryAndCapability",queryParams);
		if (practiceMst == null || practiceMst.size() == 0) {
			return null;
		}
		Collections.sort(practiceMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final PracticeMst bal1 = (PracticeMst) o1;
				final PracticeMst bal2 = (PracticeMst) o2;

				int c;
				
				c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				
				
				return c;
			}
		});
		return practiceMst;

	}



}