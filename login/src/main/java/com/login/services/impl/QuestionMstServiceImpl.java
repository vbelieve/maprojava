package com.login.services.impl;


import com.login.services.SequenceMstService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.QuestionMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.QuestionMst;
import com.login.domain.SequenceMst;
import com.login.services.QuestionMstService;
import com.login.services.basic.CoreServiceImpl;

@Service("questionMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class QuestionMstServiceImpl extends CoreServiceImpl<Long, QuestionMst> implements QuestionMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	QuestionMstDao questionMstDao;

	@Autowired
	SequenceMstService sequenceMstService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) questionMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		questionMstDao.setEntityManager(entityManager);
	}

	
	public QuestionMst saveOrUpDate(QuestionMst entity)
			throws BaseException {
		
		String loginId=entity.getCreatedBy();
		if(entity.getTenantId().equalsIgnoreCase(""))
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		String seqNo="";
		if(null==entity.getQuestionId() || entity.getQuestionId().equalsIgnoreCase(""))
		{
		try {
			 seqNo=sequenceMstService.getNextSeqNo(entity.getTenantId(), ApplicationConstants.SEQ_QUESTIONMST);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		entity.setQuestionId(seqNo);
		}
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		entity.setIsActive(1);
		entity.setCreatedDate(new Date());
		QuestionMst questionMst = getUnique(entity.getTenantId(),entity.getQuestionId(),entity.getProcess());
		if (questionMst == null) {
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = questionMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, questionMst);
			} catch (Exception e) {
				
			}
			questionMst.setId(id);
			questionMst.setIsActive(1);
			return super.saveOrUpdate(loginId, questionMst);
		}
		
	}

	

	
	
	public QuestionMst getByQuestionId(String tenantId,String questionId) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("questionId", questionId);
		
		

		List<QuestionMst> questionMst = (List<QuestionMst>) questionMstDao.findByNamedQueryAndNamedParams("QuestionMst.getByQuestionId",queryParams);
		if (questionMst == null || questionMst.size() == 0) {
			return null;
		}
		return questionMst.get(0);
	}

	


	public QuestionMst getUnique(String tenantId,String questionId,String process) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("questionId", questionId);
		queryParams.put("process", process);
		

		List<QuestionMst> questionMst = (List<QuestionMst>) questionMstDao.findByNamedQueryAndNamedParams("QuestionMst.getUnique",queryParams);
		if (questionMst == null || questionMst.size() == 0) {
			return null;
		}
		return questionMst.get(0);
	}

	
	@Override
	public List<QuestionMst> getAll(String tenantId) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		
		

		List<QuestionMst> questionMst = (List<QuestionMst>) questionMstDao.findByNamedQueryAndNamedParams("QuestionMst.getAll",queryParams);
		if (questionMst == null || questionMst.size() == 0) {
			return null;
		}
		return questionMst;


	}
	



		


}