package com.login.services.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.PiidMstDao;
import com.login.dao.PracticeAreaMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.PiidMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.basics.AccessObject;
import com.login.services.PiidMstService;
import com.login.services.PracticeAreaMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("piidMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class PiidMstServiceImpl extends CoreServiceImpl<Long, PiidMst> implements PiidMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	PiidMstDao piidMstDao;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) piidMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		piidMstDao.setEntityManager(entityManager);
	}


	
	public PiidMst getUnique(String tenantId,String project, String practiceAreaCode, String practiceCode, String artifect)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("practiceAreaCode", practiceAreaCode);
		queryParams.put("practiceCode", practiceCode);
		queryParams.put("artifect", artifect);
		
		List<PiidMst> piidMst = (List<PiidMst>) piidMstDao.findByNamedQueryAndNamedParams("piidMst.getUnique",queryParams);
		if (piidMst == null || piidMst.size()==0) {
			return null;
		}
		return piidMst.get(0);
		
	}
	
	public PiidMst saveOrUpDate(PiidMst entity,AccessObject accessObject)
			throws BaseException {
		String loginId=accessObject.getLoginId();
		entity.setCreatedDate(new Date());
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		if(entity.getTenantId()==null)
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		entity.setVersion(0);
		
		PiidMst piidMst = getUnique(entity.getTenantId(),entity.getProject(),entity.getPracticeAreaCode(),entity.getPracticeCode(),entity.getArtifect());
		if (piidMst == null) {
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = piidMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, piidMst);
			} catch (Exception e) {
				
			}
			piidMst.setId(id);
			
			return super.saveOrUpdate(loginId, piidMst);
		}
		
	}

	public String delete(PiidMst entity,AccessObject accessObject)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		PiidMst piidMst = getUnique(entity.getTenantId(),entity.getProject(),entity.getPracticeAreaCode(),entity.getPracticeCode(),entity.getArtifect());
		if (piidMst == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = piidMst.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	
public List<PiidMst> getByProjectPracticeArea(String tenantId,String project,String practiceAreaCode)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("practiceAreaCode", practiceAreaCode);
		List<PiidMst> piidMst = (List<PiidMst>) piidMstDao.findByNamedQueryAndNamedParams("piidMst.getByProjectPracticeArea",queryParams);
		if (piidMst == null || piidMst.size()==0) {
			return null;
		}
		return piidMst;
		
		
	}

public List<PiidMst> getByTenantAndProject(String tenantId,String project)
{
	Map<String, String> queryParams = new HashMap<String, String>();
	queryParams.put("tenantId", tenantId);
	queryParams.put("project", project);

	List<PiidMst> piidMst = (List<PiidMst>) piidMstDao.findByNamedQueryAndNamedParams("piidMst.getByTenantAndProject",queryParams);
	if (piidMst == null || piidMst.size()==0) {
		return null;
	}
	return piidMst;
	
	
}

	
}