package com.login.services.impl;


import com.login.dao.RelationMappingPracticeAreaPracticeDao;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import com.login.domain.RelationMappingPracticeAreaPractice;
import com.login.domain.basics.AccessObject;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.services.RelationMappingPracticeAreaPracticeService;
import com.login.services.basic.CoreServiceImpl;

@Service("relationMappingPracticeAreaPracticeService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class RelationMappingPracticeAreaPracticeServiceImpl extends CoreServiceImpl<Long, RelationMappingPracticeAreaPractice> implements RelationMappingPracticeAreaPracticeService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	RelationMappingPracticeAreaPracticeDao relationMappingPracticeAreaPracticeDao;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) relationMappingPracticeAreaPracticeDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		relationMappingPracticeAreaPracticeDao.setEntityManager(entityManager);
	}

	
	public String delete(RelationMappingPracticeAreaPractice entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		RelationMappingPracticeAreaPractice relationMappingPracticeAreaPractice = getUnique(entity.getTenantId(),entity.getPracticeAreaCode(),
				entity.getPracticeCode(),entity.getPracticeLevelCode(),entity.getProcess(),entity.getOrganizationUnit(),
				entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (relationMappingPracticeAreaPractice == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = relationMappingPracticeAreaPractice.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	
	public RelationMappingPracticeAreaPractice saveOrUpDate(RelationMappingPracticeAreaPractice entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {
		String loginId=accessObject.getLoginId();
		if(entity.getTenantId().equalsIgnoreCase(""))
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		entity.setCreatedDate(new Date());
		RelationMappingPracticeAreaPractice relationMappingPracticeAreaPractice = getUnique(entity.getTenantId(),entity.getPracticeAreaCode(),
				entity.getPracticeCode(),entity.getPracticeLevelCode(),entity.getProcess(),
				entity.getOrganizationUnit(),entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (relationMappingPracticeAreaPractice == null) {
				return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = relationMappingPracticeAreaPractice.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, relationMappingPracticeAreaPractice);
			} catch (Exception e) {
				
			}
			relationMappingPracticeAreaPractice.setId(id);
			return super.saveOrUpdate(loginId, relationMappingPracticeAreaPractice);
		}
		
	}

	

	
	
	


	public RelationMappingPracticeAreaPractice getUnique(String tenantId,String practiceAreaCode,String practiceCode,
			String practiceLevelCode,String process,String organizationUnit,String businessUnit,String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("practiceAreaCode", practiceAreaCode);
		queryParams.put("practiceCode", practiceCode);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);

		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice = (List<RelationMappingPracticeAreaPractice>) relationMappingPracticeAreaPracticeDao.findByNamedQueryAndNamedParams("relationMappingPracticeAreaPractice.getUnique",queryParams);
		if (relationMappingPracticeAreaPractice == null || relationMappingPracticeAreaPractice.size() == 0) {
			return null;
		}
		return relationMappingPracticeAreaPractice.get(0);
	}

	
	@Override
	public List<RelationMappingPracticeAreaPractice> getAll(String tenantId,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("cmmiVersion", cmmiVersion);
		

		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice = (List<RelationMappingPracticeAreaPractice>) relationMappingPracticeAreaPracticeDao.findByNamedQueryAndNamedParams("relationMappingPracticeAreaPractice.getAll",queryParams);
		if (relationMappingPracticeAreaPractice == null || relationMappingPracticeAreaPractice.size() == 0) {
			return null;
		}
		
		Collections.sort(relationMappingPracticeAreaPractice, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingPracticeAreaPractice bal1 = (RelationMappingPracticeAreaPractice) o1;
				final RelationMappingPracticeAreaPractice bal2 = (RelationMappingPracticeAreaPractice) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				if(c==0)
				{
					c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				}
				
				
				return c;
			}
		});
		
		return relationMappingPracticeAreaPractice;


	}
	
	public List<RelationMappingPracticeAreaPractice> getByTenantAndProcess(String tenantId,String process,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("process", process);
		queryParams.put("cmmiVersion", cmmiVersion);
		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice = (List<RelationMappingPracticeAreaPractice>) relationMappingPracticeAreaPracticeDao.findByNamedQueryAndNamedParams("relationMappingPracticeAreaPractice.getByTenantAndProcess",queryParams);
		if (relationMappingPracticeAreaPractice == null || relationMappingPracticeAreaPractice.size() == 0) {
			return null;
		}

		Collections.sort(relationMappingPracticeAreaPractice, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingPracticeAreaPractice bal1 = (RelationMappingPracticeAreaPractice) o1;
				final RelationMappingPracticeAreaPractice bal2 = (RelationMappingPracticeAreaPractice) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				if(c==0)
				{
					c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				}
				
				
				return c;
			}
		});
		
		return relationMappingPracticeAreaPractice;
		
		
	}

	public List<RelationMappingPracticeAreaPractice> getByTenantAndProject(String tenantId,String project,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
//		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice = (List<RelationMappingPracticeAreaPractice>) relationMappingPracticeAreaPracticeDao.findByNamedQueryAndNamedParams("relationMappingPracticeAreaPractice.getByTenantAndProject",queryParams);
		if (relationMappingPracticeAreaPractice == null || relationMappingPracticeAreaPractice.size() == 0) {
			return null;
		}
		
		Collections.sort(relationMappingPracticeAreaPractice, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingPracticeAreaPractice bal1 = (RelationMappingPracticeAreaPractice) o1;
				final RelationMappingPracticeAreaPractice bal2 = (RelationMappingPracticeAreaPractice) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				if(c==0)
				{
					c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				}
				
				
				return c;
			}
		});
		
		return relationMappingPracticeAreaPractice;
		
		
	}
	
	@Override
	public List<RelationMappingPracticeAreaPractice> getByPractice(String tenantId, String practiceCode,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("practiceCode", practiceCode);
		queryParams.put("cmmiVersion", cmmiVersion);

		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice = (List<RelationMappingPracticeAreaPractice>) relationMappingPracticeAreaPracticeDao.findByNamedQueryAndNamedParams("relationMappingPracticeAreaPractice.getPractice",queryParams);
		if (relationMappingPracticeAreaPractice == null || relationMappingPracticeAreaPractice.size() == 0) {
			return null;
		}
		
		Collections.sort(relationMappingPracticeAreaPractice, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingPracticeAreaPractice bal1 = (RelationMappingPracticeAreaPractice) o1;
				final RelationMappingPracticeAreaPractice bal2 = (RelationMappingPracticeAreaPractice) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				if(c==0)
				{
					c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				}
				
				
				return c;
			}
		});
		return relationMappingPracticeAreaPractice;


	}

	
	public RelationMappingPracticeAreaPractice getByProjectAndPractice(String tenantId,String project,String practiceCode,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("practiceCode", practiceCode);
		queryParams.put("cmmiVersion", cmmiVersion);
		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice = (List<RelationMappingPracticeAreaPractice>) relationMappingPracticeAreaPracticeDao.findByNamedQueryAndNamedParams("relationMappingPracticeAreaPractice.getByProjectAndPractice",queryParams);
		if (relationMappingPracticeAreaPractice == null || relationMappingPracticeAreaPractice.size()==0) {
			return null;
		}
		return relationMappingPracticeAreaPractice.get(0);
	}

	public List<RelationMappingPracticeAreaPractice> getRelationMappingPracticeAreaPracticeListByTenantAndProject(
			String project,Integer practiceLevel,String businessModel,String cmmiVersion)
	{
		practiceLevel=practiceLevel+1;
		 Query query = em.createQuery("SELECT R FROM RelationMappingPracticeAreaPractice R where "
		 		+ "R.project=:project AND "
		 		+ "R.practiceLevel<:practiceLevel");
//		 		+ " AND "
//		 		+ "R.practiceCode in "
//		 		+ "(select distinct practiceAreaCode  FROM PracticeAreaMst p WHERE p.isActive=1 AND p.project=:project"
//		 		+ " AND p.businessModel like :businessModel)");
		 query.setParameter("project", project);
		 query.setParameter("practiceLevel", practiceLevel);
		 query.setParameter("cmmiVersion", cmmiVersion);
		 //query.setParameter("businessModel", businessModel);
		 
		 List<RelationMappingPracticeAreaPractice> result = (List<RelationMappingPracticeAreaPractice>) query.getResultList();

	     em.close();
		return result;
	}



	
	@Override
	public List<RelationMappingPracticeAreaPractice> getByPracticeArea(String tenantId, String practiceAreaCode,String organizationUnit,String businessUnit,
			String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("practiceAreaCode", practiceAreaCode);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);

		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice = (List<RelationMappingPracticeAreaPractice>) relationMappingPracticeAreaPracticeDao.findByNamedQueryAndNamedParams("relationMappingPracticeAreaPractice.getPracticeArea",queryParams);
		if (relationMappingPracticeAreaPractice == null || relationMappingPracticeAreaPractice.size() == 0) {
			return null;
		}
		Collections.sort(relationMappingPracticeAreaPractice, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingPracticeAreaPractice bal1 = (RelationMappingPracticeAreaPractice) o1;
				final RelationMappingPracticeAreaPractice bal2 = (RelationMappingPracticeAreaPractice) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				if(c==0)
				{
					c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				}
				
				
				return c;
			}
		});
		
		return relationMappingPracticeAreaPractice;
}	
	
	@Override
	public List<RelationMappingPracticeAreaPractice> getByPracticeLevel(String tenantId, String practiceLevelCode,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("practiceLevelCode", practiceLevelCode);
		queryParams.put("cmmiVersion", cmmiVersion);
		

		List<RelationMappingPracticeAreaPractice> relationMappingPracticeAreaPractice = (List<RelationMappingPracticeAreaPractice>) relationMappingPracticeAreaPracticeDao.findByNamedQueryAndNamedParams("relationMappingPracticeAreaPractice.getPracticeByLevel",queryParams);
		if (relationMappingPracticeAreaPractice == null || relationMappingPracticeAreaPractice.size() == 0) {
			return null;
		}
		
		Collections.sort(relationMappingPracticeAreaPractice, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingPracticeAreaPractice bal1 = (RelationMappingPracticeAreaPractice) o1;
				final RelationMappingPracticeAreaPractice bal2 = (RelationMappingPracticeAreaPractice) o2;

				int c;
				
				c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				if(c==0)
				{
					c=bal1.getPracticeCode().compareTo(bal2.getPracticeCode());
				}
				
				
				return c;
			}
		});
		return relationMappingPracticeAreaPractice;
}


}