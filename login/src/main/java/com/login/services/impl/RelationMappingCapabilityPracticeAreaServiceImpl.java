package com.login.services.impl;


import com.login.dao.RelationMappingCapabilityPracticeAreaDao;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.PracticeAreaMst;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.basics.AccessObject;
import com.login.services.RelationMappingCapabilityPracticeAreaService;
import com.login.services.basic.CoreServiceImpl;

@Service("relationMappingCapabilityPracticeAreaService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class RelationMappingCapabilityPracticeAreaServiceImpl extends CoreServiceImpl<Long, RelationMappingCapabilityPracticeArea> implements RelationMappingCapabilityPracticeAreaService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	RelationMappingCapabilityPracticeAreaDao relationMappingCapabilityPracticeAreaDao;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) relationMappingCapabilityPracticeAreaDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		relationMappingCapabilityPracticeAreaDao.setEntityManager(entityManager);
	}

	
	public String delete(RelationMappingCapabilityPracticeArea entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		RelationMappingCapabilityPracticeArea relationMappingCapabilityPracticeArea = getUnique(entity.getTenantId(),
				entity.getCapabilityCode(),entity.getPracticeAreaCode(),entity.getProcess(),
				entity.getOrganizationUnit(),entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (relationMappingCapabilityPracticeArea == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = relationMappingCapabilityPracticeArea.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	
	public RelationMappingCapabilityPracticeArea saveOrUpDate(RelationMappingCapabilityPracticeArea entity,
			AccessObject accessObject,String cmmiVersion)
			throws BaseException {
		String loginId=accessObject.getLoginId();
		if(entity.getTenantId().equalsIgnoreCase(""))
		{
			entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);	
		}
		RelationMappingCapabilityPracticeArea relationMappingCapabilityPracticeArea = getUnique(entity.getTenantId(),
				entity.getCapabilityCode(),entity.getPracticeAreaCode(),entity.getProcess(),
				entity.getOrganizationUnit(),entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (relationMappingCapabilityPracticeArea == null) {
			entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = relationMappingCapabilityPracticeArea.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, relationMappingCapabilityPracticeArea);
			} catch (Exception e) {
				
			}
			relationMappingCapabilityPracticeArea.setId(id);
			return super.saveOrUpdate(loginId, relationMappingCapabilityPracticeArea);
		}
		
	}

	

	
	
	


	public RelationMappingCapabilityPracticeArea getUnique(String tenantId,String capabilityCode,
			String practiceAreaCode,String process,String organizationUnit,String businessUnit,String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("capabilityCode", capabilityCode);
		queryParams.put("practiceAreaCode", practiceAreaCode);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
	

		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea = (List<RelationMappingCapabilityPracticeArea>) relationMappingCapabilityPracticeAreaDao.findByNamedQueryAndNamedParams("relationMappingCapabilityPracticeArea.getUnique",queryParams);
		if (relationMappingCapabilityPracticeArea == null || relationMappingCapabilityPracticeArea.size() == 0) {
			return null;
		}
		return relationMappingCapabilityPracticeArea.get(0);
	}

	
	@Override
	public List<RelationMappingCapabilityPracticeArea> getAll(String tenantId,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		
		

		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea = (List<RelationMappingCapabilityPracticeArea>) relationMappingCapabilityPracticeAreaDao.findByNamedQueryAndNamedParams("relationMappingCapabilityPracticeArea.getAll",queryParams);
		if (relationMappingCapabilityPracticeArea == null || relationMappingCapabilityPracticeArea.size() == 0) {
			return null;
		}
		Collections.sort(relationMappingCapabilityPracticeArea, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingCapabilityPracticeArea bal1 = (RelationMappingCapabilityPracticeArea) o1;
				final RelationMappingCapabilityPracticeArea bal2 = (RelationMappingCapabilityPracticeArea) o2;

				int c;
				
				c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				if(c==0)
				{
					c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				}
				
				
				return c;
			}
		});
		return relationMappingCapabilityPracticeArea;


	}
	
	public List<RelationMappingCapabilityPracticeArea> getByTenantAndProcess(String tenantId,String process,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("process", process);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea = (List<RelationMappingCapabilityPracticeArea>) relationMappingCapabilityPracticeAreaDao.findByNamedQueryAndNamedParams("relationMappingCapabilityPracticeArea.getByTenantAndProcess",queryParams);
		if (relationMappingCapabilityPracticeArea == null || relationMappingCapabilityPracticeArea.size() == 0) {
			return null;
		}
		Collections.sort(relationMappingCapabilityPracticeArea, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingCapabilityPracticeArea bal1 = (RelationMappingCapabilityPracticeArea) o1;
				final RelationMappingCapabilityPracticeArea bal2 = (RelationMappingCapabilityPracticeArea) o2;

				int c;
				
				c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				if(c==0)
				{
					c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				}
				
				
				return c;
			}
		});
		return relationMappingCapabilityPracticeArea;
		
		
	}

	public List<RelationMappingCapabilityPracticeArea> getByTenantAndProject(String tenantId,String project,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
//		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea = (List<RelationMappingCapabilityPracticeArea>) relationMappingCapabilityPracticeAreaDao.findByNamedQueryAndNamedParams("relationMappingCapabilityPracticeArea.getByTenantAndProject",queryParams);
		if (relationMappingCapabilityPracticeArea == null || relationMappingCapabilityPracticeArea.size() == 0) {
			return null;
		}
		Collections.sort(relationMappingCapabilityPracticeArea, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingCapabilityPracticeArea bal1 = (RelationMappingCapabilityPracticeArea) o1;
				final RelationMappingCapabilityPracticeArea bal2 = (RelationMappingCapabilityPracticeArea) o2;

				int c;
				
				c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				if(c==0)
				{
					c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				}
				
				
				return c;
			}
		});
		return relationMappingCapabilityPracticeArea;
		
		
	}
	
	@Override
	public List<RelationMappingCapabilityPracticeArea> getByCapability(String tenantId, String capabilityCode,
			String organizationUnit,String businessUnit,String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("capabilityCode", capabilityCode);
		queryParams.put("organizationUnit", organizationUnit);
		queryParams.put("businessUnit", businessUnit);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		

		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea = (List<RelationMappingCapabilityPracticeArea>) relationMappingCapabilityPracticeAreaDao.findByNamedQueryAndNamedParams("relationMappingCapabilityPracticeArea.getCapability",queryParams);
		if (relationMappingCapabilityPracticeArea == null || relationMappingCapabilityPracticeArea.size() == 0) {
			return null;
		}
		Collections.sort(relationMappingCapabilityPracticeArea, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingCapabilityPracticeArea bal1 = (RelationMappingCapabilityPracticeArea) o1;
				final RelationMappingCapabilityPracticeArea bal2 = (RelationMappingCapabilityPracticeArea) o2;

				int c;
				
				c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				if(c==0)
				{
					c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				}
				
				
				return c;
			}
		});
		return relationMappingCapabilityPracticeArea;


	}

	@Override
	public List<RelationMappingCapabilityPracticeArea> getByPracticeArea(String tenantId, String practiceAreaCode,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase(""))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("practiceAreaCode", practiceAreaCode);
		queryParams.put("cmmiVersion", cmmiVersion);
		

		List<RelationMappingCapabilityPracticeArea> relationMappingCapabilityPracticeArea = (List<RelationMappingCapabilityPracticeArea>) relationMappingCapabilityPracticeAreaDao.findByNamedQueryAndNamedParams("relationMappingCapabilityPracticeArea.getPracticeArea",queryParams);
		if (relationMappingCapabilityPracticeArea == null || relationMappingCapabilityPracticeArea.size() == 0) {
			return null;
		}
		Collections.sort(relationMappingCapabilityPracticeArea, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final RelationMappingCapabilityPracticeArea bal1 = (RelationMappingCapabilityPracticeArea) o1;
				final RelationMappingCapabilityPracticeArea bal2 = (RelationMappingCapabilityPracticeArea) o2;

				int c;
				
				c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				if(c==0)
				{
					c=bal1.getPracticeAreaCode().compareTo(bal2.getPracticeAreaCode());
				}
				
				
				return c;
			}
		});
		return relationMappingCapabilityPracticeArea;
}	
	

	
	
	


}