package com.login.services.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.CapabilityMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.basics.AccessObject;
import com.login.services.CapabilityMstService;
import com.login.services.SequenceMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("capabilityMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class CapabilityMstServiceImpl extends CoreServiceImpl<Long, CapabilityMst> implements CapabilityMstService{

	@PersistenceContext
	EntityManager em;
	
	@Autowired
	CapabilityMstDao capabilityMstDao;

	@Autowired
	SequenceMstService sequenceMstService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) capabilityMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		capabilityMstDao.setEntityManager(entityManager);
	}

	public String delete(CapabilityMst entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {

		String loginId=accessObject.getLoginId();
		CapabilityMst capabilityMst = getUnique(entity.getTenantId(),entity.getCapabilityCode(),entity.getProcess(),
				entity.getOrganizationUnit(),entity.getBusinessUnit(),entity.getProject(),
				entity.getCmmiVersion());
		if (capabilityMst == null) {
			return "ERROR~No Record Found";
		} else {
			Long id = capabilityMst.getId();
			super.delete(id);
			
			return ApplicationConstants.RESPONSE_SUCCESS;
		}
		
	}
	
	
	public CapabilityMst saveOrUpDate(CapabilityMst entity,AccessObject accessObject,String cmmiVersion)
			throws BaseException {
		String capabilitySeqNo="0";
		try {
			capabilitySeqNo=sequenceMstService.getNextSeqNo(ApplicationConstants.DEFAULT_TENANT, ApplicationConstants.SEQ_CAPABILITYSEQ);
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		
		if(entity.getTenantId()==null)
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		String loginId=accessObject.getLoginId();
		CapabilityMst capabilityMst = getUnique(entity.getTenantId(),entity.getCapabilityCode(),entity.getProcess(),
				entity.getOrganizationUnit(),entity.getBusinessUnit(),entity.getProject(),entity.getCmmiVersion());
		if (capabilityMst == null) {
			entity.setIsActive(1);
			super.saveOrUpdate(loginId, entity);
			return entity;
		} else {
			Long id = capabilityMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, capabilityMst);
			} catch (Exception e) {
				
			}
			capabilityMst.setId(id);
			
			return super.saveOrUpdate(loginId, capabilityMst);
		}
		
	}

	

	
	
	


	public CapabilityMst getUnique(String tenantId,String capabilityCode,String process,String organizationUnit,
			String businessUnit,String project,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase("")||tenantId.equalsIgnoreCase(" "))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", "0");
		queryParams.put("capabilityCode", capabilityCode);
		queryParams.put("cmmiVersion", cmmiVersion);
		

		List<CapabilityMst> capabilityMst = (List<CapabilityMst>) capabilityMstDao.findByNamedQueryAndNamedParams("capabilityMst.getUnique",queryParams);
		if (capabilityMst == null || capabilityMst.size() == 0) {
			return null;
		}
		return capabilityMst.get(0);
	}	
	
	public CapabilityMst getByName(String tenantId,String name,String cmmiVersion) {
		Map<String, String> queryParams = new HashMap<String, String>();
		if(tenantId.equalsIgnoreCase("")||tenantId.equalsIgnoreCase(" "))
		{
			tenantId=ApplicationConstants.DEFAULT_TENANT;
		}
		queryParams.put("tenantId", tenantId);
		queryParams.put("capabilityName", name);
		queryParams.put("cmmiVersion", cmmiVersion);
		List<CapabilityMst> capabilityMst = (List<CapabilityMst>) capabilityMstDao.findByNamedQueryAndNamedParams("capabilityMst.getByName",queryParams);
		if (capabilityMst == null || capabilityMst.size() == 0) {
			return null;
		}
		return capabilityMst.get(0);
	}
	
	
	public List<CapabilityMst> getByCategory(String tenantId,Integer categoryId,String cmmiVersion)
	{
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("categoryId", categoryId);
		queryParams.put("cmmiVersion", cmmiVersion);
		List<CapabilityMst> capabilityMst = (List<CapabilityMst>) capabilityMstDao.findByNamedQueryAndNamedParams("capabilityMst.getByCategory",queryParams);
		if (capabilityMst == null || capabilityMst.size() == 0) {
			return null;
		}
		
		Collections.sort(capabilityMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final CapabilityMst bal1 = (CapabilityMst) o1;
				final CapabilityMst bal2 = (CapabilityMst) o2;

				int c;
				
				c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				
				
				return c;
			}
		});
		return capabilityMst;
		
	}

	public List<CapabilityMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
//		queryParams.put("tenantId", tenantId);
		queryParams.put("project", project);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<CapabilityMst> capabilityMst = (List<CapabilityMst>) capabilityMstDao.findByNamedQueryAndNamedParams("capabilityMst.getByTenantAndProject",queryParams);
		if (capabilityMst == null || capabilityMst.size() == 0) {
			return null;
		}
		Collections.sort(capabilityMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final CapabilityMst bal1 = (CapabilityMst) o1;
				final CapabilityMst bal2 = (CapabilityMst) o2;

				int c;
				
				c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				
				
				return c;
			}
		});
		return capabilityMst;
		
		
	}
	
	public List<CapabilityMst> getAllCapability(String tenantId,String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("cmmiVersion", cmmiVersion);
		
		List<CapabilityMst> capabilityMst = (List<CapabilityMst>) capabilityMstDao.findByNamedQueryAndNamedParams("capabilityMst.getAll",
				queryParams);
		if (capabilityMst == null || capabilityMst.size() == 0) {
			return null;
		}
		Collections.sort(capabilityMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final CapabilityMst bal1 = (CapabilityMst) o1;
				final CapabilityMst bal2 = (CapabilityMst) o2;

				int c;
				
				c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				
				
				return c;
			}
		});
		return capabilityMst;
		
		
	}


	public List<CapabilityMst> getBase(String cmmiVersion)
	{
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", "0");
		queryParams.put("cmmiVersion", cmmiVersion);
//		queryParams.put("project", project);
		List<CapabilityMst> capabilityMst = (List<CapabilityMst>) capabilityMstDao.findByNamedQueryAndNamedParams("capabilityMst.getBase",queryParams);
		if (capabilityMst == null || capabilityMst.size() == 0) {
			return null;
		}
		Collections.sort(capabilityMst, new Comparator() {
			public int compare(final Object o1, final Object o2) {

				final CapabilityMst bal1 = (CapabilityMst) o1;
				final CapabilityMst bal2 = (CapabilityMst) o2;

				int c;
				
				c=bal1.getCapabilityCode().compareTo(bal2.getCapabilityCode());
				
				
				return c;
			}
		});
		return capabilityMst;
		
		
	}

}