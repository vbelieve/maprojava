package com.login.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.PracticeMstDao;
import com.login.dao.TestMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.ProjectMst;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.RelationMappingPracticeAreaPractice;
import com.login.domain.TenantMst;
import com.login.domain.TestMst;
import com.login.domain.basics.AccessObject;
import com.login.domain.basics.DashBoardVo;
import com.login.services.PracticeMstService;
import com.login.services.ProjectMstService;
import com.login.services.TenantMstService;
import com.login.services.TestMstService;
import com.login.services.basic.CoreServiceImpl;
@Service("testMstService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class TestMstServiceImpl extends CoreServiceImpl<Long, TestMst> implements TestMstService{

	@PersistenceContext
	EntityManager em;
	

	
	@Autowired
	TestMstDao testMstDao;
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) testMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	
	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		testMstDao.setEntityManager(entityManager);
	}

	
	@Override
		public List<TestMst> getAll(String tenantId,String authStatus) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("tenantId", tenantId);
		queryParams.put("authStatus", authStatus);
		

		List<TestMst> testMstList = (List<TestMst>) testMstDao.findByNamedQueryAndNamedParams(""
				+ "testMst.getAll",queryParams);
		if (testMstList == null || testMstList.size() == 0) {
			return null;
		}		
		return testMstList;
	}
	
	@Override
	public TestMst saveOrUpDate(TestMst entity,AccessObject accessObject)
			throws BaseException {
		String loginId=accessObject.getLoginId();
		if(entity.getTenantId()=="")
		{
		entity.setTenantId(ApplicationConstants.DEFAULT_TENANT);
		}
		entity.setAuthStatus(ApplicationConstants.AUTH_AUTHORIZED);
		
		entity.setCreatedDate(new Date());
		TestMst testMst = getUnique(entity.getTenantId(), entity.getTestName());
		if (testMst == null) {
			entity.setIsActive(1);
			return super.saveOrUpdate(loginId, entity);
		} else {
			Long id = testMst.getId();
			try {
				Mapper mapper = new DozerBeanMapper();
				mapper.map(entity, testMst);
			} catch (Exception e) {
				
			}
			testMst.setId(id);
			
			return super.saveOrUpdate(loginId, testMst);
		}
		
	}

	
	
	@Override
	public TestMst getUnique(String tenantId,String testName) {
	Map<String, String> queryParams = new HashMap<String, String>();
	queryParams.put("tenantId", tenantId);
	queryParams.put("testName", testName);
	

	List<TestMst> testMstList = (List<TestMst>) testMstDao.findByNamedQueryAndNamedParams(""
			+ "testMst.getUnique",queryParams);
	if (testMstList == null || testMstList.size() == 0) {
		return null;
	}		
	return testMstList.get(0);
}

}