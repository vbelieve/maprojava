package com.login.services.impl;

import com.login.services.RelationMappingCategoryCapabilityService;
import com.login.services.RelationMappingPracticeAreaPracticeService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.login.common.exceptions.BaseException;
import com.login.dao.CapabilityMstDao;
import com.login.dao.basics.CustomDao;
import com.login.domain.ApplicationConstants;
import com.login.domain.CapabilityMst;
import com.login.domain.CategoryMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.PracticeMst;
import com.login.domain.PracticeQAMst;
import com.login.domain.PracticeWorkProductMst;
import com.login.domain.QuestionMst;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.RelationMappingPracticeAreaPractice;
import com.login.domain.TenantMst;
import com.login.domain.UserMst;
import com.login.domain.WorkProductMst;
import com.login.domain.basics.AccessObject;
import com.login.services.CapabilityMstService;
import com.login.services.CategoryMstService;
import com.login.services.CreateOrgGapDetailsService;
import com.login.services.PracticeAreaMstService;
import com.login.services.PracticeMstService;
import com.login.services.PracticeQAMstService;
import com.login.services.PracticeWorkProductMstService;
import com.login.services.QuestionMstService;
import com.login.services.RelationMappingCapabilityPracticeAreaService;
import com.login.services.SequenceMstService;
import com.login.services.UserMstService;
import com.login.services.WorkProductMstService;
import com.login.services.basic.CoreServiceImpl;

@Service("createOrgGapDetailsService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = BaseException.class)

public class CreateOrgGapDetailsServiceImpl extends CoreServiceImpl<Long, CapabilityMst>
		implements CreateOrgGapDetailsService {

	@PersistenceContext
	EntityManager em;

	@Autowired
	CapabilityMstDao capabilityMstDao;

	@Autowired
	CategoryMstService categoryMstService;

	@Autowired
	CapabilityMstService capabilityMstService;

	@Autowired
	PracticeAreaMstService practiceAreaMstService;

	@Autowired
	PracticeMstService practiceMstService;

	@Autowired
	PracticeQAMstService practiceQAMstService;

	@Autowired
	SequenceMstService sequenceMstService;

	@Autowired
	PracticeWorkProductMstService practiceWorkProductMstService;

	@Autowired
	QuestionMstService questionMstService;
	
	@Autowired
	UserMstService userMstService;
	
	@Autowired
	RelationMappingCapabilityPracticeAreaService relationMappingCapabilityPracticeAreaService;
	
	@Autowired
	RelationMappingCategoryCapabilityService relationMappingCategoryCapabilityService;
	
	@Autowired
	RelationMappingPracticeAreaPracticeService relationMappingPracticeAreaPracticeService;
	
	@Autowired
	WorkProductMstService workProductMstService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })

	@PostConstruct
	public void init() throws Exception {
		super.setDAO((CustomDao) capabilityMstDao);
	}

	@PreDestroy
	public void destroy() {
	}

	@Override
	public void setEntityManagerOnDao(EntityManager entityManager) {
		capabilityMstDao.setEntityManager(entityManager);
	}

	public void CreateOrgAdminUser(TenantMst tenant) {
				String tenantId = tenant.getTenantId();
		//Code to implement User
		List<UserMst> baseUserMst=new ArrayList<UserMst>();
		List<UserMst> newUserMst=new ArrayList<UserMst>();
		baseUserMst=userMstService.getAllUsers(tenantId);
		if(null!=baseUserMst)
		{
			
		}
		else
		{
			baseUserMst=userMstService.getAllUsers(ApplicationConstants.DEFAULT_TENANT);
			for (UserMst userMstList : baseUserMst) {
				if(userMstList.getUserType().equalsIgnoreCase("ADMIN"))
				{
						UserMst userMst = new UserMst();
						Mapper mapper = new DozerBeanMapper();
						mapper.map(userMstList, userMst);
						userMst.setId(null);
						userMst.setTenantId(tenantId);
						userMst.setIsFirstTimeLogin(Boolean.TRUE);
						userMstService.saveOrUpDate(userMst,"superadmin");
				}
					
			}					
		}
	}
	
	public void newProjectMigration(String tenantId,String organizationUnit,String businessUnit,String project,AccessObject accessObject) {
		//CreateOrgGapDetailsCategory(tenantId,organizationUnit,businessUnit,project,accessObject);
		//CreateOrgGapDetailsCapability(tenantId,organizationUnit,businessUnit,project,accessObject);
		//CreateOrgGapDetailsPracticeArea(tenantId,organizationUnit,businessUnit,project,accessObject);
		CreateOrgGapDetailsPractice(tenantId,organizationUnit,businessUnit,project,accessObject);
		CreateOrgGapDetailsWorkProduct(tenantId,organizationUnit,businessUnit,project,accessObject);
		CreateOrgGapDetailsPracticeWorkProduct(tenantId,organizationUnit,businessUnit,project,accessObject);
//		CreateOrgGapDetailsRelationCategoryCapability(tenantId,organizationUnit,businessUnit,project,accessObject);
//		CreateOrgGapDetailsRelationCapabilityPracticeArea(tenantId,organizationUnit,businessUnit,project,accessObject);
//		CreateOrgGapDetailsRelationPracticeAreaPractice(tenantId,organizationUnit,businessUnit,project,accessObject);
	}
	
	
	public void CreateOrgGapDetailsCategory(String tenantId,String organizationUnit,String businessUnit,String project,AccessObject accessObject) {
		List<CategoryMst> baseCategory = new ArrayList<CategoryMst>();
		List<CategoryMst> newCategory = new ArrayList<CategoryMst>();
		newCategory = categoryMstService.getAllCategory(tenantId,accessObject.getCmmiVersion());
		if (null != newCategory) {
			newCategory= categoryMstService.getByTenantAndProject(tenantId, project,accessObject.getCmmiVersion());
		}
		if (null != newCategory) {

		} else {
			baseCategory = categoryMstService.getAllCategory(ApplicationConstants.DEFAULT_TENANT,accessObject.getCmmiVersion());
			if (null != baseCategory) {
				for (CategoryMst baseCat : baseCategory) {
						
							CategoryMst category = new CategoryMst();
							Mapper mapper = new DozerBeanMapper();
							mapper.map(baseCat, category);
							category.setId(null);
							category.setTenantId(tenantId);
							category.setBusinessUnit(businessUnit);
							category.setOrganizationUnit(organizationUnit);
							category.setProject(project);
							categoryMstService.saveOrUpDate(category,accessObject,accessObject.getCmmiVersion());
						
				}
			}
		}		
	}
	
	public void CreateOrgGapDetailsCapability(String tenantId,String organizationUnit,String businessUnit,String project,AccessObject accessObject) {
		List<CapabilityMst> baseCapability = new ArrayList<CapabilityMst>();
		List<CapabilityMst> newCapability = new ArrayList<CapabilityMst>();
		newCapability = capabilityMstService.getAllCapability(tenantId,accessObject.getCmmiVersion());
		if (null != newCapability) {
			newCapability = capabilityMstService.getByTenantAndProject(tenantId, project,accessObject.getCmmiVersion());
		}
		if (null != newCapability) {

		} else {
			baseCapability = capabilityMstService.getAllCapability(ApplicationConstants.DEFAULT_TENANT,accessObject.getCmmiVersion());
			if (null != baseCapability) {
				for (CapabilityMst baseCap : baseCapability) {
						
							CapabilityMst capability = new CapabilityMst();
							Mapper mapper = new DozerBeanMapper();
							mapper.map(baseCap, capability);
							capability.setId(null);
							capability.setTenantId(tenantId);
							capability.setOrganizationUnit(organizationUnit);
							capability.setBusinessUnit(businessUnit);
							capability.setProject(project);
							capabilityMstService.saveOrUpDate(capability,accessObject,accessObject.getCmmiVersion());
				}
			}
		}

	}

	public void CreateOrgGapDetailsPracticeArea(String tenantId,String organizationUnit,String businessUnit,String project,AccessObject accessObject) {
		List<PracticeAreaMst> basePracticeArea = new ArrayList<PracticeAreaMst>();
		List<PracticeAreaMst> newPracticeArea = new ArrayList<PracticeAreaMst>();
		newPracticeArea = practiceAreaMstService.getAll(tenantId,accessObject.getCmmiVersion());
		if (null != newPracticeArea) {
			newPracticeArea = practiceAreaMstService.getByTenantAndProject(tenantId, project,accessObject.getCmmiVersion());
		}
		if (null != newPracticeArea) {

		} else {
			basePracticeArea = practiceAreaMstService.getAll(ApplicationConstants.DEFAULT_TENANT,accessObject.getCmmiVersion());
			if (null != basePracticeArea) {
				for (PracticeAreaMst basePracticeAreaList : basePracticeArea) {
							PracticeAreaMst practiceArea = new PracticeAreaMst();
							Mapper mapper = new DozerBeanMapper();
							mapper.map(basePracticeAreaList, practiceArea);
							practiceArea.setId(null);
							practiceArea.setTenantId(tenantId);
							practiceArea.setOrganizationUnit(organizationUnit);
							practiceArea.setBusinessUnit(businessUnit);
							practiceArea.setProject(project);
							practiceAreaMstService.saveOrUpDate(practiceArea,accessObject,accessObject.getCmmiVersion());						
					}
				}
			}
		
	}

	
	public void CreateOrgGapDetailsPractice(String tenantId,String organizationUnit,String businessUnit,String project,AccessObject accessObject) {
		List<PracticeMst> basePractice = new ArrayList<PracticeMst>();
		List<PracticeMst> newPractice = new ArrayList<PracticeMst>();
		newPractice = practiceMstService.getAll(tenantId,accessObject.getCmmiVersion());
		if (null != newPractice) {
			newPractice = practiceMstService.getPracticeByTenantOrganizationBusinesssProject(tenantId, organizationUnit, businessUnit, project,accessObject.getCmmiVersion());
		}
		if (null != newPractice) {

		} else {
			basePractice = practiceMstService.getAll(ApplicationConstants.DEFAULT_TENANT,accessObject.getCmmiVersion());
			if (null != basePractice) {
				for (PracticeMst basePracticeList : basePractice) {
							PracticeMst practice = new PracticeMst();
							Mapper mapper = new DozerBeanMapper();
							mapper.map(basePracticeList, practice);
							practice.setId(null);
							practice.setTenantId(tenantId);
							practice.setOrganizationUnit(organizationUnit);
							practice.setBusinessUnit(businessUnit);
							practice.setProject(project);
							practiceMstService.saveOrUpDate(practice,accessObject);
				}
			}
		}
		
	}

	public void CreateOrgGapDetailsWorkProduct(String tenantId,String organizationUnit,String businessUnit,String project,AccessObject accessObject) {
		List<WorkProductMst> baseWorkProductMst = new ArrayList<WorkProductMst>();
		List<WorkProductMst> newWorkProductMst = new ArrayList<WorkProductMst>();
		newWorkProductMst = workProductMstService.getAll(tenantId,accessObject.getCmmiVersion());
		if (null != newWorkProductMst) {
			newWorkProductMst = workProductMstService.getByTenantAndProject(tenantId, project,accessObject.getCmmiVersion());
		}
		if (null != newWorkProductMst) {

		} else {
			baseWorkProductMst = workProductMstService.getAll(ApplicationConstants.DEFAULT_TENANT,accessObject.getCmmiVersion());
			if (null != baseWorkProductMst) {
				for (WorkProductMst baseWorkProductMstList : baseWorkProductMst) {
							WorkProductMst workProductMst = new WorkProductMst();
							Mapper mapper = new DozerBeanMapper();
							mapper.map(baseWorkProductMstList, workProductMst);
							workProductMst.setId(null);
							workProductMst.setTenantId(tenantId);
							workProductMst.setOrganizationUnit(organizationUnit);
							workProductMst.setBusinessUnit(businessUnit);
							workProductMst.setProject(project);
							workProductMstService.saveOrUpDate(workProductMst,accessObject,accessObject.getCmmiVersion());
					
				}
			}
		}

					
	}
	
	public void CreateOrgGapDetailsPracticeWorkProduct(String tenantId,String organizationUnit,String businessUnit,String project,AccessObject accessObject) {
		List<PracticeWorkProductMst> basePracticeWorkProductMst = new ArrayList<PracticeWorkProductMst>();
		List<PracticeWorkProductMst> newPracticeWorkProductMst = new ArrayList<PracticeWorkProductMst>();
		newPracticeWorkProductMst = practiceWorkProductMstService.getAll(tenantId,accessObject.getCmmiVersion());
		if (null != newPracticeWorkProductMst) {
			newPracticeWorkProductMst = practiceWorkProductMstService.getByTenantAndProject(tenantId, project,accessObject.getCmmiVersion());
		}
		if (null != newPracticeWorkProductMst) {

		} else {
			basePracticeWorkProductMst = practiceWorkProductMstService.getAll(ApplicationConstants.DEFAULT_TENANT,accessObject.getCmmiVersion());
			if (null != basePracticeWorkProductMst) {
				for (PracticeWorkProductMst basePracticeWorkProductMstList : basePracticeWorkProductMst) {
					
							PracticeWorkProductMst practiceWorkProductMst = new PracticeWorkProductMst();
							Mapper mapper = new DozerBeanMapper();
							mapper.map(basePracticeWorkProductMstList, practiceWorkProductMst);
							practiceWorkProductMst.setId(null);
							practiceWorkProductMst.setTenantId(tenantId);
							practiceWorkProductMst.setOrganizationUnit(organizationUnit);
							practiceWorkProductMst.setBusinessUnit(businessUnit);
							practiceWorkProductMst.setProject(project);
							WorkProductMst workProductName=workProductMstService.getUnique(tenantId, practiceWorkProductMst.getWorkProductId(), 
									practiceWorkProductMst.getProcess(), organizationUnit, businessUnit, project,accessObject.getCmmiVersion());
							if(null!=workProductName)
							{
								practiceWorkProductMst.setWorkProductName(workProductName.getWorkProductName());
							}
							practiceWorkProductMstService.saveOrUpDate(practiceWorkProductMst,accessObject.getCmmiVersion());

					
				}
			}
		}
			
	}

	public void CreateOrgGapDetailsRelationCategoryCapability(String tenantId,String organizationUnit,String businessUnit,String project,AccessObject accessObject) 
	{
		// Code to implement RelationMappingCategoryCapability
		List<RelationMappingCategoryCapability> baseRelationMappingCategoryCapability = new ArrayList<RelationMappingCategoryCapability>();
		List<RelationMappingCategoryCapability> newRelationMappingCategoryCapability = new ArrayList<RelationMappingCategoryCapability>();
		newRelationMappingCategoryCapability = relationMappingCategoryCapabilityService.getAll(tenantId,accessObject.getCmmiVersion());
		if (null != newRelationMappingCategoryCapability) {
			newRelationMappingCategoryCapability = relationMappingCategoryCapabilityService.getByTenantAndProject(tenantId, project,accessObject.getCmmiVersion());
		}
		if (null != newRelationMappingCategoryCapability) {

		} else {
			baseRelationMappingCategoryCapability = relationMappingCategoryCapabilityService.getAll(ApplicationConstants.DEFAULT_TENANT,accessObject.getCmmiVersion());
			if (null != baseRelationMappingCategoryCapability) {
				for (RelationMappingCategoryCapability baseRelationMappingCategoryCapabilityList : baseRelationMappingCategoryCapability) {
					
							RelationMappingCategoryCapability relationMappingCategoryCapability = new RelationMappingCategoryCapability();
							Mapper mapper = new DozerBeanMapper();
							mapper.map(baseRelationMappingCategoryCapabilityList, relationMappingCategoryCapability);
							relationMappingCategoryCapability.setId(null);
							relationMappingCategoryCapability.setTenantId(tenantId);
							relationMappingCategoryCapability.setOrganizationUnit(organizationUnit);
							relationMappingCategoryCapability.setBusinessUnit(businessUnit);
							relationMappingCategoryCapability.setProject(project);
							relationMappingCategoryCapabilityService.saveOrUpDate(relationMappingCategoryCapability,accessObject,accessObject.getCmmiVersion());

					
				}
			}
		}

	}

	public void CreateOrgGapDetailsRelationCapabilityPracticeArea(String tenantId,String organizationUnit,String businessUnit,String project,AccessObject accessObject) 
	{
		List<RelationMappingCapabilityPracticeArea> baseRelationMappingCapabilityPracticeArea = new ArrayList<RelationMappingCapabilityPracticeArea>();
		List<RelationMappingCapabilityPracticeArea> newRelationMappingCapabilityPracticeArea = new ArrayList<RelationMappingCapabilityPracticeArea>();
		newRelationMappingCapabilityPracticeArea = relationMappingCapabilityPracticeAreaService.getAll(tenantId,accessObject.getCmmiVersion());
		if (null != newRelationMappingCapabilityPracticeArea) {
			newRelationMappingCapabilityPracticeArea = relationMappingCapabilityPracticeAreaService.getByTenantAndProject(tenantId, project,accessObject.getCmmiVersion());
		}
		if (null != newRelationMappingCapabilityPracticeArea) {

		} else {
			baseRelationMappingCapabilityPracticeArea = relationMappingCapabilityPracticeAreaService.getAll(ApplicationConstants.DEFAULT_TENANT,accessObject.getCmmiVersion());
			if (null != baseRelationMappingCapabilityPracticeArea) {
				for (RelationMappingCapabilityPracticeArea baseRelationMappingCapabilityPracticeAreaList : baseRelationMappingCapabilityPracticeArea) {
							RelationMappingCapabilityPracticeArea relationMappingCapabilityPracticeArea = new RelationMappingCapabilityPracticeArea();
							Mapper mapper = new DozerBeanMapper();
							mapper.map(baseRelationMappingCapabilityPracticeAreaList, relationMappingCapabilityPracticeArea);
							relationMappingCapabilityPracticeArea.setId(null);
							relationMappingCapabilityPracticeArea.setTenantId(tenantId);
							relationMappingCapabilityPracticeArea.setOrganizationUnit(organizationUnit);
							relationMappingCapabilityPracticeArea.setBusinessUnit(businessUnit);
							relationMappingCapabilityPracticeArea.setProject(project);
							relationMappingCapabilityPracticeAreaService.saveOrUpDate(relationMappingCapabilityPracticeArea,accessObject,accessObject.getCmmiVersion());

				}
			}
		}

	}
	
	public void CreateOrgGapDetailsRelationPracticeAreaPractice(String tenantId,String organizationUnit,String businessUnit,String project,AccessObject accessObject) 
	{
		List<RelationMappingPracticeAreaPractice> baseRelationMappingPracticeAreaPractice = new ArrayList<RelationMappingPracticeAreaPractice>();
		List<RelationMappingPracticeAreaPractice> newRelationMappingPracticeAreaPractice = new ArrayList<RelationMappingPracticeAreaPractice>();
		newRelationMappingPracticeAreaPractice = relationMappingPracticeAreaPracticeService.getAll(tenantId,accessObject.getCmmiVersion());
		if (null != newRelationMappingPracticeAreaPractice) {
			newRelationMappingPracticeAreaPractice = relationMappingPracticeAreaPracticeService.getByTenantAndProject(tenantId, project,accessObject.getCmmiVersion());
		}
		if (null != newRelationMappingPracticeAreaPractice) {

		} else {
			baseRelationMappingPracticeAreaPractice = relationMappingPracticeAreaPracticeService.getAll(ApplicationConstants.DEFAULT_TENANT,accessObject.getCmmiVersion());
			if (null != baseRelationMappingPracticeAreaPractice) {
				for (RelationMappingPracticeAreaPractice baseRelationMappingPracticeAreaPracticeList : baseRelationMappingPracticeAreaPractice) {
					
							RelationMappingPracticeAreaPractice relationMappingPracticeAreaPractice = new RelationMappingPracticeAreaPractice();
							Mapper mapper = new DozerBeanMapper();
							mapper.map(baseRelationMappingPracticeAreaPracticeList, relationMappingPracticeAreaPractice);
							relationMappingPracticeAreaPractice.setId(null);
							relationMappingPracticeAreaPractice.setTenantId(tenantId);
							relationMappingPracticeAreaPractice.setOrganizationUnit(organizationUnit);
							relationMappingPracticeAreaPractice.setBusinessUnit(businessUnit);
							relationMappingPracticeAreaPractice.setProject(project);
							
							relationMappingPracticeAreaPracticeService.saveOrUpDate(relationMappingPracticeAreaPractice,accessObject,accessObject.getCmmiVersion());

				}
			}
		}
	}
	
	@Override
	public void CreateOrgGapDetails(TenantMst tenant) {
		String tenantId = tenant.getTenantId();

//
			// Code to implement PracticeAreaMst
//			List<PracticeAreaMst> basePracticeArea = new ArrayList<PracticeAreaMst>();
//			List<PracticeAreaMst> newPracticeArea = new ArrayList<PracticeAreaMst>();
//			newPracticeArea = practiceAreaMstService.getAll(tenantId);
//			if (null != newPracticeArea) {
//
//			} else {
//				basePracticeArea = practiceAreaMstService.getAll(ApplicationConstants.DEFAULT_TENANT);
//				if (null != basePracticeArea) {
//					for (PracticeAreaMst basePracticeAreaList : basePracticeArea) {
//								PracticeAreaMst practiceArea = new PracticeAreaMst();
//								Mapper mapper = new DozerBeanMapper();
//								mapper.map(basePracticeAreaList, practiceArea);
//								practiceArea.setId(null);
//								practiceArea.setTenantId(tenantId);
//								practiceAreaMstService.saveOrUpDate(practiceArea);						
//						}
//					}
//				}
//			
//			// Code to implement PracticeMst
//			List<PracticeMst> basePractice = new ArrayList<PracticeMst>();
//			List<PracticeMst> newPractice = new ArrayList<PracticeMst>();
//			newPractice = practiceMstService.getAll(tenantId);
//			if (null != newPractice) {
//
//			} else {
//				basePractice = practiceMstService.getAll(ApplicationConstants.DEFAULT_TENANT);
//				if (null != basePractice) {
//					for (PracticeMst basePracticeList : basePractice) {
//								PracticeMst practice = new PracticeMst();
//								Mapper mapper = new DozerBeanMapper();
//								mapper.map(basePracticeList, practice);
//								practice.setId(null);
//								practice.setTenantId(tenantId);
//								practiceMstService.saveOrUpDate(practice);
//					}
//				}
//			}

//			// Code to implement PracticeQAMst
//			List<PracticeQAMst> basePracticeQA = new ArrayList<PracticeQAMst>();
//			List<PracticeQAMst> newPracticeQA = new ArrayList<PracticeQAMst>();
//			newPracticeQA = practiceQAMstService.getAll(tenantId);
//			if (null != newPracticeQA) {
//
//			} else {
//				basePracticeQA = practiceQAMstService.getAll(ApplicationConstants.DEFAULT_TENANT);
//				if (null != basePracticeQA) {
//					for (PracticeQAMst basePracticeQAList : basePracticeQA) {
//						for (String processCode : process) {
//							{
//								PracticeQAMst practiceQA = new PracticeQAMst();
//								Mapper mapper = new DozerBeanMapper();
//								mapper.map(basePracticeQAList, practiceQA);
//								practiceQA.setId(null);
//								practiceQA.setProcess(processCode);
//								practiceQA.setTenantId(tenantId);
//								practiceQAMstService.saveOrUpDate(practiceQA);
//
//							}
//						}
//					}
//				}
//			}
//			// Code to implement PracticeWorkProductMst
//			List<PracticeWorkProductMst> basePracticeWorkProductMst = new ArrayList<PracticeWorkProductMst>();
//			List<PracticeWorkProductMst> newPracticeWorkProductMst = new ArrayList<PracticeWorkProductMst>();
//			newPracticeWorkProductMst = practiceWorkProductMstService.getAll(tenantId);
//			if (null != newPracticeWorkProductMst) {
//
//			} else {
//				basePracticeWorkProductMst = practiceWorkProductMstService.getAll(ApplicationConstants.DEFAULT_TENANT);
//				if (null != basePracticeWorkProductMst) {
//					for (PracticeWorkProductMst basePracticeWorkProductMstList : basePracticeWorkProductMst) {
//						for (String processCode : process) {
//							{
//								PracticeWorkProductMst practiceWorkProductMst = new PracticeWorkProductMst();
//								Mapper mapper = new DozerBeanMapper();
//								mapper.map(basePracticeWorkProductMstList, practiceWorkProductMst);
//								practiceWorkProductMst.setId(null);
//								practiceWorkProductMst.setProcess(processCode);
//								practiceWorkProductMst.setTenantId(tenantId);
//								practiceWorkProductMstService.saveOrUpDate(practiceWorkProductMst);
//
//							}
//						}
//					}
//				}
//			}
//			// Code to implement QuestionMst
//			List<QuestionMst> baseQuestionMst = new ArrayList<QuestionMst>();
//			List<QuestionMst> newQuestionMst = new ArrayList<QuestionMst>();
//			newQuestionMst = questionMstService.getAll(tenantId);
//			if (null != newQuestionMst) {
//
//			} else {
//				baseQuestionMst = questionMstService.getAll(ApplicationConstants.DEFAULT_TENANT);
//				if (null != baseQuestionMst) {
//					for (QuestionMst baseQuestionMstList : baseQuestionMst) {
//						for (String processCode : process) {
//							{
//								QuestionMst questionMst = new QuestionMst();
//								Mapper mapper = new DozerBeanMapper();
//								mapper.map(baseQuestionMstList, questionMst);
//								questionMst.setId(null);
//								questionMst.setProcess(processCode);
//								questionMst.setTenantId(tenantId);
//								questionMstService.saveOrUpDate(questionMst);
//
//							}
//						}
//					}
//				}
//			}
//			// Code to implement RelationMappingCapabilityPracticeArea
//			List<RelationMappingCapabilityPracticeArea> baseRelationMappingCapabilityPracticeArea = new ArrayList<RelationMappingCapabilityPracticeArea>();
//			List<RelationMappingCapabilityPracticeArea> newRelationMappingCapabilityPracticeArea = new ArrayList<RelationMappingCapabilityPracticeArea>();
//			newRelationMappingCapabilityPracticeArea = relationMappingCapabilityPracticeAreaService.getAll(tenantId);
//			if (null != newRelationMappingCapabilityPracticeArea) {
//
//			} else {
//				baseRelationMappingCapabilityPracticeArea = relationMappingCapabilityPracticeAreaService.getAll(ApplicationConstants.DEFAULT_TENANT);
//				if (null != baseRelationMappingCapabilityPracticeArea) {
//					for (RelationMappingCapabilityPracticeArea baseRelationMappingCapabilityPracticeAreaList : baseRelationMappingCapabilityPracticeArea) {
//						for (String processCode : process) {
//							{
//								RelationMappingCapabilityPracticeArea relationMappingCapabilityPracticeArea = new RelationMappingCapabilityPracticeArea();
//								Mapper mapper = new DozerBeanMapper();
//								mapper.map(baseRelationMappingCapabilityPracticeAreaList, relationMappingCapabilityPracticeArea);
//								relationMappingCapabilityPracticeArea.setId(null);
//								relationMappingCapabilityPracticeArea.setProcess(processCode);
//								relationMappingCapabilityPracticeArea.setTenantId(tenantId);
//								relationMappingCapabilityPracticeAreaService.saveOrUpDate(relationMappingCapabilityPracticeArea);
//
//							}
//						}
//					}
//				}
//			}
//			// Code to implement RelationMappingCategoryCapability
//			List<RelationMappingCategoryCapability> baseRelationMappingCategoryCapability = new ArrayList<RelationMappingCategoryCapability>();
//			List<RelationMappingCategoryCapability> newRelationMappingCategoryCapability = new ArrayList<RelationMappingCategoryCapability>();
//			newRelationMappingCategoryCapability = relationMappingCategoryCapabilityService.getAll(tenantId);
//			if (null != newRelationMappingCategoryCapability) {
//
//			} else {
//				baseRelationMappingCategoryCapability = relationMappingCategoryCapabilityService.getAll(ApplicationConstants.DEFAULT_TENANT);
//				if (null != baseRelationMappingCategoryCapability) {
//					for (RelationMappingCategoryCapability baseRelationMappingCategoryCapabilityList : baseRelationMappingCategoryCapability) {
//						for (String processCode : process) {
//							{
//								RelationMappingCategoryCapability relationMappingCategoryCapability = new RelationMappingCategoryCapability();
//								Mapper mapper = new DozerBeanMapper();
//								mapper.map(baseRelationMappingCategoryCapabilityList, relationMappingCategoryCapability);
//								relationMappingCategoryCapability.setId(null);
//								relationMappingCategoryCapability.setProcess(processCode);
//								relationMappingCategoryCapability.setTenantId(tenantId);
//								relationMappingCategoryCapabilityService.saveOrUpDate(relationMappingCategoryCapability);
//
//							}
//						}
//					}
//				}
//			}
//			// Code to implement RelationMappingPracticeAreaPractice
//			List<RelationMappingPracticeAreaPractice> baseRelationMappingPracticeAreaPractice = new ArrayList<RelationMappingPracticeAreaPractice>();
//			List<RelationMappingPracticeAreaPractice> newRelationMappingPracticeAreaPractice = new ArrayList<RelationMappingPracticeAreaPractice>();
//			newRelationMappingPracticeAreaPractice = relationMappingPracticeAreaPracticeService.getAll(tenantId);
//			if (null != newRelationMappingPracticeAreaPractice) {
//
//			} else {
//				baseRelationMappingPracticeAreaPractice = relationMappingPracticeAreaPracticeService.getAll(ApplicationConstants.DEFAULT_TENANT);
//				if (null != baseRelationMappingPracticeAreaPractice) {
//					for (RelationMappingPracticeAreaPractice baseRelationMappingPracticeAreaPracticeList : baseRelationMappingPracticeAreaPractice) {
//						for (String processCode : process) {
//							{
//								RelationMappingPracticeAreaPractice relationMappingPracticeAreaPractice = new RelationMappingPracticeAreaPractice();
//								Mapper mapper = new DozerBeanMapper();
//								mapper.map(baseRelationMappingPracticeAreaPracticeList, relationMappingPracticeAreaPractice);
//								relationMappingPracticeAreaPractice.setId(null);
//								relationMappingPracticeAreaPractice.setProcess(processCode);
//								relationMappingPracticeAreaPractice.setTenantId(tenantId);
//								relationMappingPracticeAreaPracticeService.saveOrUpDate(relationMappingPracticeAreaPractice);
//
//							}
//						}
//					}
//				}
//			}
			// Code to implement WorkProductMst
//			List<WorkProductMst> baseWorkProductMst = new ArrayList<WorkProductMst>();
//			List<WorkProductMst> newWorkProductMst = new ArrayList<WorkProductMst>();
//			newWorkProductMst = workProductMstService.getAll(tenantId);
//			if (null != newWorkProductMst) {
//
//			} else {
//				baseWorkProductMst = workProductMstService.getAll(ApplicationConstants.DEFAULT_TENANT);
//				if (null != baseWorkProductMst) {
//					for (WorkProductMst baseWorkProductMstList : baseWorkProductMst) {
//								WorkProductMst workProductMst = new WorkProductMst();
//								Mapper mapper = new DozerBeanMapper();
//								mapper.map(baseWorkProductMstList, workProductMst);
//								workProductMst.setId(null);
//								workProductMst.setTenantId(tenantId);
//								workProductMstService.saveOrUpDate(workProductMst);
//						
//					}
//				}
//			}
//
//			}

		

	}

}