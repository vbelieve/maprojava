package com.login.services;

import java.util.List;

import com.login.domain.SequenceMst;
import com.login.domain.UserMst;



public interface UserMstService {
	UserMst saveOrUpDate(UserMst entity,String loginId);
	UserMst getUserMst(String tenantId,String sequenceType);
	UserMst getByName(String tenantId,String userName);
	List<UserMst> getAllUsers(String tenantId);
	List<UserMst> getAllUsersForAdmin(String tenantId);
	UserMst validateUser(String tenantId,String userName,String password);
}
