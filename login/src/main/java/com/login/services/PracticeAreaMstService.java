package com.login.services;

import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.PiidMst;
import com.login.domain.PracticeAreaMst;
import com.login.domain.basics.AccessObject;



public interface PracticeAreaMstService {
	PracticeAreaMst saveOrUpDate(PracticeAreaMst entity,AccessObject accessObject,String cmmiVersion);
	List<PracticeAreaMst> getAll(String tenantId,String cmmiVersion);
	List<PracticeAreaMst> getByCategory(String tenantId,Integer categoryId,String cmmiVersion);
	List<PracticeAreaMst> getByCapability(String tenantId,Integer capabilityId,String cmmiVersion);
	List<PracticeAreaMst> getByCategoryAndCapability(String tenantId,Integer categoryId,Integer capabilityId,String cmmiVersion);
	PracticeAreaMst getByName(String tenantId,String capabilityName,String cmmiVersion);
	public PracticeAreaMst getUnique(String tenantId,String practiceAreaCode,String process,String organizationUnit,String businessUnit,String project,String cmmiVersion);	
	public List<PracticeAreaMst> getByTenantAndProcess(String tenantId,String process,String cmmiVersion);
	public List<PracticeAreaMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion);
	public PracticeAreaMst getByProjectPracticeArea(String tenantId,String project,String practiceAreaCode,String cmmiVersion);
	public String delete(PracticeAreaMst entity,AccessObject accessObject,String cmmiVersion)throws BaseException; 
	public List<PracticeAreaMst> getBase(String cmmiVersion);
}
