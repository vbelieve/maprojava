package com.login.services;

import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.AnswerMst;
import com.login.domain.TestQuestionMst;
import com.login.domain.basics.AccessObject;


public interface TestQuestionMstService {

	List<TestQuestionMst> getAll(String tenantId,String authStatus);

		TestQuestionMst saveOrUpDate(TestQuestionMst entity, AccessObject accessObject) throws BaseException;

	TestQuestionMst getUnique(String tenantId, Long questionId);

	
	
}
