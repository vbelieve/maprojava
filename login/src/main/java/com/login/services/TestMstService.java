package com.login.services;

import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.TestMst;
import com.login.domain.basics.AccessObject;

public interface TestMstService {

	List<TestMst> getAll(String tenantId,String authStatus);

	TestMst getUnique(String tenantId, String testName);

	TestMst saveOrUpDate(TestMst entity, AccessObject accessObject) throws BaseException;
	
	
}
