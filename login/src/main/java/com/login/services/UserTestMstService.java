package com.login.services;

import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.AnswerMst;
import com.login.domain.UserTestMst;
import com.login.domain.basics.AccessObject;


public interface UserTestMstService {

	List<UserTestMst> getAll(String tenantId,String authStatus);


	UserTestMst saveOrUpDate(UserTestMst entity, AccessObject accessObject) throws BaseException;



	UserTestMst getUnique(String tenantId, String testName, Long questionId);
	
	
}
