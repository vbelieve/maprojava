package com.login.services;

import com.login.domain.basics.AccessObject;
import com.login.domain.basics.DashBoardVo;
import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.PracticeMst;



public interface PracticeMstService {
	PracticeMst saveOrUpDate(PracticeMst entity,AccessObject accessObject);
	List<PracticeMst> getAll(String tenantId,String cmmiVersion);
	List<PracticeMst> getAllActive();
	List<PracticeMst> getByCategory(String tenantId,Integer categoryId,String cmmiVersion);
	List<PracticeMst> getByCapability(String tenantId,Integer capabilityId,String cmmiVersion);
	List<PracticeMst> getByCategoryAndCapability(String tenantId,Integer categoryId,Integer capabilityId,String cmmiVersion);
	PracticeMst getByName(String tenantId,String capabilityName,String cmmiVersion);
	public PracticeMst getUnique(String tenantId,String practiceCode,String process,String organizationUnit,
			String businessUnit,String project,String cmmiVersion);
	public List<DashBoardVo> getPracticeComplianceByTenant();
	public Double getPracticeComplianceByTenantAndProcess(String tenantId,String cmmiVersion);
	List<PracticeMst> getPracticeByTenantOrganization(String tenantId,String organizationUnit,String cmmiVersion);
	List<PracticeMst> getPracticeByTenantOrganizationBusinesss(String tenantId,String organizationUnit,String businessUnit,String cmmiVersion);
	List<PracticeMst> getPracticeByTenantOrganizationBusinesssProject(String tenantId,String organizationUnit,
			String businessUnit,String project,String cmmiVersion);
	List<PracticeMst> getPracticeByTenantProject(String tenantId,String project,AccessObject accessObject,String cmmiVersion);
	List<PracticeMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion);
	public List<PracticeMst> getByTenantAndProcess(String tenantId,String process,String cmmiVersion);	
	public PracticeMst getByTenantProjectAndPractice(String tenantId,String project,String practiceCode,String cmmiVersion);
	public String delete(PracticeMst entity,AccessObject accessObject,String cmmiVersion) throws BaseException;
}
