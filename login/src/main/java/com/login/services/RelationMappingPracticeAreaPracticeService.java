package com.login.services;

import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.RelationMappingPracticeAreaPractice;
import com.login.domain.basics.AccessObject;



public interface RelationMappingPracticeAreaPracticeService {
	public RelationMappingPracticeAreaPractice saveOrUpDate(RelationMappingPracticeAreaPractice entity,AccessObject accessObject,String cmmiVersion);
	List<RelationMappingPracticeAreaPractice> getByPractice(String tenantId,String practiceCode,String cmmiVersion);
	List<RelationMappingPracticeAreaPractice> getByPracticeLevel(String tenantId,String practiceLevelCode,String cmmiVersion);
	List<RelationMappingPracticeAreaPractice> getByPracticeArea(String tenantId,String practiceArea,String organizationUnit,String businessUnit,String project,String cmmiVersion);
	List<RelationMappingPracticeAreaPractice> getAll(String tenantId,String cmmiVersion);
	RelationMappingPracticeAreaPractice getUnique(String tenantId,String practiceAreaCode,String practiceCode,String practiceLevelCode,
			String process,String organizationUnit,String businessUnit,String project,String cmmiVersion);
	public List<RelationMappingPracticeAreaPractice> getByTenantAndProcess(String tenantId,String process,String cmmiVersion);
	public List<RelationMappingPracticeAreaPractice> getByTenantAndProject(String tenantId,String project,String cmmiVersion);
	public RelationMappingPracticeAreaPractice getByProjectAndPractice(String tenantId,String project,String practiceCode,String cmmiVersion);
	public List<RelationMappingPracticeAreaPractice> getRelationMappingPracticeAreaPracticeListByTenantAndProject(
			String project,Integer practiceLevel,String businessModel,String cmmiVersion);
	public String delete(RelationMappingPracticeAreaPractice entity,AccessObject accessObject,String cmmiVersion)throws BaseException;
}
