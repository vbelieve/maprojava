package com.login.services;

import com.login.domain.basics.AccessObject;
import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.CategoryMst;



public interface CategoryMstService {
	CategoryMst saveOrUpDate(CategoryMst entity,AccessObject accessObject,String cmmiVersion);
	List<CategoryMst> getAllCategory(String tenantId,String cmmiVersion);
	CategoryMst getByName(String tenantId,String categoryName,String cmmiVersion);
	public CategoryMst getUnique(String tenantId,String categoryCode,String process,String organizationUnit,String businessUnit,String project,String cmmiVersion);
	public List<CategoryMst> getByTenantAndProcess(String tenantId,String process,String cmmiVersion);
	public List<CategoryMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion);
	public String delete(CategoryMst entity,AccessObject accessObject,String cmmiVersion) throws BaseException;
	public List<CategoryMst> getBaseCategory(String cmmiVersion);
	CategoryMst getUniqueCmmiVersion(String tenantId, String categoryCode, String process, String organizationUnit,
			String businessUnit, String project, String cmmiVersion);
	
}
