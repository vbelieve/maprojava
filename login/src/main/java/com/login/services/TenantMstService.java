package com.login.services;

import com.login.domain.basics.AccessObject;
import java.util.List;

import com.login.domain.TenantMst;

public interface TenantMstService {
	
	public List<TenantMst> getAll();
	public TenantMst getUnique(String tenantId,String organizationCode);
	public TenantMst SaveOrUpdate(TenantMst entity,AccessObject accessObject);
	public TenantMst getTenant(String tenantId);
	public String delete(TenantMst entity,AccessObject accessObject);
}
