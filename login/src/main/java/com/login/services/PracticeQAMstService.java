package com.login.services;

import com.login.domain.PracticeQAMst;
import com.login.domain.WorkProductMst;
import java.util.List;



public interface PracticeQAMstService {
	PracticeQAMst saveOrUpDate(PracticeQAMst entity);
	List<PracticeQAMst> getAll(String tenantId);
	PracticeQAMst getUnique(String tenantId,String practiceQAId,String process);
	
}
