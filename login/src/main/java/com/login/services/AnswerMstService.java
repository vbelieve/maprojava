package com.login.services;

import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.AnswerMst;
import com.login.domain.basics.AccessObject;


public interface AnswerMstService {

	List<AnswerMst> getAll(String tenantId,String authStatus);


	AnswerMst saveOrUpDate(AnswerMst entity, AccessObject accessObject) throws BaseException;

	AnswerMst getUnique(String tenantId, Long answerId);
	
	
}
