package com.login.services;

import javax.servlet.http.HttpServletRequest;

import com.login.domain.TenantMst;
import com.login.domain.basics.AccessObject;



public interface CreateOrgGapDetailsService {
	public void CreateOrgGapDetails(TenantMst tenant);
	public void CreateOrgAdminUser(TenantMst tenant);
	public void newProjectMigration(String tenantId,String organizationUnit,String businessUnit,String project,AccessObject accessObject);
	
}
