package com.login.services;

import com.login.common.exceptions.BaseException;
import com.login.domain.RelationMappingCapabilityPracticeArea;
import java.util.List;

import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.basics.AccessObject;



public interface RelationMappingCapabilityPracticeAreaService {
	public RelationMappingCapabilityPracticeArea saveOrUpDate(RelationMappingCapabilityPracticeArea entity,
			AccessObject accessObject,String cmmiVersion);
	List<RelationMappingCapabilityPracticeArea> getByCapability(String tenantId,String capabilityCode,
			String organizationUnit,String businessUnit,String project,String cmmiVersion);
	List<RelationMappingCapabilityPracticeArea> getByPracticeArea(String tenantId,String practiceArea,String cmmiVersion);
	List<RelationMappingCapabilityPracticeArea> getAll(String tenantId,String cmmiVersion);
	RelationMappingCapabilityPracticeArea getUnique(String tenantId,String capabilityCode,String practiceAreaCode,
			String process,String organizationUnit,String businessUnit,String project,String cmmiVersion);
	public List<RelationMappingCapabilityPracticeArea> getByTenantAndProcess(String tenantId,String process,String cmmiVersion);	
	public List<RelationMappingCapabilityPracticeArea> getByTenantAndProject(String tenantId,String project,String cmmiVersion);
	public String delete(RelationMappingCapabilityPracticeArea entity,AccessObject accessObject,String cmmiVersion) throws BaseException;
}
