package com.login.services;

import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.OrganizationUnitMst;
import com.login.domain.basics.AccessObject;



public interface OrganizationUnitMstService {
	OrganizationUnitMst saveOrUpDate(OrganizationUnitMst entity,AccessObject accessObject);
	List<OrganizationUnitMst> getAllActiveOrganizationUnit();
	List<OrganizationUnitMst> getAllOrganizationUnit(String tenantId);
	OrganizationUnitMst getByName(String tenantId,String organizationUnitName);
	public OrganizationUnitMst getUnique(String tenantId,String organizationUnitCode,String process);
	public List<OrganizationUnitMst> getByTenantAndProcess(String tenantId,String process);
	public String delete(OrganizationUnitMst entity,AccessObject accessObject);
	
}
