package com.login.services;

import java.util.List;

import com.login.domain.PracticeWorkProductMst;
import com.login.domain.basics.AccessObject;



public interface PracticeWorkProductMstService {
	PracticeWorkProductMst saveOrUpDate(PracticeWorkProductMst entity,String cmmiVersion);
	List<PracticeWorkProductMst> getAll(String tenantId,String cmmiVersion);
	List<PracticeWorkProductMst> getByPracticeCode(String tenantId,String practiceCode,String organizationUnit,String businessUnit,String project,String cmmiVersion);
	public List<PracticeWorkProductMst> getByTenantAndProcess(String tenantId,String process,String cmmiVersion);
	public List<PracticeWorkProductMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion);
	public PracticeWorkProductMst saveOrUpDate(PracticeWorkProductMst entity,AccessObject accessObject,String cmmiVersion);
	public String delete(PracticeWorkProductMst entity,AccessObject accessObject,String cmmiVersion);
	PracticeWorkProductMst getUnique(String tenantId, String practiceCode, String workProductName, String project,String cmmiVersion);
	
}
