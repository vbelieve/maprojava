package com.login.services;

import java.util.List;
import com.login.domain.RelationMappingCategoryCapability;
import com.login.domain.basics.AccessObject;
import com.login.common.exceptions.BaseException;
import com.login.domain.PracticeMst;



public interface RelationMappingCategoryCapabilityService {
	RelationMappingCategoryCapability saveOrUpDate(RelationMappingCategoryCapability entity,AccessObject accessobject,String cmmiVersion);
	List<RelationMappingCategoryCapability> getByCapability(String tenantId,String capabilityCode,String organizationUnit,String businessUnit,String project,String cmmiVersion);
	List<RelationMappingCategoryCapability> getByCategory(String tenantId,String categoryCode,String organizationUnit,String businessUnit,String project,String cmmiVersion);
	List<RelationMappingCategoryCapability> getAll(String tenantId,String cmmiVersion);
	RelationMappingCategoryCapability getUnique(String tenantId,String categoryCode,String capabilityCode,String process,String organizationUnit,String businessUnit,String project,String cmmiVersion);
	public List<RelationMappingCategoryCapability> getByTenantAndProcess(String tenantId,String process,String cmmiVersion);
	public List<RelationMappingCategoryCapability> getByTenantAndProject(String tenantId,String project,String cmmiVersion);
	public RelationMappingCategoryCapability getByCapabilityByProcess(String tenantId, String capabilityCode,String organizationUnit,String businessUnit,String project,String cmmiVersion);
	public String delete(RelationMappingCategoryCapability entity,AccessObject accessObject,String cmmiVersion) throws BaseException;
}
