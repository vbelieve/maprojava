package com.login.services;

import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.PiidMst;
import com.login.domain.basics.AccessObject;



public interface PiidMstService {
	public PiidMst getUnique(String tenantId,String project, String practiceAreaCode, String practiceCode, String artifect);
	public List<PiidMst> getByProjectPracticeArea(String tenantId,String project,String practiceAreaCode);
	public String delete(PiidMst entity,AccessObject accessObject)	throws BaseException;
	public PiidMst saveOrUpDate(PiidMst entity,AccessObject accessObject)	throws BaseException;
	public List<PiidMst> getByTenantAndProject(String tenantId,String project);

	
}
