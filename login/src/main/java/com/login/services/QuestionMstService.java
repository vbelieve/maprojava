package com.login.services;

import com.login.domain.QuestionMst;
import java.util.List;



public interface QuestionMstService {
	QuestionMst saveOrUpDate(QuestionMst entity);
	List<QuestionMst> getAll(String tenantId);
	public QuestionMst getUnique(String tenantId,String questionId,String process);
	public QuestionMst getByQuestionId(String tenantId,String questionId);	
}
