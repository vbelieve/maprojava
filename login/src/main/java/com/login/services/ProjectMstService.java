package com.login.services;

import com.login.domain.basics.AccessObject;
import java.util.List;

import com.login.common.exceptions.BaseException;
import com.login.domain.ProjectMst;



public interface ProjectMstService {
	ProjectMst saveOrUpDate(ProjectMst entity,AccessObject accessObject);
	List<ProjectMst> getAllProject(String tenantId);
	List<ProjectMst> getAllActiveProjects();
	ProjectMst getByName(String tenantId,String projectName);
	public ProjectMst getUnique(String tenantId,String projectCode,String process,String businessUnitCode);
	public List<ProjectMst> getByTenantAndProcess(String tenantId,String process);
	public ProjectMst getProjectByProjectCode(String tenantId,String projectCode);
	public String delete(ProjectMst entity,AccessObject accessObject);
}
