package com.login.services;

import java.util.List;
import java.util.Map;

import com.login.domain.LookupMst;
import com.login.domain.basics.AccessObject;



public interface LookupMstService {
	LookupMst getByName(String tenantId,String name);
	List<LookupMst> getAllList();
	LookupMst getById(Integer id);
	List<LookupMst> getAllActiveList();
	LookupMst Save(LookupMst obj);
	LookupMst saveOrUpDate(LookupMst obj,AccessObject accessObject);
	LookupMst updateStatus(LookupMst obj);
	List<LookupMst> getLookupMst(String tenantId,String lookupType);
	public List<LookupMst> getAllLookupMst(String tenantId);
	public Map<String,String> getAllLookupType(String tenantId);
	public List<LookupMst> getAllByTenant(String tenantId);
	public LookupMst getLookupByLookupTypeAndValue(String tenantId, String LookupType,String lookupValue);
}
