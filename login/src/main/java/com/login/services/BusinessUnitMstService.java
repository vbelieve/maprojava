package com.login.services;

import java.util.List;

import com.login.domain.BusinessUnitMst;
import com.login.domain.basics.AccessObject;



public interface BusinessUnitMstService {
	BusinessUnitMst saveOrUpDate(BusinessUnitMst entity,AccessObject accessObject);
	List<BusinessUnitMst> getAllActiveBusinessUnit();
	List<BusinessUnitMst> getAllBusinessUnit(String tenantId);
	BusinessUnitMst getByName(String tenantId,String businessUnitName);
	public BusinessUnitMst getUnique(String tenantId,String businessUnitCode,String process);
	public List<BusinessUnitMst> getByTenantAndProcess(String tenantId,String process);
	public String delete(BusinessUnitMst entity,AccessObject accessObject);
	
}
