package com.login.services;

import java.util.List;

import com.login.domain.SequenceMst;
import com.login.domain.basics.AccessObject;
import com.login.domain.CategoryMst;



public interface SequenceMstService {
	List<SequenceMst> getAllList();
	SequenceMst saveOrUpDate(SequenceMst obj,AccessObject accessObject);
	SequenceMst getSequenceMst(String tenantId,String sequenceType);
	public String getNextSeqNo(String tenantId, String seqType) throws Exception;
	public List<SequenceMst> getAll(String tenantId);
}
