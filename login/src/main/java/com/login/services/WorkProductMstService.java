package com.login.services;

import com.login.common.exceptions.BaseException;
import com.login.domain.WorkProductMst;
import com.login.domain.basics.AccessObject;

import java.util.List;



public interface WorkProductMstService {
	WorkProductMst saveOrUpDate(WorkProductMst entity,AccessObject accessObject,String cmmiVersion);
	List<WorkProductMst> getAll(String tenantId,String cmmiVersion);
	public List<WorkProductMst> getByTenantAndProcess(String tenantId,String process,String cmmiVersion);
	public List<WorkProductMst> getByTenantAndProject(String tenantId,String project,String cmmiVersion);
	public WorkProductMst getUnique(String tenantId,String workProductId,String process,String organizationUnit,String businessUnit,String project,String cmmiVersion);
	public String delete(WorkProductMst entity,AccessObject accessObject,String cmmiVersion) throws BaseException;	
}
