<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.PiidMst"%>
<%@page import="com.login.domain.PracticeAreaMst"%>
<%@page import="com.login.domain.PracticeMst"%>
<%@page import="java.util.*"%>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="theme-vora/images/logo-Client.png">
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/jquery-smartwizard/dist/css/smart_wizard.min.css" rel="stylesheet">
<link href="theme-vora/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
	<link rel="stylesheet" href="theme-vora/vendor/chartist/css/chartist.min.css">
	<!-- Vectormap -->
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="theme-vora/css/style.css" rel="stylesheet">
	<link href="theme-vora/vendor/owl-carousel/owl.carousel.css" rel="stylesheet">

  <title>Admin</title>

  <!-- Custom fonts for this template -->

</head>

<body id="page-top">
<%
String msg=(String) request.getAttribute("msg");
String[] messageType=null;
String alertType="alert alert-info alert-dismissible fade show";
String alertStr="Info!";

if(null==msg || msg.equalsIgnoreCase(""))
{
	msg="";
}
else
{
System.out.println("msg>>>>>>>>>>>>>>>>>>"+msg);	
messageType=msg.split("~");
msg=messageType[1];
}

if(!msg.equalsIgnoreCase("") && messageType.length>1)
{
	if(messageType[0].equalsIgnoreCase("SUCCESS"))
	{
	alertType="alert alert-success alert-dismissible fade show";
	alertStr="Success!";
	}
	else
	{
	alertType="alert alert-danger alert-dismissible fade show";
	alertStr="Error!";
	}
}
List<PiidMst>allPiid= (List<PiidMst>) request.getAttribute("allPiid");
List<PracticeAreaMst> practiceAreaList=(List<PracticeAreaMst>) request.getAttribute("allPracticeArea");
List<PracticeMst> practiceList=(List<PracticeMst>) request.getAttribute("allPractices");
if(null==practiceList)
{
	practiceList=new ArrayList<PracticeMst>();
}
PracticeAreaMst practiceAreaMst=new PracticeAreaMst();
if(null==practiceAreaList)
{
	
}
else
{
practiceAreaMst=practiceAreaList.get(0);
}

if(null==allPiid)
{
	allPiid=new ArrayList<PiidMst>();
}
Integer srNo=0;

%>
<div id="main-wrapper">


<%@include file="preloader.jsp"%>
<%@include file="navheader.jsp"%>
<%@include file="head.jsp"%>
<%@include file="sidebar.jsp"%>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
			<% if (!msg.equalsIgnoreCase(""))
			{
			%>
			<div class="<%=alertType%>">
									<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
									
									<strong><%=alertStr%></strong> <%=msg%>
									<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                    </button>
								</div>
								
								<%
			}
								%>
				<!-- Add Order -->
				<div class="modal fade" id="addOrderModalside">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Project</h5>
								<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form>
									<div class="form-group">
										<label class="text-black font-w500">Project Name</label>
										<input type="text" class="form-control">
									</div>
									<div class="form-group">
										<label class="text-black font-w500">Deadline</label>
										<input type="date" class="form-control">
									</div>
									<div class="form-group">
										<label class="text-black font-w500">Client Name</label>
										<input type="text" class="form-control">
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-primary">CREATE</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
                <!--<div class="page-titles">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Profile</a></li>
					</ol>
                </div>
                
				<!---------Body Start of Form--------->
			<div class="row">
                    <div class="col-xl-12 col-xxl-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">(<%=practiceAreaMst.getPracticeAreaCode()%>) <%=practiceAreaMst.getPracticeAreaName()%></h4>
                            </div>
                            <div class="card-body">
								
									<form role="form" class="form" action = "/GapAnalysis-2.0/piid/save" id="categoryRegisration" method="post">
									<input type="hidden" id="msg" value="<%=msg%>">
									<input type="hidden" id="mode" name="mode" value="CREATE">
									<input type="hidden" id="tenantId" name="tenantId" value="<%=accessObject.getTenantId()%>">
									<input type="hidden" id="project" name="project" value="<%=accessObject.getProject()%>">
										<div id="wizard_Service" class="tab-pane" role="tabpanel">
										
											<div class="row">
												
												<div hidden class="col-lg-3 mb-2">
													<div class="form-group">
														<label class="text-label">Practice Area Code</label>
														<input type="text" name="practiceAreaCode" id="practiceAreaCode" value="<%=practiceAreaMst.getPracticeAreaCode()%>" class="form-control" placeholder="" required>
													</div>
												</div>
												<div class="col-lg-2 mb-2">
													<div class="form-group">
														<label class="text-label">Practice Code</label>
													<!--	<input type="text" class="form-control" id="practiceCode" name="practiceCode" aria-describedby="categoryName" placeholder="" required>-->
												
											<select class="form-control" id="practiceCode" name="practiceCode">
																<option value="Select"  >Select</option>
																<%
										for (PracticeMst entry:practiceList) {
											
											
									%>
									<option  value="<%=entry.getPracticeCode()%>"><%=entry.getPracticeCode()%></option>
									<%
									}
										
									%>
								
									
								</select>
									</div>
												</div>
												
							
												<div class="col-lg-8 mb-2">
													<div class="form-group">
														<label class="text-label">Practice Description</label>
														<input type="text" class="form-control" id="practiceDescription" name="practiceDescription" aria-describedby="categoryName" placeholder="" required>
														
													</div>
												</div>
												<div class="col-lg-2 mb-2">
													<div class="form-group">
														<label class="text-label">Level</label>
														<input type="text" id="level" " name="level" class="form-control" value="" readonly placeholder="" >
													</div>
												</div>
												<div class="col-lg-12 mb-2">
													<div class="form-group">
														<label class="text-label">Artifect</label>
														<input type="text" class="form-control" id="artifect" name="artifect" aria-describedby="categoryName" placeholder="" required>
													</div>
												</div>
												
											</div>
											<button type="submit" class="btn btn-primary btn sweet-text" id="commit" name="commit" >ADD</button>
											
										</div>
										
									
									
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            
				<!---------Body End of Form------------>
				<!---------Body Start of Tables--------->
				    <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Additional PIID Details</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example3" class="display min-w850">
                                        <thead>
                                            <tr>
                                           
												
                                                <th>Practice Area</th>
                                                <th>Practice</th>
                                                 <th>Level</th>
												  <th>Artifect</th>
												  <th> Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<%
for(PiidMst entry:allPiid)
{
srNo=srNo+1;
%>
<tr>
                                            
												<td><%=entry.getPracticeAreaCode()%></td>
												<td><%=entry.getPracticeCode()%></td>
												<td><%=entry.getLevel()%></td>
                                                <td><%=entry.getArtifect()%></td>
												
                                                <td>
													<div class="d-flex">
													<a onclick="deletePid('<%=entry.getTenantId()%>',
													'<%=entry.getLevel()%>', '<%=entry.getPracticeAreaCode()%>','<%=entry.getPracticeCode()%>'
													,'<%=entry.getArtifect()%>','DELETE')" class="btn btn-danger shadow btn-xs sharp">
													<i class="fa fa-trash"></i></a>
													</div>												
												</td>												
                                            </tr>
<%
}
%>
                                            
											
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            
				<!---------Body End of Tables--------->
				</div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


</div>
<script src="theme-vora/vendor/global/global.min.js"></script>
	<script src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="theme-vora/js/custom.min.js"></script>
	<script src="theme-vora/js/dlabnav-init.js"></script>
	<script src="theme-vora/vendor/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="theme-vora/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="theme-vora/js/plugins-init/jquery.validate-init.js"></script>

<script src="theme-vora/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js"></script>
<script src="theme-vora/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="theme-vora/js/plugins-init/datatables.init.js"></script>	
<script src="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="theme-vora/js/plugins-init/sweetalert.init.js"></script>
	
<script type="text/javascript">
function updateCategoryDetails(tid,cCode,cName,cisactive,mode){
$("#tenantId").val(tid);
$("#categoryCode").val(cCode);
$("#categoryName").val(cName);
$("#isActive").val(cisactive);
$("#mode").val(mode);
document.getElementById('commit').innerHTML = mode;
window.scrollTo(0,0);

} 
</script>		

<script type="text/javascript">
function deletePid(tenantId,level,practiceAreaCode,practiceCode,artifect,mode){
$("#tenantId").val(tenantId);
$("#level").val(level);
$("#practiceAreaCode").val(practiceAreaCode);
$("#practiceCode").val(practiceCode);
$("#artifect").val(artifect);
$("#mode").val(mode);
document.forms["categoryRegisration"].action = "/GapAnalysis-2.0/piid/save";
document.forms["categoryRegisration"].submit();	

}
</script>


<script type="text/javascript">
	$("#practiceCode").change(function(){
	var urlToSend="practiceDetailsAjax";
	var practiceCode=$("#practiceCode").val();
	var project=$("#project").val();
	var tenantId=$("#tenantId").val();
	$.ajax({
		 type: "POST",
		url:urlToSend,
		data: {practiceCode:practiceCode,project : project,tenantId:tenantId},
	
    dataType: "json",
		success:function(jsonResponse)
		{
		$("#practiceDescription").val(jsonResponse.practiceName);
	$("#level").val(jsonResponse.practiceLevelCode);

			
		},
		error:function(e)
		{
		
		}
	});
	
}).change();
</script>

<script type="text/javascript">
function updateMode(mode){
$("#mode").val(mode);
document.getElementById('commit').innerHTML = mode;

window.scrollTo(0,0);

} 
</script>		
</body>


</html>
