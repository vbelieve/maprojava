<!DOCTYPE html>
<%@page import="com.login.domain.CategoryMst"%>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.UserMst"%>
<%@page import="com.login.domain.TenantMst"%>
<%@page import="com.login.domain.BusinessUnitMst"%>
<%@page import="com.login.domain.OrganizationUnitMst"%>
<%@page import="com.login.domain.ProjectMst"%>
<%@page import="com.login.domain.PracticeMst"%>
<%@page import="com.login.domain.PracticeAreaMst"%>
<%@page import="com.login.domain.CapabilityMst"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="java.lang.*"%>
<%@page import="com.login.domain.basics.DashBoardVo"%>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="theme-vora/images/logo-Client.png">
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/jquery-smartwizard/dist/css/smart_wizard.min.css" rel="stylesheet">
<link href="theme-vora/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
	<link rel="stylesheet" href="theme-vora/vendor/chartist/css/chartist.min.css">
	<!-- Vectormap -->
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="theme-vora/css/style.css" rel="stylesheet">
	<link href="theme-vora/vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="customized/css/new-style.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>

  <title>Admin</title>

  <!-- Custom fonts for this template -->

</head>

<body id="page-top">
<div id="main-wrapper">

<%@include file="preloader.jsp"%>
<%@include file="navheader.jsp"%>
<%@include file="head.jsp"%>
<%@include file="sidebar.jsp"%>
<%
String projectName=(String) request.getAttribute("projectName");
		String msg = (String) request.getAttribute("msg");
		Integer totalPracticeComplianceScore=0;
		Integer totalPracticeScore=0;
		
		if (null == msg) {
			msg = "";
		}
		
		List<DashBoardVo> dashBoardVoPracticeArea=new ArrayList<DashBoardVo>();
		List<DashBoardVo> dashBoardVoCategory=new ArrayList<DashBoardVo>();
		List<DashBoardVo> dashBoardVoCapability=new ArrayList<DashBoardVo>();

if(request.getAttribute("dashBoardVoCategory") != null )
{
	dashBoardVoCategory=(List<DashBoardVo>) request.getAttribute("dashBoardVoCategory");
	
}

if(request.getAttribute("dashBoardVoCapability") != null )
{
	dashBoardVoCapability=(List<DashBoardVo>) request.getAttribute("dashBoardVoCapability");
	
}
		
if(request.getAttribute("dashBoardVoPracticeArea") != null )
{
	dashBoardVoPracticeArea=(List<DashBoardVo>) request.getAttribute("dashBoardVoPracticeArea");
	
	
}

		List<PracticeMst> allPractice = (List<PracticeMst>) request.getAttribute("allPractice");
		
		if (null == allPractice) {
			allPractice = new ArrayList<PracticeMst>();
		}
		List<ProjectMst> allProjects = (List<ProjectMst>) request.getAttribute("allProjects");
		String projectCode=(String) request.getAttribute("projectCode");
		List<PracticeMst> projectPractices = (List<PracticeMst>) request.getAttribute("projectPractices");
		if(null==projectPractices)
		{
			projectPractices = new ArrayList<PracticeMst>();
		}
		if (null == allProjects) {
			allProjects = new ArrayList<ProjectMst>();
		}
		List<CategoryMst> allCategory=(List<CategoryMst>) request.getAttribute("allCategory");
		if(null==allCategory)
		{
			allCategory=new ArrayList<CategoryMst>();
		}
		
		if(allCategory.size()>0)
		{
		for( CategoryMst category:allCategory)
			{
				for(PracticeMst pp:projectPractices)
				{
					if(pp.getCategoryCode().equalsIgnoreCase(category.getCategoryCode()))
					{
						category.setComplianceScore( category.getComplianceScore()+ ((int)Math.round(pp.getPracticeCompliance())));
						category.setTotalComplianceScore(category.getTotalComplianceScore()+ 100);
					}
				}
				if(category.getTotalComplianceScore()==0)
				{
					category.setTotalComplianceScore(100);
				}
			}
			for( CategoryMst category:allCategory)
			{
				//System.out.println(">>>>"+category.getComplianceScore()+">>>>>"+category.getTotalComplianceScore());
			category.setComplianceScore(((category.getComplianceScore()*100)/category.getTotalComplianceScore()));
			}
		}
		List<CapabilityMst> allCapability=(List<CapabilityMst>) request.getAttribute("allCapability");
		if(null==allCapability)
		{
			allCapability=new ArrayList<CapabilityMst>();
		}
		
		if(allCapability.size()>0)
		{
		for( CapabilityMst capability:allCapability)
			{
				for(PracticeMst pp:projectPractices)
				{
					if(pp.getCapabilityCode().equalsIgnoreCase(capability.getCapabilityCode()))
					{
						capability.setComplianceScore( capability.getComplianceScore()+ ((int)Math.round(pp.getPracticeCompliance())));
						capability.setTotalComplianceScore(capability.getTotalComplianceScore()+ 100);
					}
				}
				if(capability.getTotalComplianceScore()==0)
				{
				capability.setTotalComplianceScore(100);
				}
				
			}
			for( CapabilityMst capability:allCapability)
			{
				//System.out.println(">>>>"+capability.getComplianceScore()+">>>>>>>"+capability.getTotalComplianceScore());
				capability.setComplianceScore(((capability.getComplianceScore()*100)/capability.getTotalComplianceScore()));
			}
		}
		
				List<PracticeAreaMst> allPracticeArea=(List<PracticeAreaMst>) request.getAttribute("allPracticeArea");
				if(null==allPracticeArea)
				{
					allPracticeArea=new ArrayList<PracticeAreaMst>();
				}
		
		if(allPracticeArea.size()>0)
		{
		for( PracticeAreaMst entry:allPracticeArea)
			{
				for(PracticeMst pp:projectPractices)
				{
					if(pp.getPracticeAreaCode().equalsIgnoreCase(entry.getPracticeAreaCode()))
					{


						totalPracticeComplianceScore=totalPracticeComplianceScore+((int)Math.round(pp.getPracticeCompliance()));
						totalPracticeScore=totalPracticeScore+100;
						//System.out.println("Practice  " +pp.getPracticeCode()+"|Compliance Score|"+((int)Math.round(pp.getPracticeCompliance())));
						
						entry.setComplianceScore( entry.getComplianceScore()+ ((int)Math.round(pp.getPracticeCompliance())));
						entry.setTotalComplianceScore( entry.getTotalComplianceScore()+ 100);
						
					}
					
				}
				if(entry.getTotalComplianceScore()==0)
				{
					entry.setTotalComplianceScore( entry.getTotalComplianceScore()+ 100);
				}
				
			}
		}
		System.out.println("Total COmpliance|"+totalPracticeComplianceScore+"|Total Score|"+totalPracticeScore);
		Integer projectScore=(int)Math.round(((totalPracticeComplianceScore)*100)/((totalPracticeScore)));
		System.out.println("Project COmpliance|"+projectScore);
		if(allPracticeArea.size()>0)
		{
		for(PracticeAreaMst entry:allPracticeArea)
			{
				entry.setComplianceScore(((entry.getComplianceScore()*100)/entry.getTotalComplianceScore()));
				
			}
		}
		
		

												String color = "primary";
												
			
												if (projectScore > 0 && projectScore < 20) {
													color = "danger";
												}
												if (projectScore > 21 && projectScore < 40) {
													color = "secondary";
												}
												if (projectScore > 41 && projectScore < 60) {
													color = "warning";
												}
												if (projectScore > 61 && projectScore < 80) {
													color = "primary";
												}
												if (projectScore > 81 && projectScore < 99) {
													color = "info";
												}
												if (projectScore > 98) {
													color = "success";
												}									
		Integer srNo = 0;
		String tidd = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
%>


        <!--**********************************
            Content body start
        ***********************************-->
<div class="content-body" style="min-height: 788px;">
            <!-- row -->
			<div class="container-fluid">
				<!-- Add Order -->
				
												
	 <div class="row">
	 
	 
	 <div class="col-xl-6 col-xxl-11 col-md-6">
								<div class="card">
									<div class="card-header shadow-sm">
										<h4>Compliance</h4>
									</div>
									<div class="card-body" id="RecentActivitiesContent">
										
										<div class="border-bottom pb-4 mb-4">
										
											
											<p class="font-w600"><a href="post-details.html" class="text-black"><%=projectName%></a></p>
											<div class="row">
												<ul class="users col-6">
													
												</ul>
												<div class="col-6 pl-0">
													<h6 class="fs-14">Progress
														<span class="pull-right font-w600"><%=projectScore%>%</span>
													</h6>
													<div class="progress" style="height:7px;">
														<div class="progress-bar bg-primary progress-animated" style="width: <%=projectScore%>%; height:7px;" role="progressbar">
															<span class="sr-only"><%=projectScore%>% Complete</span>
														</div>
													</div>
													
												</div>
												
											</div>
											
										</div>
										<div class="row">
											<a onclick="selectedProject();" class="btn btn-sm btn-success rounded-xl mb-2">Generate Summary Report</a>
												&nbsp
												<a onclick="generatePiid();" class="btn btn-sm btn-success rounded-xl mb-2">Generate PIID Report</a>
												&nbsp
												<a onclick="selectedPPT();" class="btn btn-sm btn-success rounded-xl mb-2">Generate PPT Report</a>
												
											</div>
									<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 620px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 541px;"></div></div></div>
									
								</div>
								
							</div>
							

							<div class="col-xl-3 col-lg-6 col-sm-6">
						<div class="widget-stat card bg-secondary">
							<div class="card-body p-4">
								<div class="media">
									
									<div class="media-body text-white">

										<h4 class="text-white mb-1">Capability</h4>
										<br><br>
										<%
									for(DashBoardVo dd:dashBoardVoCapability)
									{
										%>
										
										<p class="text-white"><%=dd.getCapabilityCode()%>-<%=dd.getComplianceScore()%>%</p>
										<div class="progress mb-2 bg-primary">
                                            <div class="progress-bar progress-animated bg-light" style="width: <%=dd.getComplianceScore()%>%"></div>
                                        </div>
										
										<%
									}
										%>
									</div>
								</div>
							</div>
						</div>
                    </div>
						


						
					
			<!-- Chart-->
			<div class="col-xl-8 col-xxl-8 col-lg-12 col-sm-12" >
                        <div id="user-activity" class="card">
                            <div class="card-header border-0 pb-0 d-sm-flex d-block">
                                <h4 class="card-title">Compliance Details</h4>
                                <div class="card-action mb-sm-0 my-2">
                                    
                                </div>
                            </div>

                            <div  class="card-body">
                                <div class="tab-content" id="myTabContent">
                                   <canvas id="practiceAreaChart" style="display: block; width: 594px; height: 297px;" width="594" height="297" class="chartjs-render-monitor"></canvas>
                                </div>
                            </div>
							
							
							<div  class="card-body">
                                <div class="tab-content" id="myTabContent">
                                   <canvas id="categoryChart" style="display: block; width: 594px; height: 297px;" width="594" height="297" class="chartjs-render-monitor"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
			<!-- Chart-->
			<!--Chart-->
			<!--Chart-->
			</div>
									<div hidden class="card-body">
                                <div hidden class="table-responsive">
                                    <table id="practiceAreaTable" class="display min-w850">
									<thead>
                                            <tr>
                                                <th>PracticeArea</th>
                                                <th>Score</th>
                                                 
                                            </tr>
                                        </thead>
                                        <tbody>
										<%for(DashBoardVo db2:dashBoardVoPracticeArea)
										{

											%>
										<tr>
										
										<td><%=db2.getPracticeAreaCode()%></td>
										<td><%=db2.getComplianceScore()%></td>
										</tr>
											<%
											}
											%>
										</tbody>
									</table>
									</div>
									</div>
									<div hidden class="card-body">
                                <div  class="table-responsive">
                                    <table id="categoryTable" class="display min-w850">
									<thead>
                                            <tr>
                                                <th>Category</th>
                                                <th>Score</th>
                                                 
                                            </tr>
                                        </thead>
                                        <tbody>
										<%for(DashBoardVo db:dashBoardVoCategory)
										{
											%>
										<tr>
										
										<td><%=db.getCategoryCode()%></td>
										<td><%=db.getComplianceScore()%></td>
										</tr>
											<%
											}
											%>
										</tbody>
									</table>
									</div>
									</div>
									<div hidden class="card-body">
                                <div class="table-responsive">
                                    <table id="capabilityTable" class="display min-w850">
									<thead>
                                            <tr>
                                                <th>Capability</th>
                                                <th>Score</th>
                                                 
                                            </tr>
                                        </thead>
                                        <tbody>
										<%for(DashBoardVo db:dashBoardVoCapability)
										{
											%>
										<tr>
										
										<td><%=db.getCapabilityCode()%></td>
										<td><%=db.getComplianceScore()%></td>
										</tr>
											<%
											}
											%>
										</tbody>
									</table>
									</div>
									</div>
									
		  
					 
		</div>  
  <!--**********************************
            Content body end
        ***********************************-->
<form id="projectSelect" action="#" method="post" hidden>
<input type="hidden" id="tenantId" name="tenantId" value="">
	<input type="hidden" id="project" name="project" value="<%=projectCode%>">
	<input type="hidden" id="selectedtenantId" name="selectedtenantId" value="<%=projectCode%>">

</form>
</div>

<script type="text/javascript">
$(document).ready(function() {
	
	processProjectPracticeArea('practiceAreaTable','practiceAreaChart','Practice Area');
	processProjectCategory('categoryTable','categoryChart','Category');
	processProjectCapability('capabilityTable','CapabilityChart','Capability');
	 });
</script>


<script type="text/javascript">
function processProjectCategory(tableName,chartId,selectionCode){
	
var table = document.getElementById(""+tableName);

var json = []; 
var headers =[];
for (var i = 0; i < table.rows[0].cells.length; i++) {
  headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');
  
}

// Go through cells 
for (var i = 1; i < table.rows.length; i++) {
  var tableRow = table.rows[i];
  var rowData = {};
  for (var j = 0; j < tableRow.cells.length; j++) {
    rowData[headers[j]] = tableRow.cells[j].innerHTML;
	
  }
console.log(rowData);
  json.push(rowData);
}
var labels = json.map(function (e) {
  return e.category;
});
console.log(labels); // ["2016", "2017", "2018", "2019"]

// Map JSON values back to values array
var values = json.map(function (e) {
  return Math.round(e.score)+"";
});
console.log(values); // ["10", "25", "55", "120"]
var chart = BuildChart(labels, values, selectionCode,chartId);

}
</script>



<script type="text/javascript">
function processProjectCapability(tableName,chartId,selectionCode){
	
var table = document.getElementById(""+tableName);

var json = []; 
var headers =[];
for (var i = 0; i < table.rows[0].cells.length; i++) {
  headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');
  
}

// Go through cells 
for (var i = 1; i < table.rows.length; i++) {
  var tableRow = table.rows[i];
  var rowData = {};
  for (var j = 0; j < tableRow.cells.length; j++) {
    rowData[headers[j]] = tableRow.cells[j].innerHTML;
	
  }
console.log(rowData);
  json.push(rowData);
}
var labels = json.map(function (e) {
  return e.capability;
});
console.log(labels); // ["2016", "2017", "2018", "2019"]

// Map JSON values back to values array
var values = json.map(function (e) {
  return Math.round(e.score)+"";
});
console.log(values); // ["10", "25", "55", "120"]
var chart = BuildChart(labels, values, selectionCode,chartId);

}
</script>


<script type="text/javascript">
function processProjectPracticeArea(tableName,chartId,selectionCode){
	
var table = document.getElementById(""+tableName);

var json = []; 
var headers =[];
for (var i = 0; i < table.rows[0].cells.length; i++) {
  headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');
  
}

// Go through cells 
for (var i = 1; i < table.rows.length; i++) {
  var tableRow = table.rows[i];
  var rowData = {};
  for (var j = 0; j < tableRow.cells.length; j++) {
    rowData[headers[j]] = tableRow.cells[j].innerHTML;
	
  }
console.log(rowData);
  json.push(rowData);
}
var labels = json.map(function (e) {
  return e.practicearea;
});
console.log(labels); // ["2016", "2017", "2018", "2019"]

// Map JSON values back to values array
var values = json.map(function (e) {
  return Math.round(e.score)+"";
});
console.log(values); // ["10", "25", "55", "120"]

var chart = BuildChart(labels, values, selectionCode,chartId);

}
</script>


<script type="text/javascript">
function selectedProject(pid){
$("#project").val(pid);
document.forms["masterRegistration"].action = "/GapAnalysis-2.0/selectDashboardProject";
document.forms["masterRegistration"].submit();	

}
</script>



<script type="text/javascript">
function BuildChart(labels, values, chartTitle,chartId) {
	
  var ctx = document.getElementById(chartId).getContext('2d');
  var data = {
		labels: labels, // Our labels
		datasets: [{
        label: chartTitle, // Name the series
        data: values, // Our values
		borderWidth: "0",
        backgroundColor: 'blue',
        borderColor: 'blue',
        borderWidth: 1 // Specify bar border width}]
}]}
  
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
      "hover": {
      "animationDuration": 0
    },
	"animation": {
      "duration": 1,
      "onComplete": function() {
        var chartInstance = this.chart,
          ctx = chartInstance.ctx;

         ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
         ctx.textAlign = 'center';
         ctx.textBaseline = 'bottom';

        this.data.datasets.forEach(function(dataset, i) {
          var meta = chartInstance.controller.getDatasetMeta(i);
          meta.data.forEach(function(bar, index) {
            var data = dataset.data[index]+"%";
            ctx.fillText(data, bar._model.x, bar._model.y - 5);
          });
        });
      }
    },
	events: ['blur','click'],
      responsive: true, // Instruct chart js to respond nicely.
      maintainAspectRatio: false, // Add to prevent default behavior of full-width/height 
	  scales: {
					xAxes: [{
						display: true, 
						stacked: true,
						barPercentage: 1, 
						barThickness: 20, 
						ticks: {
							display: true
						}, 
						gridLines: {
							display: false, 
							drawBorder: false
						}
					}],
					yAxes: [{
						display: true, 
						stacked: false, 
						gridLines: {
							display: true, 
							drawBorder: true
						}, 
						ticks: {
							display: true, 
							max: 100, 
							min: 0
						}
					}]
				}
    }
  });
  return myChart;
}
</script>

<script type="text/javascript">
function selectedProject(){
document.forms["projectSelect"].action = "/GapAnalysis-2.0/generateProjectSummaryReport";
document.forms["projectSelect"].submit();	

}
</script>

<script type="text/javascript">
function selectedPPT(){
document.forms["projectSelect"].action = "/GapAnalysis-2.0/generatePPT";
document.forms["projectSelect"].submit();	

}
</script>


<script type="text/javascript">
function generatePiid(){
document.forms["projectSelect"].action = "/GapAnalysis-2.0/generatePiidReport";
document.forms["projectSelect"].submit();	

}
</script>

<script src="theme-vora/vendor/global/global.min.js"></script>
	
	<script src="theme-vora/js/custom.min.js"></script>
	<script src="theme-vora/js/dlabnav-init.js"></script>
	<script src="theme-vora/vendor/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="theme-vora/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="theme-vora/js/plugins-init/jquery.validate-init.js"></script>

<script src="theme-vora/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js"></script>
<script src="theme-vora/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="theme-vora/js/plugins-init/datatables.init.js"></script>	
<script src="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="theme-vora/js/plugins-init/sweetalert.init.js"></script>

<script src="theme-vora/vendor/chart.js/Chart.bundle.min.js"></script>

	
    <!-- Init file -->
    <!--<script src="theme-vora/js/plugins-init/widgets-script-init.js"></script>-->

</script>		
</body>
</html>