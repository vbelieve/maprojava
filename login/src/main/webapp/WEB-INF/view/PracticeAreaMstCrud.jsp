<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.PracticeAreaMst"%>
<%@page import="com.login.domain.LookupMst"%>
<%@page import="java.util.*"%>

<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>Mapro</title>
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16"
	href="theme-vora/images/logo-Client.png">
<link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css"
	rel="stylesheet">
<link href="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.css"
	rel="stylesheet">
<link
	href="theme-vora/vendor/jquery-smartwizard/dist/css/smart_wizard.min.css"
	rel="stylesheet">
<link href="theme-vora/vendor/datatables/css/jquery.dataTables.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="theme-vora/vendor/chartist/css/chartist.min.css">
<!-- Vectormap -->
<link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css"
	rel="stylesheet">
<link
	href="theme-vora/vendor/bootstrap-select/dist/css/bootstrap-select.min.css"
	rel="stylesheet">
<link href="theme-vora/css/style.css" rel="stylesheet">
<link href="theme-vora/vendor/owl-carousel/owl.carousel.css"
	rel="stylesheet">

<title>Admin</title>

<!-- Custom fonts for this template -->

</head>

<body id="page-top">
	<%
		String msg = (String) request.getAttribute("msg");
		String[] messageType = null;
		String alertType = "alert alert-info alert-dismissible fade show";
		String alertStr = "Info!";

		if (null == msg || msg.equalsIgnoreCase("")) {
			msg = "";
		} else {
			System.out.println("msg>>>>>>>>>>>>>>>>>>" + msg);
			messageType = msg.split("~");
			msg = messageType[1];
		}

		if (!msg.equalsIgnoreCase("") && messageType.length > 1) {
			if (messageType[0].equalsIgnoreCase("SUCCESS")) {
				alertType = "alert alert-success alert-dismissible fade show";
				alertStr = "Success!";
			} else {
				alertType = "alert alert-danger alert-dismissible fade show";
				alertStr = "Error!";
			}
		}

		List<PracticeAreaMst> objectList = (List<PracticeAreaMst>) request.getAttribute("objectList");
		List<LookupMst> businessModelLookup = new ArrayList<LookupMst>();
		businessModelLookup = (List<LookupMst>) request.getAttribute("businessModelLookup");
		if (null == businessModelLookup) {
			businessModelLookup = new ArrayList<LookupMst>();
		}

		if (null == objectList) {
			objectList = new ArrayList<PracticeAreaMst>();
		}
		Integer srNo = 0;
	%>

	<div id="main-wrapper">


		<%@include file="preloader.jsp"%>
		<%@include file="navheader.jsp"%>
		<%@include file="head.jsp"%>
		<%@include file="sidebar.jsp"%>
		<!--**********************************
            Content body start
        ***********************************-->
		<div class="content-body">
			<div class="container-fluid">
				<%
					if (!msg.equalsIgnoreCase("")) {
				%>
				<div class="<%=alertType%>">
					<svg viewBox="0 0 24 24" width="24" height="24"
						stroke="currentColor" stroke-width="2" fill="none"
						stroke-linecap="round" stroke-linejoin="round" class="mr-2">
						<circle cx="12" cy="12" r="10"></circle>
						<line x1="12" y1="16" x2="12" y2="12"></line>
						<line x1="12" y1="8" x2="12.01" y2="8"></line></svg>

					<strong><%=alertStr%></strong>
					<%=msg%>
					<button type="button" class="close h-100" data-dismiss="alert"
						aria-label="Close">
						<span><i class="mdi mdi-close"></i></span>
					</button>
				</div>

				<%
					}
				%>
				<!-- Add Order -->
				<div class="modal fade" id="addOrderModalside">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Create Project</h5>
								<button type="button" class="close" data-dismiss="modal">
									<span>&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form action="">
									<div class="form-group">
										<label class="text-black font-w500">Project Name</label> <input
											type="text" class="form-control">
									</div>
									<div class="form-group">
										<label class="text-black font-w500">Deadline</label> <input
											type="date" class="form-control">
									</div>
									<div class="form-group">
										<label class="text-black font-w500">Client Name</label> <input
											type="text" class="form-control">
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-primary">CREATE</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!--<div class="page-titles">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Profile</a></li>
					</ol>
                </div>
                
				<!---------Body Start of Form--------->
				<div class="row">
					<div class="col-xl-12 col-xxl-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Practice Area</h4>
							</div>
							<div class="card-body">

								<form role="form" class="form"
									action="/GapAnalysis-2.0/practiceArea/save"
									id="masterRegistration" method="post">
									<input type="hidden" id="msg" value="<%=msg%>"> <input
										type="hidden" id="businessModel" name="businessModel" value="">
									<input type="hidden" id="mode" name="mode" value="CREATE">
									<div id="wizard_Service" class="tab-pane" role="tabpanel">

										<div class="row">
											<div class="col-lg-6 mb-2">
												<div class="form-group">
													<label class="text-label">Organization Id</label> <input
														type="text" id="tenantId" " name="tenantId"
														class="form-control" value=<%=accessObject.getTenantId()%> readonly placeholder="">
												</div>
											</div>
											<div class="col-lg-6 mb-2">
												<div class="form-group">
													<label class="text-label">Practice Area*</label> <input
														type="text" name="practiceAreaCode" id="practiceAreaCode"
														class="form-control" placeholder="" required>
												</div>
											</div>
											<div class="col-lg-12 mb-2">
												<div class="form-group">
													<label class="text-label">Practice Area's Name*</label> <input
														type="text" class="form-control" id="practiceAreaName"
														name="practiceAreaName" aria-describedby="categoryName"
														placeholder="" required>
												</div>
											</div>
											<div class="col-lg-12 mb-2">
												<div class="form-group">
													<label class="text-label">Additional Info</label> <input
														type="text" class="form-control"
														id="practiceAreaAdditionalInfo"
														name="practiceAreaAdditionalInfo"
														aria-describedby="categoryName" placeholder="" required>
												</div>
											</div>
											<div class="col-lg-12 mb-2">
												<div class="form-group">
													<label class="text-label">Intent</label> <input type="text"
														class="form-control" id="practiceAreaIntent"
														name="practiceAreaIntent" aria-describedby="categoryName"
														placeholder="" required>
												</div>
											</div>
											<div class="col-lg-12 mb-2">
												<div class="form-group">
													<label class="text-label">Value</label> <input type="text"
														class="form-control" id="practiceAreaValue"
														name="practiceAreaValue" aria-describedby="categoryName"
														placeholder="" required>
												</div>
											</div>
											<input type="hidden" class="form-control" id="authStatus"
												name="authStatus"> <input type="hidden"
												class="form-control" id="organizationUnit"
												name="organizationUnit"> <input type="hidden"
												class="form-control" id="process" name="process"> <input
												type="hidden" class="form-control" id="project"
												name="project"> <input type="hidden"
												class="form-control" id="capabilityCode"
												name="capabilityCode"> <input type="hidden"
												class="form-control" id="categoryCode" name="categoryCode">

											<div class="form-group">
												<label>Domain View</label> <select required multiple
													class="form-control" id="businessModel1"
													name="businessModel1">
													<%
														for (LookupMst entry : businessModelLookup) {
													%>
													<option value="<%=entry.getLookupValue()%>"><%=entry.getLookupValue()%></option>
													<%
														}
													%>
												</select>
											</div>
										</div>
										<button type="submit" class="btn btn-primary btn sweet-text"
											id="commit" name="commit">CREATE</button>

									</div>



								</form>
							</div>
						</div>
					</div>
				</div>

				<!---------Body End of Form------------>
				<!---------Body Start of Tables--------->
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Practice Area Details</h4>
							</div>
							<div class="card-body">
								<div class="table-responsive">
									<table id="example3" class="display min-w850">
										<thead>
											<tr>

												<th>Sr No</th>
												<th>Practice Area</th>
												<th>Practice Area Name</th>
												<th>Domain View</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<%
												for (PracticeAreaMst entry : objectList) {
													srNo = srNo + 1;
											%>
											<tr>

												<td><%=srNo%></td>
												<td><%=entry.getPracticeAreaCode()%></td>
												<td><%=entry.getPracticeAreaName()%></td>
												<td><%=entry.getBusinessModel()%></td>
												<td>
													<div class="d-flex">
														<a
															onclick="updateObject('<%=entry.getPracticeAreaCode()%>');"
															class="btn btn-primary shadow btn-xs sharp mr-1"><i
															class="fa fa-pencil"></i></a> <a
															onclick="deleteObject('<%=entry.getPracticeAreaCode()%>');"
															class="btn btn-danger shadow btn-xs sharp"><i
															class="fa fa-trash"></i></a>
													</div>
												</td>
											</tr>
											<%
												}
											%>


										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!---------Body End of Tables--------->
			</div>
		</div>
		<!--**********************************
            Content body end
        ***********************************-->


	</div>
	<script src="theme-vora/vendor/global/global.min.js"
		type="text/javascript"></script>
	<script
		src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"
		type="text/javascript"></script>
	<script src="theme-vora/js/custom.min.js" type="text/javascript"></script>
	<script src="theme-vora/js/dlabnav-init.js" type="text/javascript"></script>
	<script src="theme-vora/vendor/jquery-steps/build/jquery.steps.min.js"
		type="text/javascript"></script>
	<script
		src="theme-vora/vendor/jquery-validation/jquery.validate.min.js"
		type="text/javascript"></script>
	<script src="theme-vora/js/plugins-init/jquery.validate-init.js"
		type="text/javascript"></script>

	<script
		src="theme-vora/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js"
		type="text/javascript"></script>
	<script src="theme-vora/vendor/datatables/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="theme-vora/js/plugins-init/datatables.init.js"
		type="text/javascript"></script>
	<script src="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.js"
		type="text/javascript"></script>
	<script src="theme-vora/js/plugins-init/sweetalert.init.js"
		type="text/javascript"></script>


	<script type="text/javascript">
		$(document).ready(function() {

			$("#commit").click(function() {
				var businessModel = $("#businessModel1").val();
				
				$("#businessModel").val(businessModel);
			});

		});
	</script>


	<script type="text/javascript">
		function updateTableDetails(tid, cCode, cName, cBusinessModel,
				cisactive, mode, categoryCode, capabilityCode,
				practiceAreaValue, practiceAreaIntent,
				practiceAreaAdditionalInfo, process, organizationUnit,
				businessUnit, project) {
			$("#categoryCode").val(categoryCode);
			$("#capabilityCode").val(capabilityCode);
			$("#practiceAreaValue").val(practiceAreaValue);
			$("#practiceAreaIntent").val(practiceAreaIntent);
			$("#practiceAreaAdditionalInfo").val(practiceAreaAdditionalInfo);
			$("#process").val(process);
			$("#organizationUnit").val(organizationUnit);
			$("#businessUnit").val(businessUnit);
			$("#project").val(project);
			$("#tenantId").val(tid);
			$("#practiceAreaCode").val(cCode);
			$("#practiceAreaName").val(cName);

			var businessModels = [];
			var existingModels = [];

			businessModels = cBusinessModel.split(",");

			$("#businessModel1 option:selected").prop("selected", false);

			for (var i = 0; i < businessModels.length; i++) {
				$("#businessModel1 option[value='" + businessModels[i] + "']")
						.prop("selected", true);

			}

			var mySelect = $('#businessModel1');

			mySelect.find('option:selected').prop('disabled', false);
			mySelect.selectpicker('refresh');

			$("#isActive").val(cisactive);
			$("#mode").val(mode);
			document.getElementById('commit').innerHTML = mode;
			window.scrollTo(0, 0);

		}
	</script>
	
	<script type="text/javascript">
		function updateMode(mode) {
			$("#mode").val(mode);
			document.getElementById('commit').innerHTML = mode;

			window.scrollTo(0, 0);

		}
	</script>

<script type="text/javascript">
function updateObject(practiceAreaCode){
var mode="Edit";
	$.ajax({
						url: "practiceArea/getAjax",
						async: false,
						data: {
							practiceAreaCode: practiceAreaCode
						},
						success: function (jsonResponse) {
							$("#practiceAreaValue").val(jsonResponse.practiceAreaValue);
							$("#practiceAreaIntent").val(jsonResponse.practiceAreaIntent);
							$("#practiceAreaAdditionalInfo").val(jsonResponse.practiceAreaAdditionalInfo);
							$("#practiceAreaCode").val(jsonResponse.practiceAreaCode);
							$("#practiceAreaName").val(jsonResponse.practiceAreaName);
							$("#practiceAreaCode").prop("readonly", true);

			var businessModels = [];
			var existingModels = [];

			businessModels = jsonResponse.businessModel.split(",");

			$("#businessModel1 option:selected").prop("selected", false);

			for (var i = 0; i < businessModels.length; i++) {
				$("#businessModel1 option[value='" + businessModels[i] + "']")
						.prop("selected", true);

			}

			var mySelect = $('#businessModel1');
			$("#mode").val(mode);
			document.getElementById('commit').innerHTML = mode;
			window.scrollTo(0,0);

			mySelect.find('option:selected').prop('disabled', false);
			mySelect.selectpicker('refresh');

							
						}
	});

} 
</script>


<script type="text/javascript">
function deleteObject(practiceAreaCode){
var mode="DELETE";
	$.ajax({
						url: "practiceArea/getAjax",
						async: false,
						data: {
							practiceAreaCode: practiceAreaCode
						},
						success: function (jsonResponse) {
							$("#practiceAreaValue").val(jsonResponse.practiceAreaValue);
							$("#practiceAreaIntent").val(jsonResponse.practiceAreaIntent);
							$("#practiceAreaAdditionalInfo").val(jsonResponse.practiceAreaAdditionalInfo);
							$("#practiceAreaCode").val(jsonResponse.practiceAreaCode);
							$("#practiceAreaName").val(jsonResponse.practiceAreaName);
							$("#practiceAreaCode").prop("readonly", true);

			var businessModels = [];
			var existingModels = [];

			businessModels = jsonResponse.businessModel.split(",");

			$("#businessModel1 option:selected").prop("selected", false);

			for (var i = 0; i < businessModels.length; i++) {
				$("#businessModel1 option[value='" + businessModels[i] + "']")
						.prop("selected", true);

			}

			var mySelect = $('#businessModel1');

				$("#mode").val(mode);
				document.getElementById('commit').innerHTML = mode;
				window.scrollTo(0,0);
			mySelect.find('option:selected').prop('disabled', false);
			mySelect.selectpicker('refresh');

			

						}
	});

} 
</script>



</body>


</html>
