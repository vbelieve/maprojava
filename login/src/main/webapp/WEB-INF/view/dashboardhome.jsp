<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.basics.DashBoardVo"%>
<%@page import="com.login.domain.UserMst"%>
<%@page import="com.login.domain.ProjectMst"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="theme-vora/images/logo-Client.png">
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/jquery-smartwizard/dist/css/smart_wizard.min.css" rel="stylesheet">
<link href="theme-vora/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
	<link rel="stylesheet" href="theme-vora/vendor/chartist/css/chartist.min.css">
	<!-- Vectormap -->
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="theme-vora/css/style.css" rel="stylesheet">
	<link href="theme-vora/vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="customized/css/new-style.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>

  <title>Admin</title>

  <!-- Custom fonts for this template -->

</head>

<body id="page-top">
<div id="main-wrapper">

<%@include file="preloader.jsp"%>
<%@include file="navheader.jsp"%>
<%@include file="head.jsp"%>
<%@include file="sidebar.jsp"%>
<%
String msg=(String) request.getAttribute("msg");
String selectedtenantId=(String) request.getAttribute("selectedtenantId");
String[] messageType=null;
String alertType="alert alert-info alert-dismissible fade show";
String alertStr="Info!";

if(null==msg || msg.equalsIgnoreCase(""))
{
	msg="";
}
else
{
System.out.println("msg>>>>>>>>>>>>>>>>>>"+msg);	
messageType=msg.split("~");
msg=messageType[1];
}

if(!msg.equalsIgnoreCase("") && messageType.length>1)
{
	if(messageType[0].equalsIgnoreCase("SUCCESS"))
	{
	alertType="alert alert-success alert-dismissible fade show";
	alertStr="Success!";
	}
	else
	{
	alertType="alert alert-danger alert-dismissible fade show";
	alertStr="Error!";
	}
}

List<UserMst> users=new ArrayList<UserMst>();
List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
List<DashBoardVo> dashBoardVoCategoryAll=new ArrayList<DashBoardVo>();
List<DashBoardVo> dashBoardVoCategory=new ArrayList<DashBoardVo>();
List<DashBoardVo> dashBoardVoCapability=new ArrayList<DashBoardVo>();
List<DashBoardVo> dashBoardVoPracticeArea=new ArrayList<DashBoardVo>();
if(request.getAttribute("dashBoardVoCategory") != null )
{
	dashBoardVoCategory=(List<DashBoardVo>) request.getAttribute("dashBoardVoCategory");
	
}

if(request.getAttribute("dashBoardVoCapability") != null )
{
	dashBoardVoCapability=(List<DashBoardVo>) request.getAttribute("dashBoardVoCapability");
	
}

if(request.getAttribute("dashBoardVoPracticeArea") != null )
{
	dashBoardVoPracticeArea=(List<DashBoardVo>) request.getAttribute("dashBoardVoPracticeArea");
	
	
}

if(request.getAttribute("dashBoardVoCategoryAll") != null )
{
	dashBoardVoCategoryAll=(List<DashBoardVo>) request.getAttribute("dashBoardVoCategoryAll");
	
	
	
}

if(request.getAttribute("users") != null )
{
	users=(List<UserMst>) request.getAttribute("users");
	
	
	for(UserMst user:users)
	{
		System.out.println(">>>>"+user.getUserName());
	}
}

allProjects=(List<ProjectMst>) request.getAttribute("allProjects");
	if(null==allProjects)
	{
		allProjects=new ArrayList<ProjectMst>();
	}
Integer srNo=0;
SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
String[] projects=accessObject.getProject().split(",");
%>


        <!--**********************************
            Content body start
        ***********************************-->
<div class="content-body" style="min-height: 788px;">
            <!-- row -->
			<div class="container-fluid">
				<!-- Add Order -->
				
					
						
															<%
      for(ProjectMst project:allProjects)
	 {
		 
	 %>
	 <div class="row">

							<div class="col-xl-3 col-lg-6 col-sm-6">
						<div class="widget-stat card bg-secondary">
							<div class="card-body p-4" id="<%=project.getProjectCode()%>" onclick="selectedProject(this.id)">
								<div class="media">
									
									<div class="media-body text-white">
										<p class="mb-1"><%=project.getProjectName()%></p>
										<h3 class="text-white"><%=project.getComplianceScore()%>%</h3>
										<div class="progress mb-2 bg-primary">
                                            <div class="progress-bar progress-animated bg-light" style="width: <%=project.getComplianceScore()%>%"></div>
                                        </div>
										<small>Compliance</small>
									</div>
								</div>
							</div>
						</div>
                    </div>
						


						
					<%
					String cName="chart1_"+project.getProjectCode();
					String tName="table_"+project.getProjectCode();
					%>
			<!-- Chart-->
			<div class="col-xl-8 col-xxl-8 col-lg-12 col-sm-12" >
                        <div id="user-activity" class="card">
                            <div class="card-header border-0 pb-0 d-sm-flex d-block">
                                <h4 class="card-title">Compliance</h4>
                                <div class="card-action mb-sm-0 my-2">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item" onclick="processProjectCategory('category_+<%=tName%>','<%=cName%>','Category');">
                                            <a class="nav-link" data-toggle="tab" role="tab" href="#bounce" aria-selected="false">
                                                Category
                                            </a>
                                        </li>
                                        <li class="nav-item" onclick="processProjectCapability('capability_+<%=tName%>','<%=cName%>','Capability');">
                                            <a class="nav-link" data-toggle="tab" href="#bounce" role="tab" aria-selected="false">
                                                Capability
                                            </a>
                                        </li>
                                        <li class="nav-item" onclick="processProjectPracticeArea('practiceArea_+<%=tName%>','<%=cName%>','PracticeArea');">
                                            <a class="nav-link active" data-toggle="tab" href="#session-duration" role="tab" aria-selected="true">
                                                Practice Area
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div  class="card-body">
                                <div class="tab-content" id="myTabContent">
                                   <canvas id="<%=cName%>" style="display: block; width: 594px; height: 297px;" width="594" height="297" class="chartjs-render-monitor"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
			<!-- Chart-->
			</div>
									<div class="card-body">
                                <div hidden class="table-responsive">
                                    <table id="category_+<%=tName%>" class="display min-w850">
									<thead>
                                            <tr>
                                                <th>Category</th>
                                                <th>Score</th>
                                                 
                                            </tr>
                                        </thead>
                                        <tbody>
										<%for(DashBoardVo db:dashBoardVoCategory)
										{
											if(db.getProjectCode().equalsIgnoreCase(project.getProjectCode()))
											{
												
												
												

											%>
										<tr>
										
										<td><%=db.getCategoryCode()%></td>
										<td><%=(double)db.getComplianceScore()%></td>
										</tr>
											<%
											}
											}
											%>
										</tbody>
									</table>
									</div>
									</div>
									
									<!--table for PracticeArea-->
									<div hidden class="card-body">
                                <div class="table-responsive">
                                    <table id="practiceArea_+<%=tName%>" class="display min-w850">
									<thead>
                                            <tr>
                                                <th>PracticeArea</th>
                                                <th>Score</th>
                                                 
                                            </tr>
                                        </thead>
                                        <tbody>
										<%for(DashBoardVo db2:dashBoardVoPracticeArea)
										{
											if(db2.getProjectCode().equalsIgnoreCase(project.getProjectCode()))
											{
												
												
												

											%>
										<tr>
										
										<td><%=db2.getPracticeAreaCode()%></td>
										<td><%=(double)db2.getComplianceScore()%></td>
										</tr>
											<%
											}
											}
											%>
										</tbody>
									</table>
									</div>
									</div>
									<!--Table for Capability-->
									
									<!--table for capability-->
									<div hidden class="card-body">
                                <div class="table-responsive">
                                    <table id="capability_+<%=tName%>" class="display min-w850">
									<thead>
                                            <tr>
                                                <th>Capability</th>
                                                <th>Score</th>
                                                 
                                            </tr>
                                        </thead>
                                        <tbody>
										<%for(DashBoardVo db:dashBoardVoCapability)
										{
											if(db.getProjectCode().equalsIgnoreCase(project.getProjectCode()))
											{
												
												
												

											%>
										<tr>
										
										<td><%=db.getCapabilityCode()%></td>
										<td><%=(double)db.getComplianceScore()%></td>
										</tr>
											<%
											}
											}
											%>
										</tbody>
									</table>
									</div>
									</div>
									<!--Table for Capability-->
        
					 <%
	 
	 }
	%>

		</div>  
  <!--**********************************
            Content body end
        ***********************************-->
<form id="masterRegistration" action="#" method="post">
				<input type="hidden" name="project" id="project" value="">
				<input type="hidden" name="selectedtenantId" id="selectedtenantId" value="">
				</form>

</div>

<script type="text/javascript">
$(document).ready(function() {
	<%
      for(ProjectMst project:allProjects)
	 {
		 String ccName="chart1_"+project.getProjectCode();
		 String ctName="table_"+project.getProjectCode();
	 %>
	 
	processProjectPracticeArea('practiceArea_+<%=ctName%>','<%=ccName%>','Practice Area');
	 <%
	 }%>
	 
	 });
</script>


<script type="text/javascript">
function processProjectCategory(tableName,chartId,selectionCode){
	
var table = document.getElementById(""+tableName);

var json = []; 
var headers =[];
for (var i = 0; i < table.rows[0].cells.length; i++) {
  headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');
  
}

// Go through cells 
for (var i = 1; i < table.rows.length; i++) {
  var tableRow = table.rows[i];
  var rowData = {};
  for (var j = 0; j < tableRow.cells.length; j++) {
    rowData[headers[j]] = tableRow.cells[j].innerHTML;
	
  }
console.log(rowData);
  json.push(rowData);
}
var labels = json.map(function (e) {
  return e.category;
});
console.log(labels); // ["2016", "2017", "2018", "2019"]

// Map JSON values back to values array
var values = json.map(function (e) {
  return Math.round(e.score)+"";
});
console.log(values); // ["10", "25", "55", "120"]
var chart = BuildChart(labels, values, selectionCode,chartId);

}
</script>



<script type="text/javascript">
function processProjectCapability(tableName,chartId,selectionCode){
	
var table = document.getElementById(""+tableName);

var json = []; 
var headers =[];
for (var i = 0; i < table.rows[0].cells.length; i++) {
  headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');
  
}

// Go through cells 
for (var i = 1; i < table.rows.length; i++) {
  var tableRow = table.rows[i];
  var rowData = {};
  for (var j = 0; j < tableRow.cells.length; j++) {
    rowData[headers[j]] = tableRow.cells[j].innerHTML;
	
  }
console.log(rowData);
  json.push(rowData);
}
var labels = json.map(function (e) {
  return e.capability;
});
console.log(labels); // ["2016", "2017", "2018", "2019"]

// Map JSON values back to values array
var values = json.map(function (e) {
  return Math.round(e.score)+"";
});
console.log(values); // ["10", "25", "55", "120"]
var chart = BuildChart(labels, values, selectionCode,chartId);

}
</script>


<script type="text/javascript">
function processProjectPracticeArea(tableName,chartId,selectionCode){
	
var table = document.getElementById(""+tableName);

var json = []; 
var headers =[];
for (var i = 0; i < table.rows[0].cells.length; i++) {
  headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');
  
}

// Go through cells 
for (var i = 1; i < table.rows.length; i++) {
  var tableRow = table.rows[i];
  var rowData = {};
  for (var j = 0; j < tableRow.cells.length; j++) {
    rowData[headers[j]] = tableRow.cells[j].innerHTML;
	
  }
console.log(rowData);
  json.push(rowData);
}
var labels = json.map(function (e) {
  return e.practicearea;
});
console.log(labels); // ["2016", "2017", "2018", "2019"]

// Map JSON values back to values array
var values = json.map(function (e) {
  return Math.round(e.score)+"";
});
console.log(values); // ["10", "25", "55", "120"]

var chart = BuildChart(labels, values, selectionCode,chartId);

}
</script>


<script type="text/javascript">
function selectedProject(pid){
$("#project").val(pid);
document.forms["masterRegistration"].action = "/GapAnalysis-2.0/selectDashboardProject";
document.forms["masterRegistration"].submit();	

}
</script>

<script type="text/javascript">
function BuildChart(labels, values, chartTitle,chartId) {
	
  var ctx = document.getElementById(chartId).getContext('2d');
  var data = {
		labels: labels, // Our labels
		datasets: [{
        label: chartTitle, // Name the series
        data: values, // Our values
		borderWidth: "0",
        backgroundColor: 'blue',
        borderColor: 'blue',
        borderWidth: 1 // Specify bar border width}]
}]}

  var myChart = new Chart(ctx, {
    type: 'bar',
	data: data,
    options: {
		"hover": {
      "animationDuration": 0
    },
	"animation": {
      "duration": 1,
      "onComplete": function() {
        var chartInstance = this.chart,
          ctx = chartInstance.ctx;

         ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
         ctx.textAlign = 'center';
         ctx.textBaseline = 'bottom';

        this.data.datasets.forEach(function(dataset, i) {
          var meta = chartInstance.controller.getDatasetMeta(i);
          meta.data.forEach(function(bar, index) {
            var data = dataset.data[index]+"%";
            ctx.fillText(data, bar._model.x, bar._model.y - 5);
          });
        });
      }
    },
      events: ['blur','click'],
	  showNumbers: true,
      responsive: true, // Instruct chart js to respond nicely.
      maintainAspectRatio: false, // Add to prevent default behavior of full-width/height 
	  
	  	  scales: {
					xAxes: [{
						display: true, 
						stacked: true,
						barPercentage: 1, 
						barThickness: 20, 
						ticks: {
							display: true
						}, 
						gridLines: {
							display: false, 
							drawBorder: false
						}
						
					}],
					yAxes: [{
						
						display: true, 
						stacked: false, 
						
						gridLines: {
							display: true, 
							drawBorder: true
						}, 
						
						ticks: {
							beginAtZero: true,
							display: true, 
							max: 100, 
							min: 0
						}
					}]
				}
    }
  });
  return myChart;
}
</script>


<script src="theme-vora/vendor/global/global.min.js"></script>
	
	<script src="theme-vora/js/custom.min.js"></script>
	<script src="theme-vora/js/dlabnav-init.js"></script>
	<script src="theme-vora/vendor/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="theme-vora/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="theme-vora/js/plugins-init/jquery.validate-init.js"></script>

<script src="theme-vora/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js"></script>
<script src="theme-vora/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="theme-vora/js/plugins-init/datatables.init.js"></script>	
<script src="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="theme-vora/js/plugins-init/sweetalert.init.js"></script>

<script src="theme-vora/vendor/chart.js/Chart.bundle.min.js"></script>

	
    <!-- Init file -->
    <!--<script src="theme-vora/js/plugins-init/widgets-script-init.js"></script>-->

</script>		
</body>
</html>