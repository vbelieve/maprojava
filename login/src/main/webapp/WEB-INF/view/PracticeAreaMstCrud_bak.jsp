<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.PracticeAreaMst"%>
<%@page import="com.login.domain.LookupMst"%>
<%@page import="java.util.*"%>

<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Practice Area</title>
<%@include file="header.jsp"%>
  <!-- Custom fonts for this template -->
  <link href="customized/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="customized/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="customized/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.1.1.min.js">
  <script src="js/jquery-3.3.1.slim.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
var erm=$("#msg").val();
if(erm!="null" && erm!="")
{
alert(erm);
erm="";
}
$("#commit").click(function(){
document.forms["regisration"].action = "/GapAnalysis-2.0/practiceArea/save";
document.forms["regisration"].submit();	

});
  
});
</script>

<script type="text/javascript">
function updateTableDetails(tid,cCode,cName,cbusinessModel,cIsActive){
$("#tenantId").val(tid);
$("#practiceAreaCode").val(cCode);
$("#practiceAreaName").val(cName);
$("select#businessModel").val(cbusinessModel);
$("#isActive").val(cIsActive);

window.scrollTo(0,0);

} 
</script>	

</head>

<body id="page-top">
<%
String msg=(String) request.getAttribute("msg");
if(null==msg)
{
	msg="";
}
List<PracticeAreaMst>objectList= (List<PracticeAreaMst>) request.getAttribute("objectList");
List<LookupMst> businessModelLookup=new ArrayList<LookupMst>();
businessModelLookup=(List<LookupMst>)request.getAttribute("businessModelLookup");
if(null==businessModelLookup)
{
	businessModelLookup=new ArrayList<LookupMst>();
}

if(null==objectList)
{
	objectList=new ArrayList<PracticeAreaMst>();
}
Integer srNo=0;
%>
  <!-- Page Wrapper -->
  <div id="wrapper">

    
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
		
		

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
&nbsp &nbsp
	<h3 style="text-align:center">Practice Area Details</h3>     
	 <form role="form" class="form" id="regisration" method="post" style="width:50%;align:right;position:relative;left:50%;">
	<input type="hidden" id="msg" value="<%=msg%>">
              <div class="form-group">
				<label for="tenantId">Tenant Id</label>
				<input type="text" class="form-control" id="tenantId" name="tenantId" placeholder="TenatId" value="" readonly>
			</div>
			<div class="form-group">
								<label for="practiceAreaCode">Practice Area Code</label>
								<input type="text" class="form-control" id="practiceAreaCode" name="practiceAreaCode" placeholder="PracticeArea Code" value="" readonly>
							</div>
							
							<div class="form-group">
								<label for="practiceAreaName">Practice Area's Name</label>
								<input type="text" class="form-control" id="practiceAreaName" name="practiceAreaName" placeholder="Practice Area Name">
							</div>
							
							<label for="businessModel">Domain View</label>
							<div class="dropdown">
		
							<select multiple name="businessModel" id="businessModel" class="form-control" >
							<%
							for(LookupMst entry:businessModelLookup)
							{
							%>
							<option value="<%=entry.getLookupValue()%>"><%=entry.getDisplayValue()%></option>
							<%
							}
							%>
							</select>
							&nbsp
</div>
<div class="form-group">
<label for="isActive">Enable / Disable</label>
<input type="text" class="form-control" id="isActive" name="isActive" placeholder="Enter 0 or 1">
</div>

              <button type="button" id="commit"  name="commit" class="btn btn-primary">Submit</button>
              <!--<button type="button" onclick="goBack()" class="btn btn-primary">Back</button>-->
&nbsp
            </form>
&nbsp

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h3 class="m-0 font-weight-bold text-primary">Practice Area Details</h3>
            </div>
            <div class="card-body">
              <div class="table-responsive" href="#page-top">
                <table class="table table-bordered  table-hover" style="cursor: pointer;" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
					  <th>Tenant Id</th>
                      <th>Practice Area Code</th>
					  <th>Practice Area Name</th>
					  <th>Domain View</th>
	  
      </tr>
      </thead>
<tbody id="objectTable" name="objectTable" >
<%
for(PracticeAreaMst entry:objectList)
{
	srNo=srNo+1;
%>
<tr  .pointer onclick="updateTableDetails('<%=entry.getTenantId()%>',
'<%=entry.getPracticeAreaCode()%>',
'<%=entry.getPracticeAreaName()%>',
'<%=entry.getBusinessModel()%>',
'<%=entry.getIsActive()%>')";>
<td><%=srNo%></td>
<td><%=entry.getTenantId()%></td>
<td><%=entry.getPracticeAreaCode()%></td>
<td><%=entry.getPracticeAreaName()%></td>
<td><%=entry.getBusinessModel()%></td>
</tr>

<%
}
%>

                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">�</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="customized/vendor/jquery/jquery.min.js"></script>
  <script src="customized/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="customized/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="customized/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="customized/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="customized/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="customized/js/demo/datatables-demo.js"></script>

</body>
<%@include file="footer.jsp"%>

</html>
