<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.ApplicationConstants"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>
<script type="text/javascript">
function goBack(){
window.history.back();
} 
</script>	


<script type="text/javascript">
/// some script

// jquery ready start
$(document).ready(function() {
	// jQuery code

	//////////////////////// Prevent closing from click inside dropdown
    $(document).on('click', '.dropdown-menu', function (e) {
      e.stopPropagation();
    });

    // make it as accordion for smaller screens
    if ($(window).width() < 992) {
	  	$('.dropdown-menu a').click(function(e){
	  		e.preventDefault();
	        if($(this).next('.submenu').length){
	        	$(this).next('.submenu').toggle();
	        }
	        $('.dropdown').on('hide.bs.dropdown', function () {
			   $(this).find('.submenu').hide();
			})
	  	});
	}
	
}); // jquery end
</script>
<script>
function categorySelect(pid)
{
	
	$("#categoryDropdown").val(pid);
	document.forms["categorySelect"].action = "/GapAnalysis-2.0/dropDownCategorySelect";
	document.forms["categorySelect"].submit();
	
}
</script>
<script>
function capabilitySelect(pid)
{
	
	$("#capabilityDropdown").val(pid);
	document.forms["categorySelect"].action = "/GapAnalysis-2.0/dropdownCapabilitySelect";
	document.forms["categorySelect"].submit();
	
}
</script>
<script>
function practiceAreaSelect(pid)
{
	
	$("#parcticeAreaDropdown").val(pid);
	document.forms["categorySelect"].action = "/GapAnalysis-2.0/dropdownParcticeAreaSelect";
	document.forms["categorySelect"].submit();
	
}
</script>


<style type="text/css">
	@media (min-width: 992px){
		.dropdown-menu .dropdown-toggle:after{
			border-top: .3em solid transparent;
		    border-right: 0;
		    border-bottom: .3em solid transparent;
		    border-left: .3em solid;
		}

		.dropdown-menu .dropdown-menu{
			margin-left:0; margin-right: 0;
		}

		.dropdown-menu li{
			position: relative;
			width: auto;
		}
		.nav-item .submenu{ 
			display: none;
			position: absolute;
			left:100%; top:-6px;
			width: auto;
			
		}
		.nav-item .submenu-left{ 
			right:100%; left:100%;
		}

		.dropdown-menu > li:hover{ background-color: #f1f1f1 }
		.dropdown-menu > li:hover > .submenu{
			display: block;
		}
	}
</style>

<html>
<body>
<%
AccessObject accessObject = (AccessObject)request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT) ;
if(null==accessObject)
{
	accessObject=new AccessObject();
}
String allProcess=accessObject.getAllProcess();
String[] processList=null;
System.out.println(">>>>"+allProcess);
if(allProcess==null)
{
}
else
{
	processList=allProcess.split(",");
	for(String process:processList)
	{
		System.out.println(">>>>"+process);
	}
}
String tid=accessObject.getTenantId();
String homeUrl="/GapAnalysis-2.0/home";
String logoutUrl="/GapAnalysis-2.0/?tid="+tid;

%>
	<!-- header sec start -->
	<nav class="navbar navbar-expand-lg navbar-light " style="background:#02c4ac;" hidden>

<form id="categorySelect" action="#" method="post" hidden>
<input type="hidden" id="categoryDropdown" name="categoryDropdown">
<input type="hidden" id="capabilityDropdown" name="capabilityDropdown">
<input type="hidden" id="parcticeAreaDropdown" name="parcticeAreaDropdown">

</form>
  


  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
	  
        <a class="nav-link" href="<%=homeUrl%>">Home <span class="sr-only">(current)</span></a>
      </li>
      </ul>
<a class="navbar-brand" href="#" style="position:absolute; left: 330px; padding-left: 0px;">Process :<%=accessObject.getProcess()%></a>
<a class="navbar-brand" href="#"><%=accessObject.getUserFirstName()%></a>
    <ul class="navbar-nav mr-right" style="margin-right:20px;">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Profile
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Change Password</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      
    </ul>
	
      <button class="btn btn-outline-success my-2 my-sm-0" onclick="window.location.replace('<%=logoutUrl%>');">Logout</button>
    
  </div>
</nav>
	
	<!-- header sec end -->
	        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light  topbar mb-4 static-top shadow" >

          <!-- Sidebar Toggle (Topbar) -->
          <form class="form-inline">
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
              <i class="fa fa-bars"></i>
            </button>
          </form>

          <!-- Topbar Search -->
          
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            

            <!-- Nav Item - Messages -->
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
  
<% if(accessObject.getUserType().equalsIgnoreCase("USER") && !accessObject.getSelectedProject().equalsIgnoreCase(""))
{
	%>
	<a style="position:absolute; left: 3%; padding-left: 0px;">Project :<%=accessObject.getSelectedProjectName()%></a>
<li class="nav-item dropdown no-arrow" >
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 medium">
				<i class="fas fa-chart-pie fa-sm fa-fw mr-2 text-gray-400"></i>
				Dashboard</span>
                
				<span class="sr-only">(current)</span>
              </a>
			  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
			  <a class="dropdown-item" href="gotoDashboard">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Dashboard
                </a>
              </div>
  </li>
<%}%>
<% if(!accessObject.getUserType().equalsIgnoreCase("USER"))
{
	%>
<li class="nav-item dropdown no-arrow" >
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 medium">
				<i class="fas fa-chart-pie fa-sm fa-fw mr-2 text-gray-400"></i>
				Dashboard</span>
                
				<span class="sr-only">(current)</span>
              </a>
			  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
			  <a class="dropdown-item" href="gotoDashboard">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Dashboard
                </a>
              </div>
  </li>
<%}%>
  <%
if(!accessObject.getUserType().equalsIgnoreCase("SUPERADMIN"))
{
%>
 
	<li class="nav-item dropdown no-arrow" >
	<% if(!accessObject.getSelectedProject().equalsIgnoreCase(""))
{
	%>
		<a class="nav-link  dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 medium">
				<i class="fas fa-chalkboard-teacher fa-sm fa-fw mr-2 text-gray-400"></i>
				Compliance</span>
                
              </a>
		
	    <ul class="dropdown-menu dropdown-menu-right" ">
		
		  <li><a  class="dropdown-item dropdown-toggle"  href="#">Category </a>
		  	 <ul class="submenu submenu-left dropdown-menu">
			    <li><a class="dropdown-item" onclick="categorySelect('Doing');" href="#">Doing</a></li>
			    <li><a class="dropdown-item" onclick="categorySelect('Managing');" href="#">Managing</a></li>
			    <li><a class="dropdown-item" onclick="categorySelect('Enabling');" href="#">Enabling</a></li>
			    <li><a class="dropdown-item" onclick="categorySelect('Improving');" href="#">Improving</a></li>
			 </ul>
		  </li>
		
		  <li><a class="dropdown-item dropdown-toggle " href="#">Capability </a>
		  	 <ul class="submenu submenu-left dropdown-menu"">
<li><a class="dropdown-item" onclick="capabilitySelect('DMS');" href="#">Delivering and Managing Services </a></li>
<li><a class="dropdown-item" onclick="capabilitySelect('EDP');" href="#">Engineering and Development Products </a></li>
<li><a class="dropdown-item" onclick="capabilitySelect('ENQ');" href="#">Ensuring Quality</a></li>
<li><a class="dropdown-item" onclick="capabilitySelect('IMP');" href="#">Improving Performance </a></li>
<li><a class="dropdown-item" onclick="capabilitySelect('MBR');" href="#">Managing Business Resilience </a></li>
<li><a class="dropdown-item" onclick="capabilitySelect('MWF');" href="#">Managing the Work Force </a></li>
<li><a class="dropdown-item" onclick="capabilitySelect('PMW');" href="#">Planning and Managing Work</a></li>
<li><a class="dropdown-item" onclick="capabilitySelect('SHP');" href="#">Sustaining Habit & Persistence</a></li>
<li><a class="dropdown-item" onclick="capabilitySelect('SI');" href="#">Supporting Implementation</a></li>
<li><a class="dropdown-item" onclick="capabilitySelect('SMS');" href="#">Selecting and Managing Suppliers </a></li>

			    
			 </ul>
		  </li>
		   <li><a class="dropdown-item dropdown-toggle" href="#">Practice Area </a>
		  	 <ul class="submenu submenu-left dropdown-menu">
			    <li><a class="dropdown-item" onclick="practiceAreaSelect('CAR');" href="#">Causal Analysis and Resolution</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('CM');" href="#">Configuration Management</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('CO');" href="#">Continuity</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('DAR');" href="#">Decision Analysis and Resolution</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('EST');" href="#">Estimating</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('GOV');" href="#">Governance</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('II');" href="#">Implementing Infrastructure</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('IRP');" href="#">Incident Resolution and Prevention</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('MC');" href="#">Monitor and Control</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('MPM');" href="#">Managing Performance and Measurement</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('OT');" href="#">Organization Training</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('PAD');" href="#">Process Asset Development</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('PCM');" href="#">Process Management</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('PI');" href="#">Product Integration</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('PLAN');" href="#">Planning</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('PQA');" href="#">Process Quality Assurance</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('PR');" href="#">Peer Reviews</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('RDM');" href="#">Requirements Development and Management</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('RSK');" href="#">Risk and Opportunity Management</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('SAM');" href="#">Supplier Agreement and Management</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('SDM');" href="#">Service Delivery Management</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('SSM');" href="#">Stratergic Service Management</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('SSS');" href="#">Supplier Source Selection</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('TS');" href="#">Technical Solution</a></li>
<li><a class="dropdown-item" onclick="practiceAreaSelect('VV');" href="#">Verification and Validation</a></li>

			 </ul>
		  </li>
		   <!--<li><a class="dropdown-item dropdown-toggle" href="#">Practice </a>
		  	 <ul class="submenu submenu-left dropdown-menu">
			    <li><a class="dropdown-item" href="">Submenu item 1</a></li>
			    <li><a class="dropdown-item" href="">Submenu item 2</a></li>
			    <li><a class="dropdown-item" href="">Submenu item 3</a></li>
			    <li><a class="dropdown-item" href="">Submenu item 4</a></li>
			 </ul>
		  </li>-->
	    </ul>
<%}%>
	</li>
<%
}
%>
<%
System.out.println("<>>>>"+accessObject.getProject());
if(null!=accessObject.getProject() && !accessObject.getProject().equalsIgnoreCase("") && !accessObject.getProject().equalsIgnoreCase(" "))
{
%>
<li class="nav-item dropdown no-arrow" >
<% if(accessObject.getUserType().equalsIgnoreCase("USER") && !accessObject.getSelectedProject().equalsIgnoreCase(""))
{
	%>
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 medium">
				<i class="fas fa-book fa-sm fa-fw mr-2 text-gray-400"></i>
				Reports</span>
              </a>
			  <ul class="dropdown-menu dropdown-menu-right">
		  <li><a class="dropdown-item dropdown-toggle" href="#">MIS Reports </a>
		  	 <ul class="submenu submenu-left dropdown-menu">
		  	 
			    <li><a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/generateGapSummaryReport');">Gap Summary Report</a></li>
			    <!--<li><a class="dropdown-item" href="">Submenu item 2</a></li>-->
			    
			 </ul>
		  </li>
		  
	    </ul>
<%}%>	  
  </li>
 <%
}
%> 
  
<%
if(accessObject.getUserType().equalsIgnoreCase("ADMIN")||accessObject.getUserType().equalsIgnoreCase("SUPERADMIN"))
{
%>  
  <li class="nav-item dropdown no-arrow" >
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 medium">
				<i class="fas fa-bezier-curve fa-sm fa-fw mr-2 text-gray-400"></i>
				Masters</span>
                
				<span class="sr-only">(current)</span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
			  <a class="dropdown-item" href="#">
                  <i class="fas  fa-sm fa-fw mr-2 text-gray-400"></i>
                  Core
                </a>
			  <div class="dropdown-divider"></div>
              	<a class="dropdown-item" href="categoryMstCrud">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Category
                </a>
				<a class="dropdown-item" href="capabilityMstCrud">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Capability
                </a>
				<a class="dropdown-item" href="practiceAreaMstCrud">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  PracticeArea
                </a>				
				<a class="dropdown-item" href="practiceMstCrud">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Practice
                </a>				
                <a class="dropdown-item" href="workProductMstCrud">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Work Product
                </a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="#">
                  <i class="fas  fa-sm fa-fw mr-2 text-gray-600"></i>
                  RelationShip
                </a>
                <div class="dropdown-divider"></div>
				<a class="dropdown-item" href="relationMappingCategoryCapabilityCrud">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Category- Capability
                </a>
				<a class="dropdown-item" href="relationMappingCapabilityPracticeAreaCrud">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  capability-PracticeArea
                </a>
				<a class="dropdown-item" href="relationMappingPracticeAreaPracticeCrud">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  PracticeArea-Pracitce
                </a>
                <a class="dropdown-item" href="practiceWorkProductMstCrud">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Pracitce-WorkProduct
                </a>
				<div class="dropdown-divider"></div>
              </div>
  </li>
	<%
}
	%>
<!--<li class="nav-item dropdown no-arrow" >
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 medium">Users Management</span>
                
              </a>
          
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/signup');">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Create User
                </a>
                <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/userAdmin');">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  User Management
                </a>
                
              </div>
  </li>-->

<%if(accessObject.getUserType().equalsIgnoreCase("SUPERADMIN")||accessObject.getUserType().equalsIgnoreCase("MANAGER")||accessObject.getUserType().equalsIgnoreCase("ADMIN"))
{
%> 
<li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 medium">
				<i class="fas fa-cog fa-sm fa-fw mr-2 text-gray-400"></i>
				Setup</span>
                
				<span class="sr-only">(current)</span>
              </a>
              <!-- Dropdown - User Information -->
	
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
			  		  <%
if(accessObject.getUserType().equalsIgnoreCase("SUPERADMIN"))
{
%> 
                <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/tenantMstCrud');">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
				  Create Organization
                </a>
				<%
}
				%>
                <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/organizationProfile');">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Update Organization Details
                </a>
				<%
if(!accessObject.getUserType().equalsIgnoreCase("SUPERADMIN"))
{
%>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/organizationUnitMstCrud');">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Create Organization Unit
                </a>

				<a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/businessUnitMstCrud');">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Create Business Unit
                </a>
                <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/projectMstCrud');">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Create Projects
                </a>
<%}%>
                <div class="dropdown-divider"></div>
				<a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/signup');">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Create User
                </a>
                <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/userAdmin');">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  User Management
                </a>
				<div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/viewLicense');">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  View License
                </a>
             </div>
  </li>
  <%
}
%>
<%
if(accessObject.getUserType().equalsIgnoreCase("SUPERADMIN"))
{
%> 
<li class="nav-item dropdown no-arrow" >
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 medium">
				<i class="fas fa-american-sign-language-interpreting fa-sm fa-fw mr-2 text-gray-400"></i>
				Configure</span>
                
				<span class="sr-only">(current)</span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/lookupMstCrud');">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Lookup Management
                </a>
                <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/sequenceMstCrud');">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sequence management
                </a>
                
              </div>
  </li>
  <%
}
  %>
    <ul class="navbar-nav mr-auto">
	
      <li class="nav-item active">
        
		<a class="nav-link" href="<%=homeUrl%>">
		<i class="fas fa-home fa-sm fa-fw mr-2 text-gray-400"></i>
		Home  &nbsp		
		
		<span class="sr-only">
		(current)</span>
		
		
		</a>
		
		
      </li>
      </ul>


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><%=accessObject.getUserFirstName()%></span>
                <img class="img-profile rounded-circle" src="customized/img/user_logo.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/profile');">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                  <a class="dropdown-item" href="#" onclick="window.location.replace('/GapAnalysis-2.0/changePassword');">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Change Password
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" onclick="window.location.replace('<%=logoutUrl%>');" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
				  
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

</body>
</html>