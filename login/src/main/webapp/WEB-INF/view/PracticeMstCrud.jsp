<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.PracticeMst"%>
<%@page import="com.login.domain.LookupMst"%>
<%@page import="java.util.*"%>

<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>Mapro</title>
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16"
	href="theme-vora/images/logo-Client.png">
<link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css"
	rel="stylesheet">
<link href="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.css"
	rel="stylesheet">
<link
	href="theme-vora/vendor/jquery-smartwizard/dist/css/smart_wizard.min.css"
	rel="stylesheet">
<link href="theme-vora/vendor/datatables/css/jquery.dataTables.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="theme-vora/vendor/chartist/css/chartist.min.css">
<!-- Vectormap -->
<link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css"
	rel="stylesheet">
<link
	href="theme-vora/vendor/bootstrap-select/dist/css/bootstrap-select.min.css"
	rel="stylesheet">
<link href="theme-vora/css/style.css" rel="stylesheet">
<link href="theme-vora/vendor/owl-carousel/owl.carousel.css"
	rel="stylesheet">

<title>Admin</title>

<!-- Custom fonts for this template -->

</head>

<body id="page-top">
	<%
		String msg = (String) request.getAttribute("msg");
		String[] messageType = null;
		String alertType = "alert alert-info alert-dismissible fade show";
		String alertStr = "Info!";

		if (null == msg || msg.equalsIgnoreCase("")) {
			msg = "";
		} else {
			System.out.println("msg>>>>>>>>>>>>>>>>>>" + msg);
			messageType = msg.split("~");
			msg = messageType[1];
		}

		if (!msg.equalsIgnoreCase("") && messageType.length > 1) {
			if (messageType[0].equalsIgnoreCase("SUCCESS")) {
				alertType = "alert alert-success alert-dismissible fade show";
				alertStr = "Success!";
			} else {
				alertType = "alert alert-danger alert-dismissible fade show";
				alertStr = "Error!";
			}
		}

		List<PracticeMst> objectList = (List<PracticeMst>) request.getAttribute("objectList");
		List<LookupMst> practiceLevelLookup=(List<LookupMst>)request.getAttribute("practiceLevelLookup") ;


		if (null == objectList) {
			objectList = new ArrayList<PracticeMst>();
		}
		Integer srNo = 0;
	%>

	<div id="main-wrapper">


		<%@include file="preloader.jsp"%>
		<%@include file="navheader.jsp"%>
		<%@include file="head.jsp"%>
		<%@include file="sidebar.jsp"%>
		<!--**********************************
            Content body start
        ***********************************-->
		<div class="content-body">
			<div class="container-fluid">
				<%
					if (!msg.equalsIgnoreCase("")) {
				%>
				<div class="<%=alertType%>">
					<svg viewBox="0 0 24 24" width="24" height="24"
						stroke="currentColor" stroke-width="2" fill="none"
						stroke-linecap="round" stroke-linejoin="round" class="mr-2">
						<circle cx="12" cy="12" r="10"></circle>
						<line x1="12" y1="16" x2="12" y2="12"></line>
						<line x1="12" y1="8" x2="12.01" y2="8"></line></svg>

					<strong><%=alertStr%></strong>
					<%=msg%>
					<button type="button" class="close h-100" data-dismiss="alert"
						aria-label="Close">
						<span><i class="mdi mdi-close"></i></span>
					</button>
				</div>

				<%
					}
				%>
				<!-- Add Order -->
				<div class="modal fade" id="addOrderModalside">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Practice</h5>
								<button type="button" class="close" data-dismiss="modal">
									<span>&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form action="">
									<div class="form-group">
										<label class="text-black font-w500">Project Name</label> <input
											type="text" class="form-control">
									</div>
									<div class="form-group">
										<label class="text-black font-w500">Deadline</label> <input
											type="date" class="form-control">
									</div>
									<div class="form-group">
										<label class="text-black font-w500">Client Name</label> <input
											type="text" class="form-control">
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-primary">CREATE</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!--<div class="page-titles">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Profile</a></li>
					</ol>
                </div>
                
				<!---------Body Start of Form--------->
				<div class="row">
					<div class="col-xl-12 col-xxl-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Practice</h4>
							</div>
							<div class="card-body">

								<form role="form" class="form"
									action="/GapAnalysis-2.0/practice/save" id="masterRegistration"
									method="post">
									<input type="hidden" id="msg" value="<%=msg%>"> <input
										type="hidden" id="businessModel" name="businessModel" value="">
									<input type="hidden" id="mode" name="mode" value="CREATE">
									<div id="wizard_Service" class="tab-pane" role="tabpanel">

										<div class="row">
											<div class="col-lg-6 mb-2">
												<div class="form-group">
													<label class="text-label">Organization Id</label> <input
														type="text" id="tenantId" " name="tenantId"
														class="form-control" value="<%=accessObject.getTenantId()%>" readonly placeholder="">
												</div>
											</div>
											<div class="col-lg-6 mb-2">
												<div class="form-group">
													<label class="text-label">Practice*</label> <input
														type="text" name="practiceCode" id="practiceCode"
														class="form-control" placeholder="" required>
												</div>
											</div>
											<div class="col-lg-12 mb-2">
												<div class="form-group">
													<label class="text-label">Practice's Name*</label> <input
														type="text" class="form-control" id="practiceName"
														name="practiceName" aria-describedby="categoryName"
														placeholder="" required>
												</div>
											</div>

											<div class="col-lg-12 mb-2">
												<div class="form-group">
													<label class="text-label">Description*</label>
													<textarea rows="4" class="form-control"
														id="practiceDescription" name="practiceDescription"
														aria-describedby="categoryName" placeholder="" required
														cols=""></textarea>
												</div>
											</div>
											<div class="col-lg-12 mb-2">
												<div class="form-group">
													<label class="text-label">Value*</label>
													<textarea rows="4" class="form-control" id="practiceValue"
														name="practiceValue" aria-describedby="categoryName"
														placeholder="" required cols=""></textarea>
												</div>
											</div>
											<div class="col-lg-12 mb-2">
												<div class="form-group">
													<label class="text-label">Example Activities*</label>
													<textarea rows="4" class="form-control"
														id="practiceExamples" name="practiceExamples"
														aria-describedby="categoryName" placeholder="" required
														cols=""></textarea>
												</div>
											</div>
											<div class="col-lg-6 mb-2">
											<div class="form-group">
                                            <label>Practice Level</label>
                                            <select required class="form-control " id="practiceLevelCode" name="practiceLevelCode">
                                                <%
							for(LookupMst entry:practiceLevelLookup)
							{
								
							%>
							<option value="<%=entry.getLookupValue()%>"><%=entry.getDisplayValue()%></option>
							<%
							}
							%>
                                            </select>
                                        </div>
                                        </div>
										</div>
										<input type="hidden" id="authStatus" name="authStatus">
										<input type="hidden" id="businessUnit" name="businessUnit">
										<input type="hidden" id="isActive" name="isActive">
										<input type="hidden" id="organizationUnit" name="organizationUnit">
										<input type="hidden" id="process" name="process">
										<input type="hidden" id="project" name="project">
										<input type="hidden" id="capabilityCode" name="capabilityCode">
										<input type="hidden" id="categoryCode" name="categoryCode">
										<input type="hidden" id="currentGapNptes" name="currentGapNptes">
										<input type="hidden" id="currentPractice" name="currentPractice">
										<input type="hidden" id="isSaved" name="isSaved">
										<input type="hidden" id="practiceAreaCode" name="practiceAreaCode">
										<input type="hidden" id="practiceBusinessModel" name="practiceBusinessModel">
										<input type="hidden" id="practiceBusinessUnit" name="practiceBusinessUnit">
										<input type="hidden" id="practiceCompliance" name="practiceCompliance">
										<input type="hidden" id="practiceComplianceLevel" name="practiceComplianceLevel">
										<input type="hidden" id="practiceLevelCode" name="practiceLevelCode">
										<input type="hidden" id="practiceValue" name="practiceValue">
										<button type="submit" class="btn btn-primary btn sweet-text"
											id="commit" name="commit">CREATE</button>

									</div>



								</form>
							</div>
						</div>
					</div>
				</div>

				<!---------Body End of Form------------>
				<!---------Body Start of Tables--------->
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">practice Details</h4>
							</div>
							<div class="card-body">
								<div class="table-responsive">
									<table id="example3" class="display min-w850">
										<thead>
											<tr>

												<th>Sr No</th>
												<th>Practice</th>
												<th>Practice Name</th>
												<th>Description</th>
												<th>Value</th>
												<th>Example Activities</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<%
												for (PracticeMst entry : objectList) {
													srNo = srNo + 1;
											%>
											<tr>

												<td><%=srNo%></td>

												<td><%=entry.getPracticeCode()%></td>
												<td><%=entry.getPracticeName()%></td>
												<td><%=entry.getPracticeDescription()%></td>
												<td><%=entry.getPracticeValue()%></td>
												<td><%=entry.getPracticeExamples()%></td>
												<td>
													<div class="d-flex">
														<a
															onclick="updateObject('<%=entry.getPracticeCode()%>');"
															class="btn btn-primary shadow btn-xs sharp mr-1"><i
															class="fa fa-pencil"></i></a> <a
															onclick="deleteObject('<%=entry.getPracticeCode()%>');"
															class="btn btn-danger shadow btn-xs sharp"><i
															class="fa fa-trash"></i></a>
													</div>
												</td>
											</tr>
											<%
												}
											%>


										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!---------Body End of Tables--------->
			</div>
		</div>
		<!--**********************************
            Content body end
        ***********************************-->


	</div>
	<script src="theme-vora/vendor/global/global.min.js"
		type="text/javascript"></script>
	<script
		src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"
		type="text/javascript"></script>
	<script src="theme-vora/js/custom.min.js" type="text/javascript"></script>
	<script src="theme-vora/js/dlabnav-init.js" type="text/javascript"></script>
	<script src="theme-vora/vendor/jquery-steps/build/jquery.steps.min.js"
		type="text/javascript"></script>
	<script
		src="theme-vora/vendor/jquery-validation/jquery.validate.min.js"
		type="text/javascript"></script>
	<script src="theme-vora/js/plugins-init/jquery.validate-init.js"
		type="text/javascript"></script>

	<script
		src="theme-vora/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js"
		type="text/javascript"></script>
	<script src="theme-vora/vendor/datatables/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="theme-vora/js/plugins-init/datatables.init.js"
		type="text/javascript"></script>
	<script src="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.js"
		type="text/javascript"></script>
	<script src="theme-vora/js/plugins-init/sweetalert.init.js"
		type="text/javascript"></script>


	<script type="text/javascript">
		$(document).ready(function() {

			$("#commit").click(function() {
			});

		});
	</script>


	<script type="text/javascript">
		function updateTableDetails(tid, cCode, cName, pDesc, pvalue, pexample,
				cIsActive, mode,
				authStatus,
				businessUnit,
				isActive,
				organizationUnit,
				process,
				project,
				capabilityCode,
				categoryCode,
				currentGapNptes,
				currentPractice,
				isSaved,
				practiceAreaCode,
				practiceBusinessModel,
				practiceBusinessUnit,
				practiceCompliance,
				practiceComplianceLevel,
				practiceLevelCode,
				practiceValue
) {
			$("#tenantId").val(tid);
			$("#practiceCode").val(cCode);
			$("#practiceName").val(cName);
			$("#practiceDescription").val(pDesc);
			$("#practiceValue").val(pvalue);
			$("#practiceExamples").val(pexample);
			$("#isActive").val(cIsActive);
			$("#mode").val(mode);
			$("#authStatus").val(authStatus);
			$("#businessUnit").val(businessUnit);
			$("#isActive").val(isActive);
			$("#organizationUnit").val(organizationUnit);
			$("#process").val(process);
			$("#project").val(project);
			$("#capabilityCode").val(capabilityCode);
			$("#categoryCode").val(categoryCode);
			$("#currentGapNptes").val(currentGapNptes);
			$("#currentPractice").val(currentPractice);
			$("#isSaved").val(isSaved);
			$("#practiceAreaCode").val(practiceAreaCode);
			$("#practiceBusinessModel").val(practiceBusinessModel);
			$("#practiceBusinessUnit").val(practiceBusinessUnit);
			$("#practiceCompliance").val(practiceCompliance);
			$("#practiceComplianceLevel").val(practiceComplianceLevel);
			$("select#practiceLevelCode").val(practiceLevelCode);
			$("#practiceValue").val(practiceValue);

 var mySelect = $('#practiceLevelCode');

   
      mySelect.find('option:selected').prop('disabled', false);
      mySelect.selectpicker('refresh');


			document.getElementById('commit').innerHTML = mode;
			window.scrollTo(0, 0);
		}
	</script>
	
	<script type="text/javascript">
		function updateMode(mode) {
			$("#mode").val(mode);
			document.getElementById('commit').innerHTML = mode;

			window.scrollTo(0, 0);

		}
	</script>


<script type="text/javascript">
function updateObject(practiceCode){
var mode="Edit";
	$.ajax({
						url: "practice/getAjax",
						async: false,
						data: {
							practiceCode: practiceCode
						},
						success: function (jsonResponse) {
							$("#mode").val(mode);
							$("#practiceCode").val(jsonResponse.practiceCode);
							$("#practiceName").val(jsonResponse.practiceName);
							$("#practiceDescription").val(jsonResponse.practiceDescription);
							$("#practiceValue").val(jsonResponse.practiceValue);
							$("#practiceExamples").val(jsonResponse.practiceExamples);
							$("#practiceLevelCode").val(jsonResponse.practiceLevelCode);
							$("#practiceCode").prop("readonly", true);
							document.getElementById('commit').innerHTML = mode;
									window.scrollTo(0, 0);

						}
	});

} 
</script>


<script type="text/javascript">
function deleteObject(practiceCode){
var mode="DELETE";
	$.ajax({
						url: "practice/getAjax",
						async: false,
						data: {
							practiceCode: practiceCode
						},
						success: function (jsonResponse) {
							$("#mode").val(mode);
							$("#practiceCode").val(jsonResponse.practiceCode);
							$("#practiceName").val(jsonResponse.practiceName);
							$("#practiceDescription").val(jsonResponse.practiceDescription);
							$("#practiceValue").val(jsonResponse.practiceValue);
							$("#practiceExamples").val(jsonResponse.practiceExamples);
							$("#practiceLevelCode").val(jsonResponse.practiceLevelCode);
							$("#practiceCode").prop("readonly", true);
							document.getElementById('commit').innerHTML = mode;
							window.scrollTo(0, 0);
			

						}
	});

} 
</script>



</body>


</html>
