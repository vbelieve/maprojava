<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.UserMst"%>
<%@page import="com.login.domain.ProjectMst"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro</title>
    <!-- Favicon icon -->
<link href="theme-vora/css/style.css" rel="stylesheet">
<link href="customized/css/new-style.css" rel="stylesheet">
  <title>Admin</title>

  <!-- Custom fonts for this template -->

</head>

<body id="page-top">
<div id="main-wrapper">

<%@include file="preloader.jsp"%>
<%@include file="navheader.jsp"%>
<%@include file="head.jsp"%>
<%@include file="sidebar.jsp"%>
<%
String msg=(String) request.getAttribute("msg");
String[] messageType=null;
String alertType="alert alert-info alert-dismissible fade show";
String alertStr="Info!";

if(null==msg || msg.equalsIgnoreCase(""))
{
	msg="";
}
else
{
System.out.println("msg>>>>>>>>>>>>>>>>>>"+msg);	
messageType=msg.split("~");
msg=messageType[1];
}

if(!msg.equalsIgnoreCase("") && messageType.length>1)
{
	if(messageType[0].equalsIgnoreCase("SUCCESS"))
	{
	alertType="alert alert-success alert-dismissible fade show";
	alertStr="Success!";
	}
	else
	{
	alertType="alert alert-danger alert-dismissible fade show";
	alertStr="Error!";
	}
}

List<UserMst> users=new ArrayList<UserMst>();
List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
if(request.getAttribute("users") != null )
{
	users=(List<UserMst>) request.getAttribute("users");
	
	
	for(UserMst user:users)
	{
		System.out.println(">>>>"+user.getUserName());
	}
}

allProjects=(List<ProjectMst>) request.getAttribute("allProjects");
	if(null==allProjects)
	{
		allProjects=new ArrayList<ProjectMst>();
	}
Integer srNo=0;
SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
String[] projects=accessObject.getProject().split(",");
%>




        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
			<% if (!msg.equalsIgnoreCase(""))
			{
			%>
			<div class="<%=alertType%>">
									<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
									
									<strong><%=alertStr%></strong> <%=msg%>
									<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                    </button>
								</div>
								
								<%
			}
								%>
				
				<!---------Body Start of Form--------->
			<div class="row">
                    <div class="col-xl-12 col-xxl-12 ">
                        <div class="card">
                            <div class="card-header">
                    
                            </div>
                            <div class="card-body">
								
									<form role="form" class="form" action = "/GapAnalysis-2.0/userMst/save" id="formMaster" method="post">
									<input type="hidden" id="msg" value="<%=msg%>">
									<input type="hidden" id="mode" name="mode" value="CREATE">
									<input type="hidden" id="project" name="project" value="">
										<div id="wizard_Service" class="tab-pane" role="tabpanel">
									<div class="row justify-content-center">
	<%
	 for(ProjectMst project:allProjects)
	 {
	 %>
<div class="col-sm-4">
								<div style="cursor: pointer;" class="card shadow" id="<%=project.getProjectCode()%>"  onclick="selectedProject(this.id)">
									<div class="card-body ">
										<div class="media align-items-center">
											<div class="media-body mr-3">
												<h2 class="num-text fs-30">Project</h2>
												<span class="fs-16 text-black"><%=project.getProjectName()%></span>
												<br>
												<br>
												<span class="fs-16 text-black">(CMMI VERSION : <%=project.getCmmiVersion()%>)</span>
											</div>
											
										</div>
									</div>
								</div>
							</div>											
<%
	 }
	%>
</div>


&nbsp;<br><br>

<a hidden>
The <b>Gap Analysis</b> application is designed for performing an assessment of the current practices and to identify the strengths and weakness of the practice. The practices in the application are based on the CMMI V2.0 framework and factors domain views of Development and Services from maturity level 1 to maturity level 5.<br><br>

 

The application is designed for easy configuration for any organization irrespective of the products services they offer. The application includes a feature rich dashboard which provides insight on the different compliance levels namely Categories, Capability Areas, Practice areas and Practices.<br><br>

 

The administrator can create an organization and define the Organisation Unit (OU), Business Unit (BU) and add projects associated with the OU and BU. User management allows users to be allocated to specific projects or services based on the domain view selected. Role based permissions are provided for each user which allows the user to use the application for the role he or she is performing.<br><br>

 

Once the organisation setup is completed, users associated with the projects can assess the compliance levels for their respective projects by logging in the compliance interface. The compliance interface includes all Capability areas, Practice areas and Practices including guidance available for interpreting the practice . Users can input information for every practice related to current work products associated with the practice and check it's coverage for adequacy. Provision is available for capturing current or As-Is practice and the gap areas. Every practice can be characterized using 5 ratings viz, Fully Meets (FM), Largely Meets (LM), Partially Meets (PM), Does Not Meet (DM) and Not Yet (NY).<br><br>

 

Once all practices are assessed and their compliances saved, the users can view the dashboard for various levels of compliance namely Category compliance score, Capability area compliance score Practice area compliance score and Project level compliance score.<br><br>


</a>

	
												
											</div>

										</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            
				<!---------Body End of Form------------>
				<!---------Body Start of Tables--------->
				    
				<!---------Body End of Tables--------->
				</div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


</div>
<script src="theme-vora/vendor/global/global.min.js"></script>
	<script src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="theme-vora/js/custom.min.js"></script>
	<script src="theme-vora/js/dlabnav-init.js"></script>
	<script src="theme-vora/vendor/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="theme-vora/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="theme-vora/js/plugins-init/jquery.validate-init.js"></script>

<script src="theme-vora/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js"></script>
<script src="theme-vora/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="theme-vora/js/plugins-init/datatables.init.js"></script>	
<script src="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="theme-vora/js/plugins-init/sweetalert.init.js"></script>


<script type="text/javascript">
$(document).ready(function() {



});
</script>

<script type="text/javascript">
function selectedProject(pid){
$("#project").val(pid);
document.forms["formMaster"].action = "/GapAnalysis-2.0/complianceProjectSelect";
document.forms["formMaster"].submit();	

}
</script>
	
</body>
</html>