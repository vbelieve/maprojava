<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.ProjectMst"%>
<%@page import="com.login.domain.BusinessUnitMst"%>
<%@page import="com.login.domain.OrganizationUnitMst"%>
<%@page import="com.login.domain.TenantMst"%>
<%@page import="java.util.*"%>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="theme-vora/images/logo-Client.png">
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/jquery-smartwizard/dist/css/smart_wizard.min.css" rel="stylesheet">
<link href="theme-vora/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
	<link rel="stylesheet" href="theme-vora/vendor/chartist/css/chartist.min.css">
	<!-- Vectormap -->
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="theme-vora/css/style.css" rel="stylesheet">
	<link href="theme-vora/vendor/owl-carousel/owl.carousel.css" rel="stylesheet">

  <title>Admin</title>

  <!-- Custom fonts for this template -->

</head>

<body id="page-top">
<%
String msg=(String) request.getAttribute("msg");
String[] messageType=null;
String alertType="alert alert-info alert-dismissible fade show";
String alertStr="Info!";

if(null==msg || msg.equalsIgnoreCase(""))
{
	msg="";
}
else
{
System.out.println("msg>>>>>>>>>>>>>>>>>>"+msg);	
messageType=msg.split("~");
msg=messageType[1];
}

if(!msg.equalsIgnoreCase("") && messageType.length>1)
{
	if(messageType[0].equalsIgnoreCase("SUCCESS"))
	{
	alertType="alert alert-success alert-dismissible fade show";
	alertStr="Success!";
	}
	else
	{
	alertType="alert alert-danger alert-dismissible fade show";
	alertStr="Error!";
	}
}

List<ProjectMst>objectList= (List<ProjectMst>) request.getAttribute("objectList");

if(null==objectList)
{
	objectList=new ArrayList<ProjectMst>();
}
List<BusinessUnitMst>businessUnitList= (List<BusinessUnitMst>) request.getAttribute("businessUnitList");
List<OrganizationUnitMst>organizationUnitList= (List<OrganizationUnitMst>) request.getAttribute("organizationUnitList");
TenantMst tenantDetails = new TenantMst();
		tenantDetails = (TenantMst) request.getAttribute("tenantDetails");
		if (null == tenantDetails) {
			tenantDetails = new TenantMst();
			tenantDetails.setBusinessModel("DEFAULT");
		}
		String[] models = tenantDetails.getBusinessModel().split(",");
if(null==businessUnitList)
{
	businessUnitList=new ArrayList<BusinessUnitMst>();
}
if(null==organizationUnitList)
{
	organizationUnitList=new ArrayList<OrganizationUnitMst>();
}
Integer srNo=0;
%>

<div id="main-wrapper">


<%@include file="preloader.jsp"%>
<%@include file="navheader.jsp"%>
<%@include file="head.jsp"%>
<%@include file="sidebar.jsp"%>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
			<% if (!msg.equalsIgnoreCase(""))
			{
			%>
			<div class="<%=alertType%>">
									<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
									
									<strong><%=alertStr%></strong> <%=msg%>
									<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                    </button>
								</div>
								
								<%
			}
								%>
				
                
				<!---------Body Start of Form--------->
			<div class="row">
                    <div class="col-xl-12 col-xxl-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Project</h4>
                            </div>
                            <div class="card-body">
								
									<form role="form" class="form" action = "/GapAnalysis-2.0/project/save" id="masterRegistration" method="post">
									<input type="hidden" id="msg" value="<%=msg%>">
									<input type="hidden" id="isActive" name="isActive" value="">
									<input type="hidden" id="authStatus" name="authStatus" value="">
									<input type="hidden" id="mode" name="mode" value="CREATE">
									<input type="hidden" id="organizationModel" name="organizationModel">
									<input type="hidden" id="businessUnit" name="businessUnit">
									<input type="hidden" id="organizationUnit" name="organizationUnit">
									
									
										<div id="wizard_Service" class="tab-pane" role="tabpanel">
										
											<div class="row">
												<div hidden class="col-lg-6 mb-2">
													<div class="form-group">
														<label class="text-label">Organization Id</label>
														<input type="text" id="tenantId"  name="tenantId" class="form-control" readonly placeholder="" >
													</div>
												</div>
												<div hidden class="col-lg-6 mb-2">
													<div class="form-group">
														<label class="text-label">Project Code</label>
														<input type="text" id="projectCode"  name="projectCode" class="form-control" placeholder="" >
													</div>
												</div>
												<div class="col-lg-6 mb-2">
													<div class="form-group">
														<label class="text-label">Project's Name</label>
														<input type="text" id="projectName"  name="projectName" class="form-control" placeholder="" >
													</div>
												</div>
																		<div class="form-group">
                                            <label>Model</label>
                                            <select class="form-control" id="organizationModel1" name="organizationModel1">
                                                <%
							for(String entry:models)
							{
							%>
							<option value="<%=entry%>"><%=entry%></option>
							<%
							}
							%>

                                            </select>
                                        </div>
												
												
																		<div class="form-group">
                                            <label>Business Unit</label>
                                            <select class="form-control " id="businessUnit1" name="businessUnit1">
                                                <%
							for(BusinessUnitMst entry:businessUnitList)
							{
							%>
							<option value="<%=entry.getBusinessUnitCode()%>"><%=entry.getBusinessUnitCode()+"-"+entry.getBusinessUnitName()%></option>
							<%
							}
							%>

                                            </select>
                                        </div>
																<div class="form-group">
                                            <label>Location</label>
                                            <select class="form-control " id="organizationUnit1" name="organizationUnit1">
                                                <%
							for(OrganizationUnitMst entry:organizationUnitList)
							{
							%>
							<option value="<%=entry.getOrganizationUnitCode()%>"><%=entry.getOrganizationUnitCode()+"-"+entry.getOrganizationUnitName()%></option>
							<%
							}
							%>

                                            </select>
                                        </div>
											</div>
											<button type="submit" class="btn btn-primary btn sweet-text" id="commit" name="commit" >CREATE</button>
										</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            
				<!---------Body End of Form------------>
				<!---------Body Start of Tables--------->
				    <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Project Details</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example3" class="display min-w850">
                                        <thead>
                                            <tr>
                                           
											<th>Sr No</th>
                                            <th>Project Code</th>
											<th>Project Name</th>
											<th>Business Unit Code</th>
											<th>Model</th>
											<th>Location</th>
                                            <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<%
for(ProjectMst entry:objectList)
{
srNo=srNo+1;
%>
<tr>
                                            
												<td><%=srNo%></td>
												

<td><%=entry.getProjectCode()%></td>
<td><%=entry.getProjectName()%></td>
<%
							for(BusinessUnitMst ent:businessUnitList)
							{
								if(ent.getBusinessUnitCode().equalsIgnoreCase(entry.getBusinessUnit()))
								{
							%>
							<td><%=entry.getBusinessUnit()+"-"+ent.getBusinessUnitName()%></td>
							<%
							}
							}
							%>

<%
							for(String lkp:models)
							{
								if(lkp.equalsIgnoreCase(entry.getOrganizationModel()))
								{
							%>
							<td><%=lkp%></td>
							<%
							}
							}
							%>


<%
for(OrganizationUnitMst ent:organizationUnitList)
{
	if(ent.getOrganizationUnitCode().equalsIgnoreCase(entry.getOrganizationUnit()))
	{
%>
<td><%=entry.getOrganizationUnit()+"-"+ent.getOrganizationUnitName()%></td>
<%
}
}
%>



                                                <td>
													<div class="d-flex">
														<a onclick="updateTableDetails('<%=entry.getTenantId()%>',
															'<%=entry.getProjectCode()%>',
															'<%=entry.getProjectName()%>',
															'<%=entry.getBusinessUnit()%>',
															'<%=entry.getOrganizationModel()%>',
															'<%=entry.getOrganizationUnit()%>',
															'<%=entry.getIsActive()%>','EDIT',
															'<%=entry.getAuthStatus()%>');" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
														<a onclick="updateTableDetails('<%=entry.getTenantId()%>',
															'<%=entry.getProjectCode()%>',
															'<%=entry.getProjectName()%>',
															'<%=entry.getBusinessUnit()%>',
															'<%=entry.getOrganizationModel()%>',
															'<%=entry.getOrganizationUnit()%>',
															'<%=entry.getIsActive()%>','DELETE',
															'<%=entry.getAuthStatus()%>');" class="btn btn-danger shadow btn-xs sharp"><i class="fa fa-trash"></i></a>
													</div>												
												</td>												
                                            </tr>
<%
}
%>
                                            
											
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            
				<!---------Body End of Tables--------->
				</div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


</div>
<script src="theme-vora/vendor/global/global.min.js"></script>
	<script src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="theme-vora/js/custom.min.js"></script>
	<script src="theme-vora/js/dlabnav-init.js"></script>
	<script src="theme-vora/vendor/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="theme-vora/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="theme-vora/js/plugins-init/jquery.validate-init.js"></script>

<script src="theme-vora/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js"></script>
<script src="theme-vora/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="theme-vora/js/plugins-init/datatables.init.js"></script>	
<script src="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="theme-vora/js/plugins-init/sweetalert.init.js"></script>

<script type="text/javascript">
$(document).ready(function() {

$("#commit").click(function(){
var organizationUnit=$("#organizationUnit1").val();
var businessUnit=$("#businessUnit1").val();
var organizationModel=$("#organizationModel1").val();
$("#organizationUnit").val(organizationUnit);
$("#businessUnit").val(businessUnit);
$("#organizationModel").val(organizationModel);
});

});
</script>

	
<script type="text/javascript">
function updateTableDetails(tid,cCode,cName,cBusinessCode,cOrganizationModel,cOrgCode,cIsActive,mode,authstatus){
$("#tenantId").val(tid);
$("#authStatus").val(authStatus);

$("#projectCode").val(cCode);
$("#projectName").val(cName);

//////////
var organizationUnit=[];

organizationUnit=cOrgCode.split(",");

$("#organizationUnit1 option:selected").prop("selected", false);

for( var i=0;i<organizationUnit.length;i++)
{
	    $("#organizationUnit1 option[value='" + organizationUnit[i] + "']").prop("selected", true);
	
}

 var mySelect = $('#organizationUnit1');

   
      mySelect.find('option:selected').prop('disabled', false);
    //  mySelect.selectpicker('refresh');

/////////
////////
var businessUnit=[];

businessUnit=cBusinessCode.split(",");

$("#businessUnit1 option:selected").prop("selected", false);

for( var i=0;i<businessUnit.length;i++)
{
	    $("#businessUnit1 option[value='" + businessUnit[i] + "']").prop("selected", true);
	
}

 var mySelect = $('#businessUnit1');

   
      mySelect.find('option:selected').prop('disabled', false);
//      mySelect.selectpicker('refresh');

////////
var organizationModel=[];

organizationModel=cOrganizationModel.split(",");

$("#organizationModel1 option:selected").prop("selected", false);

for( var i=0;i<organizationModel.length;i++)
{
	    $("#organizationModel1 option[value='" + organizationModel[i] + "']").prop("selected", true);
	
}

 var mySelect = $('#organizationModel1');

   
      mySelect.find('option:selected').prop('disabled', false);
//      mySelect.selectpicker('refresh');




$("#isActive").val(cIsActive);
$("#mode").val(mode);
document.getElementById('commit').innerHTML = mode;
window.scrollTo(0,0);
} 
</script>		
<script type="text/javascript">
function updateMode(mode){
$("#mode").val(mode);
document.getElementById('commit').innerHTML = mode;

window.scrollTo(0,0);

} 
</script>		
</body>
</html>