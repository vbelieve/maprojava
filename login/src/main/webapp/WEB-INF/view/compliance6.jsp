<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.UserMst"%>
<%@page import="com.login.domain.ProjectMst"%>
<%@page import="com.login.domain.CategoryMst"%>
<%@page import="com.login.domain.CapabilityMst"%>
<%@page import="com.login.domain.PracticeAreaMst"%>
<%@page import="com.login.domain.PracticeMst"%>
<%@page import="com.login.domain.PracticeWorkProductMst"%>
<%@page import="com.login.domain.LookupMst"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="java.lang.*"%>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro</title>
    <!-- Favicon icon -->
<link href="theme-vora/css/style.css" rel="stylesheet">
<link href="customized/css/new-style.css" rel="stylesheet">
  <title>Admin</title>

  <!-- Custom fonts for this template -->

</head>

<body id="page-top">
<div id="main-wrapper">

<%@include file="preloader.jsp"%>
<%@include file="navheader.jsp"%>
<%@include file="head.jsp"%>
<%@include file="sidebar.jsp"%>
<%
String msg=(String) request.getAttribute("msg");
String[] messageType=null;
String alertType="alert alert-info alert-dismissible fade show";
String alertStr="Info!";

if(null==msg || msg.equalsIgnoreCase(""))
{
	msg="";
}
else
{
System.out.println("msg>>>>>>>>>>>>>>>>>>"+msg);	
messageType=msg.split("~");
msg=messageType[1];
}

if(!msg.equalsIgnoreCase("") && messageType.length>1)
{
	if(messageType[0].equalsIgnoreCase("SUCCESS"))
	{
	alertType="alert alert-success alert-dismissible fade show";
	alertStr="Success!";
	}
	else
	{
	alertType="alert alert-danger alert-dismissible fade show";
	alertStr="Error!";
	}
}
List<UserMst> users=new ArrayList<UserMst>();
List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
List<CategoryMst> allCategories=new ArrayList<CategoryMst>();
List<CapabilityMst> allCapabilities=new ArrayList<CapabilityMst>();
List<PracticeAreaMst> allPracticeArea=new ArrayList<PracticeAreaMst>();
List<PracticeMst> allPractices=new ArrayList<PracticeMst>();
List<PracticeWorkProductMst> allWorkProducts=new ArrayList<PracticeWorkProductMst>();
Map<String,String> complianceLookup=new HashMap<String,String>();
Map<String,String> coverageLookup=new HashMap<String,String>();
PracticeMst practices=new PracticeMst();
if(request.getAttribute("users") != null )
{
	users=(List<UserMst>) request.getAttribute("users");
	
	
	for(UserMst user:users)
	{
		System.out.println(">>>>"+user.getUserName());
	}
}

allProjects=(List<ProjectMst>) request.getAttribute("allProjects");
complianceLookup=(HashMap<String,String>) request.getAttribute("complianceLookup");
coverageLookup=(HashMap<String,String>) request.getAttribute("coverageLookup");
allWorkProducts=(List<PracticeWorkProductMst>) request.getAttribute("allWorkProducts");
practices=(PracticeMst) request.getAttribute("practices");
allCategories=(List<CategoryMst>) request.getAttribute("allCategories");
allCapabilities=(List<CapabilityMst>) request.getAttribute("allCapabilities");
allPracticeArea=(List<PracticeAreaMst>) request.getAttribute("allPracticeArea");
allPractices=(List<PracticeMst>) request.getAttribute("allPractices");
String complianceArray=(String)request.getAttribute("complianceArray");
String coverageArray=(String)request.getAttribute("coverageArray");
System.out.println("complianceArray>>>>"+complianceArray);
String complianceArray1[]=complianceArray.split("~");
String coverageArray1[]=coverageArray.split("~");
if(null==complianceLookup)
{
	complianceLookup=new HashMap<String,String>();
}
if(null==coverageLookup)
{
	coverageLookup=new HashMap<String,String>();
}
	if(null==allProjects)
	{
		allProjects=new ArrayList<ProjectMst>();
	}
	if(null==allWorkProducts)
	{
		allWorkProducts=new ArrayList<PracticeWorkProductMst>();
	}
	if(null==practices)
	{
		practices=new PracticeMst();
	}
	System.out.println(">>>>>>>>>"+practices.getPracticeExamples());
	if(null==allCategories)
	{
		allCategories=new ArrayList<CategoryMst>();
	}
	if(null==allCapabilities)
	{
		allCapabilities=new ArrayList<CapabilityMst>();
	}
	if(null==allPracticeArea)
	{
		allPracticeArea=new ArrayList<PracticeAreaMst>();
	}
	if(null==allPractices)
	{
		allPractices=new ArrayList<PracticeMst>();
	}
Integer srNo=0;
SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
String[] projects=accessObject.getProject().split(",");
%>




        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
			<% if (!msg.equalsIgnoreCase(""))
			{
			%>
			<div class="<%=alertType%>">
									<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
									
									<strong><%=alertStr%></strong> <%=msg%>
									<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                    </button>
								</div>
								
								<%
			}
								%>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<%=homeUrl%>">Home</a></li>
	<li class="breadcrumb-item"><a href="categories">Category</a></li>
	<li class="breadcrumb-item"><a href="capabilities">Capability</a></li>
	<li class="breadcrumb-item"><a href="practiceAreas">PracticeArea</a></li>
	<li class="breadcrumb-item"><a href="practices">Practice</a></li>
    <li class="breadcrumb-item active" aria-current="page">PracticeDetails</li>
  </ol>
</nav>				
				<!---------Body Start of Form--------->
			<div class="row">
                    <div class="col-xl-12 col-xxl-12">
                        <div class="card">
                            <div class="card-header">
                    
                            </div>
                            <div class="card-body">
								
									<form role="form" class="form" action = "#" id="formMaster" method="post">
									<input type="hidden" id="msg" value="<%=msg%>">
									<input type="hidden" id="pid" value="<%=accessObject.getProject()+"_"+practices.getPracticeCode()%>">
									<input type="hidden" id="mode" name="mode" value="CREATE">
<input type="hidden" name="tenantId" id="tenantId" value="<%=accessObject.getTenantId()%>">
<input type="hidden" name="project" id="project" value="<%=accessObject.getProject()%>">
<input type="hidden" name="category" id="category" value="<%=accessObject.getCategory()%>">
<input type="hidden" name="capability" id="capability" value="<%=accessObject.getCapability()%>">
<input type="hidden" name="practiceArea" id="practiceArea" value="<%=accessObject.getPracticeArea()%>">
<input type="hidden" name="practiceCode" id="practiceCode" value="<%=accessObject.getPractice()%>">

							<div id="wizard_Service" class="tab-pane" role="tabpanel">



<div>

<div>
<div class="row justify-content-center">
     
	<div class="col-sm-4">
								<div class="card shadow" id="<%=practices.getPracticeCode()%>">
									<div class="card-body">
										<div class="media align-items-center">
											<div class="media-body mr-3">
												<h2 class="num-text fs-30"><%=practices.getPracticeCode()%></h2>

												<span class="fs-16 text-black"><%=practices.getPracticeName()%></span>
											</div>
											
										</div>
									</div>
								</div>
							</div>	

	
	</div>
<div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Name</h6>
                </div>
                <div class="card-body">
                  <%=practices.getPracticeName()%>
                </div>
              </div>	

<div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Value</h6>
                </div>
                <div class="card-body">
                  <%=practices.getPracticeValue()%>
                </div>
              </div>

<div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Example Activities</h6>
                </div>
                <div class="card-body">
                  
                  <%String example=practices.getPracticeExamples();
					String [] examplesSplit=example.split("\\.");
					for(String aq:examplesSplit)
					{
						
%>
				<p><%=aq%></p>
				
<%
					}
				
%>
                </div>
              </div>

<div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Compliance Details</h6>
                </div>
				<div class="col-lg-12">
                <div class="card-body">
<!---->

                                <div class="table-responsive">
                                    <table class="table table-bordered table-responsive-sm">
                                        <thead><b>
                                            <tr>
                                              	<tr>
					
											<th>Current Work Product</th>
											<th>Available</th>
											<th>Coverage %</th>
											<th>Referred As</th>

										</tr>
                                            </tr>
                                        </thead>
                                    <tbody id="objectTable" name="objectTable">
										<%
											for (PracticeWorkProductMst practiceWorkProduct : allWorkProducts) {
												srNo = srNo + 1;
										%>
										<tr .pointer>
											
											<td><%=practiceWorkProduct.getWorkProductName()%></td>
											<td>
											<div class="radiooptiongroup">

									<div class="form-check form-check-inline">
									<%
									if(practiceWorkProduct.getQ1().equalsIgnoreCase("Y"))
									{	
									%>
										<input class="form-check-input" checked="true" id="<%=practiceWorkProduct.getId()%>" type="radio" name="q1<%=practiceWorkProduct.getId()%>" value="Y">
									<%
									}
									else
									{
									%>
									<input class="form-check-input" id="<%=practiceWorkProduct.getId()%>" type="radio" name="q1<%=practiceWorkProduct.getId()%>" value="Y">									
									<%}%>
										  <label class="form-check-label" for="inlineRadio1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
									<%
									if(practiceWorkProduct.getQ1().equalsIgnoreCase("N"))
									{	
									%>
										  <input class="form-check-input" type="radio" checked="true" id="<%=practiceWorkProduct.getId()%>" name="q1<%=practiceWorkProduct.getId()%>" value="N">
									<%
									}
									else
									{
									%>
										  <input class="form-check-input" type="radio" id="<%=practiceWorkProduct.getId()%>" name="q1<%=practiceWorkProduct.getId()%>" value="N">
									<%}%>
										  <label class="form-check-label" for="inlineRadio2">No</label>
									</div>
								</div>
								</td>
											<td>
											<select class="form-control" id="<%=practiceWorkProduct.getId()%>" name="q2<%=practiceWorkProduct.getId()%>">
																<option value="Select"  >Select</option>
																<%
									for (String coverage:coverageArray1) {
											String[] detailsCoverage=coverage.split(",");
											if(practiceWorkProduct.getQ2().equalsIgnoreCase(detailsCoverage[0]))
											{
												%>
												<option selected value="<%=detailsCoverage[0]%>"><%=detailsCoverage[1]%></option>
											<% }
												else
												{
									%>
									<option value="<%=detailsCoverage[0]%>"><%=detailsCoverage[1]%></option>
									<%
												}
										}
									%>
								
									
								</select>
								</td>
								<td>
								<%
									if(!practiceWorkProduct.getQ3().equalsIgnoreCase(""))
									{	
									%>
								<input type="text" class="form-control" id="<%=practiceWorkProduct.getId()%>" name="q3<%=practiceWorkProduct.getId()%>" value="<%=practiceWorkProduct.getQ3()%>" >
									<%
									}
									else
									{
									%>
								<input type="text" id="<%=practiceWorkProduct.getId()%>" name="q3<%=practiceWorkProduct.getId()%>" value="" >
								<%
									}
									%>
								</td>
											

										</tr>

										<%
											}
										%>

									</tbody></table>
                                </div>
                            
                        
                    <!---->
                </div>
              </div>
</div>
<div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Current Practice</h6>
                </div>
                <div class="card-body">
                  <textarea rows="4"  id="currentPractice" class="form-control fs-16 text-black" name="currentPractice" value="<%=practices.getCurrentPractice()%>"><%=practices.getCurrentPractice()%></textarea>
                </div>
              </div>

<div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Gap Notes</h6>
                </div>
                <div class="card-body">
                  <textarea id="currentGapNptes" class="form-control fs-16 text-black" name="currentGapNptes" value="<%=practices.getCurrentGapNptes()%>"><%=practices.getCurrentGapNptes()%></textarea>
                </div>
              </div>

<div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Compliance</h6>
                </div>
                <div class="card-body">
                  <div class="form-group">
                  <select class="form-control" id="compliance" name="compliance">
                             <%
							 
										for (String compliance:complianceArray1) {
											String[] details=compliance.split(",");
											System.out.println(">>>"+Math.round(practices.getPracticeCompliance())+">>>>>>"+details[0]);
											if(String.valueOf(Math.round(practices.getPracticeCompliance())).equalsIgnoreCase(details[0]))
											{
												%>
												<option selected value="<%=details[0]%>"><%=details[1]%></option>
											<% }
												else
												{
									%>
									<option value="<%=details[0]%>"><%=details[1]%></option>
									<%
												}
										}
									%>
                                            </select>
                                        </div>
                </div>
              </div>
&nbsp;<br><br>

<button type="submit" class="btn btn-primary btn sweet-text" id="commit" name="commit" >Submit</button>
</div>

	
												
											</div>

										</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            
				<!---------Body End of Form------------>
				<!---------Body Start of Tables--------->
				    
				<!---------Body End of Tables--------->
				</div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


</div>
<script src="theme-vora/vendor/global/global.min.js"></script>
	<script src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="theme-vora/js/custom.min.js"></script>
	<script src="theme-vora/js/dlabnav-init.js"></script>
	<script src="theme-vora/vendor/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="theme-vora/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="theme-vora/js/plugins-init/jquery.validate-init.js"></script>

<script src="theme-vora/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js"></script>
<script src="theme-vora/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="theme-vora/js/plugins-init/datatables.init.js"></script>	
<script src="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="theme-vora/js/plugins-init/sweetalert.init.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	
	var pid=$("#pid").val();

	var currentPractice=$("#currentPractice").val();
	var currentGapNptes=$("#currentGapNptes").val();
if(currentPractice!="" && currentPractice!=" "){}else{$("#currentPractice").val(getCookie(pid + "_currentPractice"));}	
if(currentGapNptes!="" && currentGapNptes!=" "){}else{$("#currentGapNptes").val(getCookie(pid + "_currentGapNptes"));}		
	
	
var erm=$("#msg").val();
if(erm!="null" && erm!="")
{
erm="";
}

$("#commit")
.click(
		function() {
			var pid=$("#pid").val();
			var compliance=$("#compliance").val();
			var currentPractice=$("#currentPractice").val();
			var currentGapNptes=$("#currentGapNptes").val();
	document.forms["formMaster"].action = "/GapAnalysis-2.0/practice/saveCompliance";
	document.forms["formMaster"]
					.submit();

		});
		
});
  

</script>



<script type="text/javascript">
$("#currentGapNptes").change(function(){
var currentGapNptes=$("#currentGapNptes").val();
var pid=$("#pid").val();
if(currentGapNptes!="" && currentGapNptes!=" ")
{
document.cookie = pid + "_currentGapNptes="+currentGapNptes+";";		
}
		
});
</script>

<script type="text/javascript">
$("#currentPractice").change(function(){
var currentPractice=$("#currentPractice").val();
var pid=$("#pid").val();
if(currentPractice!="" && currentPractice!=" ")
{
document.cookie = pid + "_currentPractice="+currentPractice+";";		
}
		
});
</script>

<script type="text/javascript">
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
</script>

<script type="text/javascript">
function selectedProject(pid){
$("#practice").val(pid);
document.forms["projectSelect"].action = "/GapAnalysis-2.0/compliancePracticeSelect";
document.forms["projectSelect"].submit();	

}
</script>


</body>
</html>