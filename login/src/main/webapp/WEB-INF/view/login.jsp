<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro - Maturity Profiler</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <link href="theme-vora/css/style.css" rel="stylesheet">

<script type="text/javascript">
function forgetPassword(){
alert("Please contact Admin to reset password");
return;
}
</script>
	
</head>

<body class="h-100">
<%
String msg="";
String erm="";
erm=(String)request.getParameter("erm");
System.out.println(">>>>>>>>>>>>>>>>>>>>MSG"+msg+"");
String[] messageType=null;
String alertType="alert alert-info alert-dismissible fade show";
String alertStr="Info!";

if(null==msg || msg.equalsIgnoreCase(""))
{
	msg="";
}
else
{
System.out.println("msg>>>>>>>>>>>>>>>>>>"+msg);	

if(null!=erm && !erm.equalsIgnoreCase("") && !erm.equalsIgnoreCase(" "))
{
if(erm.equalsIgnoreCase("02"))
{
	msg="Invalid UserName or Password";
}
if(erm.equalsIgnoreCase("03"))
{
	msg="License Date is Expired.Please contact admin.";
}
if(erm.equalsIgnoreCase("04"))
{
	msg="No License defined for Organization.Please contact admin.";
}

}

}

if(!msg.equalsIgnoreCase("") && messageType.length>1)
{
	if(messageType[0].equalsIgnoreCase("SUCCESS"))
	{
	alertType="alert alert-success alert-dismissible fade show";
	alertStr="Success!";
	}
	else
	{
	alertType="alert alert-danger alert-dismissible fade show";
	alertStr="Error!";
	}
}
%>
    <div class="authincation h-100">
        <div class="container h-100">
<% if (!msg.equalsIgnoreCase(""))
							{
							%>
							<div class="<%=alertType%>">
									<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
									
									<strong><%=alertStr%></strong> <%=msg%>
									<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                    </button>
								</div>
								
								<%
			}
								%>						
            <div class="row justify-content-center h-100 align-items-center">

				
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
							                          <div class="auth-form">
									<div class="text-center mb-3">
										<a href="#"><img src=".theme-vora/images/logo-full.png" alt=""></a>
									</div>
                                    <h4 class="text-center mb-4 text-white">Mapro (Maturity Profiler)</h4>
                                    <form id="loginform" action="/GapAnalysis-2.0/loginUser" method="post">
									<input type="hidden" autocomplete id="tenantId" name="tenantId" value="<%=request.getParameter("tid")%>">
									<input type="hidden" autocomplete id="error" name="error" value="<%=request.getParameter("erm")%>">
									<input type="hidden" id="organizationName" value="">
									<input type="hidden" id="userType" value="">
                                        <div class="form-group">
                                            <label class="mb-1 text-white"><strong>UserName</strong></label>
                                            <input type="text" autocomplete id="UserName" required  name="UserName" class="form-control" value="">
                                        </div>
                                        <div class="form-group">
                                            <label class="mb-1 text-white"><strong>Password</strong></label>
                                            <input type="password" id="password" required minlength="8" maxlength="16" name="password" class="form-control" value="">
                                        </div>
                                        <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                            <div class="form-group">
                                               <div class="custom-control custom-checkbox ml-1 text-white">
													<input type="checkbox" class="custom-control-input" id="basic_checkbox_1">
													<label hidden class="custom-control-label" for="basic_checkbox_1">Remember my preference</label>
												</div>
                                            </div>
                                            <div class="form-group">
                                                <a class="text-white" onclick="forgetPassword()" href="#">Forgot Password?</a>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" id="commit" class="btn bg-white text-primary btn-block">Login</button>
                                        </div>
                                    </form>
                                    <div class="new-account mt-3">
                                        <p hidden class="text-white">Don't have an account? <a class="text-white" href="./page-register.html">Sign up</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="theme-vora/vendor/global/global.min.js"></script>
	<script src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="theme-vora/js/custom.min.js"></script>
    <script src="theme-vora/js/dlabnav-init.js"></script>
<script type="text/javascript">
$(document).ready(function(){
var tenantId=$("#tenantId").val();
var erm=$("#error").val();
if(erm!="null")
{
if(erm=="02")
{
	alert("Invalid UserName or Password");
	$("#commit").attr("disabled","true");
	window.location.replace("/GapAnalysis-2.0/?tid="+tenantId);
	return;

}
if(erm=="03")
{
	alert("License Date is Expired.Please contact admin.");
	$("#commit").attr("disabled","true");
	window.location.replace("/GapAnalysis-2.0/?tid="+tenantId);
	return;
}
if(erm=="04")
{
	alert("No License defined for Organization.Please contact admin.")
	$("#commit").attr("disabled","true");
	window.location.replace("/GapAnalysis-2.0/?tid="+tenantId);
	return;
}
}

if(null==tenantId || tenantId=="" || tenantId=="null")
{
	alert("Tenant Id not present in URL. Please check URL and Try Again. Eg : http://localhost:8588/GapAnalysis-2.0/?tid=0");
	$("#commit").attr("disabled","true");
	window.location.replace("/GapAnalysis-2.0/?tid="+tenantId);
	return;
}
else {
$.ajax({
		type : "GET",
		url:"tenant/"+tenantId,
		success:function(jsonResponse)
		{
if(null==jsonResponse || jsonResponse=="")
{
	errorTenantNotPresent(tenantId);
}
else
{
	
	$("#orgName").text(jsonResponse.organizationName);
}
		},
		error:function(e)
		{
			errorTenantNotPresent(tenantId);
		}
	});
}

});
</script>

<script type="text/javascript">
function errorTenantNotPresent(tid){
alert("Tenant Id: "+tid+" not Present. Please check URL and Try again");
$("#commit").attr("disabled","true");
window.location.replace("/GapAnalysis-2.0/?tid="+tenantId);
return;
}
</script>



</body>

</html>