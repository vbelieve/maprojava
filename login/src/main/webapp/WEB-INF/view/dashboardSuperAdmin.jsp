<!DOCTYPE html>
<%@page import="com.login.domain.TenantMst"%>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.UserMst"%>
<%@page import="com.login.domain.ProjectMst"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="theme-vora/images/logo-Client.png">
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/jquery-smartwizard/dist/css/smart_wizard.min.css" rel="stylesheet">
<link href="theme-vora/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
	<link rel="stylesheet" href="theme-vora/vendor/chartist/css/chartist.min.css">
	<!-- Vectormap -->
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="theme-vora/css/style.css" rel="stylesheet">
	<link href="theme-vora/vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="customized/css/new-style.css" rel="stylesheet">

  <title>Admin</title>

  <!-- Custom fonts for this template -->

</head>

<body id="page-top">
<div id="main-wrapper">

<%@include file="preloader.jsp"%>
<%@include file="navheader.jsp"%>
<%@include file="head.jsp"%>
<%@include file="sidebar.jsp"%>
<%
String msg=(String) request.getAttribute("msg");
String[] messageType=null;
String alertType="alert alert-info alert-dismissible fade show";
String alertStr="Info!";

if(null==msg || msg.equalsIgnoreCase(""))
{
	msg="";
}
else
{
System.out.println("msg>>>>>>>>>>>>>>>>>>"+msg);	
messageType=msg.split("~");
msg=messageType[1];
}

if(!msg.equalsIgnoreCase("") && messageType.length>1)
{
	if(messageType[0].equalsIgnoreCase("SUCCESS"))
	{
	alertType="alert alert-success alert-dismissible fade show";
	alertStr="Success!";
	}
	else
	{
	alertType="alert alert-danger alert-dismissible fade show";
	alertStr="Error!";
	}
}

List<UserMst> users=new ArrayList<UserMst>();
List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
List<TenantMst> tenantList=new ArrayList<TenantMst>();

if(request.getAttribute("users") != null )
{
	users=(List<UserMst>) request.getAttribute("users");
	
	
	for(UserMst user:users)
	{
		System.out.println(">>>>"+user.getUserName());
	}
}

allProjects=(List<ProjectMst>) request.getAttribute("allProjects");
	if(null==allProjects)
	{
		allProjects=new ArrayList<ProjectMst>();
	}

	tenantList=(List<TenantMst>) request.getAttribute("tenantList");
if(null==tenantList)
{
	tenantList=new ArrayList<TenantMst>();
}
	
Integer srNo=0;
SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
String[] projects=accessObject.getProject().split(",");
%>

<form id="masterRegistration" action="#" method="post">
				<input type="hidden" name="selectedtenantId" id="selectedtenantId" value="">
				</form>
        <!--**********************************
            Content body start
        ***********************************-->
<div class="content-body" style="min-height: 788px;">
            <!-- row -->
			<div class="container-fluid">
				<!-- Add Order -->
				<div class="row">
					<div class="col-xl-6">
						<div class="row">
															<%
      for(TenantMst tenant:tenantList)
	 {
		 if(!tenant.getTenantId().equalsIgnoreCase("0"))
										{
											String colour="";
											
											if(tenant.getComplianceScore()<31)
											{
												colour="red";
											}
											else if(tenant.getComplianceScore()<71)
											{
												colour="#e6e600";
											}
											else if(tenant.getComplianceScore()<91)
											{
												colour="#ffbf00";
											}
											else if(tenant.getComplianceScore()>90)
											{
												colour="green";
											}
											
	 %>

							<div class="col-sm-6">
								<div class="card">
									<div class="card-body" id="<%=tenant.getTenantId()%>" onclick="selectedProject(this.id)">
										<div class="media align-items-center">
											<div class="media-body mr-3 align-items-center">
											
											<h2 class=" fs-40 text-black font-w600" style="text-align:left;">
												<%=tenant.getComplianceScore()%>%
											<span style="float:right;">
													<svg width="36"  height="36" viewBox="0 0 36 36" fill="none" color="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M34.4221 13.9831C34.3342 13.721 34.1756 13.4884 33.9639 13.3108C33.7521 13.1332 33.4954 13.0175 33.2221 12.9766L23.6491 11.5141L19.3531 2.36408C19.232 2.10638 19.04 1.88849 18.7996 1.73587C18.5592 1.58325 18.2803 1.5022 17.9956 1.5022C17.7108 1.5022 17.432 1.58325 17.1916 1.73587C16.9512 1.88849 16.7592 2.10638 16.6381 2.36408L12.3421 11.5141L2.76908 12.9766C2.49641 13.0181 2.24048 13.1341 2.02943 13.3117C1.81837 13.4892 1.66036 13.7215 1.57277 13.9831C1.48517 14.2446 1.47139 14.5253 1.53293 14.7941C1.59447 15.063 1.72895 15.3097 1.92158 15.5071L8.89808 22.6501L7.24808 32.7571C7.20306 33.0345 7.23685 33.3189 7.34561 33.578C7.45437 33.8371 7.63373 34.0605 7.86325 34.2226C8.09277 34.3847 8.36321 34.4791 8.64377 34.495C8.92432 34.5109 9.20371 34.4477 9.45008 34.3126L18.0001 29.5906L26.5501 34.3126C26.7965 34.4489 27.0762 34.5131 27.3573 34.4978C27.6385 34.4826 27.9097 34.3885 28.1399 34.2264C28.37 34.0643 28.55 33.8406 28.659 33.5811C28.7681 33.3215 28.8019 33.0365 28.7566 32.7586L27.1066 22.6516L34.0786 15.5071C34.2703 15.3091 34.4038 15.0622 34.4644 14.7933C34.525 14.5245 34.5103 14.2441 34.4221 13.9831Z" fill="<%=colour%>"></path>
											</svg>
												</span>
											</h2>
												
												<span class="fs-16" style=""><%=tenant.getOrganizationName()%></span>
											</div>
											
											
										</div>
									</div>
								</div>
							</div>
								 <%
	 }
	 }
	%>


						</div>
					</div>
					<div class="col-xl-6">
						<div class="row">
							<div class="col-xl-6 col-xxl-12 col-md-6">
								<div class="card">
									<div class="card-header shadow-sm">
										<div class="mr-2">
											<h4 class="fs-20 mb-0 font-w600 text-black">Projects</h4>
											
										</div>
										<svg width="36" height="36" viewBox="0 0 36 36" fill="green" xmlns="http://www.w3.org/2000/svg">
												<path d="M34.4221 13.9831C34.3342 13.721 34.1756 13.4884 33.9639 13.3108C33.7521 13.1332 33.4954 13.0175 33.2221 12.9766L23.6491 11.5141L19.3531 2.36408C19.232 2.10638 19.04 1.88849 18.7996 1.73587C18.5592 1.58325 18.2803 1.5022 17.9956 1.5022C17.7108 1.5022 17.432 1.58325 17.1916 1.73587C16.9512 1.88849 16.7592 2.10638 16.6381 2.36408L12.3421 11.5141L2.76908 12.9766C2.49641 13.0181 2.24048 13.1341 2.02943 13.3117C1.81837 13.4892 1.66036 13.7215 1.57277 13.9831C1.48517 14.2446 1.47139 14.5253 1.53293 14.7941C1.59447 15.063 1.72895 15.3097 1.92158 15.5071L8.89808 22.6501L7.24808 32.7571C7.20306 33.0345 7.23685 33.3189 7.34561 33.578C7.45437 33.8371 7.63373 34.0605 7.86325 34.2226C8.09277 34.3847 8.36321 34.4791 8.64377 34.495C8.92432 34.5109 9.20371 34.4477 9.45008 34.3126L18.0001 29.5906L26.5501 34.3126C26.7965 34.4489 27.0762 34.5131 27.3573 34.4978C27.6385 34.4826 27.9097 34.3885 28.1399 34.2264C28.37 34.0643 28.55 33.8406 28.659 33.5811C28.7681 33.3215 28.8019 33.0365 28.7566 32.7586L27.1066 22.6516L34.0786 15.5071C34.2703 15.3091 34.4038 15.0622 34.4644 14.7933C34.525 14.5245 34.5103 14.2441 34.4221 13.9831Z" fill="green"></path>
											</svg>
									</div>
									<div class="card-body btn btn-lg loadmore-content height620 dlab-scroll pb-0 ps ps--active-y text-left" id="RecentActivitiesContent">
									<%
									for(TenantMst tenant:tenantList)
									{
										if(!tenant.getTenantId().equalsIgnoreCase("0"))
										{
											String colour="";
											
											if(tenant.getComplianceScore()<1)
											{
												colour="#ea6020";
											}
											else if(tenant.getComplianceScore()<31)
											{
												colour="#ffbf00";
											}
											else if(tenant.getComplianceScore()<71)
											{
												colour="yellow";
											}
											else if(tenant.getComplianceScore()<101)
											{
												colour="green";
											}
									%>
										<div class="border-bottom pb-4 mb-4"  id="<%=tenant.getTenantId()%>" onclick="selectedProject(this.id)">
										
										
									
										
											
											
											<div class="row justify-content-between"  >
												<ul class="users col-6">
												<p class="mb-2"><%=tenant.getOrganizationName()%></p>
																								
											</ul>
											<div class="col-6 pl-0">
										<%for(ProjectMst project:allProjects)
									{
										if(project.getTenantId().equalsIgnoreCase(tenant.getTenantId()))
										{
										%>
												
												
													<h6 class="fs-14"><%=project.getProjectName()%>
														<span class="pull-right font-w600"><%=project.getComplianceScore()%>%</span>
													</h6>
													<div class="progress" style="height:7px;">
														<div class="progress-bar  progress-animated " style="background-color: <%=colour%> !important;  width: <%=project.getComplianceScore()%>%; color:green; height:7px;" role="progressbar">
															<span class="sr-only"><%=project.getComplianceScore()%>% Complete</span>
														</div>
													</div>
												
												<%
										}
									}%>
									</div>
											</div>
										</div>
									<%}
									}
									%>
										
									

								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>  
  <!--**********************************
            Content body end
        ***********************************-->


</div>
<script type="text/javascript">
function selectedProject(pid){
$("#selectedtenantId").val(pid);
document.forms["masterRegistration"].action = "/GapAnalysis-2.0/superAdmindashboardHome";
document.forms["masterRegistration"].submit();	

}
</script>

<script src="theme-vora/vendor/global/global.min.js"></script>
	<script src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="theme-vora/js/custom.min.js"></script>
	<script src="theme-vora/js/dlabnav-init.js"></script>
	<script src="theme-vora/vendor/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="theme-vora/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="theme-vora/js/plugins-init/jquery.validate-init.js"></script>

<script src="theme-vora/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js"></script>
<script src="theme-vora/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="theme-vora/js/plugins-init/datatables.init.js"></script>	
<script src="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="theme-vora/js/plugins-init/sweetalert.init.js"></script>


</script>		
</body>
</html>
