<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.UserMst"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="theme-vora/images/logo-Client.png">
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/jquery-smartwizard/dist/css/smart_wizard.min.css" rel="stylesheet">
<link href="theme-vora/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
	<link rel="stylesheet" href="theme-vora/vendor/chartist/css/chartist.min.css">
	<!-- Vectormap -->
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="theme-vora/css/style.css" rel="stylesheet">
	<link href="theme-vora/vendor/owl-carousel/owl.carousel.css" rel="stylesheet">

  <title>Admin</title>

  <!-- Custom fonts for this template -->

</head>

<body id="page-top">
<%
String msg=(String) request.getAttribute("msg");
String[] messageType=null;
String alertType="alert alert-info alert-dismissible fade show";
String alertStr="Info!";

if(null==msg || msg.equalsIgnoreCase(""))
{
	msg="";
}
else
{
System.out.println("msg>>>>>>>>>>>>>>>>>>"+msg);	
messageType=msg.split("~");
msg=messageType[1];
}

if(!msg.equalsIgnoreCase("") && messageType.length>1)
{
	if(messageType[0].equalsIgnoreCase("SUCCESS"))
	{
	alertType="alert alert-success alert-dismissible fade show";
	alertStr="Success!";
	}
	else
	{
	alertType="alert alert-danger alert-dismissible fade show";
	alertStr="Error!";
	}
}

List<UserMst> objectList=new ArrayList<UserMst>();
if(request.getAttribute("users") != null )
{
	objectList=(List<UserMst>) request.getAttribute("users");
	
}
Integer srNo=0;
SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
%>

<div id="main-wrapper">


<%@include file="preloader.jsp"%>
<%@include file="navheader.jsp"%>
<%@include file="head.jsp"%>
<%@include file="sidebar.jsp"%>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
			<% if (!msg.equalsIgnoreCase(""))
			{
			%>
			<div class="<%=alertType%>">
									<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
									
									<strong><%=alertStr%></strong> <%=msg%>
									<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                    </button>
								</div>
								
								<%
			}
								%>
				
                
				<!---------Body Start of Form--------->
			<div class="row">
                    <div class="col-xl-12 col-xxl-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">User Management</h4>
                            </div>
                            <div class="card-body">
								
									<form role="form" class="form" action = "/GapAnalysis-2.0/userMst/userAdminSave" id="masterRegistration" method="post">
									<input type="hidden" id="msg" value="<%=msg%>">
									<input type="hidden" id="mode" name="mode" value="CREATE">
									<input type="hidden" id="action" name="action" value="">
									
										<div id="wizard_Service" class="tab-pane" role="tabpanel">
										
											<div class="row">
												<div class="col-lg-6 mb-2">
													<div class="form-group">
														<label class="text-label">Organization Id</label>
														<input type="text" id="tenantId"  name="tenantId" class="form-control" readonly placeholder="" >
													</div>
												</div>
												<div class="col-lg-6 mb-2">
													<div class="form-group">
														<label class="text-label">UserName</label>
														<input type="text" id="UserName"  name="UserName" class="form-control" readonly placeholder="" >
													</div>
												</div>
																		
																
											</div>
											<!-- <button type="submit" class="btn btn-primary btn sweet-text" id="unlock"  name="unlock" >Unlock</button>-->
											<button type="submit" class="btn btn-primary btn sweet-text" id="reset"  name="reset" >Reset</button>
											<button type="submit" class="btn btn-primary btn sweet-text" id="deactivate"  name="deactivate" >Deactivate</button>
											<button type="submit" class="btn btn-primary btn sweet-text" id="activate"  name="activate" >Activate</button>
										</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            
				<!---------Body End of Form------------>
				<!---------Body Start of Tables--------->
				    <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">User Details</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example3" class="display min-w850">
                                        <thead>
                                            <tr>
                                           
												<th>Sr No</th>
												<th>UserName</th>
												<th>User Type</th>
												<th>Is Locked</th>
												<th>Is Active</th>
												<th>Last Login Time</th>
												
                                            </tr>
                                        </thead>
                                        <tbody>
										<%
for(UserMst entry:objectList)
{
srNo=srNo+1;
%>
<tr onclick="updateTableDetails('<%=entry.getTenantId()%>',
'<%=entry.getUserName()%>',
'<%=entry.getUserType()%>')";>
                                            
												<td><%=srNo%></td>
												<td><%=entry.getUserName()%></td>
												<td><%=entry.getUserType()%></td>
												<td><%=entry.getIsUserLocked()%></td>
												<td><%=entry.getIsActive()%></td>
												<td><%=sdf.format(entry.getLastLoginDate())%></td>
                                                												
                                            </tr>
<%
}
%>
                                            
											
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            
				<!---------Body End of Tables--------->
				</div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


</div>
<script src="theme-vora/vendor/global/global.min.js"></script>
	<script src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="theme-vora/js/custom.min.js"></script>
	<script src="theme-vora/js/dlabnav-init.js"></script>
	<script src="theme-vora/vendor/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="theme-vora/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="theme-vora/js/plugins-init/jquery.validate-init.js"></script>

<script src="theme-vora/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js"></script>
<script src="theme-vora/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="theme-vora/js/plugins-init/datatables.init.js"></script>	
<script src="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="theme-vora/js/plugins-init/sweetalert.init.js"></script>

<script type="text/javascript">
$(document).ready(function() {
var erm=$("#msg").val();
if(erm!="null" && erm!="")
{
alert(erm);
erm="";
}
$("#reset").click(function(){
$("#action").val('reset');
document.forms["userAdmin"].action = "/GapAnalysis-2.0/userMst/userAdminSave";
document.forms["userAdmin"].submit();	

});

$("#unlock").click(function(){
$("#action").val('unlock');
document.forms["userAdmin"].action = "/GapAnalysis-2.0/userMst/userAdminSave";
document.forms["userAdmin"].submit();	

});  

$("#deactivate").click(function(){
$("#action").val('deactivate');
document.forms["userAdmin"].action = "/GapAnalysis-2.0/userMst/userAdminSave";
document.forms["userAdmin"].submit();	

});

$("#activate").click(function(){
$("#action").val('activate');
document.forms["userAdmin"].action = "/GapAnalysis-2.0/userMst/userAdminSave";
document.forms["userAdmin"].submit();	

});

});
</script>

	
<script type="text/javascript">
function updateTableDetails(tid,userName,userType){
$("#tenantId").val(tid);
$("#UserName").val(userName);
$("#userType").val(userType);

window.scrollTo(0,0);
} 
</script>		
<script type="text/javascript">
function updateMode(mode){
$("#mode").val(mode);
document.getElementById('commit').innerHTML = mode;

window.scrollTo(0,0);

} 
</script>		
</body>
</html>