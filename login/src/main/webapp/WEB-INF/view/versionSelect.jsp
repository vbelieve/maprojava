
<!DOCTYPE html>
<%@page import="com.login.domain.LookupMst"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>

<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro - Maturity Profiler</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <link href="theme-vora/css/style.css" rel="stylesheet">

	
</head>

<body class="h-100">

<%
List<LookupMst> cmmiVersionLookup = new ArrayList<LookupMst>();
cmmiVersionLookup = (List<LookupMst>) request.getAttribute("cmmiVersionLookup");
if (null == cmmiVersionLookup) {
	cmmiVersionLookup = new ArrayList<LookupMst>();
}

Integer srNo = 0;
%>

    <div class="authincation h-100">
        <div class="container h-100">
				
            <div class="row justify-content-center h-100 align-items-center">

				
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
							                          <div class="auth-form">
									<div class="text-center mb-3">
										<a href="#"><img src=".theme-vora/images/logo-full.png" alt=""></a>
									</div>
                                    <h4 class="text-center mb-4 text-white">Cmmi Version Select</h4>
                                    <form id="loginform" action="/GapAnalysis-2.0/versionSelected" method="post">
									<input type="hidden" autocomplete id="tenantId" name="tenantId" value="<%=request.getParameter("tid")%>">
									<input type="hidden" autocomplete id="error" name="error" value="<%=request.getParameter("erm")%>">
									<input type="hidden" id="organizationName" value="">
									<input type="hidden" id="userType" value="">
										<div class="form-group">
											
											<div id="businessModel1">
												<%for (LookupMst entry : cmmiVersionLookup) {%>
												<div class="radio">
													<label class="text-center mb-4 text-white"> <input type="radio" name="versionSelected"
														value="<%=entry.getLookupValue()%>"> <%=entry.getDisplayValue()%>
													</label>
												</div>
												<%
            }%>
											</div>
										</div>

										
                                        <div class="text-center">
                                            <button type="submit" id="commit" class="btn bg-white text-primary btn-block">Select</button>
                                        </div>
                                    </form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="theme-vora/vendor/global/global.min.js"></script>
	<script src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="theme-vora/js/custom.min.js"></script>
    <script src="theme-vora/js/dlabnav-init.js"></script>


</body>

</html>