<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.UserMst"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin</title>
<%@include file="header.jsp"%>
  <!-- Custom fonts for this template -->
  <link href="customized/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="customized/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="customized/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.1.1.min.js">
  <script src="js/jquery-3.3.1.slim.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
var erm=$("#msg").val();
if(erm!="null" && erm!="")
{
alert(erm);
erm="";
}

});
  
});
</script>


</head>

<body id="page-top">
<%
String msg=(String) request.getAttribute("msg");
if(null==msg)
{
	msg="";
}
List<UserMst> users=new ArrayList<UserMst>();
if(request.getAttribute("users") != null )
{
	users=(List<UserMst>) request.getAttribute("users");
	for(UserMst user:users)
	{
		System.out.println(">>>>"+user.getUserName());
	}
}
Integer srNo=0;
SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
%>
  <!-- Page Wrapper -->
  <div id="wrapper">

    
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
		
		

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
&nbsp &nbsp
	 <a>
The <b>Gap Analysis</b> application is designed for performing an assessment of the current practices and to identify the strengths and weakness of the practice. The practices in the application are based on the CMMI V2.0 framework and factors domain views of Development and Services from maturity level 1 to maturity level 5.<br><br>

 

The application is designed for easy configuration for any organization irrespective of the products services they offer. The application includes a feature rich dashboard which provides insight on the different compliance levels namely Categories, Capability Areas, Practice areas and Practices.<br><br>

 

The administrator can create an organization and define the Organisation Unit (OU), Business Unit (BU) and add projects associated with the OU and BU. User management allows users to be allocated to specific projects or services based on the domain view selected. Role based permissions are provided for each user which allows the user to use the application for the role he or she is performing.<br><br>

 

Once the organisation setup is completed, users associated with the projects can assess the compliance levels for their respective projects by logging in the compliance interface. The compliance interface includes all Capability areas, Practice areas and Practices including guidance available for interpreting the practice . Users can input information for every practice related to current work products associated with the practice and check it's coverage for adequacy. Provision is available for capturing current or As-Is practice and the gap areas. Every practice can be characterized using 5 ratings viz, Fully Meets (FM), Largely Meets (LM), Partially Meets (PM), Does Not Meet (DM) and Not Yet (NY).<br><br>

 

Once all practices are assessed and their compliances saved, the users can view the dashboard for various levels of compliance namely Category compliance score, Capability area compliance score Practice area compliance score and Project level compliance score.<br><br>


</a>    
	 <form role="form" class="form" id="signup" method="post" style="width:50%;align:right;position:relative;left:50%;">
	<input type="hidden" id="msg" value="<%=msg%>">
             
	
            </form>
&nbsp



        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="customized/vendor/jquery/jquery.min.js"></script>
  <script src="customized/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="customized/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="customized/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="customized/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="customized/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="customized/js/demo/datatables-demo.js"></script>

</body>

<%@include file="footer.jsp"%>
</html>
