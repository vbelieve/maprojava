<!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a onclick="window.open(
            	  'https://www.eprama.com/',
            	  '_blank' // <- This is what makes it open in a new window.
            	);" class="brand-logo">
                <img class="logo-abbr" src="theme-vora/images/logo-Client.png" alt="">
                <img class="logo-compact" src="theme-vora/images/ePramaLogo.png" alt="">
                <img class="brand-title" src="theme-vora/images/ePramaLogo.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
<!--**********************************
            Nav header end
        ***********************************-->
		