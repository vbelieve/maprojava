<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.UserMst"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="theme-vora/images/logo-Client.png">
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="theme-vora/vendor/chartist/css/chartist.min.css">
	<!-- Vectormap -->
    <link href="theme-vora/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="theme-vora/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="theme-vora/css/style.css" rel="stylesheet">
	<link href="theme-vora/vendor/owl-carousel/owl.carousel.css" rel="stylesheet">

  <title>Admin</title>

  <!-- Custom fonts for this template -->

</head>

<body id="page-top">
<%
String msg = (String) request.getAttribute("msg");
String[] messageType = null;
String alertType = "alert alert-info alert-dismissible fade show";
String alertStr = "Info!";

if (null == msg || msg.equalsIgnoreCase("")) {
	msg = "";
} else {
	System.out.println("msg>>>>>>>>>>>>>>>>>>" + msg);
	messageType = msg.split("~");
	msg = messageType[1];
}

if (!msg.equalsIgnoreCase("") && messageType.length > 1) {
	if (messageType[0].equalsIgnoreCase("SUCCESS")) {
		alertType = "alert alert-success alert-dismissible fade show";
		alertStr = "Success!";
	} else {
		alertType = "alert alert-danger alert-dismissible fade show";
		alertStr = "Error!";
	}
}
List<UserMst> users=new ArrayList<UserMst>();
if(request.getAttribute("users") != null )
{
	users=(List<UserMst>) request.getAttribute("users");
	for(UserMst user:users)
	{
		System.out.println(">>>>"+user.getUserName());
	}
}
Integer srNo=0;
SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
%>
<div id="main-wrapper">


<%@include file="preloader.jsp"%>
<%@include file="navheader.jsp"%>
<%@include file="head.jsp"%>
<%@include file="sidebar.jsp"%>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
            <%
					if (!msg.equalsIgnoreCase("")) {
				%>
				<div class="<%=alertType%>">
					<svg viewBox="0 0 24 24" width="24" height="24"
						stroke="currentColor" stroke-width="2" fill="none"
						stroke-linecap="round" stroke-linejoin="round" class="mr-2">
						<circle cx="12" cy="12" r="10"></circle>
						<line x1="12" y1="16" x2="12" y2="12"></line>
						<line x1="12" y1="8" x2="12.01" y2="8"></line></svg>

					<strong><%=alertStr%></strong>
					<%=msg%>
					<button type="button" class="close h-100" data-dismiss="alert"
						aria-label="Close">
						<span><i class="mdi mdi-close"></i></span>
					</button>
				</div>

				<%
					}
				%>
				<!-- Add Order -->
				<div class="modal fade" id="addOrderModalside">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Create Project</h5>
								<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form>
									<div class="form-group">
										<label class="text-black font-w500">Project Name</label>
										<input type="text" class="form-control">
									</div>
									<div class="form-group">
										<label class="text-black font-w500">Deadline</label>
										<input type="date" class="form-control">
									</div>
									<div class="form-group">
										<label class="text-black font-w500">Client Name</label>
										<input type="text" class="form-control">
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-primary">CREATE</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
                <!--<div class="page-titles">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active"><a href="javascript:void(0)">Profile</a></li>
					</ol>
                </div>-->
                <!-- row -->
                <div class="row">
                    
                </div>
                <div class="row">
                    
                    <div class="col-xl-20">
                        <div class="card">
                            <div class="card-body">
                                <div class="profile-tab">
                                    <div class="custom-tab-1">
                                        <ul class="nav nav-tabs">
                                            
                                            <li class="nav-item "><a href="#gap-Analysis" data-toggle="tab" class="nav-link">Maturity Profiler (Mapro)</a>
                                            </li>
                                            <!--<li class="nav-item "><a href="#about-me" data-toggle="tab" class="nav-link">About</a>
                                            </li>-->
                                        </ul>
                                        <div class="tab-content">
                                            <div id="gap-Analysis" class="tab-pane fade active show">
                                                <div class="profile-about-me">
                                                    <div class="pt-4 border-bottom-1 pb-3">
													<div class="col-xl-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <div id="accordion-twelve" class="accordion accordion-rounded-stylish accordion-header-bg">
                                    <div class="accordion__item">
                                        <div class="accordion__header collapsed accordion__header--primary" data-toggle="collapse" data-target="#gradient_collapseOne" aria-expanded="false"> <span class="accordion__header--icon"></span>
                                            <span class="accordion__header--text">About Mapro</span>
                                            <span class="accordion__header--indicator"></span>
                                        </div>
                                        <div id="gradient_collapseOne" class=" collaps show accordion__body" data-parent="#accordion-twelve" style="">
                                            <div class="accordion__body--text">
                                                <p>The <b>Maturity Profiler (Mapro)</b> application is designed for performing an assessment of the current practices and to identify the strengths and weakness of the practice. The practices in the application are based on the CMMI V2.0 framework and factors domain views of Development and Services from maturity level 1 to maturity level 5.</p>
												<p>The application is designed for easy configuration for any organization irrespective of the products services they offer. The application includes a feature rich dashboard which provides insight on the different compliance levels namely Categories, Capability Areas, Practice areas and Practices. </p>
												
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion__item">
                                        <div class="accordion__header collapsed accordion__header--info " data-toggle="collapse" data-target="#gradient_collapseTwo"> <span class="accordion__header--icon"></span>
                                            <span class="accordion__header--text">Configure</span>
                                            <span class="accordion__header--indicator"></span>
                                        </div>
                                        <div id="gradient_collapseTwo" class="collapse accordion__body" data-parent="#accordion-twelve">
                                            <div class="accordion__body--text">
                                                <p>The administrator can create an organization and define the Organisation Unit (OU), Business Unit (BU) and add projects associated with the OU and BU. User management allows users to be allocated to specific projects or services based on the domain view selected. Role based permissions are provided for each user which allows the user to use the application for the role he or she is performing. Once the organisation setup is completed, users associated with the projects can assess the compliance levels for their respective projects by logging in the compliance interface. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion__item">
                                        <div class="accordion__header collapsed accordion__header--success" data-toggle="collapse" data-target="#gradient_collapseThree"> <span class="accordion__header--icon"></span>
                                            <span class="accordion__header--text">Assess</span>
                                            <span class="accordion__header--indicator"></span>
                                        </div>
                                        <div id="gradient_collapseThree" class="collapse accordion__body" data-parent="#accordion-twelve">
                                            <div class="accordion__body--text">
                                                <p>The compliance interface includes all Capability areas, Practice areas and Practices including guidance available for interpreting the practice . Users can input information for every practice related to current work products associated with the practice and check it's coverage for adequacy. Provision is available for capturing current or As-Is practice and identify gap areas. Every practice can be characterized using 5 ratings viz, Fully Meets (FM), Largely Meets (LM), Partially Meets (PM), Does Not Meet (DM) and Not Yet (NY).</p>
                                            </div>
                                        </div>
                                    </div>
									<div class="accordion__item">
                                        <div class="accordion__header collapsed accordion__header--alert-primary" data-toggle="collapse" data-target="#gradient_collapseFour"> <span class="accordion__header--icon"></span>
                                            <span class="accordion__header--text">Report</span>
                                            <span class="accordion__header--indicator"></span>
                                        </div>
                                        <div id="gradient_collapseFour" class="collapse accordion__body" data-parent="#accordion-twelve">
                                            <div class="accordion__body--text">
                                                <p>Once all practices are assessed and their compliances saved, the users can view the dashboard for various levels of compliance viz., Category wise compliance score, Capability area wise compliance score, Practice area wise compliance score and Project level compliance score. The application has a provision for downloading the report in pdf or excel formats.</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
						
                    </div>
                                                        <!--<h4 class="text-primary">About Me</h4>-->
                                            <p>                                                                                                                 
											</p>            
                                                    </div>
                                                </div>
                                            </div>
											<div id="about-me" class="tab-pane fade">
                                                <div class="profile-about-me">
                                                    <div class="pt-4 border-bottom-1 pb-3">
                                                        <!--<h4 class="text-primary">About Me</h4>-->
                                                        <p>About Me Para</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
                                    </div>
									<!-- Modal -->
									<div class="modal fade" id="replyModal">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title">Post Reply</h5>
													<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
												</div>
												<div class="modal-body">
													<form>
														<textarea class="form-control" rows="4">Message</textarea>
													</form>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-danger light" data-dismiss="modal">Close</button>
													<button type="button" class="btn btn-primary">Reply</button>
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
						<p style="color:white;" >The administrator can create an organization and define the Organisation Unit (OU), Business Unit (BU) and add projects associated with the OU and BU. User management allows users to be allocated to specific projects or services based on the domain view selected. Role based permissions are provided for each user which allows the user to use the application for the role he or she is performing. Once the organisation setup is completed, users associated wance interface. </p>									
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


</div>
<script src="theme-vora/vendor/global/global.min.js"></script>
	<script src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="theme-vora/js/custom.min.js"></script>
	<script src="theme-vora/js/dlabnav-init.js"></script>
		
	
	
	
</body>


</html>
