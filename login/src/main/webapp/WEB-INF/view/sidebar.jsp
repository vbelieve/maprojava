<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.ApplicationConstants"%>
<%@page import="com.login.domain.CategoryMst"%>
<%@page import="com.login.domain.CapabilityMst"%>
<%@page import="com.login.domain.PracticeAreaMst"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>
<head>

</head>
<body>
   <div class="dlabnav">
            <div class="dlabnav-scroll">
				<ul class="metismenu" id="menu">
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="flaticon-381-networking"></i>
							<span class="nav-text">MIS</span>
						</a>
                        <ul aria-expanded="false">
							<li><a href="gotoDashboard">Dashboard</a></li>
							<% if(accessObject.getUserType().equalsIgnoreCase("USER") && !accessObject.getSelectedProject().equalsIgnoreCase(""))
{
	%>
							<!--  <li><a href="generateGapSummaryReport">Reports</a></li>-->
							<li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Reports</a>
                                <ul aria-expanded="false">
								 <li><a href="generateGapSummaryReport">Summary Report</a></li>
								<li><a  href="generatePiidReport">PIID Report</a></li>							
                                
                                </ul>
                            </li>
                            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Upload</a>
                                <ul aria-expanded="false">
								<li><a  href="uploadFile">Upload Summary File</a></li>							
                                
                                </ul>
                            </li>
<%}%>

<%
Boolean accessTypeSuperAdmin=Boolean.FALSE;
Boolean accessTypeAdmin=Boolean.FALSE;
Boolean accessTypeManager=Boolean.FALSE;
Boolean accessTypeUser=Boolean.FALSE;
if(accessObject.getUserType().equalsIgnoreCase("USER"))
{
	accessTypeUser=Boolean.TRUE;
}
if(accessObject.getUserType().equalsIgnoreCase("SUPERADMIN"))
{
	accessTypeSuperAdmin=Boolean.TRUE;
}
if(accessObject.getUserType().equalsIgnoreCase("MANAGER"))
{
	accessTypeManager=Boolean.TRUE;
}
if(accessObject.getUserType().equalsIgnoreCase("ADMIN"))
{
	accessTypeAdmin=Boolean.TRUE;
}
%>
							
						</ul>
                    </li>
                    <%if(accessTypeUser && !accessObject.getSelectedProject().equalsIgnoreCase(""))
                    	{
                    	%>
					<li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="flaticon-381-television"></i>
							<span class="nav-text">Compliance</span>
						</a>
                        <ul aria-expanded="false">
                        
                                <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Category</a>
                                <ul aria-expanded="false">
								<%
								for(CategoryMst category:accessObject.getCategoryMst())
								{								
								%>
                                    	    <li><a onclick="categorySelect('<%=category.getCategoryCode()%>');" href="#"><%=category.getCategoryCode()%></a></li>
								<%
								}
								%>							
                                </ul>
                            </li>
							<li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Capability</a>
                                <ul aria-expanded="false">
								<%
								for(CapabilityMst capability:accessObject.getCapabilityMst())
								{								
								%>
                                    <li><a onclick="capabilitySelect('<%=capability.getCapabilityCode()%>');" href="#">(<%=capability.getCapabilityCode()%>) <%=capability.getCapabilityName()%></a></li>
<%
								}
								%>

                                </ul>
                            </li>
                            
							<li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Practice Area</a>
                                <ul aria-expanded="false">
<%for(PracticeAreaMst practiceArea:accessObject.getPracticeAreaMst())
{%>            
			<li><a onclick="practiceAreaSelect('<%=practiceArea.getPracticeAreaCode()%>');" href="#">
			(<%=practiceArea.getPracticeAreaCode()%>)<%=practiceArea.getPracticeAreaName()%></a></li>
<%
}%>			

                                </ul>
                            </li>
                            </ul>
							<%
                            }
                    		%>
                    		    
							  <%if(accessTypeUser && !accessObject.getSelectedProject().equalsIgnoreCase(""))
                    	{
                    	%>
								<li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="flaticon-381-television"></i>
							<span class="nav-text">Additional PIID Details</span>
						</a>
                        <ul aria-expanded="false">
                        
                                <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Practice Area</a>
                                <ul aria-expanded="false">
								<%for(PracticeAreaMst practiceArea:accessObject.getPracticeAreaMst())
									{%>            
									<li><a onclick="piidDetails('<%=practiceArea.getPracticeAreaCode()%>');" href="#">
									(<%=practiceArea.getPracticeAreaCode()%>)<%=practiceArea.getPracticeAreaName()%></a></li>
									<%
									}%>						
                                </ul>
                            </li>
							</ul>
							</li>
							<%
						}%>
							
                    <%if(accessTypeAdmin || accessTypeSuperAdmin)
                        	{%>
                        	<%if(accessTypeSuperAdmin){ %>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="flaticon-381-television"></i>
							<span class="nav-text">Masters</span>
						</a>
                        <ul aria-expanded="false">
                        
                                                   <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Core</a>
                                <ul aria-expanded="false">
                                    <li><a href="categoryMstCrud">Category</a></li>
                                    <li><a href="capabilityMstCrud">Capability</a></li>
                                    <li><a href="practiceAreaMstCrud">Practice Area</a></li>
									<li><a href="practiceMstCrud">Practice</a></li>
									<li><a href="workProductMstCrud">Work Product</a></li>
                                </ul>
                            </li>
                            
                            
							<li><a class="has-arrow" href="javascript:void()" aria-expanded="false">RelationShip</a>
                                <ul aria-expanded="false">
                                
                                    <li><a href="relationMappingCategoryCapabilityCrud">Category-Capability</a></li>
									<li><a href="relationMappingCapabilityPracticeAreaCrud">Capability-Practice Area</a></li>
									<li><a href="relationMappingPracticeAreaPracticeCrud">Practice Area-Practice</a></li>
									 <li><a href="relationMappingPracticeWorkProductMstCrud">Practice-Work Product</a></li>
									
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <%}%>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="flaticon-381-controls-3"></i>
							<span class="nav-text">Setup</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Organization</a>
                                <ul aria-expanded="false">
                                <%if(accessTypeSuperAdmin){ %>
                                
                                    <li><a href="tenantMstCrud">Create Organization</a></li>
                                <%} %>
                                <%if(accessTypeAdmin){ %>    
									<li><a href="organizationProfile">Update Organization Details</a></li>
									<li><a href="organizationUnitMstCrud">Create Organization Unit</a></li>
									<li><a href="businessUnitMstCrud">Create Business Unit</a></li>
									<li><a href="projectMstCrud">Create Project</a></li>
									<%} %>
                                </ul>
                            </li><li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Users</a>
                                <ul aria-expanded="false">
                                <%if(accessTypeAdmin){ %>
                                    <li><a href="signup">Create User</a></li>
                                    <%} %>
									<li><a href="userAdmin">User Management</a></li>
									
									
                                </ul>
                            </li>
							
                        </ul>
                    </li>
                    <%} %>
                    <%if(accessTypeSuperAdmin){ %>
                    <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="flaticon-381-settings-2"></i>
							<span class="nav-text">Configure</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="lookupMstCrud">Lookup Setting</a></li>
                            <li><a href="sequenceMstCrud">Sequence Setting</a></li>
                            

                        </ul>
                    </li>
                    <%} %>
                <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
							<i class="flaticon-381-internet"></i>
							<span class="nav-text">About</span>
						</a>
                        <ul aria-expanded="false">
                            <li><a href="viewLicense">License Information</a></li>
                            <li><a href="about">About</a></li>
                            

                        </ul>
                    </li>
			</div>
        </div>
		<form hidden id="cSelect" method="post">
		<input type="hidden" id="parcticeAreaDropdown" name="parcticeAreaDropdown">
		<input type="hidden" id="capabilityDropdown" name="capabilityDropdown">
		<input type="hidden" id="categoryDropdown" name="categoryDropdown">
		</form>
		<script>
function practiceAreaSelect(pid)
{
	
	$("#parcticeAreaDropdown").val(pid);
	
	document.forms["cSelect"].action = "/GapAnalysis-2.0/dropdownParcticeAreaSelect";
	document.forms["cSelect"].submit();
	
}
</script>

<script>
function piidDetails(pid)
{
	
	$("#parcticeAreaDropdown").val(pid);
	
	document.forms["cSelect"].action = "/GapAnalysis-2.0/piidSelect";
	document.forms["cSelect"].submit();
	
}

</script>
<script>
function capabilitySelect(pid)
{
	
	$("#capabilityDropdown").val(pid);
	document.forms["cSelect"].action = "/GapAnalysis-2.0/dropdownCapabilitySelect";
	document.forms["cSelect"].submit();
	
}
</script>
<script>
function categorySelect(pid)
{
	
	$("#categoryDropdown").val(pid);
	document.forms["cSelect"].action = "/GapAnalysis-2.0/dropDownCategorySelect";
	document.forms["cSelect"].submit();
	
}
</script>
		</body>
		</html>