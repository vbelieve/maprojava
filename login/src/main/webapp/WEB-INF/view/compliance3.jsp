<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.UserMst"%>
<%@page import="com.login.domain.ProjectMst"%>
<%@page import="com.login.domain.CategoryMst"%>
<%@page import="com.login.domain.CapabilityMst"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Mapro</title>
    <!-- Favicon icon -->
<link href="theme-vora/css/style.css" rel="stylesheet">
<link href="customized/css/new-style.css" rel="stylesheet">
  <title>Admin</title>

  <!-- Custom fonts for this template -->

</head>

<body id="page-top">
<div id="main-wrapper">

<%@include file="preloader.jsp"%>
<%@include file="navheader.jsp"%>
<%@include file="head.jsp"%>
<%@include file="sidebar.jsp"%>
<%
String msg=(String) request.getAttribute("msg");
String[] messageType=null;
String alertType="alert alert-info alert-dismissible fade show";
String alertStr="Info!";

if(null==msg || msg.equalsIgnoreCase(""))
{
	msg="";
}
else
{
System.out.println("msg>>>>>>>>>>>>>>>>>>"+msg);	
messageType=msg.split("~");
msg=messageType[1];
}

if(!msg.equalsIgnoreCase("") && messageType.length>1)
{
	if(messageType[0].equalsIgnoreCase("SUCCESS"))
	{
	alertType="alert alert-success alert-dismissible fade show";
	alertStr="Success!";
	}
	else
	{
	alertType="alert alert-danger alert-dismissible fade show";
	alertStr="Error!";
	}
}
List<UserMst> users=new ArrayList<UserMst>();
List<ProjectMst> allProjects=new ArrayList<ProjectMst>();
List<CategoryMst> allCategories=new ArrayList<CategoryMst>();
List<CapabilityMst> allCapabilities=new ArrayList<CapabilityMst>();
if(request.getAttribute("users") != null )
{
	users=(List<UserMst>) request.getAttribute("users");
	
	
	for(UserMst user:users)
	{
		System.out.println(">>>>"+user.getUserName());
	}
}

allProjects=(List<ProjectMst>) request.getAttribute("allProjects");
allCategories=(List<CategoryMst>) request.getAttribute("allCategories");
allCapabilities=(List<CapabilityMst>) request.getAttribute("allCapabilities");
	
	if(null==allProjects)
	{
		allProjects=new ArrayList<ProjectMst>();
	}
	if(null==allCategories)
	{
		allCategories=new ArrayList<CategoryMst>();
	}
	if(null==allCapabilities)
	{
		allCapabilities=new ArrayList<CapabilityMst>();
	}
Integer srNo=0;
SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
String[] projects=accessObject.getProject().split(",");
%>




        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
			<% if (!msg.equalsIgnoreCase(""))
			{
			%>
			<div class="<%=alertType%>">
									<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line></svg>
									
									<strong><%=alertStr%></strong> <%=msg%>
									<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                    </button>
								</div>
								
								<%
			}
								%>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<%=homeUrl%>">Home</a></li>
	<li class="breadcrumb-item"><a href="categories">Category</a></li>
    <li class="breadcrumb-item active" aria-current="page">Capability</li>
  </ol>
</nav>				
				<!---------Body Start of Form--------->
			<div class="row">
                    <div class="col-xl-12 col-xxl-12">
                        <div class="card">
                            <div class="card-header">
                    
                            </div>
                            <div class="card-body">
								
									<form role="form" class="form" action = "#" id="formMaster" method="post">
									<input type="hidden" id="msg" value="<%=msg%>">
									<input type="hidden" id="mode" name="mode" value="CREATE">
			<input type="hidden" name="project" id="project" value="">
			<input type="hidden" name="category" id="category" value="">
			<input type="hidden" name="capability" id="capability" value="">
							<div id="wizard_Service" class="tab-pane" role="tabpanel">
									
	
<div class="row justify-content-center">
	<%
	 for(CapabilityMst entity:allCapabilities)
	 {
	 %>
<div class="col-sm-4">
								<div style="cursor: pointer;" class="card shadow" id="<%=entity.getCapabilityCode()%>"  onclick="selectedProject(this.id)">
									<div class="card-body">
										<div class="media align-items-center">
											<div class="media-body mr-3">
												<h2 class="num-text fs-30"><%=entity.getCapabilityCode()%></h2>

												<span class="fs-16 text-black"><%=entity.getCapabilityName()%></span>
											</div>
											
										</div>
									</div>
								</div>
							</div>											
<%
	 }
	%>
</div>											


	
			


&nbsp;<br><br>

	
												
											</div>

										</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            
				<!---------Body End of Form------------>
				<!---------Body Start of Tables--------->
				    
				<!---------Body End of Tables--------->
				</div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


</div>
<script src="theme-vora/vendor/global/global.min.js"></script>
	<script src="theme-vora/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<script src="theme-vora/js/custom.min.js"></script>
	<script src="theme-vora/js/dlabnav-init.js"></script>
	<script src="theme-vora/vendor/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="theme-vora/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="theme-vora/js/plugins-init/jquery.validate-init.js"></script>

<script src="theme-vora/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js"></script>
<script src="theme-vora/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="theme-vora/js/plugins-init/datatables.init.js"></script>	
<script src="theme-vora/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="theme-vora/js/plugins-init/sweetalert.init.js"></script>


<script type="text/javascript">
$(document).ready(function() {



});
</script>

<script type="text/javascript">
function selectedProject(pid){
$("#capability").val(pid);
document.forms["formMaster"].action = "/GapAnalysis-2.0/complianceCapabilitySelect";
document.forms["formMaster"].submit();	

}
</script>

</body>
</html>