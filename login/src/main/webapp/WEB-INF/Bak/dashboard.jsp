<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.UserMst"%>
<%@page import="com.login.domain.TenantMst"%>
<%@page import="com.login.domain.BusinessUnitMst"%>
<%@page import="com.login.domain.OrganizationUnitMst"%>
<%@page import="com.login.domain.ProjectMst"%>
<%@page import="com.login.domain.PracticeMst"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="java.lang.*"%>


<!-- saved from url=(0083)file:///C:/Users/karan.gupta/Desktop/startbootstrap-sb-admin-2-gh-pages/charts.html -->
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Dashboard</title>
<%@include file="header.jsp"%>
<!-- Custom fonts for this template-->
<link href="customized/vendor/fontawesome-free/css/all.min.css"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
	rel="stylesheet">

<!-- Custom styles for this template-->
<link href="customized/css/sb-admin-2.min.css" rel="stylesheet">
<script src="js/jquery-3.3.1.slim.min.js"></script>
<style type="text/css">/* Chart.js */
@
keyframes chartjs-render-animation {
	from {opacity: .99
}

to {
	opacity: 1
}

}
.chartjs-render-monitor {
	animation: chartjs-render-animation 1ms
}

.chartjs-size-monitor, .chartjs-size-monitor-expand,
	.chartjs-size-monitor-shrink {
	position: absolute;
	direction: ltr;
	left: 0;
	top: 0;
	right: 0;
	bottom: 0;
	overflow: hidden;
	pointer-events: none;
	visibility: hidden;
	z-index: -1
}

.chartjs-size-monitor-expand>div {
	position: absolute;
	width: 1000000px;
	height: 1000000px;
	left: 0;
	top: 0
}

.chartjs-size-monitor-shrink>div {
	position: absolute;
	width: 200%;
	height: 200%;
	left: 0;
	top: 0
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		getBusinessMstDashboard();
	});
</script>

<script type="text/javascript">
	function getBusinessMstDashboard() {
		var table = document.getElementById('projectTable');
		var json = []; // first row needs to be headers 
		var headers = [];
		for (var i = 0; i < table.rows[0].cells.length; i++) {
			headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase()
					.replace(/ /gi, '');
		}
		// go through cells 
		for (var i = 1; i < table.rows.length; i++) {
			var tableRow = table.rows[i];
			var rowData = {};
			for (var j = 0; j < tableRow.cells.length; j++) {
				rowData[headers[j]] = tableRow.cells[j].innerHTML;
			}
			json.push(rowData);
		}
		var labels = json.map(function(e) {
			return e.businessunitname;
		});
		var values = json.map(function(e) {
			return e.compliance;
		});
		var chart = BuildbusinessUnitChart(labels, values, "Business Unit");

	}
</script>


<script type="text/javascript">
	function selectTenant(tid) {
		$("#tenantId").val(tid);

		alert(tid);

	};
</script>

<script type="text/javascript">
	function getProject(tid) {

		alert(tid);

	};
</script>


<script type="text/javascript">
	function BuildbusinessUnitChart(labels, values, chartTitle) {
		var ctx = document.getElementById("myPieChart");
		var myPieChart = new Chart(
				ctx,
				{
					type : 'doughnut',
					data : {
						labels : labels,
						datasets : [ {
							label : chartTitle, // Name the series
							data : values,

							backgroundColor : [ '#4e73df', '#1cc88a', '#36b9cc' ],
							hoverBackgroundColor : [ '#2e59d9', '#17a673',
									'#2c9faf' ],
							hoverBorderColor : "rgba(234, 236, 244, 1)",
							click : function(e) {
								alert("e.dataSeries.type e.dataPoint.x  e.dataPoint.y");
							}

						} ],
					},
					options : {
						maintainAspectRatio : false,
						tooltips : {
							backgroundColor : "rgb(255,255,255)",
							bodyFontColor : "#858796",
							borderColor : '#dddfeb',
							borderWidth : 1,
							xPadding : 15,
							yPadding : 15,
							displayColors : false,
							caretPadding : 10,
						},
						legend : {
							display : false
						},
						cutoutPercentage : 80,
					},
				});
		return myPieChart;
	}
</script>

<script type="text/javascript">
	function BuildChart(labels, values, chartTitle) {
	}
</script>

</head>

<body id="page-top" class="sidebar-toggled">
	<%
		String msg = (String) request.getAttribute("msg");
		if (null == msg) {
			msg = "";
		}
		List<UserMst> users = new ArrayList<UserMst>();
		TenantMst allTenants = (TenantMst) request.getAttribute("allTenants");
		if (null == allTenants) {
			allTenants = new TenantMst();
		}
		List<OrganizationUnitMst> allOrgUnits = (List<OrganizationUnitMst>) request.getAttribute("allOrgUnits");
		if (null == allOrgUnits) {
			allOrgUnits = new ArrayList<OrganizationUnitMst>();
		}
		List<BusinessUnitMst> allBusinessUnit = (List<BusinessUnitMst>) request.getAttribute("allBusinessUnit");
		if (null == allBusinessUnit) {
			allBusinessUnit = new ArrayList<BusinessUnitMst>();
		}
		List<PracticeMst> allPractice = (List<PracticeMst>) request.getAttribute("allPractice");
		if (null == allPractice) {
			allPractice = new ArrayList<PracticeMst>();
		}
		List<ProjectMst> allProjects = (List<ProjectMst>) request.getAttribute("allProjects");
		if (null == allProjects) {
			allProjects = new ArrayList<ProjectMst>();
		}
		class BusinessUnitData {
			String businessUnit;
			Double compliance;
			Double totalCompliance;

			public String getBusinessUnit() {
				return businessUnit;
			}

			public void setBusinessUnit(String businessUnit) {
				this.businessUnit = businessUnit;
			}

			public Double getCompliance() {
				return compliance;
			}

			public Double getTotalCompliance() {
				return totalCompliance;
			}

			public void setCompliance(Double compliance) {
				this.compliance = compliance;
			}

			public void setTotalCompliance(Double totalCompliance) {
				this.totalCompliance = totalCompliance;
			}
		}
		List<BusinessUnitData> businessUnitDataList = new ArrayList<BusinessUnitData>();

		for (BusinessUnitMst bu : allBusinessUnit) {
			System.out.println(">>>>>>>>>>>>>" + bu.getBusinessUnitName());
			BusinessUnitData buData = new BusinessUnitData();
			buData.setBusinessUnit(bu.getBusinessUnitName());
			buData.setCompliance(0d);
			buData.setTotalCompliance(0d);
			for (PracticeMst practice : allPractice) {
				if (practice.getPracticeBusinessUnit().equalsIgnoreCase(bu.getBusinessUnitCode())) {
					buData.setCompliance(buData.getCompliance() + practice.getPracticeCompliance());
					buData.setTotalCompliance(buData.getTotalCompliance() + 100);
				}
			}
			buData.setCompliance(Math.round((buData.getCompliance() / buData.getTotalCompliance()) * 100) * 1.00);
			businessUnitDataList.add(buData);
		}
		Integer srNo = 0;
		String tidd = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	%>
	<input type="hidden" id="tenantId" name="tenantId" value="">

	<!-- Page Wrapper -->
	<div id="wrapper">


		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">


				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				<div class="container-fluid">

					<!-- Page Heading -->
					<h1 class="h3 mb-2 text-gray-800">DashBoard</h1>
					<p class="mb-4">
						</a>
					</p>
<h1 class="h3 mb-2 text-gray-800">Location</h1>
					<div class="row">
					
<%
for(OrganizationUnitMst org:allOrgUnits)
{
%>
						<div class="col-xl-3 col-md-6 mb-4" id="" onclick="selectTenant(this.id)">
							<div class="card border-left-info shadow h-100 py-2">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div
												class="text-xs font-weight-bold text-info text-uppercase mb-1"><%=org.getOrganizationUnitName()%></div>
											<div class="row no-gutters align-items-center">
												<div class="col-auto">
													<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">0%
													</div>
												</div>
												<div class="col">
													<div class="progress progress-sm mr-2">
														<div class="progress-bar bg-info" role="progressbar"
															style="width: 0%" aria-valuenow="50"
															aria-valuemin="0" aria-valuemax="100"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
<%
}
%>

					</div>
					<h1 class="h3 mb-2 text-gray-800">Business Unit</h1>
					<div class="row">
					
<%
for(BusinessUnitMst bu:allBusinessUnit)
{
%>
						<div class="col-xl-3 col-md-6 mb-4" id="" onclick="selectTenant(this.id)">
							<div class="card border-left-info shadow h-100 py-2">
								<div class="card-body">
									<div class="row no-gutters align-items-center">
										<div class="col mr-2">
											<div
												class="text-xs font-weight-bold text-info text-uppercase mb-1"><%=bu.getBusinessUnitName()%></div>
											<div class="row no-gutters align-items-center">
												<div class="col-auto">
													<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">0%
													</div>
												</div>
												<div class="col">
													<div class="progress progress-sm mr-2">
														<div class="progress-bar bg-info" role="progressbar"
															style="width: 0%" aria-valuenow="50"
															aria-valuemin="0" aria-valuemax="100"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
<%
}
%>

					</div>
					<!-- Content Row -->
					<div class="row">

						<div class="col-xl-8 col-lg-7">


							<div class="card shadow mb-4">
								<div class="card-header py-3">
									<h6 class="m-0 font-weight-bold text-primary">Projects</h6>
								</div>
								<div class="card-body">
									<%
										for (ProjectMst units : allProjects) {
											if (units.getTenantId().equalsIgnoreCase(accessObject.getTenantId())) {
												Double maxCount = 0d;
												Double rangeCount = 0d;
												if(allPractice.size()>0){
												for (PracticeMst practice : allPractice) {
													if (practice.getTenantId().equalsIgnoreCase(accessObject.getTenantId())) {
														maxCount = maxCount + 100;
														if (practice.getPracticeProject().equalsIgnoreCase(units.getProjectCode())) {
															rangeCount = rangeCount + practice.getPracticeCompliance();
														}

													}

												}
												}
												Double average = ((rangeCount / maxCount) * 100);
												average = (Math.round(average) * 1.00);
												String color = "primary";
												if (average > 0 && average < 20) {
													color = "danger";
												}
												if (average > 21 && average < 40) {
													color = "secondary";
												}
												if (average > 41 && average < 60) {
													color = "warning";
												}
												if (average > 61 && average < 80) {
													color = "primary";
												}
												if (average > 81 && average < 99) {
													color = "info";
												}
												if (average > 98) {
													color = "success";
												}
									%>
									<h4 class="small font-weight-bold"><%=units.getProjectName()%>
										<span class="float-right"><%=average%>%</span>
									</h4>
									<div class="progress mb-4" id=<%=units.getProjectCode()%>
										onclick="getProject(this.id)">
										<div class="progress-bar bg-<%=color%>" role="progressbar"
											style="width: <%=average%>%" aria-valuenow="<%=average%>"
											aria-valuemin="0" aria-valuemax="100"></div>
									</div>
									<%
										}
										}
									%>

								</div>
							</div>

							<!-- Bar Chart -->
							<div class="card shadow mb-4">
								<div class="card-header py-3">
									<h6 class="m-0 font-weight-bold text-primary">Bar Chart</h6>
								</div>
								<div class="card-body">
									<div class="chart-bar">
										<div class="chartjs-size-monitor">
											<div class="chartjs-size-monitor-expand">
												<div class=""></div>
											</div>
											<div class="chartjs-size-monitor-shrink">
												<div class=""></div>
											</div>
										</div>
										<canvas id="myBarChart" width="748" height="320"
											class="chartjs-render-monitor"
											style="display: block; width: 748px; height: 320px;"></canvas>
									</div>
									<hr>

								</div>
							</div>

							<!-- Area Chart -->
							<div class="card shadow mb-4">
								<div class="card-header py-3">
									<h6 class="m-0 font-weight-bold text-primary">Area Chart</h6>
								</div>
								<div class="card-body">
									<div class="chart-area">
										<div class="chartjs-size-monitor">
											<div class="chartjs-size-monitor-expand">
												<div class=""></div>
											</div>
											<div class="chartjs-size-monitor-shrink">
												<div class=""></div>
											</div>
										</div>
										<canvas id="myAreaChart" width="748" height="320"
											class="chartjs-render-monitor"
											style="display: block; width: 748px; height: 320px;"></canvas>
									</div>
									<hr>

								</div>
							</div>

						</div>

						<!-- Donut Chart -->
						<div class="col-xl-4 col-lg-5" disabled>
							<div class="card shadow mb-4">
								<!-- Card Header - Dropdown -->
								<div
									class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
									<h6 class="m-0 font-weight-bold text-primary">Business
										Units</h6>
									<div class="dropdown no-arrow">
										<a class="dropdown-toggle" href="#" role="button"
											id="dropdownMenuLink" data-toggle="dropdown"
											aria-haspopup="true" aria-expanded="false"> <i
											class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
										</a>

									</div>
								</div>
								<!-- Card Body -->
								<div class="card-body">
									<div class="chart-pie pt-4 pb-2">
										<canvas id="myPieChart"></canvas>
									</div>

								</div>
							</div>
						</div>
					</div>


				</div>

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- End of Main Content -->



	</div>
	<!-- End of Content Wrapper -->
	<table class="table table-bordered  table-hover"
		style="cursor: pointer;" id="projectTable" width="100%"
		cellspacing="0">
		<thead>
			<th>BusinessUnitName</th>
			<th>Compliance</th>
			<th>TotalCompliance</th>
		</thead>
		<tbody id="objectTable" name="objectTable">
			<%
				for (BusinessUnitData entry : businessUnitDataList) {
			%>
			<tr>

				<td><%=entry.getBusinessUnit()%></td>
				<td><%=entry.getCompliance()%></td>
				<td><%=entry.getTotalCompliance()%></td>

			</tr>

			<%
				}
			%>

		</tbody>
	</table>

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded"
		href="file:///C:/Users/karan.gupta/Desktop/startbootstrap-sb-admin-2-gh-pages/charts.html#page-top"
		style="display: none;"> <i class="fas fa-angle-up"></i>
	</a>

	<!-- Logout Modal-->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">�</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready
					to end your current session.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button"
						data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary"
						href="file:///C:/Users/karan.gupta/Desktop/startbootstrap-sb-admin-2-gh-pages/login.html">Logout</a>
				</div>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript-->
	<script src="customized/vendor/jquery/jquery.min.js"></script>
	<script src="customized/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="customized/vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="customized/js/sb-admin-2.min.js"></script>

	<!-- Page level plugins -->
	<script src="customized/vendor/chart.js/Chart.min.js"></script>
	<script src="customized/vendor/chart.js"></script>

	<!-- Page level custom scripts -->
	<script src="customized/js/demo/chart-area-demo.js"></script>
	<script src="customized/js/demo/chart-bar-demo.js"></script>
	<!--<script src="customized/js/demo/chart-pie-demo.js"></script>-->





</body>
<%@include file="footer.jsp"%>
</html>