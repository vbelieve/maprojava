<!DOCTYPE html>
<html>
   <head>
      <title>Product Category</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
      <link rel="stylesheet" type="text/css" href="css/responsive.css">





      <!-- add new css -->
      <link rel="stylesheet" type="text/css" href="css/circle.css">
      <link rel="stylesheet" type="text/css" href="css/new-style.css">
      <!-- end add new css -->



      <!-- Google fonts -->
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
      <!-- Fontawesome cdn -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
      <script src="js/jquery-3.3.1.slim.min.js"></script>
      <script src="js/popper.min.js"></script> 
      <script src="js/bootstrap.js"></script> 
      <script src="js/custom.js"></script>







      <!-- add new js -->
      <script src="js/new-js/highcharts.js"></script>
      <script src="js/new-js/exporting.js"></script>
      <script src="js/new-js/export-data.js"></script>
      <script src="js/new-js/accessibility.js"></script>
      <!-- end new js -->






   </head>
   <body>
      <!-- header sec start -->
      <header class="p-3">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <a href="#" class="logo" alt=" ">
                     <h2>Company Name</h2>
                  </a>
               </div>
            </div>
         </div>
         </div>
      </header>
      <!-- header sec end -->
      <!-- side bar sec start -->
      <div class="profile-main">
         <div class="sidebar-main">
            <div class="side-bar-menu">
               <nav id="sidebar">
                  <i class="fa fa-times"></i>
                  <div class="sidebar-header py-3">
                     <h3>Practice Area</h3>
                  </div>
                  <ul id="practiceAreaList" name="practiceAreaList" class="list-unstyled components">
                     <li>
                        <a href="#"><span>(RDM)</span> Requirement Development & Management </a>
                     </li>
                     <li>
                        <a href="#"><span>(PQA)</span>Process Quality Assurance </a>
                     </li>
                     <li>
                        <a href="#"><span>(VV)</span>Verification & Validation </a>
                     </li>
                     <li>
                        <a href="#"><span>(PR)</span>Peer Review </a>
                     </li>
                     <li>
                        <a href="#"><span>(TS)</span>Technical Solution </a>
                     </li>
                     <li>
                        <a href="#"><span>(PI)</span>Product Integration </a>
                     </li>
                     <li>
                        <a href="#"><span>(SDM)</span> Service Delivery Management  </a>
                     </li>
                     <li>
                        <a href="#"><span>(SSM)</span> Strategic Service Management</a>
                     </li>
                     <li>
                        <a href="#"><span>(SSS)</span> Supplier Source Selection </a>
                     </li>
                     <li>
                        <a href="#"><span>(SAM)</span> Supplier Agreement Management </a>
                     </li>
                     <li>
                        <a href="#"><span>(EST)</span> Estimating </a>
                     </li>
                     <li>
                        <a href="#"><span>(PLAN)</span> Planning</a>
                     </li>
                     <li>
                        <a href="#"><span>(MC)</span> Monitoring & Control</a>
                     </li>
                     <li>
                        <a href="#"><span>(RSK)</span> Risk & Opportunity Management  </a>
                     </li>
                     <li>
                        <a href="#"><span>(IRP)</span> Incident Resolution & Planning </a>
                     </li>
                  </ul>
               </nav>
            </div>
         </div>
         <div id="content">
            <nav class="navbar navbar-expand-lg">
               <div class="container-fluid p-0">
                  <button type="button" id="sidebarCollapse" class="btn btn-info">
                  <i class="fa fa-align-left"></i>
                  <span>Toggle Sidebar</span>
                  </button>
                  <div class="right-panel-header bg-2 p-4 px-5">
                     <div class="panel-title">
                        <h3>Home</h3>
                     </div>
                  </div>
               </div>
            </nav>
         </div>
      </div>
      <!-- side bar sec end -->
      <!-- side bar sec end -->
      <!-- Doing main sec start -->
      <div id="mainContainer" class="doing-main ensuring-main rdm-main">




















         <!-- new html -->
         <div class="row">
         	<div class="col-md-12">
         		<div class="dash-one-filter">
         			<div class="first-filter">
         				<p>CMMI Model Domain -
         					<label>DEVELOPMENT</label>
         				</p>
         			</div>
         			<div class="second-filter">
         				<p>Maturity Level
         					<label>LEVEL 3</label>
         				</p>
         			</div>
         		</div>
         	</div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <p class="title-project-wise">Project Wise Practise Area Compliance
                  <label>Project Name</label>
               </p>
            </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <figure class="highcharts-figure">
                 <div id="container"></div>
               </figure>
            </div>
         </div>
         <!-- end new html -->

















      </div>























      <!-- add new script -->
      <script type="text/javascript">
         Highcharts.chart('container', {
           chart: {
             type: 'column'
           },
           title: {
             text: ''
           },
           subtitle: {
             text: ''
           },
           xAxis: {
             type: 'category',
             labels: {
               rotation: -45,
               style: {
                 fontSize: '13px',
                 fontFamily: 'Verdana, sans-serif'
               }
             },
             title:
             {
               text: 'Practice Area'
             }
           },
           yAxis: {
             min: 0,
             title: {
               text: 'Percenrage Compliance'
             }
           },
           legend: {
             enabled: false
           },
           tooltip: {
             pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
           },
           plotOptions: {
                column: {
                    zones: [{
                        value: 80, // Values up to 10 (not including) ...
                        color: '#fe0000' // ... have the color blue.
                    },{
                        color: '#5aa89a' // Values from 10 (including) and up have the color red
                    }]
                }
            },
           series: [{
             name: 'Population',
             data: [
               ['Shanghai', 97],
               ['Beijing', 80],
               ['Karachi', 25],
               ['Shenzhen', 69],
               ['Guangzhou', 46],
               ['Istanbul', 70],
               ['Mumbai', 19],
               ['Moscow', 88],
               ['São Paulo', 97],
               ['Delhi', 86],
               ['Kinshasa', 42],
               ['Tianjin', 69],
               ['Lahore', 57],
               ['Jakarta', 93],
               ['Dongguan', 67],
               ['Lagos', 37],
               ['Bengaluru', 84],
               ['Seoul', 42],
               ['Foshan', 73],
               ['Tokyo', 46]
             ],
             dataLabels: {
               enabled: true,
               rotation: 0,
               color: '#000',
               align: 'right',
               // format: '{point.y:.1f}', // one decimal
               y: 0, // 10 pixels down from the top
               x: 4, // 10 pixels down from the top
               style: {
                 fontSize: '13px',
                 fontFamily: 'Verdana, sans-serif'
               }
             }
           }]
         });
      </script>
      <!-- end new script -->



















   </body>
</html>