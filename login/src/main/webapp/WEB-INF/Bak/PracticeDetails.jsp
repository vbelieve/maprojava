<!DOCTYPE html>
<html>
<head>
	<title>Product Category</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

	<!-- Fontawesome cdn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js">
	<script src="js/jquery-3.3.1.slim.min.js"></script>
	<script src="js/popper.min.js"></script> 
	<script src="js/bootstrap.js"></script> 
	<script src="js/custom.js"></script>
<script type="text/javascript">
$(document).ready(function(){
//hiding fields on page load
$('#practiceAreaDetails').hide();
//end of hiding fields on page load
//start of function for practice Level from Lookups
var urlToSend="lookup/0/PRACTICELEVELLOOKUP";

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
var practiceLevelStr="";
  $.each(jsonResponse, function (key, entry) {
practiceLevelStr=practiceLevelStr+"<a href=\"javascript:void(0)\" onclick=\"showPractices(this.id)\" class=\"dropdown-item\" id="+entry.lookupValue+">"+entry.displayValue+"</a>"; 
  })
$('#practiceLevelList').append(practiceLevelStr);


		
			
		},
		error:function(e)
		{
			
		}
	});
//end of function	
//Start of Funtion - to update category list from master
var urlToSend="category/0";

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
var categoryStr="";
var categoryListCardsStr="";
  $.each(jsonResponse, function (key, entry) {
categoryStr=categoryStr+"<a href=\"javascript:void(0)\" onclick=\"selectedCategory(this.id)\" class=\"dropdown-item\" id="+entry.categoryName+">"+entry.categoryName+"</a>"; 
categoryListCardsStr=categoryListCardsStr+"<div class=\"doing-box col-lg-3\"><a href=\"javascript:void(0)\" onclick=\"selectedCategory(this.id)\" id="+entry.categoryCode+"><div class=\"card\" ><div class=\"card-body\"  ><p>"+entry.categoryCode+"</p></div></div></a><p class=\"card-text\">"+entry.categoryName+"</p></div>";
  })
$('#categoryList').append(categoryStr);
$('#categoryListCards').html("");
$('#categoryListCards').append(categoryListCardsStr);
		
			
		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
//Start of Funtion - to update capability list from master
var urlToSend="capability/0";

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
var capabilityStr="";
  $.each(jsonResponse, function (key, entry) {
capabilityStr=capabilityStr+"<a href=\"javascript:void(0)\" onclick=\"selectedCapability(this.id)\" class=\"dropdown-item\" id="+entry.capabilityName+">"+entry.capabilityName+"</a>"; 

  })
$('#capabilityListCards').html("");
$('#capabilityList').append(capabilityStr);
//$('#capabilityListCards').hide();
		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
//Start of Funtion - to update practiceArea list from master
var urlToSend="practiceArea/0";

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
var practiceAreaStr="";
var practiceAreaListStr="";

  $.each(jsonResponse, function (key, entry) {
practiceAreaStr=practiceAreaStr+"<a href=\"javascript:void(0)\" onclick=\"updatePracticeAreaCode(this.id)\" class=\"dropdown-item\" id="+entry.practiceAreaCode+">"+entry.practiceAreaCode+"</a>"; 
practiceAreaListStr=practiceAreaListStr+"<li onclick=\"updatePracticeAreaCode(this.id)\" id="+entry.practiceAreaCode+"><a ><span>("+entry.practiceAreaCode+")</span>"+entry.practiceAreaName+"</a></li>";
  })
    
$('#practiceAreaList2').append(practiceAreaStr);
$('#practiceAreaList').append(practiceAreaListStr);
		
			
		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
//Start of Funtion - to update practice list from master
var urlToSend="practice/0";

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
var practiceStr="";
  $.each(jsonResponse, function (key, entry) {
practiceStr=practiceStr+"<a href=\"javascript:void(0)\" onclick=\"updatePracticeCode(this.id)\" class=\"dropdown-item\" id="+entry.practiceCode+">"+entry.practiceCode+"</a>"; 
  })
$('#practiceList').append(practiceStr);
		
			
		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
});


</script>

<script type="text/javascript">
function showPractices(type){
$('#categoryListCards').html("");
$('#capabilityListCards').html("");
$('#practiceAreaListCards').html("");
$('#practiceListCards').html("");
	var practiceAreaCode=$('#practiceAreaCode').val();
	$('#practiceAreaDetails').hide();
	updatePracticeList(type);
	//alert("show practices for "+practiceAreaCode);
}
</script>
<script type="text/javascript">
function updateCategoryCode(cid){
	$('#categoryCode').val(cid);
}
</script>
<script type="text/javascript">
function selectedCategory(cid){
$("#categoryListLabel").html("");
$("#categoryListLabel").append(cid);
$("#capabilityListLabel").html("");
$("#capabilityListLabel").append("select Capability");	
	$('#categoryCode').val(cid);
	updateCapabilityList(cid);
}
</script>
<script type="text/javascript">
function selectedCapability(cid){
$("#capabilityListLabel").html("");
$("#capabilityListLabel").append(cid);	
	$('#capabilityCode').val(cid);
	updatePracticeAreaList(cid);
}
</script>
<script type="text/javascript">
function updatePracticeAreaList(cid){
	
//Start of Funtion - to update capability list from master
var urlToSend="relationMappingCapabilityPracticeArea/getByCapability/0/"+cid;

var practiceAreaMappedList="";
$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
$.each(jsonResponse, function (key, entry) {
	
practiceAreaMappedList=practiceAreaMappedList+entry.practiceAreaCode+",";
  })	

	if(practiceAreaMappedList.length>0)
	{
		var practiceAreaCodes=practiceAreaMappedList.split(",");
		$('#practiceAreaListCards').html("");
		$('#capabilityListCards').html("");
		$('#practiceAreaListLabel').html("");
		$('#practiceAreaListLabel').html("Select PracticeArea");
		$('#categoryListCards').html("");
		$('#practiceAreaList2').html("");
		$('#practiceAreaDetails').hide();
		var practiceAreaStr="";
		var practiceAreaListCardsStr="";
		for(var i=0;i<practiceAreaCodes.length;i++)
	{
		if(practiceAreaCodes[i]!="")
		{
			
//Start of Funtion - to update capability list from master
var urlToSend="practiceArea/0/"+practiceAreaCodes[i];
var jsonResponse="";
$.ajax({
		type : "GET",
		url:urlToSend,
		
		success:function(jsonResponse)
		{
		
  $.each(jsonResponse, function (key, entry) {

practiceAreaStr="<a href=\"javascript:void(0)\" onclick=\"updatePracticeAreaCode(this.id)\" class=\"dropdown-item\" id="+jsonResponse.practiceAreaCode+">"+jsonResponse.practiceAreaName+"</a>"; 
practiceAreaListCardsStr="<div class=\"doing-box col-lg-3\"><a href=\"javascript:void(0)\" onclick=\"updatePracticeAreaCode(this.id)\" id="+jsonResponse.practiceAreaCode+"><div class=\"card\" ><div class=\"card-body\"  ><p>"+jsonResponse.practiceAreaCode+"</p></div></div></a><p class=\"card-text\">"+jsonResponse.practiceAreaName+"</p></div>";

  })
  
$('#practiceAreaList2').append(practiceAreaStr);
$('#practiceAreaListCards').append(practiceAreaListCardsStr);

		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
		}
	}
	
	}

		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
}
</script>

<script type="text/javascript">
function updateCapabilityList(cid){
	
//Start of Funtion - to update capability list from master
var urlToSend="relationMappingCategoryCapability/getBycategory/0/"+cid;
var capabilityMappedList="";
$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
$.each(jsonResponse, function (key, entry) {
	
capabilityMappedList=capabilityMappedList+entry.capabilityCode+",";
  })	
	if(capabilityMappedList.length>0)
	{
		var capabilityCodes=capabilityMappedList.split(",");
		$('#capabilityListCards').html("");
		$('#categoryListCards').html("");
		$('#capabilityList').html("");
		$('#practiceAreaDetails').hide();
		var capabilityStr="";
		var capabilityListCardsStr="";
		for(var i=0;i<capabilityCodes.length;i++)
	{
		if(capabilityCodes[i]!="")
		{
			
//Start of Funtion - to update capability list from master
var urlToSend="capability/0/"+capabilityCodes[i];
var jsonResponse="";
$.ajax({
		type : "GET",
		url:urlToSend,
		
		success:function(jsonResponse)
		{
		
  $.each(jsonResponse, function (key, entry) {

capabilityStr="<a href=\"javascript:void(0)\" onclick=\"selectedCapability(this.id)\" class=\"dropdown-item\" id="+jsonResponse.capabilityCode+">"+jsonResponse.capabilityName+"</a>"; 
capabilityListCardsStr="<div class=\"doing-box col-lg-3\"><a href=\"javascript:void(0)\" onclick=\"selectedCapability(this.id)\" id="+jsonResponse.capabilityCode+"><div class=\"card\" ><div class=\"card-body\"  ><p>"+jsonResponse.capabilityCode+"</p></div></div></a><p class=\"card-text\">"+jsonResponse.capabilityName+"</p></div>";

  })
  
$('#capabilityList').append(capabilityStr);
$('#capabilityListCards').append(capabilityListCardsStr);

		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
		}
	}
	
	}

		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
}
</script>


<script type="text/javascript">
function updatePracticeList(cid){
var urlToSend="";
var practiceAreaCode=$('#practiceAreaCode').val();

$('#practiceListCards').html("");
if(cid=="ALL")
{
urlToSend="relationMappingPracticeAreaPractice/getByPracticeArea/0/"+practiceAreaCode;
}
else
{
urlToSend="relationMappingPracticeAreaPractice/getByPracticeLevel/0/"+cid;
}

var practiceListCodesStr="";
var practiceLevelCodes="";
$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
		
	


  $.each(jsonResponse, function (key, entry) {
	  practiceListCodesStr=practiceListCodesStr+entry.practiceCode+",";
	    });
 practiceLevelCodes=practiceListCodesStr.split(",");

	for(var i=0;i<practiceLevelCodes.length;i++)
	{	 
 if(practiceLevelCodes[i]=="")
	 {
		 continue;
	 }
	 //getting ajax and updating card
	 var urlToSend="practice/0/"+practiceLevelCodes[i];
	var jsonResponse="";
$.ajax({
		type : "GET",
		url:urlToSend,
		
		success:function(jsonResponse)
		{
		var practiceListCardsStr="";
  $.each(jsonResponse, function (key, entry) {


practiceListCardsStr="<div class=\"doing-box col-lg-3\"><a href=\"practices\\"+jsonResponse.practiceCode+"\" onclick=\"selectedCapability(this.id)\" id="+jsonResponse.practiceCode+"><div class=\"card\" ><div class=\"card-body\"  ><p>"+jsonResponse.practiceCode+"</p></div></div></a><p class=\"card-text\">"+jsonResponse.practiceName+"</p></div>";

  })
  


$('#practiceListCards').append(practiceListCardsStr);

		},
		error:function(e)
		{
			
		}
	});
    

	 //end getting ajax and updating card
 } 		
		},
		error:function(e)
		{
	
		}
		  

	});

//Now getting practice information based on array

	


//End getting practice information
}
</script>

<script type="text/javascript">
function updateCapabilityCode(cid){
	$('#capabilityCode').val(cid);

}
</script>
<script type="text/javascript">
function updatePracticeAreaCode(cid){
	$('#practiceAreaCode').val(cid);
	getPracticeAreaDetails(cid);
	updatePracticeDropDown(cid);
}
</script>
<script type="text/javascript">
function updatePracticeCode(cid){
	$('#practiceCode').val(cid);
}
</script>
<script type="text/javascript">
function updatePracticeLevelCode(cid){
	$('#practiceLevelCode').val(cid);
}
</script>
<script type="text/javascript">
function updatePracticeDropDown(practiceAreaCode){
	//get values from relation table
	//Start of Funtion - to update practice list from master
var urlToSend="relationMappingPracticeAreaPractice/getByPracticeArea/0/"+practiceAreaCode;

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
			if(jsonResponse=="")
			{
				alert("No practices mapped against given practice Area");
			}
var practiceStr="";
var practiceLevelListStr= [];
var practiceListCardsStr="";
var unique=[];
  $.each(jsonResponse, function (key, entry) {
	  

   if (!unique[entry.practiceLevelCode])
   {
            practiceLevelListStr.push(entry.practiceLevelCode);
			    unique[entry.practiceLevelCode] = 1;


   }

practiceStr=practiceStr+"<a href=\"javascript:void(0)\" onclick=\"updatePracticeCode(this.id)\" class=\"dropdown-item\" id="+entry.practiceCode+">"+entry.practiceCode+"</a>"; 
practiceListCardsStr=practiceListCardsStr+entry.practiceCode+",";
  })
$('#practiceListCardsStr').val(practiceListCardsStr);
 $('#practiceList').html("");
$('#practiceList').append(practiceStr);
 $('#practiceLevelList').html("");
 practiceLevelListStr=practiceLevelListStr+"";
 var practiceLevelCodes=practiceLevelListStr.split(",");
 for(var i=0;i<practiceLevelCodes.length;i++)
 {
	 $('#practiceLevelList').append("<a href=\"javascript:void(0)\" onclick=\"updatepracticeLevelCode(this.id)\" class=\"dropdown-item\" id="+practiceLevelCodes[i]+">"+practiceLevelCodes[i]+"</a>"); 
 }
			
		},
		error:function(e)
		{
	
		}
	});
    

	
//End of Funtion
}
</script>
<script type="text/javascript">
function getPracticeAreaDetails(practiceAreaCode){
$('#categoryListCards').html("");
$('#capabilityListCards').html("");
$('#practiceAreaListCards').html("");
$('#practiceAreaListLabel').html("");
$('#practiceAreaListLabel').append(practiceAreaCode);

	//Start of Funtion - to update practiceArea list from master
var urlToSend="practiceArea/0/"+practiceAreaCode;
$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
			
			//entry.practiceAreaCode
			var updatePageTitle="";
			var updateCardTitle="";
			var updateCardDetails="";
			var updatePracticeAreaIntentCard="";
			var updatePracticeAreaValueCard="";
			var updatePracticeAreaAdditionalInfoCard="";
			
			updatePageTitle="<li aria-current=\"page\" class=\"breadcrumb-item active\" >"+jsonResponse.practiceAreaName+"</li>";
			updateCardTitle="<h5 class=\"card-title\">"+jsonResponse.practiceAreaCode+"</h5>";
			updateCardDetails="<p class=\"card-text\">"+jsonResponse.practiceAreaName+"</p>";
			updatePracticeAreaIntentCard="<p>"+jsonResponse.practiceAreaIntent+"</p>";
			updatePracticeAreaValueCard="<p>"+jsonResponse.practiceAreaValue+"</p>";
			updatePracticeAreaAdditionalInfoCard="<p>"+jsonResponse.practiceAreaAdditionalInfo+"</p>";

		$('#practiceAreaDetails').show();
		$('#practiceAreaIntent').html("");
		$('#practiceAreaTitle').html("");
		$('#practiceAreaCardTitle').html("");
		$('#practiceAreaIntent').html("");
		$('#practiceAreaValue').html("");
		$('#practiceAreaAdditionalInfo').html("");
		$('#practiceAreaTitle').append(updatePageTitle);
		$('#practiceAreaCardTitle').append(updateCardTitle);
		$('#practiceAreaCardTitle').append(updateCardDetails);
		$('#practiceAreaIntent').append(updatePracticeAreaIntentCard);
		$('#practiceAreaValue').append(updatePracticeAreaValueCard);
		$('#practiceAreaAdditionalInfo').append(updatePracticeAreaAdditionalInfoCard);
		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion

}
</script>
</head>
<body>

	<!-- header sec start -->
	<header class="p-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href="#" class="logo" alt=" ">
						<h2>Company Name</h2>
					</a>
				</div>
			</div>
		</div>
	</header>
	<!-- header sec end -->

	<!-- side bar sec start -->
	<div class="profile-main">
		<div class="sidebar-main">
			<div class="side-bar-menu">
				<nav id="sidebar">
					<i class="fa fa-times"></i>
					<div class="sidebar-header py-3">
						<h3>Practice Area</h3>
					</div>
					<ul id="practiceAreaList" name="practiceAreaList" class="list-unstyled components">
					
					
					</ul>
				</nav>
			</div>
		</div>


		<div id="content">
			<nav class="navbar navbar-expand-lg">
				<div class="container-fluid p-0">
					<button type="button" id="sidebarCollapse" class="btn btn-info">
						<i class="fa fa-align-left"></i>
						<span>Toggle Sidebar</span>
					</button>
					<div class="right-panel-header bg-2 p-4">
						<div class="panel-title">
							<h3>Home</h3>
							<div class="form-group">
							
							<input type="hidden" class="form-control" id="practiceListCodesStr" value="" placeholder="" >
								<input type="hidden" class="form-control" id="categoryCode" value="" placeholder="" >
								<input type="hidden" class="form-control" id="capabilityCode" value="" placeholder="" >
								<input type="hidden" class="form-control" id="practiceAreaCode" value="" placeholder="" >
								<input type="hidden" class="form-control" id="practiceCode" value="" placeholder="" >
								<input type="hidden" class="form-control" id="practiceLevelCode" value="" placeholder="" >
							</div>
							<div class="dropdown">
								<button id="categoryListLabel" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									select category
								</button>
								<div id="categoryList" name="categoryList" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									
								</div>
							</div>
							<div class="dropdown">
								<button id="capabilityListLabel" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									select Capability
								</button>
								<div id="capabilityList" name="capabilityList" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									
								</div>
							</div>
							<div class="dropdown">
								<button id="practiceAreaListLabel" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									select PracticeArea
								</button>
								<div id="practiceAreaList2" name="practiceAreaList2" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									
								</div>
							</div>
							<div class="dropdown">
								<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									select Level
								</button>
								<div id="practiceLevelList" name="practiceLevelList" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									
								</div>
							</div>
							<div class="dropdown">
								<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									select Practice
								</button>
								<div id="practiceList" name="practiceList" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									
								</div>
							</div>
							
						</div>
					</div>


				</div>
			</nav>	
		</div>
	</div>

	<!-- side bar sec end -->
	<!-- side bar sec end -->

	<!-- Doing main sec start -->
	<div class="doing-main ensuring-main rdm-main" id="practiceAreaDetails" name="practiceAreaDetails">
		<div class="doing-title">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" id="practiceAreaTitle" name="practiceAreaTitle">
					
				</ol>
			</nav>
			<br>
		</div>

		<div class="doing-categry-main">
			<div class="bg-color"></div>


			<div class="container">
				<div class="row">
					<div class="doing-box-main col-lg-12">
						<div class="doing-box col-lg-3">
							<a href="javascript:void(0)" onclick="showPractices('ALL');">
								<div class="card">
									<div button type="button" class="card-body" id="practiceAreaCardTitle" name="practiceAreaCardTitle" ></div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			
			<div class="rdm-main-cntnt">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="rdm-text">
								<div class="rdm-text-area">
									<h2 class="para-title">required practice area information</h2>
									<div class="rdm-inr-cntnt">
										<p class="para-title" >intent</p>
										<div  id="practiceAreaIntent" name="practiceAreaIntent">
										</div>
									</div>
									<div class="rdm-inr-cntnt">
										<p class="para-title">value</p>
										<div  id="practiceAreaValue" name="practiceAreaValue">
										</div>
									</div>

									<div class="rdm-inr-cntnt full-cntnt">
										<p class="para-title">additional process area information</p>
										<div  id="practiceAreaAdditionalInfo" name="practiceAreaAdditionalInfo">
										</div>
									</div>
									<button type="button" onclick="showPractices('All');" class="btn btn-danger next">Next</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Doing main sec end -->
	
<!--Doing main select for Category-->
<div class="doing-main ensuring-main rdm-main">
		<div class="doing-title">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" >
					<a></a>
				</ol>
			</nav>
			<br>
		</div>
	<div class="doing-title">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					
				</ol>
			</nav>
			<br>
		</div>

		<div class="doing-categry-main">
			


			<div class="container">
				<div class="row">
					<div class="doing-box-main col-lg-12" id="categoryListCards" name="categoryListCards"></div>
					<div class="doing-box-main col-lg-12" id="capabilityListCards" name="capabilityListCards"></div>
					<div class="doing-box-main col-lg-12" id="practiceAreaListCards" name="practiceAreaListCards"></div>
					<div class="doing-box-main col-lg-12" id="practiceListCards" name="practiceListCards"></div>
				</div>
			</div>
<!--Doing select for Category End-->




	<script type="text/javascript">
		$(document).ready(function () {
			$('#sidebarCollapse').on('click', function () {
				$('#sidebar').toggleClass('active');
			});
		});
	</script>
	
</body>
</html>