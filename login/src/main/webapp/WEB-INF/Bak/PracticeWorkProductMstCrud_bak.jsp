<%@ page contentType="text/html; charset=iso-8859-1" language="java" %>

<!DOCTYPE html>
<html>
		

<head>
	<title>WorkProduct</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js">
	<script src="js/jquery-3.3.1.slim.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
getWorkProductId();

//Start of Getting practices List
$.ajax({
		type : "GET",
		url:"questionMst/0",
		success:function(jsonResponse)
		{
			
	
var questionListStr="";

  $.each(jsonResponse, function (key, entry) {
	questionListStr=questionListStr+"<option value="+entry.questionId+">"+entry.questionId+"-"+entry.questionDescription+"</option>"; 
    

  })
$("#questionList").html("");
$("#questionList").append(questionListStr);
			
		},
		error:function(e)
		{
			alert("FAILED");
		}
	});
//End of Getting Questions List

//Start of Getting Updated Relations List
$.ajax({
		type : "GET",
		url:"workProductMst/0",
		success:function(jsonResponse)
		{
			
	
var workProductMstTableStr="";
var srNo=1;

  $.each(jsonResponse, function (key, entry) {
	workProductMstTableStr=workProductMstTableStr+"<tr onclick=\"updateWorkProductDetails("+entry.workProductId+",'"+entry.workProductName+"','"+entry.questionId+"')\";><th scope=\"row\">"+srNo+"</th><td>"+entry.workProductName+"</td><td>"+entry.questionId+"</td></tr>";
    srNo=srNo+1;
    

  })
$(workProductMstTable).html("");
$("#workProductMstTable").append(workProductMstTableStr);
			
		},
		error:function(e)
		{
			alert("FAILED");
		}
	});
//End of Getting Work product List


$("#commit").click(function(){
	
	var workProductId=$("#workProductId").val()+'';
	var workProductName=$("#workProductName").val()+'';
	var questionList=$("#questionList").val()+'';

	if(workProductId=="")
	{
		workProductId=$("#workProductIdSeq").val()+'';
	}
	if(workProductId=="")
	{
		alert ("Unable to Fetch Work Product ID.");
		return false();
	}
	if(workProductName=="")
	{
		alert ("workProductName  Is Mandatory");
		return false();
	}
	if(questionList=="")
	{
		alert ("questionList  Is Mandatory");
		return false();
	}
	
	
	
	

		var formData={
	workProductId:workProductId,
	workProductName:workProductName,
	questionId:questionList,
	}
			
   
	
	$.ajax({
		type : "POST",
		contentType:"application/json",
		url:"workProductMst/save",
		data:JSON.stringify(formData),
		dataType:'json',
		success:function(jsonResponse)
		{

			window.location.replace("/GapAnalysis-1.0/workProductMstCrud");
			
		},
		error:function(e)
		{
			alert("FAILED");
		}
	});
		
		
		
	
			

	
});
});
</script>
<script type="text/javascript">
function updatePracticeAreaCode(cid){
//Start of Funtion - to update Capability Code
$("#practiceAreaCode").val(cid);
$("#practiceAreaListLabel").html("");
$("#practiceAreaListLabel").append(cid);
} 
</script>
<script type="text/javascript">
function updateWorkProductDetails(cid,names,qid){
//Start of Funtion - to update Capability Code

$("#workProductId").val(cid);
$("#workProductName").val(names);
$("#workProductName").attr("Readonly",true);
$("select#questionList").val(qid);	

} 
</script>
<script type="text/javascript">
function getWorkProductId(){
	
//Start of Funtion - to update Capability Code
//Start of Getting Sequence No
$.ajax({
		type : "GET",
		url:"sequence/getNext/0/WORKPRODUCTSEQ",
		success:function(jsonResponse)
		{
			
	
var workProductIdStr="";
var srNo=1;

  
	workProductIdStr=jsonResponse+'';
  

$("#workProductIdSeq").val(workProductIdStr);

		},
		error:function(e)
		{
			alert("FAILED");
		}
	});
//End of Getting Work product List
} 
</script>
<script type="text/javascript">
function updatePracticeLevelCode(cid){
$("#practiceLevelCode").val(cid);
$("#practiceLevelListLabel").html("");
$("#practiceLevelListLabel").append(cid);
} 
</script>	
	
</head>
<body>
	<div id="login">
		<div class="container">
			<div class="gap">
				<h3>gap analsis</h3>
			</div>
			<div id="login-row" class="row justify-content-end">
				<div id="login-column" class="col-md-5">
					<div id="login-box">
					<form id="userRegistration" class="form" action="" method="post">
						<form class="w-100 bg-white p-5 sign-up-form">
							<div class="sign-up-title">
								<h3>Work Product Details</h3>
							</div>

<br>

<input type="hidden" id="questionId">
<input type="hidden" id="workProductIdSeq" name="workProductIdSeq">
<input type="hidden" id="workProductId" name="workProductId">

					<div class="form-group">
								<label for="workProductName">Work Product Name</label>
								<input type="text" class="form-control" id="workProductName" placeholder="">
							</div>
							<br>
							
							
							<br>
							<a>Documents Linked</a>
							<div class="row">
  <div class="col-md-12">

    <select id="questionList" class="mdb-select colorful-select dropdown-primary md-form width=10px " multiple searchable="Search here..">
      
    </select>
    
    

  </div>
</div>
					
							<div class="form-group float-right">	
							<button type="button" id="commit"  name="commit" class="btn btn-primary">Submit</button>
								
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<!--Table-->
<table id="tableWorkProductMst" class="table  table-bordered table-hover ">
<!--Table head-->
  <thead>
    <tr>
      <th>#</th>
      <th>Work Product Name</th>
	  <th>Question Id</th>
	   </tr>
  </thead>
  <!--Table head-->
  <!--Table body-->
<tbody id="workProductMstTable" name="workProductMstTable">

</tbody>
  <!--Table body-->
</table>
<!--Table-->

	
	<script src="js/popper.min.js"></script> 
	<script src="js/bootstrap.js"></script> 
	<script src="js/custom.js"></script>


</body>
</html>


