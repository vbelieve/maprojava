<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.ApplicationConstants"%>
<html>
<head>
	<title>Product Category</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

	<!-- Fontawesome cdn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js">
	<script src="js/jquery-3.3.1.slim.min.js"></script>
	<script src="js/popper.min.js"></script> 
	<script src="js/bootstrap.js"></script> 
	<script src="js/custom.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("#commit").click(function(){
	var practiceCode=$('#practiceCode').val();
	var practiceDescription=$('#practiceDescription').val();
	var practiceExamples=$('#practiceExamples').val();
	var practiceName=$('#practiceName').val();
	var practiceValue=$('#practiceValue').val();
	var currentGapNptes=$('#currentGapNptes').val();
	var practiceComplianceLevel=$('#practiceComplianceLevel').val();

	
	var formData={
	practiceCode:practiceCode,
	practiceDescription:practiceDescription,
	practiceExamples:practiceExamples,
	practiceName:practiceName,
	practiceValue:practiceValue,
	currentGapNptes:currentGapNptes,
	practiceComplianceLevel:practiceComplianceLevel,
	}
			
   
		
	$.ajax({
		type : "POST",
		contentType:"application/json",
		url:"practice/save",
		data:JSON.stringify(formData),
		dataType:'json',
		success:function(jsonResponse)
		{
			alert("Practice Details Saved Successful.");
			window.location.replace("/GapAnalysis-1.0/");
			
		},
		error:function(e)
		{
			alert("FAILED");
		}
	});		
			
	
});

//hiding fields on page load


$('#practiceAreaDetails').hide();
$('#practiceDetailsContainer').hide();
$('#mainContainer').show();
//end of hiding fields on page load
//start of function for practice Level from Lookups
var urlToSend="lookup/0/PRACTICELEVELLOOKUP";

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
var practiceLevelStr="";
  $.each(jsonResponse, function (key, entry) {
practiceLevelStr=practiceLevelStr+"<a href=\"javascript:void(0)\" onclick=\"showPractices(this.id)\" class=\"dropdown-item\" id="+entry.lookupValue+">"+entry.displayValue+"</a>"; 
  })
$('#practiceLevelList').append(practiceLevelStr);


		
			
		},
		error:function(e)
		{
			
		}
	});
//end of function	
//Start of Funtion - to update category list from master
var urlToSend="category/0";

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
var categoryStr="";
var categoryListCardsStr="";
  $.each(jsonResponse, function (key, entry) {
categoryStr=categoryStr+"<a href=\"javascript:void(0)\" onclick=\"selectedCategory(this.id)\" class=\"dropdown-item\" id="+entry.categoryName+">"+entry.categoryName+"</a>"; 
categoryListCardsStr=categoryListCardsStr+"<div class=\"doing-box col-lg-3  mb-3\"><a href=\"javascript:void(0)\" onclick=\"selectedCategory(this.id)\" id="+entry.categoryCode+"><div class=\"card\" ><div class=\"card-body\"  ><h5  class=\"card-title\">"+entry.categoryCode+"</h5><p class=\"card-text\">"+entry.categoryName+"</p></div></div></a></div>";
  })
$('#categoryList').append(categoryStr);
$('#categoryListCards').html("");
$('#categoryListCards').append(categoryListCardsStr);
		
			
		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
//Start of Funtion - to update capability list from master
var urlToSend="capability/0";

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
var capabilityStr="";
  $.each(jsonResponse, function (key, entry) {
capabilityStr=capabilityStr+"<a href=\"javascript:void(0)\" onclick=\"selectedCapability(this.id)\" class=\"dropdown-item\" id="+entry.capabilityCode+">"+entry.capabilityName+"</a>"; 

  })
$('#capabilityListCards').html("");
$('#capabilityList').append(capabilityStr);
//$('#capabilityListCards').hide();
		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
//Start of Funtion - to update practiceArea list from master
var urlToSend="practiceArea/0";

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
var practiceAreaStr="";
var practiceAreaListStr="";

  $.each(jsonResponse, function (key, entry) {
practiceAreaStr=practiceAreaStr+"<a href=\"javascript:void(0)\" onclick=\"updatePracticeAreaCode(this.id)\" class=\"dropdown-item\" id="+entry.practiceAreaCode+">"+entry.practiceAreaCode+"</a>"; 
practiceAreaListStr=practiceAreaListStr+"<li onclick=\"updatePracticeAreaCode(this.id)\" id="+entry.practiceAreaCode+"><a ><span>("+entry.practiceAreaCode+")</span>"+entry.practiceAreaName+"</a></li>";
  })
    
$('#practiceAreaList2').append(practiceAreaStr);
$('#practiceAreaList').append(practiceAreaListStr);
		
			
		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
//Start of Funtion - to update practice list from master
var urlToSend="practice/0";

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
var practiceStr="";
  $.each(jsonResponse, function (key, entry) {
practiceStr=practiceStr+"<a href=\"javascript:void(0)\" onclick=\"updatePracticeCode(this.id)\" class=\"dropdown-item\" id="+entry.practiceCode+">"+entry.practiceCode+"</a>"; 
  })
$('#practiceList').append(practiceStr);
		
			
		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
});


</script>

<script type="text/javascript">
function showPractices(type){
	$('#practiceDetailsContainer').hide();
$('#categoryListCards').html("");
$('#capabilityListCards').html("");
$('#practiceAreaListCards').html("");
$('#practiceListCards').html("");
	var practiceAreaCode=$('#practiceAreaCode').val();
	$('#practiceAreaDetails').hide();
	updatePracticeList(type);
	//alert("show practices for "+practiceAreaCode);
}
</script>
<script type="text/javascript">
function updateCategoryCode(cid){
	$('#categoryCode').val(cid);
}
</script>
<script type="text/javascript">
function selectedCategory(cid){

$('#practiceDetailsContainer').hide();
$("#categoryListLabel").html("");
$("#categoryListLabel").append(cid);
$("#capabilityListLabel").html("");
$('#capabilityListCards').html("");
$('#capabilityList').html("");
$("#capabilityListLabel").append("select Capability");	
$('#categoryCode').val(cid);
$('#practiceAreaList2').html("");
$('#practiceAreaListCards').html("");
$('#practiceListCards').html("");

	updateCapabilityList(cid);
}
</script>


<script type="text/javascript">
function updateCompliance(cid){
	if(cid=='1')
	{
$('#LabelCompliance').text("FI");
	}
	if(cid=='0.75')
	{
$('#LabelCompliance').text("LI");
	}
	if(cid=='0.50')
	{
$('#LabelCompliance').text("PI");
	}
	if(cid=='0')
	{
$('#LabelCompliance').text("NI");
	}
$('#practiceComplianceLevel').val(cid);

}
</script>

<script type="text/javascript">
function selectedCapability(cid){
$('#practiceAreaList2').html("");
$('#practiceAreaListCards').html("");
$('#practiceListCards').html("");
$('#practiceDetailsContainer').hide();
$("#capabilityListLabel").html("");
$("#capabilityListLabel").append(cid);	
	$('#capabilityCode').val(cid);
	updatePracticeAreaList(cid);
}
</script>
<script type="text/javascript">
function showMainDetails(){
$('#practiceDetailsContainer').hide();
$('#mainContainer').show();	
}
</script>
<script type="text/javascript">
function updatePracticeWorkProductDetails(){
var practiceCode=$('#practiceCode').val();

//Start of Funtion - to update WorkProduct Details
var urlToSend="practiceWorkProductMst/0/"+practiceCode;
var jsonResponse="";
$.ajax({
		type : "GET",
		url:urlToSend,
		
		success:function(jsonResponse)
		{
		
  $.each(jsonResponse, function (key, entry) {
	  
if(entry.answereType=='ANSWERETYPETEXT')
{
	document.getElementById(entry.fieldName).value=entry.answereValue;
}
else
{
	
	document.getElementById(entry.fieldName).checked = true;
}

  })
  

		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion

//document.getElementById("WP115250").checked = true;
//$('#WP115250').prop( "checked", true );

}
</script>
<script type="text/javascript">
function showPracticeDetails(cid){

//alert(cid);
$('#practiceDetailsContainer').show();
$('#practiceCode').val(cid);
$('#mainContainer').hide();
//get json for practice
 //getting ajax and updating card
	 var urlToSend="practice/0/"+cid;
	var jsonResponse="";
	$('#practiceCardTitle').html("");
	$('#practiceCardDetails').html("");
	$('#practiceValue').html("");
	$('#practiceExampleActivities').html(".");
$.ajax({
		type : "GET",
		url:urlToSend,
		
		success:function(jsonResponse)
		{
		var practiceCardDetailsScr="";
		var practiceCardTitleScr="";
		var practiceValueScr="";
		var practiceExampleActivitiesStr="";
		var practiceExampleActivitiesStrDetailed="";
  $.each(jsonResponse, function (key, entry) {
practiceCardTitleScr=jsonResponse.practiceCode;
practiceCardDetailsScr=jsonResponse.practiceDescription;
practiceValueScr=jsonResponse.practiceValue;
practiceExampleActivitiesStr=jsonResponse.practiceExamples;
$('#practiceDescription').val(jsonResponse.practiceDescription);
$('#practiceExamples').val(jsonResponse.practiceExamples);
$('#practiceName').val(jsonResponse.practiceName);
$('#practiceValue').val(jsonResponse.practiceValue);
$('#currentGapNptes').val(jsonResponse.currentGapNptes);
$('#practiceComplianceLevel').val(jsonResponse.practiceComplianceLevel);


  })
  
				
updateCompliance($('#practiceComplianceLevel').val());
$('#practiceCardTitle').append(practiceCardTitleScr);
$('#practiceCardDetails').append(practiceCardDetailsScr);
$('#practiceValue').append(practiceValueScr);
var exampleActivitiesCodes=practiceExampleActivitiesStr.split(".");
for(var i=0;i<exampleActivitiesCodes.length;i++)
{
	practiceExampleActivitiesStrDetailed=practiceExampleActivitiesStrDetailed+"<li>"+exampleActivitiesCodes[i]+"</li>";
	
}
$('#practiceExampleActivities').append(practiceExampleActivitiesStrDetailed);
updateCompliance($('#practiceComplianceLevel').val());
	},
		error:function(e)
		{
			
		}
	});
    

	 //end getting ajax and updating card
//get json for practice
updatePracticeWorkProductDetails();
}
</script>


<script type="text/javascript">
function updatePracticeAreaList(cid){
	
//Start of Funtion - to update capability list from master
var urlToSend="relationMappingCapabilityPracticeArea/getByCapability/0/"+cid;

var practiceAreaMappedList="";
$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
$.each(jsonResponse, function (key, entry) {
	
practiceAreaMappedList=practiceAreaMappedList+entry.practiceAreaCode+",";
  })	

	if(practiceAreaMappedList.length>0)
	{
		var practiceAreaCodes=practiceAreaMappedList.split(",");
		$('#practiceAreaListCards').html("");
		$('#capabilityListCards').html("");
		$('#practiceAreaListLabel').html("");
		$('#practiceAreaListLabel').html("Select PracticeArea");
		$('#categoryListCards').html("");
		$('#practiceAreaList2').html("");
		$('#practiceAreaDetails').hide();
		var practiceAreaStr="";
		var practiceAreaListCardsStr="";
		for(var i=0;i<practiceAreaCodes.length;i++)
	{
		if(practiceAreaCodes[i]!="")
		{
			
//Start of Funtion - to update capability list from master
var urlToSend="practiceArea/0/"+practiceAreaCodes[i];
var jsonResponse="";
$.ajax({
		type : "GET",
		url:urlToSend,
		
		success:function(jsonResponse)
		{
		
  $.each(jsonResponse, function (key, entry) {

practiceAreaStr="<a href=\"javascript:void(0)\" onclick=\"updatePracticeAreaCode(this.id)\" class=\"dropdown-item\" id="+jsonResponse.practiceAreaCode+">"+jsonResponse.practiceAreaName+"</a>"; 
practiceAreaListCardsStr="<div class=\"doing-box col-lg-3  mb-3\"><a href=\"javascript:void(0)\" onclick=\"updatePracticeAreaCode(this.id)\" id="+jsonResponse.practiceAreaCode+"><div class=\"card\" ><div class=\"card-body\"  ><h5  class=\"card-title\">"+jsonResponse.practiceAreaCode+"</h5><p class=\"card-text\">"+jsonResponse.practiceAreaName+"</p></div></div></a></div>";

  })
  
$('#practiceAreaList2').append(practiceAreaStr);
$('#practiceAreaListCards').append(practiceAreaListCardsStr);

		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
		}
	}
	
	}

		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
}
</script>

<script type="text/javascript">
function updateCapabilityList(cid){
	
//Start of Funtion - to update capability list from master
var urlToSend="relationMappingCategoryCapability/getBycategory/0/"+cid;
var capabilityMappedList="";
$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
			
$.each(jsonResponse, function (key, entry) {
	
capabilityMappedList=entry.capabilityCode+",";
  
  })	
  
	if(capabilityMappedList.length>0)
	{
		var capabilityCodes=capabilityMappedList.split(",");
		$('#capabilityListCards').html("");
		$('#categoryListCards').html("");
		$('#capabilityList').html("");
		$('#practiceAreaDetails').hide();
		var capabilityStr="";
		var capabilityListCardsStr="";
		for(var i=0;i<capabilityCodes.length;i++)
	{
		if(capabilityCodes[i]!="")
		{
			
//Start of Funtion - to update capability list from master
var urlToSend="capability/0/"+capabilityCodes[i];
var jsonResponse="";
$.ajax({
		type : "GET",
		url:urlToSend,
		
		success:function(jsonResponse)
		{
		
  $.each(jsonResponse, function (key, entry) {

capabilityStr="<a href=\"javascript:void(0)\" onclick=\"selectedCapability(this.id)\" class=\"dropdown-item\" id="+jsonResponse.capabilityCode+">"+jsonResponse.capabilityName+"</a>"; 
capabilityListCardsStr="<div class=\"doing-box col-lg-3  mb-3\"><a href=\"javascript:void(0)\" onclick=\"selectedCapability(this.id)\" id="+jsonResponse.capabilityCode+"><div class=\"card\" ><div class=\"card-body\"  ><h5  class=\"card-title\">"+jsonResponse.capabilityCode+"</h5><p class=\"card-text\">"+jsonResponse.capabilityName+"</p></div></div></a></div>";

  })
  
$('#capabilityList').append(capabilityStr);
$('#capabilityListCards').append(capabilityListCardsStr);

		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
		}
	}
	
	}

		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion
}
</script>


<script type="text/javascript">
function updatePracticeList(cid){
var urlToSend="";
var practiceAreaCode=$('#practiceAreaCode').val();

$('#practiceListCards').html("");
if(cid=="ALL")
{
urlToSend="relationMappingPracticeAreaPractice/getByPracticeArea/0/"+practiceAreaCode;
}
else
{
urlToSend="relationMappingPracticeAreaPractice/getByPracticeLevel/0/"+cid;
}

var practiceListCodesStr="";
var practiceLevelCodes="";
$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
		
	


  $.each(jsonResponse, function (key, entry) {
	  practiceListCodesStr=practiceListCodesStr+entry.practiceCode+",";
	    });
 practiceLevelCodes=practiceListCodesStr.split(",");

	for(var i=0;i<practiceLevelCodes.length;i++)
	{	 
 if(practiceLevelCodes[i]=="")
	 {
		 continue;
	 }
	 //getting ajax and updating card
	 var urlToSend="practice/0/"+practiceLevelCodes[i];
	var jsonResponse="";
$.ajax({
		type : "GET",
		url:urlToSend,
		
		success:function(jsonResponse)
		{
		var practiceListCardsStr="";
  $.each(jsonResponse, function (key, entry) {


practiceListCardsStr="<div class=\"doing-box col-lg-3 mb-3\" ><a href=\"#\" onclick=\"showPracticeDetails(this.id)\" id="+jsonResponse.practiceCode+"><div class=\"card\" ><div class=\"card-body\"  ><h5 class=\"card-title\">"+jsonResponse.practiceCode+"</h5><p class=\"card-text\">"+jsonResponse.practiceName+"</p></div></div></a></div>";

  })
  


$('#practiceListCards').append(practiceListCardsStr);

		},
		error:function(e)
		{
			
		}
	});
    

	 //end getting ajax and updating card
 } 		
		},
		error:function(e)
		{
	
		}
		  

	});

//Now getting practice information based on array

	


//End getting practice information
}
</script>

<script type="text/javascript">
function updateCapabilityCode(cid){
	$('#capabilityCode').val(cid);

}
</script>
<script type="text/javascript">
function updatePracticeAreaCode(cid){
	showMainDetails();
	$('#practiceAreaCode').val(cid);
	getPracticeAreaDetails(cid);
	updatePracticeDropDown(cid);
	
}
</script>
<script type="text/javascript">
function getLastValue(){
	alert("getLastValue");
	
}
</script>

<script type="text/javascript">
function handleChange(workProduct,questionId,answereType,answereValue){
var workProductMstTableStr="";
var practiceAreaCode=$('#practiceAreaCode').val();
var practiceCode=$('#practiceCode').val();
var fieldName=workProduct+questionId+answereValue;
if(answereType=='ANSWERETYPETEXT')
{
	fieldName=workProduct+questionId;
}

workProductMstTableStr=workProductMstTableStr+"<tr ><th scope=\"row\">"+workProduct+"</th><td>"+questionId+"</td><td>"+answereValue+"</td></tr>";
var formData={
	practiceAreaCode:practiceAreaCode,
	practiceCode:practiceCode,
	workProductId:workProduct,
	questionId:questionId,
	answereValue:answereValue,
	entryDate:"13072020",
	answereType:answereType,
	fieldName:fieldName,
	}
			
   
	
	$.ajax({
		type : "POST",
		contentType:"application/json",
		url:"practiceWorkProductMst/save",
		data:JSON.stringify(formData),
		dataType:'json',
		success:function(jsonResponse)
		{

	//		window.location.replace("/GapAnalysis-1.0/questionMstCrud");
			
		},
		error:function(e)
		{
			alert("FAILED");
		}
	});		
		
//$('#tableWorkProductMst').append(workProductMstTableStr);
//alert(answeresList);	
}
</script>

<script type="text/javascript">
function updateQuestionList(cid,questionId){
	//alert("updateQuestionList"+cid+"-"+questionId);
var questionIdCodes=questionId.split(",");
for(var i=0;i<questionIdCodes.length;i++)
{
//Code to get Question Details
var urlToSend="questionMst/0/"+questionIdCodes[i];

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
			var practiceCode=$('#practiceCode').val();
			var ccid=cid+"";
			var answereType="";
			if(jsonResponse.answereType=="ANSWERETYPEYESNO")
			{
				answereType=
				"<div class=\"form-check custom-control custom-radio\">"+
									"<input type=\"radio\" class=\"custom-control-input\" onclick=\"handleChange('"+ccid+"',"+jsonResponse.questionId+",'"+jsonResponse.answereType+"','Y');\" name=\""+ccid+jsonResponse.questionId+"\" id=\""+ccid+jsonResponse.questionId+"Y\" >"+
									"<label class=\"custom-control-label\" for=\""+ccid+jsonResponse.questionId+"Y\">Yes</label>"+
									"</div>"+
									"<div class=\"form-check custom-control custom-radio\">"+
									"<input type=\"radio\" class=\"custom-control-input\" onclick=\"handleChange('"+ccid+"',"+jsonResponse.questionId+",'"+jsonResponse.answereType+"','N');\" name=\""+ccid+jsonResponse.questionId+"\" id=\""+ccid+jsonResponse.questionId+"N\">"+
									"<label class=\"custom-control-label\" for=\""+ccid+jsonResponse.questionId+"N\">No</label>"+
									"</div>";
			}
			if(jsonResponse.answereType=="ANSWERETYPEPERCENTAGE")
			{
				answereType=		"<div class=\"form-check custom-control custom-radio\">"+
									"<input type=\"radio\" class=\"custom-control-input\" onclick=\"handleChange('"+ccid+"',"+jsonResponse.questionId+",'"+jsonResponse.answereType+"','25');\"  name=\""+ccid+jsonResponse.questionId+"\" id=\""+ccid+jsonResponse.questionId+"25\">"+
									"<label class=\"custom-control-label\" for=\""+ccid+jsonResponse.questionId+"25\">25</label>"+
									"</div>"+
									"<div class=\"form-check custom-control custom-radio\">"+
									"<input type=\"radio\" class=\"custom-control-input\" onclick=\"handleChange('"+ccid+"',"+jsonResponse.questionId+",'"+jsonResponse.answereType+"','50');\" name=\""+ccid+jsonResponse.questionId+"\" id=\""+ccid+jsonResponse.questionId+"50\">"+
									"<label class=\"custom-control-label\" for=\""+ccid+jsonResponse.questionId+"50\">50</label>"+
									"</div>"+
									"<div class=\"form-check custom-control custom-radio\">"+
									"<input type=\"radio\" class=\"custom-control-input\" onclick=\"handleChange('"+ccid+"',"+jsonResponse.questionId+",'"+jsonResponse.answereType+"','75');\" name=\""+ccid+jsonResponse.questionId+"\" id=\""+ccid+jsonResponse.questionId+"75\" >"+
									"<label class=\"custom-control-label\" for=\""+ccid+jsonResponse.questionId+"75\">75</label>"+
									"</div>"+
									"<div class=\"form-check custom-control custom-radio\">"+
									"<input type=\"radio\" class=\"custom-control-input\" onclick=\"handleChange('"+ccid+"',"+jsonResponse.questionId+",'"+jsonResponse.answereType+"','100');\" name=\""+ccid+jsonResponse.questionId+"\" id=\""+ccid+jsonResponse.questionId+"100\" >"+
									"<label class=\"custom-control-label\" for=\""+ccid+jsonResponse.questionId+"100\">100</label>"+
									"</div>";
			}
			if(jsonResponse.answereType=="ANSWERETYPETEXT")
			{
				answereType="<div class=\"form-group\"><textarea class=\"form-control\" onChange=\"handleChange('"+ccid+"',"+jsonResponse.questionId+",'"+jsonResponse.answereType+"',(this.value));\" value=\"\" id=\""+ccid+jsonResponse.questionId+"\"></textarea></div>";
			}
			//
			var questionStr="";
			questionStr="<div class=\"card-body\"><h5>"+jsonResponse.questionDescription+"</h5><form>"+answereType+"</form></div>";
			
			
			//questionStr="aa";
			var divs = document.getElementById(cid);
divs.innerHTML +=questionStr;
//document.getElementById(cid).appendChild(questionStr);

		},
		error:function(e)
		{
	
		}
	});
//Code end to get question details		
}

}
</script>
<script type="text/javascript">
function updateWorkProductDetails(){
	var workProductList=$('#workProductList').val();
	var workProductListSplitCodes=workProductList.split(",");
	$('#workProductListCards').html("");
	for(var i=0;i<workProductListSplitCodes.length;i++)
	{
		if(workProductListSplitCodes[i]=="") {} else
		{
	//Code to get workProduct Json
var urlToSend="workProductMst/0/"+workProductListSplitCodes[i];

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
		$('#workProductListCards').append("<div class=\"list-box\"><div class=\"card\" id=\"WP"+jsonResponse.workProductId+"\"><div class=\"prdct-title\"><h3>"+jsonResponse.workProductName+"</h3></div></div>");
		updateQuestionList('WP'+jsonResponse.workProductId,jsonResponse.questionId,jsonResponse.workProductName);
		
		},
		error:function(e)
		{
	
		}
	});
	//Code to get work product json
	}
	}
	//alert("updateWorkProductDetails"+workProductList);
}
</script>
<script type="text/javascript">
function updatePracticeCode(cid){
	$('#practiceCode').val(cid);
}
</script>
<script type="text/javascript">
function updatePracticeLevelCode(cid){
	$('#practiceLevelCode').val(cid);
	showPractices(cid);
}
</script>
<script type="text/javascript">
function updatePracticeDropDown(practiceAreaCode){
	//get values from relation table
	//Start of Funtion - to update practice list from master
var urlToSend="relationMappingPracticeAreaPractice/getByPracticeArea/0/"+practiceAreaCode;

$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
			if(jsonResponse=="")
			{
				alert("No practices mapped against given practice Area");
			}
var practiceStr="";
var practiceLevelListStr= [];
var practiceListCardsStr="";
var unique=[];
  $.each(jsonResponse, function (key, entry) {
	  

   if (!unique[entry.practiceLevelCode])
   {
            practiceLevelListStr.push(entry.practiceLevelCode);
			    unique[entry.practiceLevelCode] = 1;


   }

practiceStr=practiceStr+"<a href=\"javascript:void(0)\" onclick=\"updatePracticeCode(this.id)\" class=\"dropdown-item\" id="+entry.practiceCode+">"+entry.practiceCode+"</a>"; 
practiceListCardsStr=practiceListCardsStr+entry.practiceCode+",";
  })
$('#practiceListCardsStr').val(practiceListCardsStr);
 $('#practiceList').html("");
$('#practiceList').append(practiceStr);
 $('#practiceLevelList').html("");
 practiceLevelListStr=practiceLevelListStr+"";
 var practiceLevelCodes=practiceLevelListStr.split(",");
 for(var i=0;i<practiceLevelCodes.length;i++)
 {
	 $('#practiceLevelList').append("<a href=\"javascript:void(0)\" onclick=\"updatePracticeLevelCode(this.id)\" class=\"dropdown-item\" id="+practiceLevelCodes[i]+">"+practiceLevelCodes[i]+"</a>"); 
 }
			
		},
		error:function(e)
		{
	
		}
	});
    

	
//End of Funtion
}
</script>
<script type="text/javascript">
function getPracticeAreaDetails(practiceAreaCode){
$('#categoryListCards').html("");
$('#capabilityListCards').html("");
$('#practiceAreaListCards').html("");
$('#practiceAreaListLabel').html("");
$('#practiceAreaListLabel').append(practiceAreaCode);

	//Start of Funtion - to update practiceArea list from master
var urlToSend="practiceArea/0/"+practiceAreaCode;
$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
			
			//entry.practiceAreaCode
			var updatePageTitle="";
			var updateCardTitle="";
			var updateCardDetails="";
			var updatePracticeAreaIntentCard="";
			var updatePracticeAreaValueCard="";
			var updatePracticeAreaAdditionalInfoCard="";
			var workProductListCons="";
			updatePageTitle="<li aria-current=\"page\" class=\"breadcrumb-item active\" >"+jsonResponse.practiceAreaName+"</li>";
			updateCardTitle="<h5 class=\"card-title\">"+jsonResponse.practiceAreaCode+"</h5>";
			updateCardDetails="<p class=\"card-text\">"+jsonResponse.practiceAreaName+"</p>";
			updatePracticeAreaIntentCard="<p>"+jsonResponse.practiceAreaIntent+"</p>";
			updatePracticeAreaValueCard="<p>"+jsonResponse.practiceAreaValue+"</p>";
			updatePracticeAreaAdditionalInfoCard="<p>"+jsonResponse.practiceAreaAdditionalInfo+"</p>";
			workProductListCons=jsonResponse.workProductList;
			//workProductList
		$('#practiceAreaDetails').show();
		$('#practiceAreaIntent').html("");
		$('#practiceAreaTitle').html("");
		$('#practiceAreaCardTitle').html("");
		$('#practiceAreaIntent').html("");
		$('#practiceAreaValue').html("");
		$('#practiceAreaAdditionalInfo').html("");
		$('#practiceAreaTitle').append(updatePageTitle);
		$('#practiceAreaCardTitle').append(updateCardTitle);
		$('#practiceAreaCardTitle').append(updateCardDetails);
		$('#practiceAreaIntent').append(updatePracticeAreaIntentCard);
		$('#practiceAreaValue').append(updatePracticeAreaValueCard);
		$('#practiceAreaAdditionalInfo').append(updatePracticeAreaAdditionalInfoCard);
		$('#workProductList').html("");
		$('#workProductList').val(workProductListCons);
		updateWorkProductDetails();
		},
		error:function(e)
		{
			
		}
	});
    

	
//End of Funtion

}
</script>
</head>
<body>
<%
AccessObject accessObject = (AccessObject)request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT) ;
%>
	<!-- header sec start -->
	<header class="p-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href="#" class="logo" alt=" ">
						<h2><%=accessObject.getOrgName()%></h2>
					</a>
					<button type="button" onclick="window.location.replace('/GapAnalysis-1.0/?tid=0');" class="btn btn-danger" style="
					">log out</button>
				</div>
				</div>
			</div>
		</div>
	</header>
	<!-- header sec end -->

	<!-- side bar sec start -->
	<div class="profile-main">
		<div class="sidebar-main">
			<div class="side-bar-menu">
				<nav id="sidebar">
					<i class="fa fa-times"></i>
					<div class="sidebar-header py-3">
						<h3>Practice Area</h3>
					</div>
					<ul id="practiceAreaList" name="practiceAreaList" class="list-unstyled components">
					
					
					</ul>
				</nav>
			</div>
		</div>


		<div id="content">
			<nav class="navbar navbar-expand-lg">
				<div class="container-fluid p-0">
					<button type="button" id="sidebarCollapse" class="btn btn-info">
						<i class="fa fa-align-left"></i>
						<span>Toggle Sidebar</span>
					</button>
					<div class="right-panel-header bg-2 p-4 px-5">
						<div class="panel-title">
							<h3>Home</h3>
							<div class="form-group">

								<input type="hidden" class="form-control" id="tenantId" value="<%=accessObject.getTenantId()%>" placeholder="" >
								<input type="hidden" class="form-control" id="practiceComplianceLevel" value="" placeholder="" >
								<input type="hidden" class="form-control" id="practiceListCodesStr" value="" placeholder="" >
								<input type="hidden" class="form-control" id="categoryCode" value="" placeholder="" >
								<input type="hidden" class="form-control" id="capabilityCode" value="" placeholder="" >
								<input type="hidden" class="form-control" id="practiceAreaCode" value="" placeholder="" >
								<input type="hidden" class="form-control" id="practiceCode" value="" placeholder="" >
								<input type="hidden" class="form-control" id="practiceLevelCode" value="" placeholder="" >
								<input type="hidden" class="form-control" id="workProductList" value="" placeholder="" >
								<input type="hidden" class="form-control" id="practiceDescription" value="" placeholder="" >
								<input type="hidden" class="form-control" id="practiceExamples" value="" placeholder="" >
								<input type="hidden" class="form-control" id="practiceName" value="" placeholder="" >
								<input type="hidden" class="form-control" id="practiceValue" value="" placeholder="" >
								
								
							</div>
							<div class="dropdown">
								<button id="categoryListLabel" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									select category
								</button>
								<div id="categoryList" name="categoryList" class="dropdown-menu" aria-labelledby="dropdownMenuButton">

								</div>
							</div>
							&nbsp;&nbsp;
							<div class="dropdown">
								<button id="capabilityListLabel" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									select Capability
								</button>
								<div id="capabilityList" name="capabilityList" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									
								</div>
							</div>
							&nbsp;&nbsp;
							<div class="dropdown">
								<button id="practiceAreaListLabel" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									select PracticeArea
								</button>
								<div id="practiceAreaList2" name="practiceAreaList2" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									
								</div>
							</div>
							&nbsp;&nbsp;
							<div class="dropdown">
								<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									select Level
								</button>
								<div id="practiceLevelList" name="practiceLevelList" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									
								</div>
							</div>
							&nbsp;&nbsp;
							<div class="dropdown">
								<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									select Practice
								</button>
								<div id="practiceList" name="practiceList" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									
								</div>
							</div>
							
						</div>
					</div>


				</div>
			</nav>	
		</div>
	</div>

	<!-- side bar sec end -->
	<!-- side bar sec end -->

	<!-- Doing main sec start -->
	<div id="mainContainer" class="doing-main ensuring-main rdm-main">
	<div class="doing-main ensuring-main rdm-main" id="practiceAreaDetails" name="practiceAreaDetails">
		<div class="doing-title">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" id="practiceAreaTitle" name="practiceAreaTitle">
					
				</ol>
			</nav>
			<br>
		</div>

		<div class="doing-categry-main">
			<div class="bg-color"></div>


			<div class="container">
				<div class="row">
					<div class="doing-box-main col-lg-12">
						<div class="doing-box col-lg-3">
							<a href="javascript:void(0)" onclick="showPractices('ALL');">
								<div class="card">
									<div button type="button" class="card-body" id="practiceAreaCardTitle" name="practiceAreaCardTitle" ></div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			
			<div class="rdm-main-cntnt">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="rdm-text">
								<div class="rdm-text-area">
									<h2 class="para-title">required practice area information</h2>
									<div class="rdm-inr-cntnt">
										<p class="para-title" >intent</p>
										<div  id="practiceAreaIntent" name="practiceAreaIntent">
										</div>
									</div>
									<div class="rdm-inr-cntnt">
										<p class="para-title">value</p>
										<div  id="practiceAreaValue" name="practiceAreaValue">
										</div>
									</div>

									<div class="rdm-inr-cntnt full-cntnt">
										<p class="para-title">additional process area information</p>
										<div  id="practiceAreaAdditionalInfo" name="practiceAreaAdditionalInfo">
										</div>
									</div>
									<a href="javascript:void(0)" onclick="showPractices('ALL');">
									<button type="button" onclick="showPractices('All');" class="btn btn-danger next">Next</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Doing main sec end -->
	
<!--Doing main select for Category-->
<div class="doing-main ensuring-main rdm-main">
		<div class="doing-title">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" >
					<a></a>
				</ol>
			</nav>
			<br>
		</div>
	<div class="doing-title">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					
				</ol>
			</nav>
			<br>
		</div>

		<div class="doing-categry-main">
			


			<div class="container">
				<div class="row">
					<div class="doing-box-main col-lg-12" id="categoryListCards" name="categoryListCards"></div>
					<div class="doing-box-main col-lg-12" id="capabilityListCards" name="capabilityListCards"></div>
					<div class="doing-box-main col-lg-12" id="practiceAreaListCards" name="practiceAreaListCards"></div>
					<div class="doing-box-main col-lg-12" id="practiceListCards" name="practiceListCards"></div>
				</div>
			</div>
			</div>
			</div>
			</div>
			
<!--Doing select for Category End-->
<!--Container for Practice Details -->
<div id="practiceDetailsContainer" class="rdm-box rdm-level-2">
<br>						
						<div class="rdm-box rdm-level-2">
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<div class="media col-lg-4">
											<div class="media-body">
												<a href="#">
													<div class="card">
														<div class="card-body">
															<h5 id="practiceCardTitle" class="card-title">RDM 2.1</h5>
															<p id="practiceCardDetails" class="card-text">Elicit stakeholder needs, ex-pectations, constraints, and interfaces or connections </p>
														</div>
													</div>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>


<div class="rdm-level-maincntnt float-left">
				<div class="value pt-5">
					<h3 class="comn-title">value</h3>
					<p id="practiceValue" class="comn-para">
						Recorded requirements are the basis for successfully addressing customer needs and expectations
					</p>
				</div>
				<div class="activity">
					<h3 class="comn-title">example activities</h3>
					<ul id="practiceExampleActivities" class="comn-para">
						<li>Record the requirements</li>
						<li>Record the requirements</li>
						<li>Record the requirements</li>
					</ul>
				</div>
				<div class="work-prdcts pt-4 float-left">
					<h3 class="comn-title">required work products</h3>

<!--Code portion for Work Product List And Data -->
					<div id="workProductListCards">
					<div class="list-box"><div class="card"><div class="prdct-title"><h3>list of requirements</h3></div>
						<div id="112" class="card-body">Coverage</div></div></div>
					<div class="list-box"><div class="card"><div class="prdct-title"><h3>list of requirements</h3></div></div></div>
					<div class="list-box"><div class="card"><div class="prdct-title"><h3>list of requirements</h3></div></div></div>
					</div>
					
<!--Code portion End for Work Product List And Data -->

<div class="btn-group">
  <button class="btn btn-primary btn-lg dropdown-toggle" id="LabelCompliance" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    Select Compliance Level
  </button>
  <div class="dropdown-menu">
    <a href="javascript:void(0)" onclick="updateCompliance(this.id)" class="dropdown-item" id="1">FI</a>
	<a href="javascript:void(0)" onclick="updateCompliance(this.id)" class="dropdown-item" id="0.75">LI</a>
	<a href="javascript:void(0)" onclick="updateCompliance(this.id)" class="dropdown-item" id="0.50">PI</a>
	<a href="javascript:void(0)" onclick="updateCompliance(this.id)" class="dropdown-item" id="0">NI</a>
  </div>
</div>
<br><br>


				<div class="gap-notes">	
					<h5>Current gap notes</h5>
					<div class="form-group">
						<textarea class="form-control" id="currentGapNptes" rows="10" placeholder="Mention additional information if any">
						</textarea>
					</div>
				</div>

			</div>

			
		</div>
		<a> </a><button type="button" id="commit" class="btn btn-danger next" style="float: centre;">Save/Update</button> 

	</div>
	
</div>
</div>
</div>
<!--Container for practice Details -->


	
</body>
</html>