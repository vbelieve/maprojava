<!DOCTYPE html>
<html>
<head>
	<title>Question</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js">
	<script src="js/jquery-3.3.1.slim.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
//Start of AnswereType level List
$.ajax({
		type : "GET",
		url:"lookup/0/ANSWERETYPES",
		success:function(jsonResponse)
		{
			
	
var answereTypeListStr="";

  $.each(jsonResponse, function (key, entry) {
    answereTypeListStr=answereTypeListStr+"<a href=\"javascript:void(0)\" onclick=\"updateAnswereType(this.id)\" class=\"dropdown-item\" id="+entry.lookupValue+">"+entry.displayValue+"</a>"; 
  })
$("#answereTypeList").html("");
$("#answereTypeList").append(answereTypeListStr);

			
		},
		error:function(e)
		{
			alert("FAILED");
		}
	});
//End of Getting Practice Level List

//Start of Getting Updated Relations List
$.ajax({
		type : "GET",
		url:"questionMst/0",
		success:function(jsonResponse)
		{
			
	
var questionMstTableStr="";
var srNo=1;

  $.each(jsonResponse, function (key, entry) {
	questionMstTableStr=questionMstTableStr+"<tr onclick=\"(updateQuestionCode("+entry.tenantId+",'"+entry.questionId+"','"+entry.questionDescription+"'))\" id="+entry.questionId+"><th scope=\"row\">"+srNo+"</th><td>"+entry.questionId+"</td><td>"+entry.questionDescription+"</td><td>"+entry.answereType+"</td></tr>";
    srNo=srNo+1;
    

  })
$(questionMstTable).html("");
$("#questionMstTable").append(questionMstTableStr);
			
		},
		error:function(e)
		{
			alert("FAILED");
		}
	});
//End of Getting Practices List


$("#commit").click(function(){

var questionId=$("#questionCode").val()+'';	

	var questionDescription=$("#questionDescription").val()+'';
	var answereType=$("#answereType").val()+'';
	
	
	if(questionDescription=="")
	{
		alert ("questionDescription  Is Mandatory");
		return false();
	}
	if(answereType=="")
	{
		alert ("answereType  Is Mandatory");
		return false();
	}
	
	
	var formData={
	questionId:questionId,
	questionDescription:questionDescription,
	answereType:answereType,
	}
			
   
	
	$.ajax({
		type : "POST",
		contentType:"application/json",
		url:"questionMst/save",
		data:JSON.stringify(formData),
		dataType:'json',
		success:function(jsonResponse)
		{

			window.location.replace("/GapAnalysis-1.0/questionMstCrud");
			
		},
		error:function(e)
		{
			alert("FAILED");
		}
	});		
			
	
});
});
</script>
<script type="text/javascript">
function updateQuestionCode(tenantId,questionCode,questionDescription){
$("#tenantId").val(tenantId);
$("#questionCode").val(questionCode);
$("#questionDescription").val(questionDescription);



}
</script>

<script type="text/javascript">
function updateAnswereType(cid){
//Start of Funtion - to update Capability Code
$("#answereType").val(cid);
$("#answereTypeListLabel").html("");
$("#answereTypeListLabel").append(cid);
} 
</script>	

</head>
<body>
	<div id="login">
		<div class="container">
			<div class="gap">
				<h3>gap analsis</h3>
			</div>
			<div id="login-row" class="row justify-content-end">
				<div id="login-column" class="col-md-5">
					<div id="login-box">
					<form id="userRegistration" class="form" action="" method="post">
						<form class="w-100 bg-white p-5 sign-up-form">
							<div class="sign-up-title">
								<h3>Question</h3>
							</div>

<br>

<input type="hidden" id="answereType">
<input type="hidden" id="questionCode" value="" placeholder="">

					<div class="form-group">
								<label for="questionDescription">Question</label>
								<input type="text" class="form-control" id="questionDescription" placeholder="">
							</div>
							<br>
							<a>Answere Type</a>
							<div class="dropdown" >
					<button id="answereTypeListLabel" class="btn btn-secondary dropdown-toggle dropdown-item-text" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									select AnswereType
					</button>
								<br>
								<div id="answereTypeList" name="answereTypeList" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									
								</div>
							</div>
							<br>
		
							
							<div class="form-group float-right">	
							<button type="button" id="commit"  name="commit" class="btn btn-primary">Submit</button>
							<button type="button" id="back" onclick="window.location='/GapAnalysis-1.0/admin';"  name="back" class="btn btn-primary">Back</button>
								
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
<!--Table-->
<table id="tablequestionMst" class="table  table-bordered table-hover ">
<!--Table head-->
  <thead>
    <tr>
      <th>#</th>
      <th>QuestionId</th>
	  <th>Question</th>
	  <th>AnswereType</th>
	   </tr>
  </thead>
  <!--Table head-->
  <!--Table body-->
<tbody id="questionMstTable" name="questionMstTable">

</tbody>
  <!--Table body-->
</table>
<!--Table-->
	</div>


	
	<script src="js/popper.min.js"></script> 
	<script src="js/bootstrap.js"></script> 
	<script src="js/custom.js"></script>


</body>
</html>


