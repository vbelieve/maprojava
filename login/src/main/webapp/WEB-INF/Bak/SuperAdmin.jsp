<!DOCTYPE html>
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>
<%@page import="com.login.domain.basics.DashBoardVo"%>
<html>
   <head>
	<%@include file="header.jsp"%>  
      <title>Product Category</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
      <link rel="stylesheet" type="text/css" href="css/responsive.css">





      <!-- add new css -->
      <link rel="stylesheet" type="text/css" href="css/circle.css">
      <link rel="stylesheet" type="text/css" href="css/new-style.css">




      <!-- Google fonts -->
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
      <!-- Fontawesome cdn -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
      <script src="js/jquery-3.3.1.slim.min.js"></script>
      <script src="js/popper.min.js"></script> 
      <script src="js/bootstrap.js"></script> 
      <script src="js/custom.js"></script>
   </head>
   <body>
   <%
   Map<String, Double> complianceListForAll = new HashMap<String, Double>();
   Map<String, String> tenantList = new HashMap<String, String>();
   complianceListForAll=(Map<String, Double>)request.getAttribute("complianceListForAll");
   if(complianceListForAll == null){ complianceListForAll = new HashMap<String, Double>(); }
   
	
	List<DashBoardVo> dashBoardVoList=new ArrayList<DashBoardVo>();
	String previousTenant="";
	dashBoardVoList=(List<DashBoardVo>) request.getAttribute("dashBoardVoList");
	if(dashBoardVoList==null){dashBoardVoList=new ArrayList<DashBoardVo>();} else
		{
			String previousOrg="";
			for(DashBoardVo DBVO:dashBoardVoList)
			{
				if(previousTenant!=DBVO.getTenantId())
				{
					tenantList.put(DBVO.getTenantId(),DBVO.getTenantName());
					System.out.println(">>>>"+DBVO.getTenantId()+">>>>"+DBVO.getTenantName());
				}
				
			}
			
		}
		
		Long compliance=0L;
		Long practiceCount=0L;
						for(Map.Entry<String, String> entry : tenantList.entrySet())
						{
							for(DashBoardVo DBVO:dashBoardVoList)
						{
						if(entry.getKey()==DBVO.getTenantId())
						{
						//	compliance=compliance+getP
						}
				
						}
						}
	
Double d1=90.00;

	
   %>
					
      <!-- side bar sec start -->
      <div class="profile-main">
         <div class="sidebar-main">
            <div class="side-bar-menu">
               <nav id="sidebar" style="margin-left:20px">
                  
                  <div class="sidebar-header py-3" >
                     <h3>Menu</h3>
                  </div>
                  <ul id="practiceAreaList" name="practiceAreaList" class="list-unstyled components">
                     
					 <li>
					 <hr class="sidebar-divider">
					<h5 style="margin-left:20px;" >Core Masters</h5>                     
					
					 	<ul>
						<li><a href="workProductMstCrud">Work Product Master</a></li>
						<li><a href="categoryMstCrud">Category Management</a></li>
						<li><a href="capabilityMstCrud">Capability Management</a></li>
						<li><a href="practiceAreaMstCrud">PracticeArea Management</a></li>
						<li><a href="practiceMstCrud">Practice Management</a></li>
						</ul>
					</li>	
                     
					 &nbsp
					 <hr class="sidebar-divider">
					 <h5 style="margin-left:20px;">RelationShip masters</h5>                     
					
					 <ul>
						<li><a href="relationMappingCategoryCapabilityCrud">Category- Capability Master</a></li>
						<li><a href="relationMappingCapabilityPracticeAreaCrud">capability-PracticeArea Management</a></li>
						<li><a href="relationMappingPracticeAreaPracticeCrud">PracticeArea-Pracitce Management</a></li>
						</ul>
                     </li>
                  </ul>
				  &nbsp
				  <hr class="sidebar-divider">
					 <h5 style="margin-left:20px;">Users Configuration Menu</h5>                     
					 
					 <ul>
						<li><a href="signup">Create User</a></li>
						<li><a href="#">Unlock User</a></li>
						<li><a href="#">Reset User</a></li>
						</ul>
                     </li>
                  </ul>
				  &nbsp
				  <hr class="sidebar-divider">
					 <h5 style="margin-left:20px;">Administration</h5>                     
					 
					 <ul>
					<li><a href="tenantMstCrud">Organization Master</a></li>
					<li><a href="adminUserMst">Create Admin</a></li>

						</ul>
                     </li>
                  </ul>
				  &nbsp
				  <hr class="sidebar-divider">
					 <h5 style="margin-left:20px;">Dev Configuration</h5>                     
					 
					 <ul>
					<li><a href="sequenceMstCrud">Sequence Management</a></li>
			<li><a href="lookupMstCrud">Lookups Management</a></li>
			<li><a href="questionMstCrud">Questions Management</a></li>
			<hr class="sidebar-divider">
			<hr class="sidebar-divider d-none d-md-block">
						</ul>
                     </li>
                  </ul>
               </nav>
            </div>
         </div>
         
      </div>
      <!-- side bar sec end -->
      <!-- side bar sec end -->
      <!-- Doing main sec start -->
      <div id="mainContainer" class="doing-main ensuring-main rdm-main">




      	<!-- new html -->
       

         <div class="row">
         	<div class="col-md-12">
         		<div class="unit-level-box">
         			<p class="title-unit-level">Project Level Compliance</p>
         			<div class="multiple-circle">
					<% for(Map.Entry<String, Double> entry : complianceListForAll.entrySet())
						{
					%>
         				<div class="clearfix">
         					<!-- only class change p90 any print like P65 -->
			                <div class="c100 p<%=Math.round(entry.getValue())%>" id="<%=entry.getKey()%>" onclick=check(this.id)>
			                    <span><%=Math.round(entry.getValue())%>%</span>
			                    <span class="secondspan">Achived</span>
			                    <div class="slice">
			                        <div class="bar"></div>
			                        <div class="fill"></div>
			                    </div>
			                </div>
			                <p class="title-circle"><%=entry.getKey()%></p>
			            </div>
						<%
						}
						%>
			           
         			</div>
         		</div>
         	</div>
         	<div class="col-md-12">
         		<div class="unit-wise-box">
         			<p class="title-unit-wise">
         				Unit wise - Project Level Compliance
         				<label>Unit Name</label>
         			</p>
         			<div class="multiple-circle">
         				<div class="clearfix">
         					<!-- only class change p90 any print like P65 -->
			                <div class="c100 p<%=d1%>%">
			                    <span><%=d1%>%</span>
			                    <span class="secondspan">Achived</span>
			                    <div class="slice">
			                        <div class="bar"></div>
			                        <div class="fill"></div>
			                    </div>
			                </div>
			                <p class="title-circle">Project 1</p>
			            </div>
			            <div class="clearfix">
         					<!-- only class change p90 any print like P65 -->
			                <div class="c100 p90">
			                    <span>90%</span>
			                    <span class="secondspan">Achived</span>
			                    <div class="slice">
			                        <div class="bar"></div>
			                        <div class="fill"></div>
			                    </div>
			                </div>
			                <p class="title-circle">Project 2</p>
			            </div>
			            <div class="clearfix">
         					<!-- only class change p90 any print like P65 -->
			                <div class="c100 p90">
			                    <span>90%</span>
			                    <span class="secondspan">Achived</span>
			                    <div class="slice">
			                        <div class="bar"></div>
			                        <div class="fill"></div>
			                    </div>
			                </div>
			                <p class="title-circle">Project 3</p>
			            </div>
			            <div class="clearfix">
         					<!-- only class change p90 any print like P65 -->
			                <div class="c100 p90">
			                    <span>90%</span>
			                    <span class="secondspan">Achived</span>
			                    <div class="slice">
			                        <div class="bar"></div>
			                        <div class="fill"></div>
			                    </div>
			                </div>
			                <p class="title-circle">Project 4</p>
			            </div>
			            <div class="clearfix">
         					<!-- only class change p90 any print like P65 -->
			                <div class="c100 p90">
			                    <span>90%</span>
			                    <span class="secondspan">Achived</span>
			                    <div class="slice">
			                        <div class="bar"></div>
			                        <div class="fill"></div>
			                    </div>
			                </div>
			                <p class="title-circle">Project 5</p>
			            </div>
			            <div class="clearfix">
         					<!-- only class change p90 any print like P65 -->
			                <div class="c100 p90">
			                    <span>90%</span>
			                    <span class="secondspan">Achived</span>
			                    <div class="slice">
			                        <div class="bar"></div>
			                        <div class="fill"></div>
			                    </div>
			                </div>
			                <p class="title-circle">Project 6</p>
			            </div>
         			</div>
         		</div>
         	</div>
         </div>
         <!-- end new html -->


      </div>
      <%@include file="footer.jsp"%>
   </body>
</html>