<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.ApplicationConstants"%>
<%@page import="java.util.*"%>

<html>
<head>
<%@include file="header_user.jsp"%>
	<title>Home Page</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script> 
<script src="js/bootstrap.js"></script> 
<script src="js/custom.js"></script>
	<!-- add new css -->
	<link rel="stylesheet" type="text/css" href="css/new-style.css">
	<!-- end new css -->



	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

	<!-- Fontawesome cdn -->



<script type="text/javascript">
function selectProcess(pid){
$('#process').val(pid);
document.forms["userForm"].action = "/GapAnalysis-1.0/processSelect";
document.forms["userForm"].submit();	

}
</script>

</head>
<body>

	<!-- Doing main sec start -->
<form id="userForm" class="form" action="#" method="post">
<input type="hidden" id="process" name="process" value="">
	<div class="doing-main ensuring-main rdm-main rdm-level-2-main">
		<!-- html start -->
		<div class="rmdlevelpage" style="padding-left: 0px;">
			
			<div class="breadcrumb-div">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">

					</ol>
				</nav>
				<!-- <button>Back</button> -->
			</div>

			<div class="levelrdm" >
			<%
			for(String process:processList)
			{
				%>
				<div id="<%=process%>"  class="single-mainlevel" onclick="selectProcess(this.id)">
					
					<p class="titlelevel">Process</p>
					<p><%=process%></p>
					
				</div>
				<%
			}
				%>
				
			</div>
		</div>
		<!-- end html start -->
	</div>
</div>
&nbsp
</form>
<%@include file="footer.jsp"%>
<!-- Doing main sec end -->


</body>
</html>