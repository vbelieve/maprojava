<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.ApplicationConstants"%>

<html>
		

<head>
	<title>Sign Up</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js">
	<script src="js/jquery-3.3.1.slim.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	

//Start of Function
	$("#tenantId").change(function(){
	var tenantId=$("#tenantId").val();
	if(tenantId!="")
	{
		
		}
	
}).change();
//End of function	
$("#commit").click(function(){
	var tenantId=$("#tenantId").val();
	var userName=$("#userName").val();
	var password=$("#password").val();
	var userType=$("#userType").val();
	alert(userType);
	if(tenantId=="")
	{
		alert ("Tenant Id Is Mandatory");
		return false();
	}
	if(userName=="")
	{
		alert ("User Name Id Is Mandatory");
		return false();
	}
	if(password=="")
	{
		alert ("password Id Is Mandatory");
		return false();
	}
	var formData={
	userType:userType,
	tenantId:$("#tenantId").val(),
	userName:$("#userName").val(),
	password:$("#password").val()
	}
			
   
		
	$.ajax({
		type : "POST",
		contentType:"application/json",
		url:"userMst/save",
		data:JSON.stringify(formData),
		dataType:'json',
		success:function(jsonResponse)
		{
			alert(jsonResponse.userName);
			
		},
		error:function(e)
		{
			alert(jsonResponse.userName);
		}
	});		
			
	
});
});
</script>
	
</head>
<body>
<%
AccessObject accessObject = (AccessObject)request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT) ;
%>
	<div id="login">
		<div class="container">
			<div class="gap">
				<h3>gap analsis</h3>
			</div>
			<div id="login-row" class="row justify-content-end">
				<div id="login-column" class="col-md-5">
					<div id="login-box">
					<form id="userRegistration" class="form" action="" method="post">
						<form class="w-100 bg-white p-5 sign-up-form">
							<div class="sign-up-title">
								<h3>sign up</h3>
							</div>
							<div class="form-group">
								<label for="tenantId">Tenant Id</label>
								<input readonly type="text" class="form-control" id="tenantId" value="<%=accessObject.getTenantId()%>" placeholder="Tenant ID">
							</div>
							<div class="form-group">
								<label for="userName">user Name</label>
								<input type="text" class="form-control" id="userName" placeholder="User Name">
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="password" placeholder="*******">
								<small>Password should be atleast 8 character</small>
							</div>
							<label for="userType">User Type</label>
							<div class="dropdown">
		
							<select id="userType" autocomplete class="btn btn-secondary dropdown-toggle" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							
							<option class="dropdown-item" value="">-- Select --</option>
							<option class="dropdown-item" value="ADMIN">ADMIN</option>
							<option class="dropdown-item" value="USER">USER</option>
							</select>
</div>
							<div class="form-group float-right">	
							<button type="button" id="commit"  name="commit" class="btn btn-primary">Submit</button>
							<button type="button" id="back" onclick="window.location='/GapAnalysis-1.0/admin';"  name="back" class="btn btn-primary">Back</button>
								
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script src="js/popper.min.js"></script> 
	<script src="js/bootstrap.js"></script> 
	<script src="js/custom.js"></script>


</body>
</html>
