<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.ApplicationConstants"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>

<html>
<body>
<%
AccessObject accessObject = (AccessObject)request.getSession().getAttribute(ApplicationConstants.SESSION_PARAM_ACCESSOBJECT) ;
if(null==accessObject)
{
	accessObject=new AccessObject();
}
String allProcess=accessObject.getAllProcess();
String[] processList=null;
System.out.println(">>>>"+allProcess);
if(allProcess==null)
{
}
else
{
	processList=allProcess.split(",");
	for(String process:processList)
	{
		System.out.println(">>>>"+process);
	}
}
String tid=accessObject.getTenantId();
String homeUrl="";
String logoutUrl="/GapAnalysis-1.0/?tid="+tid;
if(accessObject.getUserType().equalsIgnoreCase("user"))
{homeUrl="/GapAnalysis-1.0/user";}
else if(accessObject.getUserType().equalsIgnoreCase("superadmin"))
{homeUrl="/GapAnalysis-1.0/superAdmin";}
else if(accessObject.getUserType().equalsIgnoreCase("manager"))
{homeUrl="/GapAnalysis-1.0/manager";}
else {homeUrl="/GapAnalysis-1.0/admin";}

%>
	<!-- header sec start -->
	<nav class="navbar navbar-expand-lg navbar-light " style="background:#02c4ac;">
  

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<%=homeUrl%>">Home <span class="sr-only">(current)</span></a>
      </li>
      </ul>
<a class="navbar-brand" href="#" style="position:absolute; left: 330px; padding-left: 0px;">Process :<%=accessObject.getProcess()%></a>
<a class="navbar-brand" href="#"><%=accessObject.getLoginId()%></a>
    <ul class="navbar-nav mr-right" style="margin-right:20px;">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Profile
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Change Password</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      
    </ul>
	
      <button class="btn btn-outline-success my-2 my-sm-0" onclick="window.location.replace('<%=logoutUrl%>');">Logout</button>
    
  </div>
</nav>
	
	<!-- header sec end -->
	
</body>
</html>