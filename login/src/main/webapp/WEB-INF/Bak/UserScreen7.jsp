<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.CategoryMst"%>
<%@page import="com.login.domain.CapabilityMst"%>
<%@page import="com.login.domain.PracticeAreaMst"%>
<%@page import="com.login.domain.PracticeMst"%>
<%@page import="com.login.domain.PracticeWorkProductMst"%>
<%@page import="java.util.*"%>

<html>
<head>
<%@include file="header_user.jsp"%>
	<title>Category</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">


	<!-- add new css -->
	<link rel="stylesheet" type="text/css" href="css/new-style.css">
	<!-- end new css -->



	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

	<!-- Fontawesome cdn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Doing main sec end -->

<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script> 
<script src="js/bootstrap.js"></script> 
<script src="js/custom.js"></script>

<script type="text/javascript">
function selectedCapability(pid){
$("#capability").val(pid);
document.forms["capabilityForm"].action = "/GapAnalysis-1.0/capabilitySelect";
document.forms["capabilityForm"].submit();	
}
</script>
<script type="text/javascript">
function handleChange(pid,aid){
	if(aid!="Select")
	{
		alert(pid+";"+aid);
$("#abbr"+pid).val(aid);
alert($("#abbr"+pid).val())
	}

}
</script>


<script type="text/javascript">
function selectedPractice(pid){
$("#practice").val(pid);
document.forms["practiceDetailForm"].action = "/GapAnalysis-1.0/practiceSelect";
document.forms["practiceDetailForm"].submit();	
}
</script>


<script type="text/javascript">
function selectedPracticeArea(pid){
$("#practiceArea").val(pid);
document.forms["practiceDetailForm"].action = "/GapAnalysis-1.0/practiceAreaSelect";
document.forms["practiceDetailForm"].submit();	
}
</script>

<script type="text/javascript">
$(document).ready(function(){

$("#practiceOption").change(function(){
var practice=$("#practiceOption").val();
if(practice!="")
{
$("#practice").val(practice);
document.forms["practiceDetailForm"].action = "/GapAnalysis-1.0/practiceSelect";
document.forms["practiceDetailForm"].submit();	
}
}).change();

$("#capabilityOption").change(function(){
var capability=$("#capabilityOption").val();
if(capability!="")
{
$("#capability").val(capability);
document.forms["practiceDetailForm"].action = "/GapAnalysis-1.0/capabilitySelect";
document.forms["practiceDetailForm"].submit();	
}
}).change();

$("#capabilityOption").change(function(){
var capability=$("#capabilityOption").val();
if(capability!="")
{
$("#capability").val(capability);
document.forms["practiceDetailForm"].action = "/GapAnalysis-1.0/capabilitySelect";
document.forms["practiceDetailForm"].submit();	
}
}).change();

$("#categoryOption").change(function(){
var category=$("#categoryOption").val();
if(category!="")
{
$("#category").val(category);
document.forms["practiceDetailForm"].action = "/GapAnalysis-1.0/categorySelect";
document.forms["practiceDetailForm"].submit();	
}
}).change();

$("#practiceAreaOption").change(function(){
var practiceArea=$("#practiceAreaOption").val();
if(practiceArea!="")
{
$("#practiceArea").val(practiceArea);
document.forms["practiceDetailForm"].action = "/GapAnalysis-1.0/practiceAreaSelect";
document.forms["practiceDetailForm"].submit();	
}
}).change();



$("#commit").click(function(){

document.forms["practiceDetailForm"].action = "/GapAnalysis-1.0/practiceDetailsSubmit";
document.forms["practiceDetailForm"].submit();	
}).change();

});
</script>


</head>
<body>
<%
List<CategoryMst> categories=(List<CategoryMst>)request.getAttribute("categories") ;
List<CapabilityMst> capabilities=(List<CapabilityMst>)request.getAttribute("capabilities") ;
List<CapabilityMst> capabilitiesMapped=(List<CapabilityMst>)request.getAttribute("capabilitiesMapped") ;
List<PracticeWorkProductMst> practiceWorkProductMstMapped=(List<PracticeWorkProductMst>)request.getAttribute("practiceWorkProductMstMapped") ;
if(practiceWorkProductMstMapped==null){practiceWorkProductMstMapped=new ArrayList<PracticeWorkProductMst>();}
List<PracticeAreaMst> practiceArea=(List<PracticeAreaMst>)request.getAttribute("practiceArea") ;
List<PracticeAreaMst> practiceAreaMapped=(List<PracticeAreaMst>)request.getAttribute("practiceAreaMapped") ;
List<PracticeMst> practices=(List<PracticeMst>)request.getAttribute("practices") ;
List<PracticeMst> practicesMapped=(List<PracticeMst>)request.getAttribute("practicesMapped") ;
if(categories==null){categories=new ArrayList<CategoryMst>();}
if(capabilities==null){capabilities=new ArrayList<CapabilityMst>();}
if(capabilitiesMapped==null){capabilitiesMapped=new ArrayList<CapabilityMst>();}
if(practices==null){practices=new ArrayList<PracticeMst>();}
if(practicesMapped==null){practicesMapped=new ArrayList<PracticeMst>();}
if(practiceArea==null){practiceArea=new ArrayList<PracticeAreaMst>();}
if(practiceAreaMapped==null){practiceAreaMapped=new ArrayList<PracticeAreaMst>();}
for(CategoryMst category1:categories)
{
System.out.println(">>>>>>>>>>>>>>>"+category1);
}
%>
<form id="practiceDetailForm" class="form" action="#" method="post">
<input type="hidden" id="category" name="category" value="<%=accessObject.getCategory()%>">
<input type="hidden" id="capability" name="capability" value="<%=accessObject.getCapability()%>">
<input type="hidden" id="practiceArea" name="practiceArea" value="<%=accessObject.getPracticeArea()%>">
<input type="hidden" id="practice" name="practice" value="<%=accessObject.getPractice()%>">
	<!-- side bar sec start -->
	<div class="profile-main">
		<div class="sidebar-main">
			<div class="side-bar-menu">
				<nav id="sidebar">
					<i class="fa fa-times"></i>
					<div class="sidebar-header py-3">
						<h3>Practice Area</h3>
					</div>
					<ul class="list-unstyled components" style="padding-bottom:10%">
<%for (PracticeAreaMst practiceAreas:practiceArea)
{
%>
						<li>
							<a id="<%=practiceAreas.getPracticeAreaCode()%>" onclick="selectedPracticeArea(this.id)">
							<span>(<%=practiceAreas.getPracticeAreaCode()%>)</span>
							<%=practiceAreas.getPracticeAreaName()%></a>
						</li>
<%
}
%>

					</ul>
				</nav>
			</div>
		</div>
	</div>

	<!-- side bar sec end -->

	<!-- Doing main sec start -->
	<div class="doing-main ensuring-main rdm-main rdm-level-2-main">
		<!-- html start -->
		<div class="rmdlevelpage leveltwo" style="padding-left: 27.69%;">
			<div class="main-category-home">
				<p> </p>
				<select id="categoryOption" style="width:150px">
					<option value="" >Category</option>
<%for(CategoryMst category:categories)
{
%>
				<option  value="<%=category.getCategoryCode()%>"><%=category.getCategoryName()%></option>
				
<%
}
%>
				</select>
				<select id="capabilityOption" style="width:150px">
				<option value="">Capability</option>
				<%for(CapabilityMst capability:capabilities)
{
%>
				<option  value="<%=capability.getCapabilityCode()%>"><%=capability.getCapabilityName()%></option>
				
<%
}
%>
					
				</select>
				<select id="practiceAreaOption" style="width:150px">
					<option value="" >PracticeArea</option>
									<%for(PracticeAreaMst pa:practiceArea)
{
%>
				<option  value="<%=pa.getPracticeAreaCode()%>"><%=pa.getPracticeAreaName()%></option>
				
<%
}
%>
				</select>
				<select id="practiceOption" >
					<option value="">Practice</option>
														<%for(PracticeMst pc:practicesMapped)
{
%>
				<option  value="<%=pc.getPracticeCode()%>"><%=pc.getPracticeCode()%></option>
				
<%
}
%>
				</select>
			</div>
			<form class="rdmpageform" >
			<div class="rmdbox" style="padding-left: 5%;">
				<div class="rdmtitle">
							<%for(PracticeMst practice:practices)
{
				if(practice.getPracticeCode().equalsIgnoreCase(accessObject.getPractice()))
				{
%>
				<p><%=practice.getPracticeName()%></p>
				
<%
				}
}
%>
					
					<button>Back</button>
				</div>
				<%for(PracticeMst practiceMapped:practicesMapped)
{
				if(practiceMapped.getPracticeCode().equalsIgnoreCase(accessObject.getPractice()))
				{
%>
				<div class="singlebox">
					<p class="titlebox"><%=practiceMapped.getPracticeCode()%></p>
					<p><%=practiceMapped.getPracticeDescription()%></p>
				</div>
				
<%
				}
				else
				{
					%>
					<div class="singlebox disabled">
					<p class="titlebox"><%=practiceMapped.getPracticeCode()%></p>
					<p><%=practiceMapped.getPracticeDescription()%></p>
				</div>
					<%
				}
}
%>
				
			</div>


			<div class="valuerecord" style="padding-left: 5%;">
				<p class="titlerecord">Value</p>
											<%for(PracticeMst practice:practices)
{
				if(practice.getPracticeCode().equalsIgnoreCase(accessObject.getPractice()))
				{
%>
				<p><%=practice.getPracticeValue()%></p>
				
<%
				}
}
%>
				
			</div>
			<div class="exampleactivities" style="padding-left: 5%;">
				<p class="titlerecord">Example Activities</p>
				<%for(PracticeMst practice:practices)
{
				if(practice.getPracticeCode().equalsIgnoreCase(accessObject.getPractice()))
				{
					System.out.println("AB>>>>>>>"+practice.getPracticeExamples());
					String example=practice.getPracticeExamples();
					String [] examplesSplit=example.split(".");
					for(String aq:examplesSplit)
					{
						
%>
				<p><%=aq%></p>
				
<%
					}
				}
}
%>
				
			</div>


			<div class="exampleworkproducts" style="padding-left: 5%;">
				<div class="maindivexamplework">
					<div class="titleallpart">
						<p>Current Work Products</p>
						<p>Coverage %</p>
						<p>Referred as</p>
					</div>
					<div class="exampleproductsrecord">
					<%
					for(PracticeWorkProductMst practiceWorkProduct:practiceWorkProductMstMapped)
					{
					%>
					<div class="singlerecord">
							<div class="first-part">
								<p><%=practiceWorkProduct.getWorkProductName()%></p>
								
								<div class="radiooptiongroup">

									<div class="form-check form-check-inline">
									<input class="form-check-input" id="<%=practiceWorkProduct.getId()%>" type="radio" name="q1<%=practiceWorkProduct.getId()%>" value="Y">
									<%
									if(practiceWorkProduct.getQ1().equalsIgnoreCase("Y"))
									{	
									%>
										<input class="form-check-input" checked="true" id="<%=practiceWorkProduct.getId()%>" type="radio" name="q1<%=practiceWorkProduct.getId()%>" value="Y">
									<%
									}
									%>									
										  <label class="form-check-label" for="inlineRadio1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" id="<%=practiceWorkProduct.getId()%>" name="q1<%=practiceWorkProduct.getId()%>" value="N">
										<%
									if(practiceWorkProduct.getQ1().equalsIgnoreCase("N"))
									{	
									%>
										  <input class="form-check-input" type="radio" checked="true" id="<%=practiceWorkProduct.getId()%>" name="q1<%=practiceWorkProduct.getId()%>" value="N">
									<%
									}
									%>
										
										  <label class="form-check-label" for="inlineRadio2">No</label>
									</div>
								</div>
							</div>
							<div class="second-part">
								<select id="<%=practiceWorkProduct.getId()%>" name="q2<%=practiceWorkProduct.getId()%>">
								<option value="Select" >Select</option>
								<%
									if(!practiceWorkProduct.getQ2().equalsIgnoreCase(""))
									{	
									%>
								<option selected="true" value="<%=practiceWorkProduct.getQ2()%>" ><%=practiceWorkProduct.getQ2()%></option>
									<%
									}
									%>
									<option value="5" >5</option>
									<option value="15" >15</option>
									<option value="25" >25</option>
									<option value="75" >75</option>
								</select>
							</div>
							<div class="third-part">
							
							<%
									if(!practiceWorkProduct.getQ3().equalsIgnoreCase(""))
									{	
									%>
								<input type="text" id="<%=practiceWorkProduct.getId()%>" name="q3<%=practiceWorkProduct.getId()%>" value="<%=practiceWorkProduct.getQ3()%>" >
									<%
									}
									else
									{
									%>
								<input type="text" id="<%=practiceWorkProduct.getId()%>" name="q3<%=practiceWorkProduct.getId()%>" value="" >
								<%
									}
									%>
								
							</div>
						</div>
					<%
					}
					%>
						


						
					</div>
				</div>
			</div>


			<div class="currentpracticediv" style="padding-left: 5%;">
				<p class="titlecurrentparactice">Example Practice/Work Product</p>
				<div class="textareavalue">
					
					<p>aaa</p>
					<p>bb</p>
					
				</div>
				<div class="complianceselect">
					<div class="titlecompliance">
						<p>Compliance</p>
					</div>
					<div class="selectcompliance">
						<select id="practiceCompliance" name="practiceCompliance">
						<%
			for(PracticeMst practicea:practicesMapped)
			{
				
				if(practicea.getPracticeCode().equalsIgnoreCase(accessObject.getPractice()))
				{
				if(!practicea.getPracticeComplianceLevel().equalsIgnoreCase(""))
				{
				
				%>
				<option selected value="<%=practicea.getPracticeComplianceLevel()%>"><%=practicea.getPracticeComplianceLevel()%></option>
				<option value="0">Li</option>
				<option value="0.25">Di</option>
				<option value="0.75">Gi</option>
				<option value="1">Mi</option>
				<%
				}
				else
				{
				%>
				<option value="0">Li</option>
				<option value="0.25">Di</option>
				<option value="0.75">Gi</option>
				<option value="1">Mi</option>
				<%				
				}
				}
			}
			
				%>
							
						</select>
					</div>
				</div>
			</div>


			<div class="gapnotesdiv" style="padding-left: 5%;">
				<p>GAP Notes</p>
			<%
			for(PracticeMst practicea:practicesMapped)
			{
				if(practicea.getPracticeCode().equalsIgnoreCase(accessObject.getPractice()))
				{
					System.out.println(">>>>"+practicea.getCurrentGapNptes());
			%>
				<textarea id="currentGapNptes" name="currentGapNptes" value="<%=practicea.getCurrentGapNptes()%>"><%=practicea.getCurrentGapNptes()%></textarea>
				<%
				}
			}
				%>
			</div>

			<div class="formsubmitbtn" >
				<button id="commit">Save</button>
			</div>
		</form>
		<!-- end new html -->	
		</div>
		
	</div>
</div>
</form>
<%@include file="footer.jsp"%>
</body>
</html>