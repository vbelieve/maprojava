<!DOCTYPE html>
<html>
<head>
<meta name="viewport"
	content="width=device-width,initial-scale=1,maximum-scale=1" />
<title>WebRTC Try Out 1</title>
</head>
<body>
	<!-- Title & header of demo aplication. -->
	<div>

		<h3 style="position: relative; left: 10px;">
			Sample Conf <br />
		</h3>
	</div>
	<form>
		<input id="textMessage" type="text">
		<input 	onclick="sendMessage();" value="Send Message" type="button">
	</form>
	<br>
	<textarea id="messageTextArea" rows="10" cols="50"></textarea>
<!-- 	<script type="text/javascript">
var webSocket=new WebSocket("ws://"+window.location.host+"/GapAnalysis-2.0/endPoint");
var messageTextArea=document.getElementById("messageTextArea");
webSocket.onopen=function(message){processOpen(message);};
webSocket.onclose=function(message){processClose(message);};
webSocket.onerror=function(message){processError(message);};
webSocket.onmessage=function(message){processMessage(message);};
function processOpen(message){
messageTextArea.value +="Server Connect .."+"\n";
}
function processMessage(message){
webSocket.send("Client Disconnected");
messageTextArea.value +="Received from Server .."+message.data+"\n";
}
function sendMessage(){
	alert(messageTextArea.value);
if(textMessage.value!="close"){
webSocket.send(textMessage.value);
messageTextArea.value +="Send to server .."+textMessage.value+"\n";
textMessage.value="";
}
else
webSocket.close();

}
function processClose(message){
webSocket.send("Client Disconnected");
messageTextArea.value +="Server DisConnected .."+"\n";
}

function processError(message){
webSocket.send("Client Disconnected");
messageTextArea.value +="Error Message .."+"\n";
}
 </script>
 -->
 <div>
		<h3 style="margin: 5px">Other Person</h3>
		<video style="width: 60%; height: 60%;" id="remoteVideo"
			poster="https://img.icons8.com/fluent/48/000000/person-male.png" autoplay></video>
	</div>
 
	<!-- Your camera video will show up here. -->
	<div>
		<h3 style="margin: 5px">You</h3>
		<video style="width: 60%; height: 60%;" id="localVideo"
			poster="https://img.icons8.com/fluent/48/000000/person-male.png" autoplay muted></video>
	</div>
 
	<!-- Button to leave video conference. -->
	<div class="box">
		<button id="leaveButton" style="background-color: #008CBA; color: white; ">Leave Video Conference</button>
	</div>
 
	<script type="text/javascript" src="js/WebScreenShare.js?reload=true"></script>
 
</body>
</html>