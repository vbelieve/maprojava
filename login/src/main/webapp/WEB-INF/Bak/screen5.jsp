<!DOCTYPE html>
<html>
<head>
	<title>Requirement Development & Management Level 2.1</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">


	<!-- add new css -->
	<link rel="stylesheet" type="text/css" href="css/new-style.css">
	<!-- end new css -->



	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

	<!-- Fontawesome cdn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

	<!-- header sec start -->
	<header class="p-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href="#" class="logo" alt=" ">
						<h2>Company Name</h2>
					</a>
				</div>
			</div>
		</div>
	</header>
	<!-- header sec end -->

	<!-- side bar sec start -->
	<div class="profile-main">
		<div class="sidebar-main">
			<div class="side-bar-menu">
				<nav id="sidebar">
					<i class="fa fa-times"></i>
					<div class="sidebar-header py-3">
						<h3>Practice Area</h3>
					</div>
					<ul class="list-unstyled components">
						<li>
							<a href="#"><span>(RDM)</span> Requirement Development & Management </a>
						</li>
						<li>
							<a href="#"><span>(PQA)</span>Process Quality Assurance </a>
						</li>
						<li>
							<a href="#"><span>(VV)</span>Verification & Validation </a>
						</li>
						<li>
							<a href="#"><span>(PR)</span>Peer Review </a>
						</li>
						<li>
							<a href="#"><span>(TS)</span>Technical Solution </a>
						</li>
						<li>
							<a href="#"><span>(PI)</span>Product Integration </a>
						</li>
						<li>
							<a href="#"><span>(SDM)</span> Service Delivery Management  </a>
						</li>
						<li>
							<a href="#"><span>(SSM)</span> Strategic Service Management</a>
						</li>
						<li>
							<a href="#"><span>(SSS)</span> Supplier Source Selection </a>
						</li>
						<li>
							<a href="#"><span>(SAM)</span> Supplier Agreement Management </a>
						</li>
						<li>
							<a href="#"><span>(EST)</span> Estimating </a>
						</li>
						<li>
							<a href="#"><span>(PLAN)</span> Planning</a>
						</li>
						<li>
							<a href="#"><span>(MC)</span> Monitoring & Control</a>
						</li>
						<li>
							<a href="#"><span>(RSK)</span> Risk & Opportunity Management  </a>
						</li>
						<li>
							<a href="#"><span>(IRP)</span> Incident Resolution & Planning </a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>

	<!-- side bar sec end -->

	<!-- Doing main sec start -->
	<div class="doing-main ensuring-main rdm-main rdm-level-2-main">













		<!-- html start -->
		<div class="rmdlevelpage">
			<div class="main-category-home">
				<p>Home</p>
				<select>
					<option>Doing</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
				</select>
			</div>

			<div class="breadcrumb-div">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item"><a href="#">Category</a></li>
						<li class="breadcrumb-item"><a href="#">Doing</a></li>
					</ol>
				</nav>
				<!-- <button>Back</button> -->
			</div>

			<div class="levelrdm">
				<div class="single-mainlevel">
					<p class="titlelevel">RDM</p>
					<p>Requirement Development & Management</p>
				</div>
				<div class="single-mainlevel">
					<p class="titlelevel">RDM</p>
					<p>Requirement Development & Management</p>
				</div>
				<div class="single-mainlevel">
					<p class="titlelevel">RDM</p>
					<p>Requirement Development & Management</p>
				</div>
				<div class="single-mainlevel">
					<p class="titlelevel">RDM</p>
					<p>Requirement Development & Management</p>
				</div>
			</div>
		</div>
		<!-- end html start -->








	</div>
</div>
<!-- Doing main sec end -->

<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script> 
<script src="js/bootstrap.js"></script> 
<script src="js/custom.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$('#sidebarCollapse').on('click', function () {
			$('#sidebar').toggleClass('active');
		});
	});
</script>

</body>
</html>