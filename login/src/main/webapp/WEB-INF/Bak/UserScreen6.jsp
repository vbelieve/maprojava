<!DOCTYPE html>
<html>
<head>
	<title>Requirement Development & Management Level 2.1</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">






	<!-- add new css -->
	<link rel="stylesheet" type="text/css" href="css/new-style.css">
	<!-- end new css -->





	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

	<!-- Fontawesome cdn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

	<!-- header sec start -->
	<header class="p-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href="#" class="logo" alt=" ">
						<h2>Company Name</h2>
					</a>
				</div>
			</div>
		</div>
	</header>
	<!-- header sec end -->

	<!-- side bar sec start -->
	<div class="profile-main">
		<div class="sidebar-main">
			<div class="side-bar-menu">
				<nav id="sidebar">
					<i class="fa fa-times"></i>
					<div class="sidebar-header py-3">
						<h3>Practice Area</h3>
					</div>
					<ul class="list-unstyled components">
						<li>
							<a href="#"><span>(RDM)</span> Requirement Development & Management </a>
						</li>
						<li>
							<a href="#"><span>(PQA)</span>Process Quality Assurance </a>
						</li>
						<li>
							<a href="#"><span>(VV)</span>Verification & Validation </a>
						</li>
						<li>
							<a href="#"><span>(PR)</span>Peer Review </a>
						</li>
						<li>
							<a href="#"><span>(TS)</span>Technical Solution </a>
						</li>
						<li>
							<a href="#"><span>(PI)</span>Product Integration </a>
						</li>
						<li>
							<a href="#"><span>(SDM)</span> Service Delivery Management  </a>
						</li>
						<li>
							<a href="#"><span>(SSM)</span> Strategic Service Management</a>
						</li>
						<li>
							<a href="#"><span>(SSS)</span> Supplier Source Selection </a>
						</li>
						<li>
							<a href="#"><span>(SAM)</span> Supplier Agreement Management </a>
						</li>
						<li>
							<a href="#"><span>(EST)</span> Estimating </a>
						</li>
						<li>
							<a href="#"><span>(PLAN)</span> Planning</a>
						</li>
						<li>
							<a href="#"><span>(MC)</span> Monitoring & Control</a>
						</li>
						<li>
							<a href="#"><span>(RSK)</span> Risk & Opportunity Management  </a>
						</li>
						<li>
							<a href="#"><span>(IRP)</span> Incident Resolution & Planning </a>
						</li>
					</ul>
				</nav>
			</div>
		</div>

	</div>

	<!-- side bar sec end -->

	<!-- Doing main sec start -->
	<div class="doing-main ensuring-main rdm-main rdm-level-2-main">



















		<!-- add new html -->
		<div class="main-category-select">
			<div class="selectmaindiv">
				<select>
					<option>Doing</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
				</select>
			</div>
			<div class="selectmaindiv">
				<select>
					<option>Select Capability</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
				</select>
			</div>
			<div class="selectmaindiv">
				<select>
					<option>Select Practice area</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
				</select>
			</div>
			<div class="selectmaindiv">
				<select>
					<option>Select Level</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
				</select>
			</div>
			<div class="selectmaindiv">
				<select>
					<option>Select Practice</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
				</select>
			</div>
		</div>

		<form class="rdmpageform">
			<div class="rmdbox">
				<div class="rdmtitle">
					<p>Requirements Development and Management(RDM)</p>
					<button>Back</button>
				</div>
				<div class="singlebox">
					<p class="titlebox">rdm 2.1</p>
					<p>Elicit stakeholder needs, expectation, constraints, and interfaces or connections</p>
				</div>
				<div class="singlebox disabled">
					<p class="titlebox">rdm 2.2</p>
					<p>Elicit stakeholder needs, expectation, constraints, and interfaces or connections</p>
				</div>
				<div class="singlebox disabled">
					<p class="titlebox">rdm 2.3</p>
					<p>Elicit stakeholder needs, expectation, constraints, and interfaces or connections</p>
				</div>
				<div class="singlebox disabled">
					<p class="titlebox">rdm 2.4</p>
					<p>Elicit stakeholder needs, expectation, constraints, and interfaces or connections</p>
				</div>
				<div class="singlebox disabled">
					<p class="titlebox">rdm 2.5</p>
					<p>Elicit stakeholder needs, expectation, constraints, and interfaces or connections</p>
				</div>
				<div class="singlebox disabled">
					<p class="titlebox">rdm 2.6</p>
					<p>Elicit stakeholder needs, expectation, constraints, and interfaces or connections</p>
				</div>
			</div>


			<div class="valuerecord">
				<p class="titlerecord">Value</p>
				<p>Recorded requirements are the basic for successfully addressing customer needs and expectations</p>
			</div>
			<div class="exampleactivities">
				<p class="titlerecord">Example Activities</p>
				<p>Recorded the requirements</p>
			</div>


			<div class="exampleworkproducts">
				<div class="maindivexamplework">
					<div class="titleallpart">
						<p>Example Work Products</p>
						<p>Coverage %</p>
						<p>Referred as</p>
					</div>
					<div class="exampleproductsrecord">
						<div class="singlerecord">
							<div class="first-part">
								<p>List of requirements</p>
								<div class="radiooptiongroup">
									<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
										  <label class="form-check-label" for="inlineRadio1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
										  <label class="form-check-label" for="inlineRadio2">No</label>
									</div>
								</div>
							</div>
							<div class="second-part">
								<select>
									<option>75</option>
									<option>5</option>
									<option>15</option>
									<option>25</option>
								</select>
							</div>
							<div class="third-part">
								<input type="text" name="">
							</div>
						</div>
						<div class="singlerecord">
							<div class="first-part">
								<p>List of requirements</p>
								<div class="radiooptiongroup">
									<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
										  <label class="form-check-label" for="inlineRadio1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
										  <label class="form-check-label" for="inlineRadio2">No</label>
									</div>
								</div>
							</div>
							<div class="second-part">
								<select>
									<option>75</option>
									<option>5</option>
									<option>15</option>
									<option>25</option>
								</select>
							</div>
							<div class="third-part">
								<input type="text" name="">
							</div>
						</div>
						<div class="singlerecord">
							<div class="first-part">
								<p>List of requirements</p>
								<div class="radiooptiongroup">
									<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
										  <label class="form-check-label" for="inlineRadio1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
										  <label class="form-check-label" for="inlineRadio2">No</label>
									</div>
								</div>
							</div>
							<div class="second-part">
								<select>
									<option>75</option>
									<option>5</option>
									<option>15</option>
									<option>25</option>
								</select>
							</div>
							<div class="third-part">
								<input type="text" name="">
							</div>
						</div>
						<div class="singlerecord">
							<div class="first-part">
								<p>List of requirements</p>
								<div class="radiooptiongroup">
									<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
										  <label class="form-check-label" for="inlineRadio1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
										  <label class="form-check-label" for="inlineRadio2">No</label>
									</div>
								</div>
							</div>
							<div class="second-part">
								<select>
									<option>75</option>
									<option>5</option>
									<option>15</option>
									<option>25</option>
								</select>
							</div>
							<div class="third-part">
								<input type="text" name="">
							</div>
						</div>
						<div class="singlerecord">
							<div class="first-part">
								<p>List of requirements</p>
								<div class="radiooptiongroup">
									<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
										  <label class="form-check-label" for="inlineRadio1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
										  <label class="form-check-label" for="inlineRadio2">No</label>
									</div>
								</div>
							</div>
							<div class="second-part">
								<select>
									<option>75</option>
									<option>5</option>
									<option>15</option>
									<option>25</option>
								</select>
							</div>
							<div class="third-part">
								<input type="text" name="">
							</div>
						</div>
						<div class="singlerecord">
							<div class="first-part">
								<p>List of requirements</p>
								<div class="radiooptiongroup">
									<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
										  <label class="form-check-label" for="inlineRadio1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
										  <label class="form-check-label" for="inlineRadio2">No</label>
									</div>
								</div>
							</div>
							<div class="second-part">
								<select>
									<option>75</option>
									<option>5</option>
									<option>15</option>
									<option>25</option>
								</select>
							</div>
							<div class="third-part">
								<input type="text" name="">
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="currentpracticediv">
				<p class="titlecurrentparactice">Current Practice/Work Product</p>
				<div class="textareavalue">
					<textarea placeholder="Mention the current practice adopted by the organisation"></textarea>
				</div>
				<div class="complianceselect">
					<div class="titlecompliance">
						<p>Compliance</p>
					</div>
					<div class="selectcompliance">
						<select>
							<option>Li</option>
							<option>Di</option>
							<option>Gi</option>
							<option>Mi</option>
						</select>
					</div>
				</div>
			</div>


			<div class="gapnotesdiv">
				<p>GAP Notes</p>
				<textarea></textarea>
			</div>

			<div class="formsubmitbtn">
				<button>Save</button>
			</div>
		</form>
		<!-- end new html -->











		
	</div>
</div>
<!-- Doing main sec end -->

<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script> 
<script src="js/bootstrap.js"></script> 
<script src="js/custom.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$('#sidebarCollapse').on('click', function () {
			$('#sidebar').toggleClass('active');
		});
	});
</script>

</body>
</html>