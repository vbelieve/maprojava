<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.TenantMst"%>
<%@page import="com.login.domain.LookupMst"%>
<%@page import="java.util.*"%>
<html>
		

<head>
	<title>Organization</title>
<%@include file="header.jsp"%>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js">
	<script src="js/jquery-3.3.1.slim.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
var erm=$("#msg").val();
if(erm!="null" && erm!="")
{
alert(erm);
erm="";
}
$("#commit").click(function(){
document.forms["tenantRegisration"].action = "/GapAnalysis-1.0/tenant/save";
document.forms["tenantRegisration"].submit();	

});
  
});
</script>

<script type="text/javascript">
function updateTenantDetails(tid,orgCode,cName,cAddress,cTel,cProcess){
alert(tid+orgCode+cName+cAddress+cTel+cProcess);
$("#tenantId").val(tid);
$("#orgCode").val(orgCode);
$("#organizationName").val(cName);
$("#address").val(cAddress);
$("#telephone1").val(cTel);
$("select#process").val(cProcess);

} 
</script>	
</head>
<body>
<%
String msg=(String) request.getAttribute("msg");
if(null==msg)
{
	msg="";
}
List<LookupMst> processLookup=(List<LookupMst>)request.getAttribute("processLookup") ;
List<TenantMst>tenantList= (List<TenantMst>) request.getAttribute("tenantList");
System.out.println(">>>>>>>>>>>>"+tenantList);
if(null==tenantList)
{
	tenantList=new ArrayList<TenantMst>();
}
Integer srNo=0;
%>
<input type="hidden" id="msg" value="<%=msg%>">
	<div id="login">
		<div class="container">
			<div class="gap">
				<h3>gap analsis</h3>
			</div>
			<div id="login-row" class="row justify-content-end">
				<div id="login-column" class="col-md-5">
					<div id="login-box">
					<form id="tenantRegisration" class="form" action="" method="post" style="position: absolute; right: 50px; top:20px; padding-bottom: 50px;">
						<form class="w-100 bg-white p-5 sign-up-form">
							<div class="sign-up-title">
								<h3>Organization Details</h3>
															</div>
							<div class="form-group">
								<label for="tenantId">Tenant Id</label>
								<input type="text" class="form-control" id="tenantId" name="tenantId" placeholder="TenatId" value="" readonly>
							</div>
							
							<div class="form-group">
								<label for="orgCode">Organization Code</label>
								<input type="text" class="form-control" id="orgCode" name="orgCode" placeholder="Organization Code" value="" readonly>
							</div>
							
							<div class="form-group">
								<label for="organizationName">Company's Name</label>
								<input type="text" class="form-control" id="organizationName" name="organizationName" placeholder="Name">
							</div>
							<div class="form-group">
								<label for="address">Address</label>
								<input type="text" class="form-control" id="address" name="address" placeholder="Address">
							</div>
							<div class="form-group">
								<label for="telephone1">Telephone Number</label>
								<input type="text" class="form-control" id="telephone1" name="telephone1" placeholder="Telephone Number">
								
							</div>
							<label >Process</label>
							<div class="form-group">
														
								<select id="process" name="process" multiple>
								<%
								for(LookupMst entry:processLookup)
								{
								%>
								<option value="<%=entry.getLookupValue()%>"><%=entry.getDisplayValue()%></option>
								<%
								}
								%>
								</select>
							</div>
							<div class="form-group float-right">
							<button type="button" id="commit"  name="commit" class="btn btn-primary">Submit</button>
							<% if(accessObject.getUserType()=="SUPERADMIN")
							{
							%>
							<button type="button" id="back"  onclick="window.location='/GapAnalysis-1.0/admin';"  name="back" class="btn btn-primary">Back</button>
							<%
							}else{
							%>
							<button type="button" id="back" onclick="window.location='/GapAnalysis-1.0/superAdmin';"  name="back" class="btn btn-primary">Back</button>	
							<%
							}
							%>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</div>
<div>
<table id="tableTenantMst" class="table  table-bordered table-hover " style="margin-bottom:40px" >
  <thead>
    <tr>
      <th>#</th>
      <th>Tenant Id</th>
      <th>Organization Code</th>
	  <th>Name</th>
	  <th>Telephone</th>
	  <th>Address</th>
	  <th>Process</th>
      
    </tr>
  </thead>
<tbody id="tenantTable" name="tenantTable" style="margin-bottom:30px">
<%
for(TenantMst entry:tenantList)
{
	srNo=srNo+1;
%>
<tr onclick="updateTenantDetails('<%=entry.getTenantId()%>',
'<%=entry.getOrganizationCode()%>',
'<%=entry.getOrganizationName()%>',
'<%=entry.getTelephone1()%>',
'<%=entry.getAddress()%>',
'<%=entry.getProcess()%>')";>
<td><%=srNo%></td>
<td><%=entry.getTenantId()%></td>
<td><%=entry.getOrganizationCode()%></td>
<td><%=entry.getOrganizationName()%></td>
<td><%=entry.getTelephone1()%></td>
<td><%=entry.getAddress()%></td>
<td><%=entry.getProcess()%></td>
</tr>

<%
}
%>
</tbody>
</table>

</div>

	
	<script src="js/popper.min.js"></script> 
	<script src="js/bootstrap.js"></script> 
	<script src="js/custom.js"></script>


</body>
<%@include file="footer.jsp"%>
</html>


