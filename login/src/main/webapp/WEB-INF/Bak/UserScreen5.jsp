<!DOCTYPE html>
<%@page import="com.login.domain.basics.AccessObject"%>
<%@page import="com.login.domain.CategoryMst"%>
<%@page import="com.login.domain.CapabilityMst"%>
<%@page import="com.login.domain.PracticeAreaMst"%>
<%@page import="com.login.domain.PracticeMst"%>
<%@page import="java.util.*"%>

<html>
<head>
<%@include file="header_user.jsp"%>
	<title>Category</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">


	<!-- add new css -->
	<link rel="stylesheet" type="text/css" href="css/new-style.css">
	<!-- end new css -->



	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

	<!-- Fontawesome cdn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Doing main sec end -->

<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script> 
<script src="js/bootstrap.js"></script> 
<script src="js/custom.js"></script>

<script type="text/javascript">
function selectedCapability(pid){
$("#capability").val(pid);
document.forms["capabilityForm"].action = "/GapAnalysis-1.0/capabilitySelect";
document.forms["capabilityForm"].submit();	
}
</script>

<script type="text/javascript">
function selectedPractice(pid){
$("#practice").val(pid);
document.forms["capabilityForm"].action = "/GapAnalysis-1.0/practiceSelect";
document.forms["capabilityForm"].submit();	
}
</script>


<script type="text/javascript">
function selectedPracticeArea(pid){
$("#practiceArea").val(pid);
document.forms["capabilityForm"].action = "/GapAnalysis-1.0/practiceAreaSelect";
document.forms["capabilityForm"].submit();	
}
</script>

<script type="text/javascript">
$(document).ready(function(){

$("#capabilityOption").change(function(){
var capability=$("#capabilityOption").val();
if(capability!="")
{
$("#capability").val(capability);
document.forms["capabilityForm"].action = "/GapAnalysis-1.0/capabilitySelect";
document.forms["capabilityForm"].submit();	
}
}).change();

$("#categoryOption").change(function(){
var category=$("#categoryOption").val();
if(category!="")
{
$("#category").val(category);
document.forms["capabilityForm"].action = "/GapAnalysis-1.0/categorySelect";
document.forms["capabilityForm"].submit();	
}
}).change();

$("#practiceAreaOption").change(function(){
var practiceArea=$("#practiceAreaOption").val();
if(practiceArea!="")
{
$("#practiceArea").val(practiceArea);
document.forms["capabilityForm"].action = "/GapAnalysis-1.0/practiceAreaSelect";
document.forms["capabilityForm"].submit();	
}
}).change();
});
</script>


</head>
<body>
<%
List<CategoryMst> categories=(List<CategoryMst>)request.getAttribute("categories") ;
List<CapabilityMst> capabilities=(List<CapabilityMst>)request.getAttribute("capabilities") ;
List<CapabilityMst> capabilitiesMapped=(List<CapabilityMst>)request.getAttribute("capabilitiesMapped") ;

List<PracticeAreaMst> practiceArea=(List<PracticeAreaMst>)request.getAttribute("practiceArea") ;
List<PracticeAreaMst> practiceAreaMapped=(List<PracticeAreaMst>)request.getAttribute("practiceAreaMapped") ;
List<PracticeMst> practices=(List<PracticeMst>)request.getAttribute("practices") ;
List<PracticeMst> practicesMapped=(List<PracticeMst>)request.getAttribute("practicesMapped") ;
if(categories==null){categories=new ArrayList<CategoryMst>();}
if(capabilities==null){capabilities=new ArrayList<CapabilityMst>();}
if(capabilitiesMapped==null){capabilitiesMapped=new ArrayList<CapabilityMst>();}
if(practices==null){practices=new ArrayList<PracticeMst>();}
if(practicesMapped==null){practicesMapped=new ArrayList<PracticeMst>();}
if(practiceArea==null){practiceArea=new ArrayList<PracticeAreaMst>();}
if(practiceAreaMapped==null){practiceAreaMapped=new ArrayList<PracticeAreaMst>();}
for(CategoryMst category1:categories)
{
System.out.println(">>>>>>>>>>>>>>>"+category1);
}
%>
<form id="capabilityForm" class="form" action="#" method="post">
<input type="hidden" id="category" name="category" value="<%=accessObject.getCategory()%>">
<input type="hidden" id="capability" name="capability" value="<%=accessObject.getCapability()%>">
<input type="hidden" id="practiceArea" name="practiceArea" value="<%=accessObject.getPracticeArea()%>">
<input type="hidden" id="practice" name="practice" value="">
	<!-- side bar sec start -->
	<div class="profile-main">
		<div class="sidebar-main">
			<div class="side-bar-menu">
				<nav id="sidebar">
					<i class="fa fa-times"></i>
					<div class="sidebar-header py-3">
						<h3>Practice Area</h3>
					</div>
					<ul class="list-unstyled components" style="padding-bottom:10%">
<%for (PracticeAreaMst practiceAreas:practiceArea)
{
%>
						<li>
							<a id="<%=practiceAreas.getPracticeAreaCode()%>" onclick="selectedPracticeArea(this.id)">
							<span>(<%=practiceAreas.getPracticeAreaCode()%>)</span>
							<%=practiceAreas.getPracticeAreaName()%></a>
						</li>
<%
}
%>

					</ul>
				</nav>
			</div>
		</div>
	</div>

	<!-- side bar sec end -->

	<!-- Doing main sec start -->
	<div class="doing-main ensuring-main rdm-main rdm-level-2-main">
		<!-- html start -->
		<div class="rmdlevelpage leveltwo" style="padding-left: 27.69%;">
			<div class="main-category-home">
				<p> </p>
				<select id="categoryOption" style="width:150px">
					<option value="" >Category</option>
<%for(CategoryMst category:categories)
{
%>
				<option  value="<%=category.getCategoryCode()%>"><%=category.getCategoryName()%></option>
				
<%
}
%>
				</select>
				<select id="capabilityOption" style="width:150px">
				<option value="">Capability</option>
				<%for(CapabilityMst capability:capabilities)
{
%>
				<option  value="<%=capability.getCapabilityCode()%>"><%=capability.getCapabilityName()%></option>
				
<%
}
%>
					
				</select>
				<select id="practiceAreaOption" style="width:150px">
					<option value="" >PracticeArea</option>
									<%for(PracticeAreaMst pa:practiceArea)
{
%>
				<option  value="<%=pa.getPracticeAreaCode()%>"><%=pa.getPracticeAreaName()%></option>
				
<%
}
%>
				</select>
				<select >
					<option value="">Practice</option>
														<%for(PracticeMst pc:practices)
{
%>
				<option  value="<%=pc.getPracticeCode()%>"><%=pc.getPracticeCode()%></option>
				
<%
}
%>
				</select>
				
			</div>
			<div class="breadcrumb-div">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/GapAnalysis-1.0/user">Home</a></li>
						<li class="breadcrumb-item"><a href="/GapAnalysis-1.0/userScreen1">Category</a></li>
						<li class="breadcrumb-item"><a href="/GapAnalysis-1.0/userScreen2">Capability</a></li>
						<li class="breadcrumb-item"><a href="/GapAnalysis-1.0/userScreen3">PracticeArea</a></li>
					</ol>
				</nav>
				
			</div>

			<div class="levelrdm">
			<%for(PracticeAreaMst practiceAreas:practiceArea)
{
				if(practiceAreas.getPracticeAreaCode().equalsIgnoreCase(accessObject.getPracticeArea()))
				{
%>
				<div class="single-mainlevel">
					<p class="titlelevel"><%=practiceAreas.getPracticeAreaCode()%></p>
					<p><%=practiceAreas.getPracticeAreaName()%></p>
				</div>
				
<%
				}
}
%>
			
				

				<div class="sublevel">
								<%for(PracticeMst practice2:practicesMapped)
{
%>
				<div class="single-mainlevel" id="<%=practice2.getPracticeCode()%>" onclick="selectedPractice(this.id)">
					<p class="titlelevel"><%=practice2.getPracticeCode()%></p>
					<p><%=practice2.getPracticeName()%></p>
				</div>
				
<%
}
%>
					
					
				</div>
			</div>


		
		</div>
		<!-- end html -->


















	</div>
</div>
</form>
<%@include file="footer.jsp"%>
</body>
</html>