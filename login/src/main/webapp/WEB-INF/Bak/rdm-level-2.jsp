<!DOCTYPE html>
<html>
<head>
	<title>Requirement Development & Management Level 2</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/rdm-level-2.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

	<!-- Fontawesome cdn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

	<!-- header sec start -->
	<header class="p-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href="#" class="logo" alt=" ">
						<h2>Company Name</h2>
					</a>
				</div>
			</div>
		</div>
	</header>
	<!-- header sec end -->

	<!-- side bar sec start -->
	<div class="profile-main">
		<div class="sidebar-main">
			<div class="side-bar-menu">
				<nav id="sidebar">
					<i class="fa fa-times"></i>
					<div class="sidebar-header py-3">
						<h3>Practice Area</h3>
					</div>
					<ul class="list-unstyled components">
						<li>
							<a href="#"><span>(RDM)</span> Requirement Development & Management </a>
						</li>
						<li>
							<a href="#"><span>(PQA)</span>Process Quality Assurance </a>
						</li>
						<li>
							<a href="#"><span>(VV)</span>Verification & Validation </a>
						</li>
						<li>
							<a href="#"><span>(PR)</span>Peer Review </a>
						</li>
						<li>
							<a href="#"><span>(TS)</span>Technical Solution </a>
						</li>
						<li>
							<a href="#"><span>(PI)</span>Product Integration </a>
						</li>
						<li>
							<a href="#"><span>(SDM)</span> Service Delivery Management  </a>
						</li>
						<li>
							<a href="#"><span>(SSM)</span> Strategic Service Management</a>
						</li>
						<li>
							<a href="#"><span>(SSS)</span> Supplier Source Selection </a>
						</li>
						<li>
							<a href="#"><span>(SAM)</span> Supplier Agreement Management </a>
						</li>
						<li>
							<a href="#"><span>(EST)</span> Estimating </a>
						</li>
						<li>
							<a href="#"><span>(PLAN)</span> Planning</a>
						</li>
						<li>
							<a href="#"><span>(MC)</span> Monitoring & Control</a>
						</li>
						<li>
							<a href="#"><span>(RSK)</span> Risk & Opportunity Management  </a>
						</li>
						<li>
							<a href="#"><span>(IRP)</span> Incident Resolution & Planning </a>
						</li>
					</ul>
				</nav>
			</div>
		</div>

		<div id="content">
			<nav class="navbar navbar-expand-lg">
				<div class="container-fluid p-0">
					<button type="button" id="sidebarCollapse" class="btn btn-info">
						<i class="fa fa-align-left"></i>
						<span>Toggle Sidebar</span>
					</button>
					<div class="right-panel-header bg-2 p-4">
						<div class="panel-title">
							<h3>Home</h3>
							<div class="dropdown doing">
								<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Doing
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="#">Doing</a>
									<a class="dropdown-item" href="#">Managing</a>
									<a class="dropdown-item" href="#">Enabling</a>
									<a class="dropdown-item" href="#">Improving</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</nav>	
		</div>
	</div>

	<!-- side bar sec end -->

	<!-- Doing main sec start -->
	<div class="doing-main ensuring-main rdm-main">
		<div class="doing-title">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Category</a></li>
					<li class="breadcrumb-item"><a href="#">Doing</a></li>
					<li class="breadcrumb-item"><a href="#">Ensuring Quality</a></li>
					<li class="breadcrumb-item"><a href="#">Requirement Development & Management</a></li>
					<li class="breadcrumb-item active" aria-current="page">Level 2</li>
				</ol>
			</nav>
			<button type="button" class="btn btn-danger">Back</button>
		</div>

		<div class="doing-categry-main">
			<div class="bg-color"></div>

			<div class="container">
				<div class="row">
					<div class="doing-box-main rdm-wrapper col-lg-12">
						<div class="doing-box col-lg-3">
							<a href="#">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">RDM</h5>
										<p class="card-text">Requirement development & management</p>
									</div>
								</div>
							</a>
						</div>

						<div class="rdm-box bg-color"></div> 
						<div class="rdm-box">
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<div class="media col-lg-4">
											<div class="media-body">
												<a href="#">
													<div class="card">
														<div class="card-body">
															<h5 class="card-title">RDM 2.1</h5>
															<p class="card-text">Elicit stakeholder needs, ex-pectations, constraints, and interfaces or connections </p>
														</div>
													</div>
												</a>
											</div>
										</div>

										<div class="media col-lg-4">
											<a href="#">
												<div class="card">
													<div class="card-body">
														<h5 class="card-title">RDM 2.2</h5>
														<p class="card-text">Transform stakeholder needs, expectations, constraints, and interfaces or connections into priorotized customer requirements </p>
													</div>
												</div>
											</a>
										</div>

										<div class="media col-lg-4">
											<a href="#">
												<div class="card">
													<div class="card-body">
														<h5 class="card-title">RDM 2.3</h5>
														<p class="card-text">Develop and understanding with the requirements proi-ders on the meaning of the requirements </p>
													</div>
												</div>
											</a>
										</div>

										<div class="media col-lg-4">
											<div class="media-body">
												<a href="#">
													<div class="card">
														<div class="card-body">
															<h5 class="card-title">RDM 2.4</h5>
															<p class="card-text">Obtain commitments from the project participants that they can implement the re-quirements </p>
														</div>
													</div>
												</a>
											</div>
										</div>

										<div class="media col-lg-4">
											<a href="#">
												<div class="card">
													<div class="card-body">
														<h5 class="card-title">RDM 2.5</h5>
														<p class="card-text">Develop, record, and main-tain bidirectional traceability among requirements and ac-tivities or work products </p>
													</div>
												</div>
											</a>
										</div>

										<div class="media col-lg-4">
											<a href="#">
												<div class="card">
													<div class="card-body">
														<h5 class="card-title">RDM 2.6</h5>
														<p class="card-text">Ensure that plans and activi-ties or work products remain consistent with requirements </p>
													</div>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<button type="button" class="btn btn-danger next" style="float: right;">Next</button> 
			</div>



		</div>
	</div>
	<!-- Doing main sec end -->

	<script src="js/jquery-3.3.1.slim.min.js"></script>
	<script src="js/popper.min.js"></script> 
	<script src="js/bootstrap.js"></script> 
	<script src="js/custom.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			$('#sidebarCollapse').on('click', function () {
				$('#sidebar').toggleClass('active');
			});
		});
	</script>

</body>
</html>