<!DOCTYPE html>
<html>
		

<head>
	<title>Sign Up</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js">
	<script src="js/jquery-3.3.1.slim.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){

$.ajax({
		type : "GET",
		url:"tenant/all",
		success:function(jsonResponse)
		{
			
var tenantIdStr="";
tenantIdStr="<option class=\"dropdown-item\" value=\"\">-- Select --</option>";
  $.each(jsonResponse, function (key, entry) {
	tenantIdStr=tenantIdStr+"<option value="+entry.tenantId+">"+entry.organizationName+"</option>"; 
	getListOfUsers(entry.tenantId,entry.organizationName);
   
  })
$("#tenantId").html("");
$("#tenantId").append(tenantIdStr);		
			
		},
		error:function(e)
		{
			alert("FAILED");
		}
	});
	
	

//Start of Function
	$("#tenantId").change(function(){
	var tenantId=$("#tenantId").val();
	if(tenantId!="")
	{
		
	}
	
}).change();
//End of function	
$("#commit").click(function(){
	var tenantId=$("#tenantId").val();
	var userName=$("#userName").val();
	var password=$("#password").val();
	if(tenantId=="")
	{
		alert ("Tenant Id Is Mandatory");
		return false();
	}
	if(userName=="")
	{
		alert ("User Name Id Is Mandatory");
		return false();
	}
	if(password=="")
	{
		alert ("password Id Is Mandatory");
		return false();
	}
	if(password!="" && password.length<8)
	{
	alert ("password Length should be mininmum 8 characters");
		return false();
	}
	var formData={
	tenantId:$("#tenantId").val(),
	userName:$("#userName").val(),
	password:$("#password").val(),
	userType:"ADMIN",
	}
			
   
		
	$.ajax({
		type : "POST",
		contentType:"application/json",
		url:"userMst/save",
		data:JSON.stringify(formData),
		dataType:'json',
		success:function(jsonResponse)
		{
			alert("User with username :"+jsonResponse.userName+" successfully Created.");
		window.location.replace("/GapAnalysis-1.0/adminUserMst");	
		},
		error:function(e)
		{
			alert("FAILED");
		}
	});		
			
	
});
});
</script>

<script type="text/javascript">
function getListOfUsers(tenantId,orgName){
//Start of Funtion - to update Capability Code

var urlToSend="userMst/"+tenantId;
$.ajax({
		type : "GET",
		url:urlToSend,
		success:function(jsonResponse)
		{
		var userMstTableStr="";
		var srNo=1;
		var orgName1=orgName;
		$.each(jsonResponse, function (key, entry) {
			if(entry.userType=="ADMIN")
			{
	userMstTableStr=userMstTableStr+"<tr ><th scope=\"row\">"+srNo+"</th><td>"+entry.tenantId+"</td><td>"+orgName1+"</td><td>"+entry.userName+"</td><td>"+entry.isUserLocked+"</td>	<td>"+entry.isUserSuspended+"</td></tr>"; 
	srNo=srNo+1;
			}
  })
	$('#userMstTableStr').html("");
$('#userMstTable').append(userMstTableStr);
		},
		error:function(e)
		{
			alert("FAILED");
		}
	});

	
//End of Funtion

}
</script>	

</head>
<body>
	<div id="login">
		<div class="container">
			<div class="gap">
				<h3>gap analsis</h3>
			</div>
			<div id="login-row" class="row justify-content-end">
				<div id="login-column" class="col-md-5">
					<div id="login-box">
					<form id="userRegistration" class="form" action="" method="post">
						<form class="w-100 bg-white p-5 sign-up-form">
							<div class="sign-up-title">
								<h3>Create Admin</h3>
							</div>
							</div>
								<label for="tenantId">Organization Name</label>
							<div class="dropdown">
		
							<select id="tenantId" class="mdb-select colorful-select dropdown-primary md-form width=10px ">
      						<option class="dropdown-item" value="">-- Select --</option>
							</select>
							</div>
							<div class="form-group">
								<label for="userName">user Name</label>
								<input type="text" class="form-control" id="userName" placeholder="User Name">
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="text" class="form-control" id="password" placeholder="Enter Password">
								<small>Password should be atleast 8 character</small>
							</div>
							
							<div class="form-group float-right">	
							<button type="button" id="commit"  name="commit" class="btn btn-primary">Submit</button>
							<button type="button" id="back" onclick="window.location='/GapAnalysis-1.0/admin';"  name="back" class="btn btn-primary">Back</button>
								
							</div>
						</form>
					</div>
				</div>
			</div>
<table id="tableUserMst" class="table  table-bordered table-hover ">
  <thead>
    <tr>
      <th>#</th>
      <th>Tenant Id</th>
      <th>Organization Name</th>
	  <th>User Name</th>
	  <th>IsUserLocked</th>
	  <th>IsUserSuspended</th>
    </tr>
  </thead>
<tbody id="userMstTable" name="tenantTable">
</tbody>
</table>
	
	</div>
	
	</div>
	
	<script src="js/popper.min.js"></script> 
	<script src="js/bootstrap.js"></script> 
	<script src="js/custom.js"></script>


</body>
</html>
