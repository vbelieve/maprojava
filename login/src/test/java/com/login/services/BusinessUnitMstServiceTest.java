package com.login.services;

import com.login.domain.TenantMst;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;

import org.junit.Test;
import org.mockito.internal.util.reflection.Fields;
public class BusinessUnitMstServiceTest {

	@Test
	public void test() throws IOException, IllegalArgumentException, IllegalAccessException {
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MMM-yyyy");
		TenantMst tenant=new TenantMst();
		tenant.setTenantId("139");
		tenant.setOrganizationCode("AA");
	
		
		Class c=tenant.getClass();
		Field[] fields = c.getDeclaredFields();
		String dd="insert into "+c.getSimpleName()+" (Tenantid,";
		String fieldNames="";
		String fieldValues="'139',";
		String endChar=",";
		for(int i = 0; i < fields.length; i++) {
			Column column = fields[i].getAnnotation(Column.class);
			   if (null==column)
			   {
			    continue;
			   }
			if(i==fields.length-1)
			{
				endChar=") values (";	
			}
			
			fieldNames=fieldNames+fields[i].getName()+endChar;
			
	        
	      }
	
		for(int i = 0; i < fields.length; i++) {
			Column column = fields[i].getAnnotation(Column.class);
			   if (null==column)
			   {
			    continue;
			   }
			endChar=",";
			if(i==fields.length-1)
			{
				endChar=");";
			}
			if(fields[i].getType().equals(String.class)){
				fieldValues=fieldValues+"'"+fields[i].get(tenant)+"'"+endChar;
				
	            }
	         else if(fields[i].getType().equals(Boolean.class)){
	        	 fieldValues=fieldValues+fields[i].get(tenant)+endChar;
	            }
	         else if(fields[i].getType().equals(Date.class)){
	        	 fieldValues=fieldValues+sdf.format(fields[i].get(tenant).toString())+endChar;
	    
	            }
	         else if(fields[i].getType().equals(Long.class)){
	        	 fieldValues=fieldValues+fields[i].get(tenant)+endChar;
	        	 
	            }
	         else if(fields[i].getType().equals(Integer.class)){
	        	 fieldValues=fieldValues+fields[i].get(tenant)+endChar;
	        	 
	            }
			
			
	      }
		
		
		 System.out.println(dd+fieldNames+fieldValues);
	}
	
//		File destinationfile = new File("d:\\Interest_subvention_report_2020-07-04.xlsx");
//
//
//		FileInputStream fis=null;
//		try {
//			fis = new FileInputStream((destinationfile));
//			XSSFWorkbook workbook = new XSSFWorkbook (fis);
//			XSSFSheet sheet = workbook.getSheetAt(0);
//			XSSFRow row1 = sheet.createRow(1);
//			XSSFCell cell0 = row1.createCell(1);
//			cell0.setCellValue("LA_No");
//			fis.close();
//			
//			HttpServletRequest request = ServletActionContext.getRequest();
//			HttpServletResponse response = ServletActionContext.getResponse();
//		
//			
//			OutputStream outstream = null;
//			PrintWriter out = null;
//			response.setContentType("application/vnd.ms-excel");
//			response.addHeader("content-disposition",
//					"attachment; filename=Interest_SUbvention_Report.xlsx");
//			outstream = response.getOutputStream();
//			IOUtils.copy(fis, outstream);
//			
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

}
