UPDATE kafalah.dbo.D930025 SET url=replace(url,'5017','9080') where url LIKE '%5017%';
UPDATE kafalah.dbo.D930025 SET url=replace(url,'7070','9080') where url LIKE '%7070%';
UPDATE D001008 SET isSSO='0' WHERE tenantId=139 AND isActive=1;
SELECT ssoAutoLoginAllow, ssoLoginAllow, ssoLoginId, * FROM D002001 WHERE ssoLoginId='gms1'

-----Update syntax=-------------

UPDATE
    Table_A
SET
    Table_A.oldFundingNatureID=CASE  when Table_B.FundingNatureID IS NOT NULL THEN Table_B.FundingNatureID ELSE 2 END 
FROM
    renewGuaranteeMap AS Table_A
    INNER JOIN  Guarantee AS Table_B
        ON Table_A.oldGA=Table_B.GuaranteeNo;



-----TDS utilization query----------------------------
SELECT memberCode,entryDate,mainAcctId,cashFlowType,fcyTrnAmt,currentApplicableRate,limitBeforeTxn,
limitPostTxn FROM D009040 --WHERE splitTds=1
WHERE cashFlowType IN ('TDINTPBL','TDTDSDR','TDINTCR')
AND entryDate='31-Mar-2024'
AND memberCode=275

-------------------------------------------------------------
---------------------------------------------------------------------------------------
SELECT srNo,loanLimitdate,openingBalance,principal,interest,tds,balance,* FROM D030242 WHERE prdAcctId='3021020000008'
--D009040-------------


SELECT entrydate,valueDate,vcrModule,  cashflowtype,activitycode,setNo, scrollNo, drCr
, vcrAcctId,  mainAcctId, 
 -- CASE WHEN drcr='D' THEN fcyTrnAmt*-1 ELSE fcyTrnAmt END AS Amount,
  CASE WHEN drcr='D' THEN fcyTrnAmt*-1 ELSE 0 END AS debit,
  CASE WHEN drcr='C' THEN fcyTrnAmt ELSE 0 END AS Credit
  ,vcrAcctName, remarks,narration
FROM D009040 --WHERE entryDate='10-Nov-2021' AND setNo=20
where valuedate=(select max(valuedate) from d009040)


---Check accounts exists from d009022-------------
SELECT t.parameter_accounts, d.prdacctid
FROM d009022 d
right JOIN (
    SELECT DISTINCT v.value AS parameter_accounts
FROM D930065
CROSS APPLY (VALUES
    (interestAccrualAccount),
    (interestPayableAccount),
    (interestIncomeAccount),
    (tdsReceivableAccount),
    (maturityReversalAccount),
    (tdsPayableAccount),
    (amortizationReceivedType),
    (interestExpenseAccount),
    (dailyInterestAccrualAccount),
    (dailyInterestPayableAccount)
) AS v(value)
) t ON d.prdAcctId = t.parameter_accounts;



SELECT valueDate, setNo, scrollNo, drCr
, vcrAcctId,  mainAcctId, 
  fcyTrnAmt,CASE WHEN drcr='D' THEN fcyTrnAmt*-1 ELSE fcyTrnAmt END AS amt,payeeName,vcrAcctName, narration
FROM D009040 --WHERE entryDate='10-Nov-2021' AND setNo=20



SELECT batchCode, branchCode, entryDate, setNo, scrollNo, drCr
, vcrAcctId, vcrModule, mainAcctId, mainModule, activityCode, cashFlowType , fcyTrnAmt, narration,remarks, authFlag, authStatus
FROM D009040 --WHERE entryDate='10-Nov-2021' AND setNo=20

--D030003 && D009030-----------Loan Account Balance

SELECT  
prdAcctId,disbursedAmtFcy, disbursedAmtLcy,mainBalFcy, mainBalLcy,
othChgPaidFcy, othChgPrvdFcy,fiOs, vatPaid, vatPrvd,lastChargeFromDate, lastChargeToDate
FROM D030003 WHERE prdAcctId IN ('018000100000193','018000100000194')


SELECT
prdAcctId,actClrBalFcy, actTotBalFcy,chargesPaid, chargesPrvd,
fiOs, vatPrvd,  vatPaid,lastChargeFromDate, lastChargeToDate
FROM D009030 WHERE prdAcctId IN ('018000200000036')


--D009022------------------Account Master



SELECT 
institutecode,prdAcctId,acctStat,dateClosed,dateOpen,fundingMode, fundingType, instituteCode, facilityName, instituteBranch, applicationNo, facilityId, liquidationRemarks, reasonForClosure
FROM D009022 WHERE prdAcctId IN ('018000100000193','018000100000194')


--------------------reset password---------------------------
UPDATE dbo.D002001
SET pwd = '925f43c3cfb956bbe3c6aa8023ba7ad5cfa21d104186fffc69e768e55940d9653b1cd36fba614fba2e1844f4436da20f83750c6ec1db356da154691bdd71a9b1'
                , pwdSalt = 'c471f806c84e642f91f033209ce649d0b10509647e4fff7d03253a3744fdf73e1e1733bbdfc0b4cb7ff6b3c6caf31e1e309e7d5daab15b98113a35e0330b6e12'
                ,forcePwdChgYN=0,
                isUserLocked=0,
                noOfBadLogin=0,
                nextPwdChgOn='01-Jan-2099'
WHERE loginId = 'shruti' AND tenantId = 139
GO


-------

--D030042--------------------------------------

SELECT 
prdAcctId,effectiveDate,applicableRate,
loanAmount,compoundingFrequency,guarCoverPerc, guarCoverValue, guarFeePercent, guarFeeValue, guarTenor,
installmentStartDate,endDate
FROM D030042


SELECT 
prdAcctId,effFromDate,expDate,guarCoverPerc, guarCoverValue, guarFeePercent, guarFeeValue, guarTenor, loanTerm, instituteCode,
kafalahCashCoverAmount,liquidatedAccountno, liquidatedDate
FROM d013042


--------------------------------Cibil Check query-------------------------
[4:25 PM] Prashant Vishwakarma
SELECT c.branchCode,
c.PRDACCTID,
c.CBLDATE PREV_CBLDATE,
COALESCE(c.BALANCE4, 0) PREV_BALANCE4,
c.TOT_TXN_AMT,
COALESCE(c.CALC_GLBAL, 0) CALC_BALANCE4,
d.CBLDATE NEW_CBLDATE,
COALESCE(d.BALANCE4, 0) NEW_BALANCE4,
(c.CALC_GLBAL - COALESCE(d.BALANCE4, 0)) DIFF
FROM
(
SELECT b.branchCode,
b.VCRACCTID PRDACCTID,
a.CBLDATE,
(COALESCE(a.BALANCE4, 0)) BALANCE4,
b.DRLCYTRNAMT,
b.CRLCYTRNAMT,
(b.DRLCYTRNAMT + b.CRLCYTRNAMT) TOT_TXN_AMT,
(COALESCE(a.BALANCE4, 0) + b.CRLCYTRNAMT + b.DRLCYTRNAMT) CALC_GLBAL
FROM D010014 a
RIGHT JOIN
(
SELECT aa.branchCode,
aa.POSTDATE,
aa.VCRACCTID,
sum(COALESCE(aa.DRLCYTRNAMT, 0)) DRLCYTRNAMT,
sum(COALESCE(aa.CRLCYTRNAMT, 0)) CRLCYTRNAMT
FROM
(
SELECT branchCode,
POSTDATE,
VCRACCTID,
DRCR,
CASE
WHEN DRCR = 'D' THEN
sum(LCYTRNAMT) * -1
END AS DRLCYTRNAMT,
CASE
WHEN DRCR = 'C' THEN
sum(LCYTRNAMT)
END AS CRLCYTRNAMT
FROM D009040
WHERE branchCode = 1
AND ENTRYDATE = '10-Nov-2021'
AND vcrModule = 99
AND authFlag = 'A'
AND CANCELEDFLAG <> 'C'
GROUP BY branchCode,
POSTDATE,
VCRACCTID,
DRCR
) aa
GROUP BY aa.branchCode,
aa.VCRACCTID,
aa.POSTDATE
) b
ON a.branchCode = b.branchCode
AND a.prdacctid = b.VCRACCTID
AND a.CBLDATE =
(
SELECT max(CBLDATE)
FROM D010014
WHERE CBLDATE < '10-Nov-2021'
AND PrdAcctId = a.PRDACCTID
AND branchCode = a.branchCode
)
) c
LEFT JOIN D010014 d
ON d.branchCode = c.branchCode
AND d.prdacctid = c.prdacctid
AND d.CBLDATE =
(
SELECT max(CBLDATE)
FROM D010014
WHERE CBLDATE <= '10-Nov-2021'
AND PrdAcctId = d.PRDACCTID
AND branchCode = d.branchCode
)
ORDER BY c.PRDACCTID

-----------------------------------------------------------------

Repayment schedule query

SELECT a.prdAcctId,b.memberCode,b.accountName,c.loanApplNo,a.srNo,format(a.loanLimitdate,'dd-MM-yyyy') AS dueDate,a.openingBalance,
a.principal,a.interest,a.emi,a.balance FROM D030242 a
INNER JOIN D009022 b ON a.prdAcctId=b.prdAcctId
INNER JOIN D030042 c ON a.prdAcctId=c.prdAcctId
INNER JOIN D030003 d ON a.prdAcctId=d.prdAcctId AND d.mainBalFcy<>0
WHERE b.moduleCode='30' --AND b.memberCode=282


------------- Query to get dues on date-----------------
SELECT 'Loan',(SELECT bod FROM D001003) AS operationDate,b.sanctionId,b.prdacctId AS account,c.memberCode AS partyId,
c.accountName,a.srNo,format(a.loanLimitdate,'dd-MMM-yyyy') AS dueDates,a.principal,
a.principalPaid,a.intAmountPaid, a.interest,a.tds FROM d030242 a
INNER JOIN  D930038 b ON a.prdAcctId=b.prdacctId

INNER JOIN d009022 c ON c.prdacctId=a.prdAcctId AND c.acctStat=13 AND c.moduleCode=30
WHERE a.loanLimitdate<(SELECT bod+1 FROM d001003) --AND D030042.authStatus='A'
UNION all
SELECT 'LOC',(SELECT bod FROM D001003) AS operationDate,b.sanctionId,b.prdacctId AS account,c.memberCode AS partyId,
c.accountName,1,format((SELECT bod FROM D001003),'dd-MMM-yyyy') AS dueDates,a.actClrBalFcy*-1 AS principal,
0 AS principalPaid,0 AS intAmountPaid,0 AS interest,0 AS tds FROM d009030 a
INNER JOIN  D930038 b ON a.prdAcctId=b.prdacctId
INNER JOIN d009022 c ON c.prdacctId=a.prdAcctId AND c.acctStat=13 AND c.moduleCode=14
